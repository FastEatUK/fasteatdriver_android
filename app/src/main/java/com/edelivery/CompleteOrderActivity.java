package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;

import com.edelivery.fragments.FeedbackFragment;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.StoreDetail;
import com.edelivery.models.datamodels.UserDetail;
import com.edelivery.utils.Const;

import static com.edelivery.utils.Const.Params.REQUEST_ID;

public class CompleteOrderActivity extends BaseAppCompatActivity {

    public OrderPayment orderPayment;
    public String payment;
    public String requestId;
    public UserDetail userDetail;
    public StoreDetail storeDetail;
    public Order availableOrderList;
    public boolean backToActiveDelivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_complete);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_invoice));
        findViewById();
        setViewListener();
        getExtraData();

    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings

    }

    @Override
    protected void setViewListener() {
        // do somethings

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings


    }


    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras().getBundle(Const.BUNDLE);
            backToActiveDelivery = getIntent().getExtras().getBoolean(Const
                    .BACK_TO_ACTIVE_DELIVERY);
            if (bundle.getBoolean(Const.GO_TO_INVOICE)) {
                if (bundle.getParcelable(Const.ORDER_PAYMENT) == null) {

                    //// AvailableOrderList Model Receive from ActiveFragmentDelivery
                    requestId = bundle.getString(REQUEST_ID);
                    availableOrderList = bundle.getParcelable(Const
                            .USER_DETAIL);
                } else {

                    //// UserDetail and OrderPayment Model Receive from ActiveDeliveryActivity And
                    /// HistoryDetailActivity

                    orderPayment = bundle.getParcelable(Const.ORDER_PAYMENT);
                    payment = bundle.getString(Const.PAYMENT);
                    requestId = bundle.getString(REQUEST_ID);
                    userDetail = bundle.getParcelable(Const.USER_DETAIL);
                }
                goToHistoryActivity();
            } else {

                /// UserDetail and StoreDetail Model Receive from HistoryDetailActivity

                requestId = bundle.getString(REQUEST_ID);
                userDetail = bundle.getParcelable(Const.USER_DETAIL);
                storeDetail = bundle.getParcelable(Const.STORE_DETAIL);
              //  onBackPressed();
               // goToFeedbackFragment();
            }


        }

    }

    @Override
    public void onBackPressed() {
        if (backToActiveDelivery) {
            goToAvailableDelivery();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    public void goToAvailableDelivery() {
        Intent intent = new Intent(this, AvailableDeliveryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void addFragment(Fragment fragment, boolean addToBackStack,
                             boolean isAnimate, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (isAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left, R.anim.slide_in_left,
                    R.anim.slide_out_right);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.order_contain_frame, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    /*private void goToInvoiceFragment() {
        if (getSupportFragmentManager().findFragmentByTag(Const.Tag.INVOICE_FRAGMENT) == null) {
            InvoiceFragment invoiceFragment = new InvoiceFragment();
            addFragment(invoiceFragment, false, false, Const.Tag.INVOICE_FRAGMENT);
        }
    }*/

    private void goToHistoryActivity(){
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToFeedbackFragment() {
        if (getSupportFragmentManager().findFragmentByTag(Const.Tag.FEEDBACK_FRAGMENT) == null) {
            FeedbackFragment feedbackFragment = new FeedbackFragment();
            addFragment(feedbackFragment, false, false, Const.Tag.FEEDBACK_FRAGMENT);
        }
    }


}

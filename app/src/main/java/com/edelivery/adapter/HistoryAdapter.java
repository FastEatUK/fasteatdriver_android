package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.OrderHistory;
import com.edelivery.models.datamodels.StoreDetail;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeSet;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 02-May-17.
 */

public abstract class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        PinnedHeaderItemDecoration.PinnedHeaderAdapter {
    private final int HEADER = 1;
    private final int ITEM = 2;
    private TreeSet<Integer> separatorSet;
    private ArrayList<OrderHistory> orderHistoryArrayList;
    private ParseContent parseContent;
    private Context context;

    public HistoryAdapter(TreeSet<Integer> separatorSet, ArrayList<OrderHistory>
            orderHistoryArrayList) {
        parseContent = ParseContent.getInstance();
        this.orderHistoryArrayList = orderHistoryArrayList;
        this.separatorSet = separatorSet;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (viewType == HEADER) {
            return new OrderHistoryDateHolder(LayoutInflater.from(parent.getContext()).inflate(R
                    .layout
                    .item_store_product_header, parent, false));
        } else {
            return new OrderHistoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .item_order_hsitory, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        OrderHistory orderHistory = orderHistoryArrayList.get(position);
        try {

            String currentDate = parseContent.dateFormat.format(new Date());
            String dateCompleted = orderHistory.getCompletedAt();

            if (holder instanceof OrderHistoryDateHolder) {
                OrderHistoryDateHolder orderHistoryDateHolder = (OrderHistoryDateHolder) holder;
                Utils.setTagBackgroundRtlView(context, orderHistoryDateHolder.tvHistoryDate);
                if (dateCompleted.equals(currentDate)) {
                    orderHistoryDateHolder.tvHistoryDate.setText(context
                            .getString(R.string.text_today));
                } else if (dateCompleted
                        .equals(getYesterdayDateString())) {
                    orderHistoryDateHolder.tvHistoryDate.setText(context
                            .getString(R.string.text_yesterday));
                } else {
                    Date date = parseContent.dateFormat.parse(orderHistory
                            .getCompletedAt());
                    String dateString = Utils.getDayOfMonthSuffix(Integer.valueOf
                            (parseContent.day
                                    .format(date))) + " " + parseContent.dateFormatMonth.format
                            (date);
                    orderHistoryDateHolder.tvHistoryDate.setText(dateString);
                }

            } else {
                OrderHistoryHolder orderHistoryHolder = (OrderHistoryHolder) holder;
                StoreDetail storesItem = orderHistory.getStoreDetail();
                Glide.with(context).load(BASE_URL + storesItem
                        .getImageUrl())
                        .dontAnimate()
                        .placeholder(ResourcesCompat.getDrawable(context
                                        .getResources(), R.drawable.placeholder, null)).fallback
                        (ResourcesCompat
                                .getDrawable
                                        (context
                                                .getResources(), R.drawable.placeholder, null)).into
                        (orderHistoryHolder.ivHistoryStoreImage);
                if (orderHistory.getDeliveryStatus() != Const.ProviderStatus
                        .FINAL_ORDER_COMPLETED) {
                    orderHistoryHolder.ivCanceled.setVisibility(View.VISIBLE);
                    orderHistoryHolder.ivHistoryStoreImage.setColorFilter(ResourcesCompat
                            .getColor(context
                                    .getResources(), R.color.color_app_transparent_white, null));
                } else {
                    orderHistoryHolder.ivCanceled.setVisibility(View.GONE);
                    orderHistoryHolder.ivHistoryStoreImage.setColorFilter(ResourcesCompat
                            .getColor(context
                                    .getResources(), android.R.color.transparent, null));
                }
                orderHistoryHolder.tvHistoryOrderPrice.setText(orderHistory
                        .getCurrency() + parseContent.decimalTwoDigitFormat.format
                        (orderHistoryArrayList
                                .get(position)
                                .getTotal()));
                orderHistoryHolder.tvHistoryStoreName.setText(storesItem.getName());
                String orderNumber = context.getResources().getString(R.string.text_request_number)
                        + " " + "#" + orderHistory.getUniqueId();
                orderHistoryHolder.tvHistoryOrderNumber.setText(orderNumber);
                orderHistoryHolder.tvHistoryOrderTime.setText(parseContent.timeFormat_am.format
                        (parseContent
                                .webFormat.parse(orderHistory.getCompletedAt())).toUpperCase());

                if (orderHistoryArrayList.size() - 1 == position || separatorSet.contains
                        (position + 1)) {
                    orderHistoryHolder.viewDivProductItem.setVisibility(View.GONE);
                } else {
                    orderHistoryHolder.viewDivProductItem.setVisibility(View.VISIBLE);
                }
                orderHistoryHolder.tvProviderProfit.setText(context.getResources().getString(R
                        .string.text_profit) +
                        " " +
                        ": " + orderHistory.getCurrency() +
                        parseContent.decimalTwoDigitFormat.format(orderHistoryArrayList.get
                                (position)
                                .getProviderProfit()));
            }

        } catch (ParseException e) {
            AppLog.handleException(HistoryAdapter.class.getName(), e);
        }
    }

    @Override
    public int getItemCount() {
        return orderHistoryArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return separatorSet.contains(position) ? HEADER : ITEM;
    }

    private String getYesterdayDateString() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return parseContent.dateFormat.format(cal.getTime());
    }

    public abstract void onOrderClick(int position);

    @Override
    public boolean isPinnedViewType(int viewType) {
        return viewType==HEADER;
    }

    protected class OrderHistoryDateHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvHistoryDate;

        public OrderHistoryDateHolder(View itemView) {
            super(itemView);
            tvHistoryDate = (CustomFontTextView) itemView.findViewById(R.id.tvStoreProductName);
        }
    }

    protected class OrderHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivHistoryStoreImage, ivCanceled;
        CustomFontTextView tvHistoryOrderTime,
                tvHistoryOrderNumber, tvProviderProfit;
        CustomFontTextViewTitle tvHistoryStoreName, tvHistoryOrderPrice;
        View viewDivProductItem;
        LinearLayout llProduct;

        public OrderHistoryHolder(View itemView) {
            super(itemView);
            ivHistoryStoreImage = (ImageView) itemView.findViewById(R.id.ivHistoryStoreImage);
            tvHistoryStoreName = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvHistoryStoreName);
            tvHistoryOrderTime = (CustomFontTextView) itemView.findViewById(R.id
                    .tvHistoryOrderTime);
            tvHistoryOrderPrice = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvHistoryOrderPrice);
            tvHistoryOrderNumber = (CustomFontTextView) itemView.findViewById(R.id
                    .tvHistoryOrderNumber);
            viewDivProductItem = itemView.findViewById(R.id.viewDivProductItem);
            ivCanceled = (ImageView) itemView.findViewById(R.id.ivCanceled);
            tvProviderProfit = (CustomFontTextView) itemView.findViewById(R.id.tvProviderProfit);
            llProduct = (LinearLayout) itemView.findViewById(R.id.llProduct);
            llProduct.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.llProduct:
                    onOrderClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }
}

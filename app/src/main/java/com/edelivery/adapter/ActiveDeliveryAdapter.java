package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.AvailableDeliveryActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.AvailableOrder;
import com.edelivery.models.datamodels.Order;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 15-Mar-17.
 */

public class ActiveDeliveryAdapter extends RecyclerView.Adapter<ActiveDeliveryAdapter
        .ActiveDeliveryHolder> {

    private ArrayList<AvailableOrder> activeOrderList;
    private AvailableDeliveryActivity availableDeliveryActivity;
    private String currentDate;

    private ParseContent parseContent;
    private Context context;

    public ActiveDeliveryAdapter(AvailableDeliveryActivity availableDeliveryActivity,
                                 ArrayList<AvailableOrder>
                                         activeOrderList) {
        this.activeOrderList = activeOrderList;
        this.parseContent = ParseContent.getInstance();
        this.availableDeliveryActivity = availableDeliveryActivity;
        currentDate = parseContent.webFormat.format(new Date());
    }

    @Override
    public ActiveDeliveryHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .layout_delivery, parent, false);

        return new ActiveDeliveryHolder(view);
    }

    @Override
    public void onBindViewHolder(ActiveDeliveryHolder holder, int position) {

        Order order = activeOrderList.get(position).getOrderList()
                .get(0);

        try {

            if (!TextUtils.isEmpty(order.getEstimatedTimeForReadyOrder())
                    && (order.getDeliveryStatus() == Const.ProviderStatus
                    .DELIVERY_MAN_ACCEPTED || order.getDeliveryStatus() == Const
                    .ProviderStatus.DELIVERY_MAN_COMING || order.getDeliveryStatus()
                    == Const.ProviderStatus.DELIVERY_MAN_ARRIVED)) {
                Date date = parseContent.webFormat.parse(order
                        .getEstimatedTimeForReadyOrder
                                ());
                holder.tvDeliveryDate.setText(context.getResources().getString(R.string
                        .text_pick_up_order_after) + " " + parseContent.dateTimeFormat.format
                        (date));
            } else {
                Date date = parseContent.webFormat.parse(order.getCreatedAt());
                String stringBuilder = Utils.getDayOfMonthSuffix(Integer.valueOf(parseContent.day
                        .format(date))) +
                        " " +
                        parseContent.dateFormatMonth.format(date);
                holder.tvDeliveryDate.setText(stringBuilder);
            }

            String image, name;
            if (isOrderPickedUp(order
                    .getDeliveryStatus
                            ())) {
                name = order.getUserFirstName() + " " +
                        order
                                .getUserLastName();
                image = BASE_URL + order.getUserImage();
                holder.tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
                holder.llAddressOne.setVisibility(View.GONE);
            } else {
                name = order.getStoreName();
                image = BASE_URL + order.getStoreImage();
                holder.tvAddressOne.setText(order.getPickupAddresses().get(0).getAddress());
                holder.tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
                holder.llAddressOne.setVisibility(View.VISIBLE);
            }

            holder.tvCustomerName.setText(name);
            holder.tvDeliveryStatus.setText(getStringDeliveryStatus(order
                    .getDeliveryStatus
                            ()));
            String orderNumber = context.getResources().getString(R.string.text_request_number)
                    + " " + "#" + order.getRequestUniqueId();

            holder.tvOrderNumber.setText(orderNumber);
            Glide.with(availableDeliveryActivity).load(image)
                    .dontAnimate().placeholder
                    (ResourcesCompat.getDrawable(availableDeliveryActivity
                            .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                    .getDrawable
                            (availableDeliveryActivity
                                    .getResources(), R.drawable.placeholder, null)).into
                    (holder.ivCustomerImage);


        } catch (ParseException e) {
            AppLog.handleException(ActiveDeliveryAdapter.class.getName(), e);
        }
    }

    @Override
    public int getItemCount() {
        return activeOrderList.size();
    }

    private String getStringDeliveryStatus(int status) {
        String message = "";
        switch (status) {
            case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_delivery_man_accepted);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_COMING:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_delivery_man_coming);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_delivery_man_arrived);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_delivery_man_picked_order);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_delivery_man_started_delivery);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_delivery_man_arrived_at_destination);
                break;
            default:
                // do with default
                break;
        }
        return message;
    }

    private boolean isOrderPickedUp(int orderStatus) {
        switch (orderStatus) {
            case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
            case Const.ProviderStatus.DELIVERY_MAN_COMING:
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                return false;
            case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
            case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                return true;
            default:

                // do with default
                break;

        }
        return false;
    }

    protected class ActiveDeliveryHolder extends RecyclerView.ViewHolder {
        ImageView ivCustomerImage;
        CustomFontTextViewTitle tvCustomerName;
        CustomFontTextView tvDeliveryDate, tvAddressTwo, tvAddressOne,
                tvDeliveryStatus, tvOrderNumber;
        LinearLayout llAddressOne;

        public ActiveDeliveryHolder(View itemView) {
            super(itemView);
            ivCustomerImage = (ImageView) itemView.findViewById(R.id.ivCustomerImage);
            tvCustomerName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvCustomerName);
            tvDeliveryDate = (CustomFontTextView) itemView.findViewById(R.id.tvDeliveryDate);
            tvAddressTwo = (CustomFontTextView) itemView.findViewById(R.id.tvAddressTwo);
            tvAddressOne = (CustomFontTextView) itemView.findViewById(R.id.tvAddressOne);
            tvDeliveryStatus = (CustomFontTextView) itemView.findViewById(R.id.tvDeliveryStatus);
            llAddressOne = (LinearLayout) itemView.findViewById(R.id.llAddressOne);
            tvOrderNumber = (CustomFontTextView) itemView.findViewById(R.id.tvOrderNumber);
        }
    }

}

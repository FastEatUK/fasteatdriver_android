package com.edelivery.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.SpecificationSubItem;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.SectionedRecyclerViewAdapter;

import java.util.List;


public class OrderDetailSpecificationAdapter extends SectionedRecyclerViewAdapter<RecyclerView
        .ViewHolder> {

    private List<Specifications> itemSpecificationList;
    private Context context;
    private String currency;

    OrderDetailSpecificationAdapter(Context context, List<Specifications>
            itemSpecificationList, String currency) {
        this.context = context;
        this.itemSpecificationList = itemSpecificationList;
        this.currency = currency;
    }

    @Override
    public int getSectionCount() {
        return itemSpecificationList.size();
    }

    @Override
    public int getItemCount(int section) {
        return itemSpecificationList.get(section).getList().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

        IteamSpecificationHeader iteamSpecificationHeader = (IteamSpecificationHeader) holder;

        iteamSpecificationHeader.tvSectionSpeci_root.setText(itemSpecificationList.get(section)
                .getName());


        if (itemSpecificationList.get(section).getPrice() > 0) {
            String price = currency + ParseContent.getInstance().decimalTwoDigitFormat
                    .format(itemSpecificationList.get
                            (section).getPrice());
            iteamSpecificationHeader.tvSectionSpeciPrice.setText(price);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int
            relativePosition, int absolutePosition) {
        IteamSpecificationFooter iteamSpecificationFooter = (IteamSpecificationFooter) holder;
        SpecificationSubItem productSpecification = itemSpecificationList.get(section).getList()
                .get(relativePosition);
        if (productSpecification.getPrice() > 0) {
            iteamSpecificationFooter.tvSpecPrice.setText(currency
                    + ParseContent.getInstance().decimalTwoDigitFormat.format(productSpecification
                    .getPrice()));
        }

        iteamSpecificationFooter.txSpeciName.setText(productSpecification.getName());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new IteamSpecificationHeader(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .item_order_specification_section, parent, false));
            case VIEW_TYPE_ITEM:
                return new IteamSpecificationFooter(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .item_order_specification_item, parent, false));

        }

        return null;
    }

    protected class IteamSpecificationHeader extends RecyclerView.ViewHolder {

        CustomFontTextViewTitle tvSectionSpeci_root, tvSectionSpeciPrice;

        public IteamSpecificationHeader(View itemView) {
            super(itemView);
            tvSectionSpeci_root = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvSectionSpeci);
            tvSectionSpeciPrice = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvSectionSpeciPrice);
        }
    }

    private class IteamSpecificationFooter extends RecyclerView.ViewHolder {


        CustomFontTextView txSpeciName, tvSpecPrice;

        public IteamSpecificationFooter(View itemView) {
            super(itemView);

            txSpeciName = (CustomFontTextView) itemView.findViewById(R.id.tvSpecification);
            tvSpecPrice = (CustomFontTextView) itemView.findViewById(R.id.tvSubSpeciPrice);


        }
    }
}

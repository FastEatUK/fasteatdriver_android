package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.edelivery.AvailableDeliveryActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.AvailableOrder;
import com.edelivery.models.datamodels.Order;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 15-Mar-17.
 */

public class PendingDeliveryAdapter extends RecyclerView.Adapter<PendingDeliveryAdapter
        .PendingDeliveryHolder> {

    private ArrayList<AvailableOrder> pendingOrderList;
    private AvailableDeliveryActivity availableDeliveryActivity;

    private ParseContent parseContent;
    private Context context;

    public PendingDeliveryAdapter(AvailableDeliveryActivity availableDeliveryActivity,
                                  ArrayList<AvailableOrder>
                                          pendingOrderList) {
        this.pendingOrderList = pendingOrderList;
        this.parseContent = ParseContent.getInstance();
        this.availableDeliveryActivity = availableDeliveryActivity;
    }

    @Override
    public PendingDeliveryHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .layout_delivery, parent, false);

        return new PendingDeliveryHolder(view);
    }

    @Override
    public void onBindViewHolder(PendingDeliveryHolder holder, int position) {



        Order order = pendingOrderList.get(position).getOrderList()
                .get(0);

        try {
            if (TextUtils.isEmpty(order.getEstimatedTimeForReadyOrder())) {
                Date date = parseContent.webFormat.parse(order.getCreatedAt());
                String stringBuilder = Utils.getDayOfMonthSuffix(Integer.valueOf(parseContent.day
                        .format(date))) +
                        " " +
                        parseContent.dateFormatMonth.format(date);
                holder.tvDeliveryDate.setText(stringBuilder);
            } else {
                Date date = parseContent.webFormat.parse(order
                        .getEstimatedTimeForReadyOrder
                                ());
                holder.tvDeliveryDate.setText(context.getResources().getString(R.string
                        .text_pick_up_order_after) + " " + parseContent.dateTimeFormat.format
                        (date));
            }


            holder.tvCustomerName.setText(order.getStoreName());
            holder.tvAddressOne.setText(order.getPickupAddresses().get(0).getAddress
                    ());
            holder.tvAddressTwo.setText(order.getDestinationAddresses().get(0)
                    .getAddress());
            holder.tvDeliveryStatus.setText(getStringDeliveryStatus(order
                    .getDeliveryStatus
                            ()));
            String orderNumber = context.getResources().getString(R.string.text_request_number)
                    + " " + "#" + order.getRequestUniqueId();

            holder.tvOrderNumber.setText(orderNumber);
            Glide.with(availableDeliveryActivity).load(BASE_URL + order
                    .getStoreImage())
                    .dontAnimate().placeholder
                    (ResourcesCompat.getDrawable(availableDeliveryActivity
                            .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                    .getDrawable
                            (availableDeliveryActivity
                                    .getResources(), R.drawable.placeholder, null)).into
                    (holder.ivCustomerImage);


        } catch (ParseException e) {
            AppLog.handleException(PendingDeliveryAdapter.class.getName(), e);
        }
    }

    @Override
    public int getItemCount() {
        return pendingOrderList.size();
    }

    private String getStringDeliveryStatus(int status) {
        String message = "";
        switch (status) {
            case Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY:
                message = availableDeliveryActivity.getResources().getString(R.string
                        .msg_new_delivery);
                break;

            default:
                // do with default
                break;
        }
        return message;
    }

    protected class PendingDeliveryHolder extends RecyclerView.ViewHolder {
        ImageView ivCustomerImage;
        CustomFontTextView tvDeliveryDate, tvAddressTwo, tvAddressOne,
                tvDeliveryStatus, tvOrderNumber;
        CustomFontTextViewTitle tvCustomerName;

        public PendingDeliveryHolder(View itemView) {
            super(itemView);
            ivCustomerImage = (ImageView) itemView.findViewById(R.id.ivCustomerImage);
            tvCustomerName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvCustomerName);
            tvDeliveryDate = (CustomFontTextView) itemView.findViewById(R.id.tvDeliveryDate);
            tvAddressTwo = (CustomFontTextView) itemView.findViewById(R.id.tvAddressTwo);
            tvAddressOne = (CustomFontTextView) itemView.findViewById(R.id.tvAddressOne);
            tvDeliveryStatus = (CustomFontTextView) itemView.findViewById(R.id.tvDeliveryStatus);
            tvOrderNumber = (CustomFontTextView) itemView.findViewById(R.id.tvOrderNumber);
        }
    }
}

package com.edelivery.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.parser.ParseContent;

import java.util.List;

/**
 * Created by elluminati on 28-Jun-17.
 */

public class OrderDayEarningAdaptor extends RecyclerView.Adapter<OrderDayEarningAdaptor
        .OrderDayView> {

    private List<Object> orderPaymentsItemList;
    private Context context;
    private ParseContent parseContent;

    public OrderDayEarningAdaptor(Context context, List<Object> orderPaymentsItemList) {
        this.orderPaymentsItemList = orderPaymentsItemList;
        this.context = context;
        parseContent = ParseContent.getInstance();
    }

    @Override
    public OrderDayView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_payment_and_earning,
                parent, false);
        return new OrderDayView(view);
    }

    @Override
    public void onBindViewHolder(OrderDayView holder, int position) {
        OrderPayment orderPayment = (OrderPayment) orderPaymentsItemList.get(position);
        holder.tvOderNumber.setText(String.valueOf(orderPayment.getOrderUniqueId()));
        holder.tvTotal.setText(parseContent.decimalTwoDigitFormat.format(orderPayment.getTotal()));
        holder.tvServiceFees.setText(parseContent.decimalTwoDigitFormat.format(orderPayment
                .getTotalServicePrice()));
        holder.tvProfit.setText(parseContent.decimalTwoDigitFormat.format(orderPayment
                .getTotalProviderIncome()));
        holder.tvPaid.setText(parseContent.decimalTwoDigitFormat.format(orderPayment
                .getProviderPaidOrderPayment()));
        holder.tvCash.setText(parseContent.decimalTwoDigitFormat.format(orderPayment
                .getProviderHaveCashPayment()));
        holder.tvEarn.setText(parseContent.decimalTwoDigitFormat.format(orderPayment
                .getPayToProvider()));
        if (orderPayment.isPaymentModeCash()) {
            holder.tvPayBy.setText(context.getResources().getString(R.string.text_cash));
        } else {
            holder.tvPayBy.setText(context.getResources().getString(R.string.text_card));
        }


    }

    @Override
    public int getItemCount() {
        return orderPaymentsItemList.size();
    }


    protected class OrderDayView extends RecyclerView.ViewHolder {

        CustomFontTextView tvOderNumber, tvPayBy, tvTotal, tvServiceFees, tvProfit, tvPaid,
                tvCash, tvEarn;

        public OrderDayView(View itemView) {
            super(itemView);
            tvOderNumber = (CustomFontTextView) itemView.findViewById(R.id.tvOderNumber);
            tvPayBy = (CustomFontTextView) itemView.findViewById(R.id.tvPayBy);
            tvTotal = (CustomFontTextView) itemView.findViewById(R.id.tvTotal);
            tvServiceFees = (CustomFontTextView) itemView.findViewById(R.id.tvServiceFees);
            tvProfit = (CustomFontTextView) itemView.findViewById(R.id.tvProfit);
            tvPaid = (CustomFontTextView) itemView.findViewById(R.id.tvPaid);
            tvCash = (CustomFontTextView) itemView.findViewById(R.id.tvCash);
            tvEarn = (CustomFontTextView) itemView.findViewById(R.id.tvEarn);

        }
    }
}

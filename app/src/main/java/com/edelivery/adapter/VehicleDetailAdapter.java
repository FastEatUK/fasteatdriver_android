package com.edelivery.adapter;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.VehicleDetailActivity;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Vehicle;
import com.edelivery.utils.PreferenceHelper;

import java.util.List;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 27-Oct-17.
 */

public class VehicleDetailAdapter extends RecyclerView.Adapter<VehicleDetailAdapter
        .VehicleItemView> {
    private VehicleDetailActivity vehicleDetailActivity;
    private List<Vehicle> vehicleList;

    public VehicleDetailAdapter(VehicleDetailActivity vehicleDetailActivity, List<Vehicle>
            vehicleList) {
        this.vehicleDetailActivity = vehicleDetailActivity;
        this.vehicleList = vehicleList;
    }


    @Override
    public VehicleItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vehicle,
                parent, false);
        return new VehicleItemView(view);
    }

    @Override
    public void onBindViewHolder(final VehicleItemView holder, final int position) {
        holder.tvVehicleName.setText(vehicleList.get(position).getVehicleName());
        holder.tvVehicleModel.setText(vehicleList.get(position).getVehicleModel());
        holder.rbSelectVehicle.setChecked(TextUtils.equals(vehicleList.get(position).getId(),
                PreferenceHelper.getInstance(vehicleDetailActivity).getSelectedVehicleId()));
        holder.rbSelectVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vehicleList.get(position).getApproved() && !TextUtils.isEmpty(vehicleList.get
                        (position).getAdminVehicleId()
                )) {
                    vehicleDetailActivity.selectVehicle(position);
                } else {
                    holder.rbSelectVehicle.setChecked(false);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vehicleDetailActivity.goToAddVehicleDetailActivity(vehicleList.get(position));
            }
        });
        if (!vehicleList.get(position).getVehicleDetail().isEmpty()) {
            Glide.with(vehicleDetailActivity).load(BASE_URL + vehicleList.get(position)
                    .getVehicleDetail().get(0)
                    .getImageUrl())
                    .dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(vehicleDetailActivity
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (vehicleDetailActivity
                                            .getResources(), R.drawable.placeholder, null)).into
                    (holder.ivVehicle);
        }

    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }


    protected class VehicleItemView extends RecyclerView.ViewHolder {
        ImageView ivVehicle;
        RadioButton rbSelectVehicle;
        CustomFontTextView tvVehicleName, tvVehicleModel;

        public VehicleItemView(View itemView) {
            super(itemView);
            ivVehicle = (ImageView) itemView.findViewById(R.id.ivVehicle);
            rbSelectVehicle = (RadioButton) itemView.findViewById(R.id.rbSelectVehicle);
            tvVehicleModel = (CustomFontTextView) itemView.findViewById(R.id.tvVehicleModel);
            tvVehicleName = (CustomFontTextView) itemView.findViewById(R.id.tvVehicleName);
        }


    }
}

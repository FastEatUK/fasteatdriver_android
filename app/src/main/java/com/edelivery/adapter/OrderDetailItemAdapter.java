package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.OrderProductItem;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.SectionedRecyclerViewAdapter;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import static com.edelivery.BuildConfig.BASE_URL;


public class OrderDetailItemAdapter extends SectionedRecyclerViewAdapter<RecyclerView
        .ViewHolder> {

    List<OrderProductItem> itemList;
    private Context context;
    private String currency;
    private boolean isImageVisible;

    public OrderDetailItemAdapter(Context context, List<OrderProductItem> itemList, String
            currency, boolean isImageVisible) {
        this.context = context;
        this.itemList = itemList;
        this.currency = currency;
        this.isImageVisible = isImageVisible;
    }

    @Override
    public int getSectionCount() {
        return itemList.size();
    }

    @Override
    public int getItemCount(int section) {

        return 1;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

        IteamChildHeader iteamChildHeader = (IteamChildHeader) holder;
        OrderProductItem individualIteam = itemList.get(section);
        iteamChildHeader.tvItemName.setText(individualIteam.getItemName());
        if (individualIteam.getTotalItemAndSpecificationPrice() > 0) {
            iteamChildHeader.tvItemsPrice.setText(currency + ParseContent.getInstance()
                    .decimalTwoDigitFormat.format(individualIteam
                            .getTotalItemAndSpecificationPrice()));
        }
        if (!individualIteam.getImageUrl().isEmpty() && isImageVisible) {
            Glide.with(context).load(BASE_URL + individualIteam.getImageUrl().get(0))
                    .dontAnimate()
                    .placeholder(ResourcesCompat.getDrawable
                            (context
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (context.getResources(), R.drawable.placeholder,
                                            null)).into
                    (iteamChildHeader.ivItems);
            iteamChildHeader.ivItems.setVisibility(View.VISIBLE);
        } else {
            iteamChildHeader.ivItems.setVisibility(View.GONE);
        }
        if (individualIteam.getItemPrice() + individualIteam.getTotalSpecificationPrice() > 0) {
            iteamChildHeader.tvItemCounts.setText(context.getResources().getString(R.string
                    .text_qty)
                    + " " +
                    individualIteam.getQuantity() + " X " +
                    currency + ParseContent.getInstance()
                    .decimalTwoDigitFormat.format(individualIteam.getItemPrice() + individualIteam
                            .getTotalSpecificationPrice()));
        } else {
            iteamChildHeader.tvItemCounts.setText(context.getResources().getString(R.string
                    .text_qty)
                    + individualIteam.getQuantity());
        }

        if (0 == section) {
            iteamChildHeader.ivListDivider.setVisibility(View.GONE);
        } else {
            iteamChildHeader.ivListDivider.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int
            relativePosition, int absolutePosition) {

        IteamFooter iteamFooter = (IteamFooter) holder;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);

        OrderDetailSpecificationAdapter adapter = new OrderDetailSpecificationAdapter(context,
                itemList.get(section).getSpecifications(), currency);
        iteamFooter.root_recycler.setLayoutManager(layoutManager);
        iteamFooter.root_recycler.setAdapter(adapter);

        String itemNote = itemList.get(section).getItemNote();
        if (TextUtils.isEmpty(itemNote)) {
            iteamFooter.tvItemNote.setVisibility(View.GONE);
            iteamFooter.tvItemNoteHeading.setVisibility(View.GONE);
        } else {
            iteamFooter.tvItemNote.setVisibility(View.VISIBLE);
            iteamFooter.tvItemNoteHeading.setVisibility(View.VISIBLE);
            iteamFooter.tvItemNote.setText(itemNote);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new IteamChildHeader(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .item_oreder_detail_section, parent, false));
            case VIEW_TYPE_ITEM:
                return new IteamFooter(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .item_order_detail_items, parent, false));

        }
        return null;
    }

    protected class IteamChildHeader extends RecyclerView.ViewHolder {

        RoundedImageView ivItems;
        View ivListDivider;
        CustomFontTextView tvItemCounts;
        CustomFontTextViewTitle tvItemName, tvItemsPrice;

        public IteamChildHeader(View itemView) {
            super(itemView);
            ivItems = (RoundedImageView) itemView.findViewById(R.id.ivItems);
            tvItemName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvItemName);
            tvItemCounts = (CustomFontTextView) itemView.findViewById(R.id.tvItemCounts);
            tvItemsPrice = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvItemsPrice);
            ivListDivider = itemView.findViewById(R.id.ivListDivider);
        }
    }

    protected class IteamFooter extends RecyclerView.ViewHolder {


        RecyclerView root_recycler;
        CustomFontTextViewTitle tvItemNoteHeading;
        CustomFontTextView tvItemNote;

        public IteamFooter(View itemView) {
            super(itemView);
            root_recycler = (RecyclerView) itemView.findViewById(R.id.root_recycler);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            root_recycler.setLayoutManager(layoutManager);
            tvItemNote = (CustomFontTextView) itemView.findViewById(R.id.tvItemNote);
            tvItemNoteHeading = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvItemNoteHeading);
        }
    }
}

package com.edelivery.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.OrderProducts;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.SectionedRecyclerViewAdapter;
import com.edelivery.utils.Utils;

import java.util.List;


public class OrderDetailsAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder>
        implements PinnedHeaderItemDecoration.PinnedHeaderAdapter {

    private List<OrderProducts> orderDetailsItemList;
    private Context context;
    private String currency;
    private boolean isImageVisible;

    public OrderDetailsAdapter(Context context, List<OrderProducts> orderDetailsItemList, String
            currency, boolean isImageVisible) {
        this.orderDetailsItemList = orderDetailsItemList;
        this.context = context;
        this.currency = currency;
        this.isImageVisible = isImageVisible;
    }


    @Override
    public int getSectionCount() {
        return orderDetailsItemList.size();
    }

    @Override
    public int getItemCount(int section) {

        return 1;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

        IteamHeader iteamHeaderHolder = (IteamHeader) holder;
        iteamHeaderHolder.tvSection.setText(orderDetailsItemList.get(section).getProductName());
        iteamHeaderHolder.tvSection.setFocusable(true);
        Utils.setTagBackgroundRtlView(context, iteamHeaderHolder.tvSection);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int
            relativePosition, int absolutePosition) {
        IteamFooterSpecification iteamFooterSpecification = (IteamFooterSpecification) holder;

        OrderDetailItemAdapter orderDetailItemAdapter = new OrderDetailItemAdapter(context,
                orderDetailsItemList.get(section).getItems(), currency, isImageVisible);
        iteamFooterSpecification.recyclerView.setAdapter(orderDetailItemAdapter);


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {

            case VIEW_TYPE_HEADER:
                return new IteamHeader(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .item_store_product_header, parent, false));
            case VIEW_TYPE_ITEM:
                return new IteamFooterSpecification(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .layout_recyclerview, parent, false));
        }

        return null;
    }

    @Override
    public boolean isPinnedViewType(int viewType) {
        return viewType == VIEW_TYPE_HEADER;
    }

    protected class IteamHeader extends RecyclerView.ViewHolder {

        CustomFontTextView tvSection;

        public IteamHeader(View view) {
            super(view);
            tvSection = (CustomFontTextView) view.findViewById(R.id.tvStoreProductName);


        }
    }

    protected class IteamFooterSpecification extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;

        public IteamFooterSpecification(View view) {
            super(view);
            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);

            recyclerView.setLayoutManager(layoutManager);


        }
    }
}

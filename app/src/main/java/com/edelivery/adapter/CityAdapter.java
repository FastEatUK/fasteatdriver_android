package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Cities;

import java.util.ArrayList;

/**
 * Created by elluminati on 03-Feb-2017.
 */
public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CountryViewHolder> {

    private ArrayList<Cities> cityList;


    public CityAdapter(ArrayList<Cities> cityList) {
        this.cityList = cityList;


    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_name,
                parent, false);

        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {

        holder.tvCityName.setText(cityList.get(position).getCityNickName());

    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }


    protected class CountryViewHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvCityName;

        public CountryViewHolder(View itemView) {
            super(itemView);

            tvCityName = (CustomFontTextView) itemView.findViewById(R.id.tvItemCityName);

        }
    }

}

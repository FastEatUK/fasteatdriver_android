package com.edelivery.adapter;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.fragments.WalletHistoryFragment;
import com.edelivery.models.datamodels.WalletHistory;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by elluminati on 01-Nov-17.
 */

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter
        .WalletHistoryHolder> {
    private ArrayList<WalletHistory> walletHistories;
    private WalletHistoryFragment walletHistoryFragment;

    public WalletHistoryAdapter(WalletHistoryFragment walletHistoryFragment,
                                ArrayList<WalletHistory> walletHistories) {
        this.walletHistories = walletHistories;
        this.walletHistoryFragment = walletHistoryFragment;
    }

    @Override
    public WalletHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .item_wallet_history, parent, false);
        return new WalletHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(WalletHistoryHolder holder, int position) {

        WalletHistory walletHistory = walletHistories.get
                (position);
        try {
            Date date = ParseContent.getInstance().webFormat.parse(walletHistory.getCreatedAt
                    ());
            holder.tvTransactionDate.setText(ParseContent.getInstance().dateFormat3.format(date));
            holder.tvTransactionTime.setText(ParseContent.getInstance().timeFormat_am.format(date));
            holder.tvWithdrawalId.setText(walletHistoryFragment.getResources().getString(R.string
                    .text_id) + " " + walletHistory.getUniqueId());

            holder.tvTransactionState.setText(walletComment(walletHistory.getWalletCommentId
                    ()));
            switch (walletHistory.getWalletStatus()) {
                case Const.Wallet.ADD_WALLET_AMOUNT:
                    holder.ivWalletStatus.setBackgroundColor(ResourcesCompat
                            .getColor(walletHistoryFragment.getResources(), R.color
                                    .color_app_wallet_added, null));
                    holder.tvTransactionAmount.setTextColor(ResourcesCompat
                            .getColor(walletHistoryFragment.getResources(), R.color
                                    .color_app_wallet_added, null));
                    holder.tvTransactionAmount.setText("+" + ParseContent.getInstance()
                            .decimalTwoDigitFormat
                            .format(walletHistory
                                    .getAddedWallet()) + " " + walletHistory.getToCurrencyCode());
                    break;
                case Const.Wallet.REMOVE_WALLET_AMOUNT:
                    holder.ivWalletStatus.setBackgroundColor(ResourcesCompat
                            .getColor(walletHistoryFragment.getResources(), R.color
                                    .color_app_wallet_deduct, null));
                    holder.tvTransactionAmount.setTextColor(ResourcesCompat
                            .getColor(walletHistoryFragment.getResources(), R.color
                                    .color_app_wallet_deduct, null));
                    holder.tvTransactionAmount.setText("-" + ParseContent.getInstance()
                            .decimalTwoDigitFormat
                            .format(walletHistory
                                    .getAddedWallet()) + " " + walletHistory.getFromCurrencyCode());

                    break;


            }
        } catch (ParseException e) {
            AppLog.handleException(WalletHistoryAdapter.class.getName(), e);
        }

    }

    @Override
    public int getItemCount() {
        return walletHistories.size();
    }

    private String walletComment(int id) {
        String comment;
        switch (id) {
            case Const.Wallet.ADDED_BY_ADMIN:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_added_by_admin);
                break;
            case Const.Wallet.ADDED_BY_CARD:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_added_by_card);
                break;
            case Const.Wallet.ADDED_BY_REFERRAL:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_added_by_referral);
                break;
            case Const.Wallet.ORDER_REFUND:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_order_refund);
                break;
            case Const.Wallet.ORDER_PROFIT:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_order_profit);
                break;
            case Const.Wallet.ORDER_CANCELLATION_CHARGE:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_order_cancellation_charge);
                break;
            case Const.Wallet.ORDER_CHARGED:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_order_charged);
                break;
            case Const.Wallet.WALLET_REQUEST_CHARGE:
                comment = walletHistoryFragment.getResources().getString(R.string
                        .text_wallet_status_wallet_request_charge);
                break;


            default:
                // do with default
                comment = "NA";
                break;
        }
        return comment;
    }

    protected class WalletHistoryHolder extends RecyclerView.ViewHolder {

        CustomFontTextViewTitle tvTransactionState, tvTransactionAmount;
        CustomFontTextView tvTransactionDate, tvWithdrawalId,
                tvTransactionTime;
        ImageView ivTransactionType;
        View ivWalletStatus;


        public WalletHistoryHolder(View itemView) {
            super(itemView);
            tvWithdrawalId = (CustomFontTextView) itemView.findViewById(R.id.tvWithdrawalID);
            tvTransactionAmount = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvTransactionAmount);
            tvTransactionDate = (CustomFontTextView) itemView.findViewById(R.id.tvTransactionDate);
            tvTransactionState = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvTransactionState);
            ivTransactionType = (ImageView) itemView.findViewById(R.id.ivTransactionType);
            tvTransactionTime = (CustomFontTextView) itemView.findViewById(R.id.tvTransactionTime);
            ivWalletStatus = itemView.findViewById(R.id.ivWalletStatus);
        }
    }
}

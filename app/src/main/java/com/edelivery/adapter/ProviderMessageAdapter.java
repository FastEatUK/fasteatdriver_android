package com.edelivery.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.ActiveDeliveryActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;

import java.util.ArrayList;

public abstract class ProviderMessageAdapter extends RecyclerView.Adapter<ProviderMessageAdapter.ProviderMessageHolder> {

    private ArrayList<String> messageList;
    private ActiveDeliveryActivity activeDeliveryActivity;

    public ProviderMessageAdapter(ArrayList<String> messageList, ActiveDeliveryActivity activeDeliveryActivity){
        this.messageList = messageList;
        this.activeDeliveryActivity = activeDeliveryActivity;
    }
    @NonNull
    @Override
    public ProviderMessageAdapter.ProviderMessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .item_message, parent, false);

        return new ProviderMessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProviderMessageAdapter.ProviderMessageHolder holder, int position) {
            holder.tvMessage.setText(messageList.get(position));

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public abstract void onMessageClick(int position);

    public class ProviderMessageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CustomFontTextView tvMessage;
        public ProviderMessageHolder(View itemView) {
            super(itemView);
            tvMessage = (CustomFontTextView) itemView.findViewById(R.id.tvMessage);
            tvMessage.setOnClickListener(this);
        }



        @Override
        public void onClick(View v) {
            onMessageClick(getAdapterPosition());
        }
    }
}

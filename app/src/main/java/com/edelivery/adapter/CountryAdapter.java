package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Countries;

import java.util.ArrayList;

/**
 * Created by elluminati on 03-Feb-2017.
 */
public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    private ArrayList<Countries> countryList;


    public CountryAdapter(ArrayList<Countries> countryList) {
        this.countryList = countryList;


    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country_code,
                parent, false);
        CountryViewHolder countryViewHolder = new CountryViewHolder(view);

        return countryViewHolder;
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        holder.tvCountryCodeDigit.setText(countryList.get(position)
                .getCountryPhoneCode());
        holder.tvCountryName.setText(countryList.get(position).getCountryName());
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }


    protected class CountryViewHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvCountryCodeDigit, tvCountryName;

        public CountryViewHolder(View itemView) {
            super(itemView);

            tvCountryCodeDigit = (CustomFontTextView) itemView.findViewById(R.id
                    .tvCountryCodeDigit);
            tvCountryName = (CustomFontTextView) itemView.findViewById(R.id.tvItemCountryName);

        }
    }


}

package com.edelivery.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.models.datamodels.OrderProductItem;
import com.edelivery.models.datamodels.OrderProducts;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.SectionedRecyclerViewAdapter;

import java.util.List;

/**
 * Created by elluminati on 02-May-17.
 */

public class HistoryDetailAdapter extends SectionedRecyclerViewAdapter<RecyclerView
        .ViewHolder> {
    private List<OrderProducts> orderList;
    private Context context;
    private ParseContent parseContent;

    public HistoryDetailAdapter(List<OrderProducts> orderList) {
        this.orderList = orderList;
        parseContent = ParseContent.getInstance();
    }

    @Override
    public int getSectionCount() {
        return orderList.size();
    }

    @Override
    public int getItemCount(int section) {
        return orderList.get(section).getItems().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int
            relativePosition, int absolutePosition) {

        OrderHistoryDetailHolder orderHistoryDetailHolder = (OrderHistoryDetailHolder) holder;
        List<OrderProductItem> cartProductItemses = orderList.get(section).getItems();
        OrderProductItem cartProductItems = cartProductItemses.get(relativePosition);
        orderHistoryDetailHolder.tvOderItemName.setText(cartProductItems.getItemName());
        orderHistoryDetailHolder.tvOrderQuantity.setText(
                String.valueOf(cartProductItems.getQuantity()));
        orderHistoryDetailHolder.tvOrderItemPrice.setText(CurrentOrder.getInstance()
                .getCurrency() + parseContent.decimalTwoDigitFormat.format(cartProductItems
                .getTotalItemAndSpecificationPrice()));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_divider,
                        parent, false);
                return new OrderViewHolder(view);
            case VIEW_TYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                                .item_order_history_detail_item,
                        parent, false);
                return new OrderHistoryDetailHolder(view);
            default:
                // do with default
                break;
        }
        return null;
    }

    protected class OrderViewHolder extends RecyclerView.ViewHolder {


        public OrderViewHolder(View itemView) {
            super(itemView);
        }
    }

    protected class OrderHistoryDetailHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvOderItemName, tvOrderQuantity, tvOrderItemPrice;

        public OrderHistoryDetailHolder(View itemView) {
            super(itemView);
            tvOderItemName = (CustomFontTextView) itemView.findViewById(R.id.tvOderItemName);
            tvOrderQuantity = (CustomFontTextView) itemView.findViewById(R.id.tvOrderQuantity);
            tvOrderItemPrice = (CustomFontTextView) itemView.findViewById(R.id.tvOrderItemPrice);
        }
    }
}

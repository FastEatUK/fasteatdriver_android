package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Documents;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;

import java.text.ParseException;
import java.util.List;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 28-Apr-17.
 */

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.DocumentViewHolder> {

    private List<Documents> documentsList;
    private Context context;
    private ParseContent parseContent;

    public DocumentAdapter(List<Documents> documentsList) {
        this.documentsList = documentsList;
        parseContent = ParseContent.getInstance();
    }

    @Override
    public DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document,
                parent, false);

        return new DocumentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DocumentViewHolder holder, int position) {
        Documents documents = documentsList.get(position);
        if (documents.getDocumentDetails().isIsUniqueCode()) {
            holder.tvIdNumber.setVisibility(View.VISIBLE);
            String id = context.getResources().getString(R.string.text_id_number);
            if (!TextUtils.isEmpty(documents.getUniqueCode())) {
                id = id + " " + documents.getUniqueCode();
                holder.tvIdNumber.setText(id);
            } else {
                holder.tvIdNumber.setText(id);
            }

        } else {
            holder.tvIdNumber.setVisibility(View.GONE);
        }
        if (documents.getDocumentDetails().isIsExpiredDate()) {
            String date = context.getResources().getString(R.string.text_expire_date);
            holder.tvExpireDate.setVisibility(View.VISIBLE);
            try {
                if (!TextUtils.isEmpty(documents.getExpiredDate())) {
                    date = date + " " + parseContent.dateFormat.format(parseContent
                            .webFormat
                            .parse(documents.getExpiredDate()));
                    holder.tvExpireDate.setText(date);
                } else {
                    holder.tvExpireDate.setText(date);
                }

            } catch (ParseException e) {
                AppLog.handleException(DocumentAdapter.class.getName(), e);
            }

        } else {
            holder.tvExpireDate.setVisibility(View.GONE);
        }

        if (documents.getDocumentDetails().isIsMandatory()) {
            holder.tvOption.setVisibility(View.VISIBLE);
        } else {
            holder.tvOption.setVisibility(View.GONE);
        }

        holder.tvDocumentTittle.setText(documents.getDocumentDetails().getDocumentName());
        Glide.with(context).load(BASE_URL + documents.getImageUrl())
                .dontAnimate().placeholder
                (ResourcesCompat.getDrawable(context
                        .getResources(), R.drawable.uploading, null)).fallback(ResourcesCompat
                .getDrawable
                        (context
                                .getResources(), R.drawable.uploading, null)).into
                (holder.ivDocumentImage);
    }

    @Override
    public int getItemCount() {
        return documentsList.size();
    }

    protected class DocumentViewHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvDocumentTittle, tvIdNumber, tvExpireDate, tvOption;
        ImageView ivDocumentImage;

        public DocumentViewHolder(View itemView) {
            super(itemView);
            tvDocumentTittle = (CustomFontTextView) itemView.findViewById(R.id.tvDocumentTittle);
            tvIdNumber = (CustomFontTextView) itemView.findViewById(R.id.tvIdNumber);
            tvExpireDate = (CustomFontTextView) itemView.findViewById(R.id.tvExpireDate);
            ivDocumentImage = (ImageView) itemView.findViewById(R.id.ivDocumentImage);
            tvOption = (CustomFontTextView) itemView.findViewById(R.id.tvOption);
        }
    }
}

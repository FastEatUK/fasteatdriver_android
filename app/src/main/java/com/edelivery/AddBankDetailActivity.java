package com.edelivery;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomPhotoDialog;
import com.edelivery.models.datamodels.BankDetail;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.ImageHelper;
import com.edelivery.utils.Utils;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.ImageHelper.CHOOSE_PHOTO_FROM_GALLERY;
import static com.edelivery.utils.ImageHelper.TAKE_PHOTO_FROM_CAMERA;

public class AddBankDetailActivity extends BaseAppCompatActivity {


    private CustomFontEditTextView etAccountNumber, etAccountHolderName, etRoutingNumber,
            etPersonalIdNumber;
    private CustomFontTextView tvDob;
    private Uri picUri;
    private ImageHelper imageHelper;
    private CustomDialogAlert closedPermissionDialog;
    private String uploadImage;
    private ImageView ivDocumentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_bank_detail));
        setToolbarRightIcon(R.drawable.ic_check_black_24dp, this);
        findViewById();
        setViewListener();
        imageHelper = new ImageHelper(this);

    }

    @Override
    protected boolean isValidate() {
        // do something

        String msg = null;
        if (TextUtils.isEmpty(etAccountHolderName.getText().toString())) {
            msg = getString(R.string.msg_plz_account_name);
            etAccountHolderName.requestFocus();
            etAccountHolderName.setError(msg);
        } else if (TextUtils.isEmpty(etAccountNumber.getText().toString().trim())) {
            msg = getString(R.string.msg_plz_valid_account_number);
            etAccountNumber.requestFocus();
            etAccountNumber.setError(msg);
        } else if (TextUtils.isEmpty(etRoutingNumber.getText().toString().trim())) {
            msg = getString(R.string.msg_plz_valid_routing_number);
            etRoutingNumber.requestFocus();
            etRoutingNumber.setError(msg);
        } else if (etPersonalIdNumber.getText().toString().length() != 9) {
            msg = getString(R.string.msg_plz_valid_personal_id_number);
            etPersonalIdNumber.requestFocus();
            etPersonalIdNumber.setError(msg);
        } else if (TextUtils.equals(tvDob.getText().toString(), getResources().getString(R.string
                .text_dob))) {
            msg = getString(R.string.msg_add_dob);
            Utils.showToast(msg, this);
        } else if (TextUtils.isEmpty(uploadImage)) {
            msg = getString(R.string.msg_add_document_image);
            Utils.showToast(msg, this);
        }
        return TextUtils.isEmpty(msg);
    }

    @Override
    protected void findViewById() {
        // do something
        etRoutingNumber = (CustomFontEditTextView) findViewById(R.id.etRoutingNumber);
        etAccountNumber = (CustomFontEditTextView) findViewById(R.id.etBankAccountNumber);
        etPersonalIdNumber = (CustomFontEditTextView) findViewById(R.id.etPersonalIdNumber);
        etAccountHolderName = findViewById(R.id.etAccountHolderName);
        tvDob = findViewById(R.id.tvDob);
        ivDocumentImage = findViewById(R.id.ivDocumentImage);
    }

    @Override
    protected void setViewListener() {
        // do something
        tvDob.setOnClickListener(this);
        ivDocumentImage.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        // do something
        Utils.hideSoftKeyboard(this);
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do something
        switch (view.getId()) {
            case R.id.ivToolbarRightIcon:
                if (isValidate()) {
                    openVerifyAccountDialog();
                }
                break;
            case R.id.tvDob:
                openDatePickerDialog();
                break;
            case R.id.ivDocumentImage:
                checkPermission();
                break;
            default:
                // do with default
                break;
        }
    }

    private void openVerifyAccountDialog() {
        if (TextUtils.isEmpty(preferenceHelper.getSocialId())) {
            CustomDialogVerification customDialogVerification = new CustomDialogVerification(this,
                    getResources()
                            .getString(R.string
                                    .text_verify_account), getResources()
                    .getString(R.string.msg_enter_password_which_used_in_register),
                    getResources().getString(R.string.text_cancel), getResources()
                    .getString(R.string.text_ok), "", getResources()
                    .getString(R.string
                            .text_password), false, InputType.TYPE_CLASS_TEXT, InputType
                    .TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT) {
                @Override
                public void onClickLeftButton() {
                    dismiss();

                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {
                    if (!etDialogEditTextTwo.getText().toString().isEmpty()) {
                        addBankDetail(etDialogEditTextTwo.getText().toString());
                        dismiss();

                        // implementation remain
                    } else {
                        etDialogEditTextTwo.setError(getString(R.string.msg_enter_password));
                    }
                }

            };
            customDialogVerification.show();
        } else {
            addBankDetail("");
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    /**
     * this method call webservice for add or update bank detail;
     *
     * @param pass string
     */
    private void addBankDetail(String pass) {
        Utils.showCustomProgressDialog(this, false);
        BankDetail bankDetailAdd = new BankDetail();

        bankDetailAdd.setBankHolderId(preferenceHelper.getProviderId());
        bankDetailAdd.setBankHolderType(Const.TYPE_PROVIDER);
        bankDetailAdd.setBankAccountHolderName(etAccountHolderName.getText().toString().trim());
        bankDetailAdd.setPassword(pass);
        bankDetailAdd.setSocialId(preferenceHelper.getSocialId());
        bankDetailAdd.setAccountNumber(etAccountNumber.getText().toString());
        bankDetailAdd.setDob(tvDob.getText().toString());
        bankDetailAdd.setPersonalIdNumber(etPersonalIdNumber.getText().toString());
        bankDetailAdd.setRoutingNumber(etRoutingNumber.getText().toString());
        bankDetailAdd.setDocument(uploadImage);
        bankDetailAdd.setAccountHolderType(Const.Bank.BANK_ACCOUNT_HOLDER_TYPE);
        bankDetailAdd.setBusinessName(getResources().getString(R.string.app_name));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall;
        responseCall = apiInterface.getAddBankDetail(ApiClient
                .makeGSONRequestBody(bankDetailAdd));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        if (TextUtils.isEmpty(response.body().getStripeError())) {
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    AddBankDetailActivity.this);
                        } else {
                            openBankDetailErrorDialog(response.body().getStripeError());
                        }

                    }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.BANK_DETAIL_ACTIVITY, t);
            }
        });
    }


    private void openDatePickerDialog() {

        final Calendar calendar = Calendar.getInstance();
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDate = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog
                .OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                tvDob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                tvDob.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
            }
        };
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener,
                currentYear,
                currentMonth,
                currentDate);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.YEAR , maxDate.get(Calendar.YEAR) - 13);
        long now = maxDate.getTimeInMillis();
        datePickerDialog.getDatePicker().setMaxDate(now);
        datePickerDialog.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppLog.Log(Const.Tag.PROFILE_ACTIVITY, requestCode + "");
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TAKE_PHOTO_FROM_CAMERA:
                    onCaptureImageResult();
                    break;
                case CHOOSE_PHOTO_FROM_GALLERY:
                    onSelectFromGalleryResult(data);
                    break;
                case Crop.REQUEST_CROP:
                    handleCrop(resultCode, data);
                    break;
                default:
                    //Do something
                    break;

            }
        }


    }

    /**
     * This method is used for crop the placeholder which selected or captured
     */
    public void beginCrop(Uri sourceUri) {

        Uri outputUri = Uri.fromFile(imageHelper.createImageFile());
        Crop.of(sourceUri, outputUri).asSquare().start(this);
    }

    /**
     * This method is used for handel result after select placeholder from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            beginCrop(picUri);
        }
    }

    /**
     * This method is used for handel result after captured placeholder from camera .
     */
    private void onCaptureImageResult() {
        beginCrop(picUri);

    }

    /**
     * This method is used for  handel crop result after crop the placeholder.
     */
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            picUri = Crop.getOutput(result);
            Glide.with(this).load(picUri).transition(new DrawableTransitionOptions().crossFade())
                    .into(ivDocumentImage);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                        picUri);
                uploadImage = Utils.convertImageToBase64(bitmap);
            } catch (IOException e) {
                AppLog.handleException(AddBankDetailActivity.class.getSimpleName(), e);
            }
            AppLog.Log("PIC", picUri.getPath());

        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.showToast(Crop.getError(result).getMessage(), this);
        }
    }

    private void goWithCameraAndStoragePermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.CAMERA)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.READ_EXTERNAL_STORAGE)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        } else {
            //
            openPhotoSelectDialog();
        }
    }

    public void openPhotoSelectDialog() {
        //Do the stuff that requires permission...
        CustomPhotoDialog customPhotoDialog = new CustomPhotoDialog(this, getResources()
                .getString(R.string.text_set_profile_photos)) {
            @Override
            public void clickedOnCamera() {
                takePhotoFromCamera();
                dismiss();
            }

            @Override
            public void clickedOnGallery() {
                choosePhotoFromGallery();
                dismiss();
            }
        };
        customPhotoDialog.show();

    }

    private void openCameraPermissionDialog() {
        if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
            return;
        }
        closedPermissionDialog = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(AddBankDetailActivity.this, new String[]{android
                        .Manifest
                        .permission
                        .CAMERA, android.Manifest
                        .permission
                        .READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

        };
        closedPermissionDialog.show();
    }

    private void closedPermissionDialog() {
        if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
            closedPermissionDialog.dismiss();
            closedPermissionDialog = null;

        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission
                (this, android.Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        } else {
            //
            openPhotoSelectDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    //Do som thing
                    break;
            }
        }
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = imageHelper.createImageFile();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
         //   picUri = FileProvider.getUriForFile(this, getPackageName(), file);
            picUri = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), file);

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, TAKE_PHOTO_FROM_CAMERA);
    }

    private void choosePhotoFromGallery() {
       /* Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, CHOOSE_PHOTO_FROM_GALLERY);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, CHOOSE_PHOTO_FROM_GALLERY);
    }

    protected void openBankDetailErrorDialog(String message) {
        CustomDialogAlert exitDialog = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_error), message, this.getResources()
                .getString(R.string.text_cancel), this.getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        exitDialog.btnDialogEditTextLeft.setVisibility(View.GONE);
        exitDialog.show();
    }
}

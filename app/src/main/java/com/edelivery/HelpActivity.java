package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.LinearLayout;

import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

public class HelpActivity extends BaseAppCompatActivity {
    private LinearLayout llMail, llCall;
    private CustomFontTextView tvTandC, tvPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_help));
        findViewById();
        setViewListener();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        llCall = (LinearLayout) findViewById(R.id.llCall);
        llMail = (LinearLayout) findViewById(R.id.llMail);
        tvTandC = (CustomFontTextView) findViewById(R.id.tvTandC);
        tvPolicy = (CustomFontTextView) findViewById(R.id.tvPolicy);
        tvTandC.setText(Utils.fromHtml
                ("<a href=\"" + preferenceHelper.getTermsANdConditions() + "\"" + ">" +
                        getResources().getString(R.string.text_t_and_c) +
                        "</a>"));
        tvTandC.setMovementMethod(LinkMovementMethod.getInstance());
        tvPolicy.setText(Utils.fromHtml
                ("<a href=\"" + preferenceHelper.getPolicy() + "\"" + ">" +
                        getResources().getString(R.string.text_policy) +
                        "</a>"));
        tvPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void setViewListener() {
        llCall.setOnClickListener(this);
        llMail.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCall:
                makePhoneCallToAdmin();
                break;
            case R.id.llMail:
                contactUsWithAdmin();
                break;
            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void makePhoneCallToAdmin() {
       /* if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission
                    .CALL_PHONE}, Const
                    .PERMISSION_FOR_CALL);
        } else {*/
            //Do the stuff that requires permission...
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + preferenceHelper.getAdminContact()));
            startActivity(intent);
      //  }

    }

    private void openCallPermissionDialog() {

        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(HelpActivity.this, new String[]{android.Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                dismiss();
            }

        };
        customDialogAlert.show();
    }


    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            makePhoneCallToAdmin();

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (HelpActivity.this, android.Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                default:
                    //do with default
                    break;
            }
        }

    }


}

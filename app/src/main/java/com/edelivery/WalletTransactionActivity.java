package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.WalletHistoryFragment;
import com.edelivery.fragments.WalletTransactionFragment;

public class WalletTransactionActivity extends BaseAppCompatActivity {

    private TabLayout orderHistoryTabsLayout;
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transection);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_history));
        findViewById();
        setViewListener();
        initTabLayout();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(getResources().getDimensionPixelSize(R.dimen
                    .dimen_app_tab_elevation));
        }
        orderHistoryTabsLayout = (TabLayout) findViewById(R.id
                .transTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.transViewpager);
    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }


    private void initTabLayout() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new WalletHistoryFragment(), getResources()
                .getString(R.string.text_wallet_history));
        viewPagerAdapter.addFragment(new WalletTransactionFragment(), getResources()
                .getString(R.string.text_wallet_transaction));
        viewPager.setAdapter(viewPagerAdapter);
        orderHistoryTabsLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

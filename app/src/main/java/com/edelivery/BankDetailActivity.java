package com.edelivery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;

import com.edelivery.adapter.BankDetailAdapter;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.models.datamodels.BankDetail;
import com.edelivery.models.responsemodels.BankDetailResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetailActivity extends BaseAppCompatActivity {

    private RecyclerView rcvBankDetail;
    private BankDetailAdapter bankDetailAdapter;
    private ArrayList<BankDetail> bankDetails = new ArrayList<>();
    private FloatingActionButton floatingBtnAddBankDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details_view);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bankDetails = new ArrayList<>();
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_bank_detail));
        findViewById();
        setViewListener();
        initRcvBank();
        getBankDetail();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Const.REQUEST_UPDATE_BANK_DETAIL:
                    getBankDetail();
                    break;

                default:
                    // do with default
                    break;
            }

        }

    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        rcvBankDetail = (RecyclerView) findViewById(R.id.rcvBankDetail);
        floatingBtnAddBankDetail = (FloatingActionButton) findViewById(R.id
                .floatingBtnAddBankDetail);
    }

    @Override
    protected void setViewListener() {
        floatingBtnAddBankDetail.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingBtnAddBankDetail:
                goToAddBankDetailActivity(null);
                break;

            default:
                // do with default
                break;
        }
    }

    private void initRcvBank() {
        rcvBankDetail.setLayoutManager(new LinearLayoutManager(this));
        bankDetailAdapter = new BankDetailAdapter(this, bankDetails);
        rcvBankDetail.setAdapter(bankDetailAdapter);
        rcvBankDetail.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration
                .VERTICAL));
    }

    /**
     * this method  call webservice for get bank detail
     */
    private void getBankDetail() {
        Utils.showCustomProgressDialog(this, false);
        final BankDetail bankDetail = new BankDetail();
        bankDetail.setBankHolderId(preferenceHelper.getProviderId());
        bankDetail.setBankHolderType(Const.TYPE_PROVIDER);
        bankDetail.setServerToken(preferenceHelper.getSessionToken());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BankDetailResponse> responseCall = apiInterface.getBankDetail(ApiClient
                .makeGSONRequestBody(bankDetail));
        responseCall.enqueue(new Callback<BankDetailResponse>() {
            @Override
            public void onResponse(Call<BankDetailResponse> call, Response<BankDetailResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        bankDetails.clear();
                        bankDetails.addAll(response.body().getBankDetail());
                        bankDetailAdapter.notifyDataSetChanged();

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), BankDetailActivity
                                .this);
                    }

            }

            @Override
            public void onFailure(Call<BankDetailResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.BANK_DETAIL_ACTIVITY, t);
            }
        });

    }

    public void goToAddBankDetailActivity(BankDetail bankDetail) {
        Intent intent = new Intent(this, AddBankDetailActivity.class);
        intent.putExtra(Const.BUNDLE, bankDetail);
        startActivityForResult(intent, Const.REQUEST_UPDATE_BANK_DETAIL);
    }

    public void openVerifyAccountDialog(final BankDetail bankDetail) {
        if (TextUtils.isEmpty(preferenceHelper.getSocialId())) {
            CustomDialogVerification customDialogVerification = new CustomDialogVerification(this,
                    getResources()
                            .getString(R.string
                                    .text_verify_account), getResources()
                    .getString(R.string.msg_enter_password_which_used_in_register),
                    getResources().getString(R.string.text_cancel), getResources()
                    .getString(R.string.text_ok), "", getResources()
                    .getString(R.string
                            .text_password), false, InputType.TYPE_CLASS_TEXT, InputType
                    .TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT) {
                @Override
                public void onClickLeftButton() {
                    dismiss();

                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {
                    if (!etDialogEditTextTwo.getText().toString().isEmpty()) {
                        bankDetail.setPassword(etDialogEditTextTwo.getText().toString());
                        deleteBankDetail(bankDetail);
                        dismiss();
                        // implementation remain
                    } else {
                        etDialogEditTextTwo.setError(getString(R.string.msg_enter_password));
                    }
                }

            };
            customDialogVerification.show();
        } else {
            bankDetail.setPassword("");
            deleteBankDetail(bankDetail);
        }


    }

    private void deleteBankDetail(BankDetail bankDetail) {
        Utils.showCustomProgressDialog(this, false);
        bankDetail.setBankDetailId(bankDetail.getId());
        bankDetail.setBankHolderId(preferenceHelper.getProviderId());
        bankDetail.setBankHolderType(Const.TYPE_PROVIDER);
        bankDetail.setSocialId(preferenceHelper.getSocialId());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BankDetailResponse> responseCall = apiInterface.deleteBankDetail(ApiClient
                .makeGSONRequestBody(bankDetail));
        responseCall.enqueue(new Callback<BankDetailResponse>() {
            @Override
            public void onResponse(Call<BankDetailResponse> call, Response<BankDetailResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        bankDetails.clear();
                        bankDetails.addAll(response.body().getBankDetail());
                        bankDetailAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), BankDetailActivity
                                .this);
                    }

            }

            @Override
            public void onFailure(Call<BankDetailResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.BANK_DETAIL_ACTIVITY, t);
            }
        });

    }

    public void selectBankDetail(final BankDetail bankDetail) {
        Utils.showCustomProgressDialog(this, false);
        bankDetail.setBankDetailId(bankDetail.getId());
        bankDetail.setBankHolderId(preferenceHelper.getProviderId());
        bankDetail.setBankHolderType(Const.TYPE_PROVIDER);
        bankDetail.setSocialId(preferenceHelper.getSocialId());
        bankDetail.setServerToken(preferenceHelper.getSessionToken());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.selectBankDetail(ApiClient
                .makeGSONRequestBody(bankDetail));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        for (BankDetail detail : bankDetails) {
                            detail.setSelected(false);
                        }
                        bankDetail.setSelected(true);
                        bankDetailAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), BankDetailActivity
                                .this);
                    }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.BANK_DETAIL_ACTIVITY, t);
            }
        });

    }
}

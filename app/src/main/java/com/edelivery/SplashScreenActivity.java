package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;


import com.edelivery.component.CustomDialogAlert;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenActivity extends BaseAppCompatActivity implements BaseAppCompatActivity.NetworkListener
{

    private CustomDialogAlert customDialogEnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* Fabric.with(this, new Crashlytics());*/

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


      /*  try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.fastEat.deliveryman",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature: info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e ) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
*/

        checkPermission();
    }

    /**
     * method used to call a webservice for get admin setting detail for  device type
     */
    private void getSettingsDetail() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
            jsonObject.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.SPLASH_SCREEN_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AppSettingDetailResponse> detailResponseCall = apiInterface.getAppSettingDetail
                (ApiClient.makeJSONRequestBody(jsonObject));
        detailResponseCall.enqueue(new Callback<AppSettingDetailResponse>() {
            @Override
            public void onResponse(Call<AppSettingDetailResponse> call,
                                   Response<AppSettingDetailResponse> response) {

                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.parseAppSettingDetail(response)) {

                    if (response.body().isOpenUpdateDialog() && checkVersionCode(response.body()
                            .getVersionCode())) {
                        openUpdateAppDialog(response.body().isForceUpdate());
                    } else {
                        goToActivity();
                    }
                }

            }

            @Override
            public void onFailure(Call<AppSettingDetailResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.SPLASH_SCREEN_ACTIVITY, t);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.ACTION_SETTINGS:
                checkIfGpsOrInternetIsEnable();
                break;
            default:
                // do with default
                break;
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        //do something
    }

    @Override
    protected void setViewListener() {
        //do something
    }

    @Override
    protected void onBackNavigation() {
        //do something
    }

    /**
     * this method will check play service is updated
     *
     * @return
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 12)
                        .show();
            } else {
                AppLog.Log("Google Paly Service", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        //do something
    }

    /**
     * this method is check that internet or GPS is ON or OFF
     */
    private void checkIfGpsOrInternetIsEnable() {
        if (!Utils.isInternetConnected(this)) {
            openInternetDialog(this);
            setNetworkListener(this);
        } else {
            closedEnableDialogInternet();
            if (checkPlayServices()) {
                getSettingsDetail();
            }
        }

    }

    /**
     * this method will check particular  permission will be granted by user or not
     */
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            //Do the stuff that requires permission...
            checkIfGpsOrInternetIsEnable();
        }
    }

    /**
     * this method will make decision according to permission result
     *
     * @param grantResults set result from system or OS
     */
    private void goWithLocationPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            checkIfGpsOrInternetIsEnable();
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
           /* // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.ACCESS_COARSE_LOCATION) && ActivityCompat
                    .shouldShowRequestPermissionRationale(this, android.Manifest
                            .permission.ACCESS_FINE_LOCATION)) {
                openPermissionDialog();
            } else {
                closedPermissionDialog();
            }*/
           checkIfGpsOrInternetIsEnable();
        }
    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }

    private void openPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogAlert(this, getResources().getString(R.string
                .text_attention), getResources().getString(R.string
                .msg_reason_for_permission_location), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
                finishAffinity();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{android
                        .Manifest
                        .permission
                        .ACCESS_FINE_LOCATION, android.Manifest.permission
                        .ACCESS_COARSE_LOCATION}, Const
                        .PERMISSION_FOR_LOCATION);
                closedPermissionDialog();
            }

        };
        if (!isFinishing()) {
            customDialogEnable.show();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    goWithLocationPermission(grantResults);
                    break;
                default:
                    //do with default
                    break;
            }
        }

    }

    @Override
    public void onNetworkChange(boolean isEnable) {
        checkIfGpsOrInternetIsEnable();
    }


    void openUpdateAppDialog(final boolean isForceUpdate) {
        String btnNegative;
        if (isForceUpdate) {
            btnNegative = getResources()
                    .getString(R.string.text_exit);
        } else {
            btnNegative =
                    getResources()
                            .getString(R.string.text_later);
        }
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_update_app), this.getResources()
                .getString(R.string.msg_new_app_update_available), btnNegative, this.getResources()
                .getString(R.string.text_update)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
                if (isForceUpdate) {
                    finishAffinity();
                } else {
                    goToActivity();
                }

            }

            @Override
            public void onClickRightButton() {
                final String appPackageName = getPackageName(); // getPackageName() from Context
                // or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                            + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    AppLog.handleException(SplashScreenActivity.class.getName(), anfe);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google" +
                            ".com/store/apps/details?id=" + appPackageName)));
                }
                dismiss();
                finishAffinity();
            }
        };
        if (!isFinishing()) {
            customDialogAlert.show();
        }


    }

    /**
     * this method will check that is our app is updated or not ,according to admin app version
     * code
     *
     * @param code
     * @return
     */
    private boolean checkVersionCode(String code) {

        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode < Integer
                    .valueOf(code);
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.handleException(SplashScreenActivity.class.getName(), e);
        }
        return false;

    }

    private void goToActivity() {
        if (preferenceHelper.getSessionToken().equals("")) {
            goToLoginActivity();
        } else {
            goToHomeActivity();
        }
    }
}

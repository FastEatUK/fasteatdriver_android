package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.R;


/**
 * Created by elluminati on 06-Feb-2017.
 */
public abstract class CustomMapdialog extends Dialog implements View.OnClickListener {

    private CustomFontTextView tvCamera, tvGallery, tvPhotoDialogTitle,tvhere;

    public CustomMapdialog(Context context, String title) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_map);
        tvPhotoDialogTitle = (CustomFontTextView) findViewById(R.id.tvPhotoDialogTitle);
        tvPhotoDialogTitle.setText(title);
        tvCamera = (CustomFontTextView) findViewById(R.id.tvCamera);
        tvGallery = (CustomFontTextView) findViewById(R.id.tvGallery);
        tvGallery.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
    }

    public CustomMapdialog(Context context, String title, String textOne, String textTwo,String three) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_map);
        tvPhotoDialogTitle = (CustomFontTextView) findViewById(R.id.tvPhotoDialogTitle);
        tvPhotoDialogTitle.setText(title);
        tvCamera = (CustomFontTextView) findViewById(R.id.tvCamera);
        tvGallery = (CustomFontTextView) findViewById(R.id.tvGallery);
        tvhere=(CustomFontTextView)findViewById(R.id.tvhere);
        tvhere.setText(three);
        tvCamera.setText(textOne);
        tvGallery.setText(textTwo);
        tvGallery.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvhere.setOnClickListener(this);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCamera:
                clickedOnCamera();
                break;
            case R.id.tvGallery:
                clickedOnGallery();
                break;
            case R.id.tvhere:
                clickheremap();
            default:
                // set default
                break;
        }
    }

    public abstract void clickedOnCamera();

    public abstract void clickedOnGallery();

    public abstract void clickheremap();
}

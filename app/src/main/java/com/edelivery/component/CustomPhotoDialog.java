package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.R;


/**
 * Created by elluminati on 06-Feb-2017.
 */
public abstract class CustomPhotoDialog extends Dialog implements View.OnClickListener {

    private CustomFontTextView tvCamera, tvGallery, tvPhotoDialogTitle;

    public CustomPhotoDialog(Context context, String title) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_picture);
        tvPhotoDialogTitle = (CustomFontTextView) findViewById(R.id.tvPhotoDialogTitle);
        tvPhotoDialogTitle.setText(title);
        tvCamera = (CustomFontTextView) findViewById(R.id.tvCamera);
        tvGallery = (CustomFontTextView) findViewById(R.id.tvGallery);
        tvGallery.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
    }

    public CustomPhotoDialog(Context context, String title, String textOne, String textTwo) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_picture);
        tvPhotoDialogTitle = (CustomFontTextView) findViewById(R.id.tvPhotoDialogTitle);
        tvPhotoDialogTitle.setText(title);
        tvCamera = (CustomFontTextView) findViewById(R.id.tvCamera);
        tvGallery = (CustomFontTextView) findViewById(R.id.tvGallery);
        tvCamera.setText(textOne);
        tvGallery.setText(textTwo);
        tvGallery.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCamera:
                clickedOnCamera();
                break;
            case R.id.tvGallery:
                clickedOnGallery();
                break;

            default:
                // set default
                break;
        }
    }

    public abstract void clickedOnCamera();

    public abstract void clickedOnGallery();
}

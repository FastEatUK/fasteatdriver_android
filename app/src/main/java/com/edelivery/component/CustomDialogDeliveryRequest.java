package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.ActiveDeliveryActivity;
import com.edelivery.AvailableDeliveryActivity;
import com.edelivery.R;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.PushDataResponse;
import com.edelivery.models.responsemodels.OrderStatusResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.SoundHelper;
import com.edelivery.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 16-Mar-17.
 */

public class CustomDialogDeliveryRequest extends Dialog implements View.OnClickListener {

    private ImageView ivCustomerImage;
    private CustomFontTextView tvDeliveryDate, tvPickupAddress,
            tvDeliveryAddress, tvOrderRemainTime, tvDeliveryStatus;
    private CustomFontTextViewTitle tvCustomerName;
    private CustomFontTextView btnDialogAlertLeft, btnDialogAlertRight, tvOrderNumber, tvProfit,
            tvOrderPrice,tvOrderId;
    private CountDownTimer countDownTimer;
    private boolean isCountDownTimerStart;
    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;
    private Context context;
    private PushDataResponse pushDataResponse;
    private String orderResponse;
    private SoundHelper soundHelper;
    private int count = 0;
    private Gson gson;
    private LinearLayout llPrice, llProfit;


    public CustomDialogDeliveryRequest(Context context, String orderResponse, String
            orderTimeRemain) {
        super(context);
        this.context = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_delivery_request);
        gson = new Gson();
        soundHelper = SoundHelper.getInstance(context);
        this.orderResponse = orderResponse;
        preferenceHelper = PreferenceHelper.getInstance(context);
        parseContent = ParseContent.getInstance();
        ivCustomerImage = (ImageView) findViewById(R.id.ivCustomerImage);
        tvCustomerName = (CustomFontTextViewTitle) findViewById(R.id.tvCustomerName);
        tvDeliveryDate = (CustomFontTextView) findViewById(R.id.tvDeliveryDate);
        tvDeliveryAddress = (CustomFontTextView) findViewById(R.id.tvAddressTwo);
        tvPickupAddress = (CustomFontTextView) findViewById(R.id.tvAddressOne);
        tvOrderRemainTime = (CustomFontTextView) findViewById(R.id.tvOrderRemainTime);
        btnDialogAlertLeft = (CustomFontTextView) findViewById(R.id.btnDialogAlertLeft);
        btnDialogAlertRight = (CustomFontTextView) findViewById(R.id.btnDialogAlertRight);
        tvDeliveryStatus = (CustomFontTextView) findViewById(R.id.tvDeliveryStatus);
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        tvDeliveryStatus.setPaintFlags(tvDeliveryStatus.getPaintFlags() | Paint
                .UNDERLINE_TEXT_FLAG);
        btnDialogAlertLeft.setOnClickListener(this);
        btnDialogAlertRight.setOnClickListener(this);
        tvDeliveryStatus.setOnClickListener(this);
        llPrice = (LinearLayout) findViewById(R.id.llPrice);
        llProfit = (LinearLayout) findViewById(R.id.llProfit);
        tvProfit = (CustomFontTextView) findViewById(R.id.tvProfit);
        tvOrderPrice = (CustomFontTextView) findViewById(R.id.tvOrderPrice);
        tvOrderId=findViewById(R.id.tvOrderId);
        tvOrderId.setVisibility(View.VISIBLE);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        setCancelable(false);
        loadData();
        startCountDownTimer(Integer.valueOf(orderTimeRemain));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDialogAlertLeft:
                stopCountDownTimer();
                rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED);
                dismiss();
                break;
            case R.id.btnDialogAlertRight:
                stopCountDownTimer();
                setOrderStatus(Const.ProviderStatus.DELIVERY_MAN_ACCEPTED);
                break;
            case R.id.tvDeliveryStatus:
                stopCountDownTimer();
                dismiss();
                goToActivity();
                break;
            default:
                // do with default
                break;
        }
    }

    public void goToActivity() {
        Intent intent;
        if (count >= 2) {
            intent = new Intent(context, AvailableDeliveryActivity.class);
        } else {
            intent = new Intent(context, ActiveDeliveryActivity.class);
            intent.putExtra(Const.Params.ORDER_ID, pushDataResponse.getRequestId());
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    /**
     * this method is set countDown timer for count a trip accepting time
     *
     * @param seconds
     */
    private void startCountDownTimer(int seconds) {
        AppLog.Log("CountDownTimer", "Start");
        if (!isCountDownTimerStart && seconds > 0) {
            isCountDownTimerStart = true;
            final long milliSecond = 1000;
            long millisUntilFinished = seconds * milliSecond;
            countDownTimer = null;
            if (preferenceHelper.getIsNewOrderSoundOn()) {
                soundHelper.playWhenNewOrderSound();
            } else {
                soundHelper.stopWhenNewOrderSound(context);
            }
            countDownTimer = new CountDownTimer(millisUntilFinished, milliSecond) {

                public void onTick(long millisUntilFinished) {

                    final long seconds = millisUntilFinished / milliSecond;
                    tvOrderRemainTime.setText(String.valueOf(seconds) + "s");

                }

                public void onFinish() {
                    stopCountDownTimer();
                    rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED);
                    dismiss();

                }

            }.start();
        }
    }

    private void stopCountDownTimer() {
        AppLog.Log("CountDownTimer", "Stop");
        if (isCountDownTimerStart) {
            isCountDownTimerStart = false;
            countDownTimer.cancel();
            tvOrderRemainTime.setText("");
        }
        soundHelper.stopWhenNewOrderSound(context);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopCountDownTimer();
    }


    private void rejectOrCancelDeliveryOrder(int orderStatus) {
        Utils.showCustomProgressDialog(context, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, pushDataResponse
                    .getRequestId());
            jsonObject.put(Const.Params.DELIVERY_STATUS, orderStatus);
        } catch (JSONException e) {
            AppLog.handleThrowable("DIALOG_DELiVERY", e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.rejectOrCancelDelivery(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.isSuccessful(response)) {
                    sendBroadcast();
                    Utils.hideCustomProgressDialog();
                    if (response.body().isSuccess()) {
                        // add
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                context);
                    }
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable("DIALOG_DELiVERY", t);

            }
        });
    }

    private void loadData() {

        pushDataResponse = gson.fromJson(orderResponse, PushDataResponse
                .class);
        tvCustomerName.setText(pushDataResponse.getStoreName());
        try {
            if (TextUtils.isEmpty(pushDataResponse.getEstimatedTimeForReadyOrder())) {
                Date date = parseContent.webFormat.parse(pushDataResponse.getCreatedAt());
                String stringBuilder = Utils.getDayOfMonthSuffix(Integer.valueOf(parseContent.day
                        .format(date))) +
                        " " +
                        parseContent.dateFormatMonth.format(date);
                tvDeliveryDate.setText(stringBuilder);
            } else {
                Date date = parseContent.webFormat.parse(pushDataResponse
                        .getEstimatedTimeForReadyOrder
                                ());
                tvDeliveryDate.setText(context.getResources().getString(R.string
                        .text_pick_up_order_after) + " " + parseContent.dateTimeFormat.format
                        (date));
            }


        } catch (ParseException e) {
            AppLog.handleThrowable("DIALOG_DELiVERY", e);
        }
        String requestNumber = context.getResources().getString(R.string.text_request_number)
                + " " + "#" + pushDataResponse.getUniqueId();
        String orderNumber = context.getResources().getString(R.string.text_order_number)
                + " " + "#" + pushDataResponse.getOrderUniqueId();
        tvOrderNumber.setText(requestNumber);
        tvOrderId.setText(orderNumber);
        tvDeliveryAddress.setText(pushDataResponse.getDestinationAddresses().get(0).getAddress());
        tvPickupAddress.setText(pushDataResponse.getPickupAddresses().get(0).getAddress());
        Glide.with(context).load(BASE_URL + pushDataResponse
                .getStoreImage()).into(ivCustomerImage);
        AppLog.Log("NEW_ORDER_ID", pushDataResponse.getRequestId() + "");
        count = pushDataResponse.getOrderCount();
        llProfit.setVisibility(View.VISIBLE);
        llPrice.setVisibility(View.VISIBLE);

        tvOrderPrice.setText(pushDataResponse.getCurrency() +  parseContent
                .decimalTwoDigitFormat.format(Double.valueOf(pushDataResponse.getTotalOrderPrice())));
        tvProfit.setText(pushDataResponse.getCurrency() +parseContent
                .decimalTwoDigitFormat.format( Double.valueOf(pushDataResponse.getTotalProviderIncome())));

        updateUI();
    }

    private void setOrderStatus(int orderStatus) {

        Utils.showCustomProgressDialog(context, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, pushDataResponse
                    .getRequestId());
            jsonObject.put(Const.Params.DELIVERY_STATUS, orderStatus);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }
        Log.i("setOrderStatus",jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderStatusResponse> responseCall = apiInterface.setRequestStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse>
                    response) {
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.isSuccessful(response)) {
                    sendBroadcast();
                    Utils.hideCustomProgressDialog();
                    if (response.body().isSuccess()) {
                        // add
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                context);
                    }
                    dismiss();
                }

            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });

    }


    private void sendBroadcast() {
        context.sendBroadcast(new Intent(Const.Action.ACTION_ORDER_STATUS));
    }

    public void notifyDataSetChange(String orderResponse) {
        pushDataResponse = gson.fromJson(orderResponse, PushDataResponse
                .class);
        count = pushDataResponse.getOrderCount();
        updateUI();
    }

    private void updateUI() {

        if (count >= 2) {
            btnDialogAlertRight.setVisibility(View.GONE);
            btnDialogAlertLeft.setVisibility(View.GONE);
            tvDeliveryStatus.setText(context.getResources().getString(R.string.text_view_more) + " "
                    + "+"
                    + (count - 1));
        } else {
            btnDialogAlertRight.setVisibility(View.VISIBLE);
            btnDialogAlertLeft.setVisibility(View.VISIBLE);
            tvDeliveryStatus.setText(context.getResources().getString(R.string.text_view_more));
        }


    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopCountDownTimer();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                AppLog.handleException(CustomDialogDeliveryRequest.class.getName(), e);
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}

package com.edelivery.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import com.google.android.material.textfield.TextInputEditText;
import android.util.AttributeSet;

import com.edelivery.R;
import com.edelivery.utils.AppLog;


/**
 * @author Elluminati elluminati.in
 */
public class CustomFontEditTextView extends TextInputEditText {

//	private static final String TAG = "EditText";

    private Typeface typeface;
    public static final String TAG = "CustomFontEditTextView";
    public CustomFontEditTextView(Context context) {
        super(context);
    }

    public CustomFontEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomFontEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.app);
        String customFont = a.getString(R.styleable.app_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    private boolean setCustomFont(Context context, String asset) {
        try {
            if (typeface == null) {
                // Log.i(TAG, "asset:: " + "fonts/" + asset);
                typeface = Typeface.createFromAsset(context.getAssets(),
                        "fonts/ClanPro-News.otf");
            }

        } catch (Exception e) {
            AppLog.handleException(TAG,e);
            // Log.e(TAG, "Could not get typeface: " + e.getMessage());
            return false;
        }

        setTypeface(typeface);
        return true;
    }
}
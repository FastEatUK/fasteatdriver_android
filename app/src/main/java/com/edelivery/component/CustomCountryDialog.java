package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.R;
import com.edelivery.adapter.CountryAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Countries;

import java.util.ArrayList;

;

/**
 * Created by elluminati on 04-08-2016.
 */
public abstract class CustomCountryDialog extends Dialog {

    private RecyclerView rcvCountryCode;
    private CountryAdapter countryAdapter;
    private Context context;

    public CustomCountryDialog(Context context, ArrayList<Countries> countryList) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_country_code);
        this.context = context;
        rcvCountryCode = (RecyclerView) findViewById(R.id.rcvCountryCode);
        rcvCountryCode.setLayoutManager(new LinearLayoutManager(context));
        countryAdapter = new CountryAdapter(countryList);
        rcvCountryCode.setAdapter(countryAdapter);
        rcvCountryCode.addItemDecoration(new DividerItemDecoration(context,
                LinearLayoutManager.VERTICAL));
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rcvCountryCode.addOnItemTouchListener(new RecyclerTouchListener(context, rcvCountryCode,
                new ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        onSelect(position);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    public abstract void onSelect(int position);


}

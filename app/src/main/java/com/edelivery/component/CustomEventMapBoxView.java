package com.edelivery.component;

        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.location.Location;
        import android.util.AttributeSet;
        import android.util.Log;

        import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;

        import com.mapbox.android.core.location.LocationEngine;
        import com.mapbox.android.core.location.LocationEngineCallback;
        import com.mapbox.android.core.location.LocationEngineProvider;
        import com.mapbox.android.core.location.LocationEngineRequest;
        import com.mapbox.android.core.location.LocationEngineResult;
        import com.mapbox.android.core.permissions.PermissionsManager;
        import com.mapbox.mapboxsdk.geometry.LatLng;
        import com.mapbox.mapboxsdk.location.LocationComponent;
        import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
        import com.mapbox.mapboxsdk.location.modes.CameraMode;
        import com.mapbox.mapboxsdk.location.modes.RenderMode;
        import com.mapbox.mapboxsdk.maps.MapView;
        import com.mapbox.mapboxsdk.maps.MapboxMap;
        import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
        import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
        import com.mapbox.mapboxsdk.maps.Style;

        import static android.os.Looper.getMainLooper;

public class CustomEventMapBoxView extends MapView implements OnMapReadyCallback {

    public MapBoxMapReady mapBoxMapReady;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;

    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    MainActivityLocationCallback callback = new MainActivityLocationCallback();

    public CustomEventMapBoxView(@NonNull Context context) {
        super(context);
        this.mapBoxMapReady = mapBoxMapReady;
    }

    public CustomEventMapBoxView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEventMapBoxView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomEventMapBoxView(@NonNull Context context, @Nullable MapboxMapOptions options) {
        super(context, options);
    }


    public CustomEventMapBoxView(Context applicationContext, MapView mapbox, MapBoxMapReady mapBoxMapReady) {
        this(applicationContext);
        this.mapBoxMapReady = mapBoxMapReady;
        mapbox.getMapAsync(this);
    }

    /*public CustomEventMapBoxView(Context applicationContext, MapView mapbox, AddUserLocationActivity demoMapboxActivity) {
        super(applicationContext);
        this.mapBoxMapReady = mapBoxMapReady;
        mapbox.getMapAsync(this);
    }*/

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {


        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {


                mapBoxMapReady.mapBoxMapReady(mapboxMap, style);





               /* enableLocationComponent(style, mapboxMap);

                mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        LatLng mLatLng = mapboxMap.getCameraPosition().target;
                        if(mLatLng != null){
                            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
//                    geocoder.setAccessToken(getString(R.string.MAPBOX_ACCESS_TOKEN));
                            try {
                                List<Address> addresses = geocoder.getFromLocation(
                                        mLatLng.getLatitude(),
                                        mLatLng.getLongitude(),
                                        1);
                                if(addresses != null && addresses.size() > 0 && addresses.get(0) != null){
                                    Log.e("tagMap", "onResponse: " + addresses.get(0).toString());
                                    if(addresses.get(0).getCountryName() != null){
                                        Log.e("tagMap", "onResponse: " + addresses.get(0).getCountryName());
//                                        acDeliveryAddress.setText(addresses.get(0).getAddressLine(0));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });*/
            }
        });
    }



    /**
     * Initialize the Maps SDK's LocationComponent
     */
    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle, MapboxMap mapboxMap) {
// Check if permissions are enabled and if not mLocationEngineRequest
        if (PermissionsManager.areLocationPermissionsGranted(getContext())) {

// Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

// Set the LocationComponent activation options
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(getContext(), loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();

// Activate with the LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(locationComponentActivationOptions);

// Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

// Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);

            initLocationEngine();
        } else {
           /* permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);*/
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(getContext());

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }


    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        /*private final WeakReference<StoreLocationActivity> activityWeakReference;

        MainActivityLocationCallback(StoreLocationActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }*/
        MainActivityLocationCallback() {

        }

        @Override
        public void onSuccess(LocationEngineResult result) {
//            StoreLocationActivity activity = activityWeakReference.get();

            /*if (activity != null) {


            }*/
            Location location = result.getLastLocation();

            if (location == null) {
                return;
            }

           /* if(!isCurrentLocationGet){
                isCurrentLocationGet = true;
                // Move map camera to the selected location
                zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
            }*/
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
//            StoreLocationActivity activity = activityWeakReference.get();
            /*if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }*/
        }
    }

    public void zoomCamera(LatLng latLng) {
        /*ma.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(14)
                        .build()), 4000);*/
    }

    public MainActivityLocationCallback getCallback() {
        return callback;
    }

    public void setCallback(MainActivityLocationCallback callback) {
        this.callback = callback;
    }
}

package com.edelivery.component;

import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;

public interface MapBoxMapReady {
    public void mapBoxMapReady(MapboxMap mapboxMap, Style style);

}

package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.R;
import com.edelivery.adapter.CityAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Cities;

import java.util.ArrayList;

/**
 * Created by elluminati on 04-08-2016.
 */
public abstract class CustomCityDialog extends Dialog {

    private RecyclerView rcvCountryCode;
    private CustomFontTextView tvCountryDialogTitle;
    private CityAdapter cityAdapter;
    private Context context;

    public CustomCityDialog(Context context, ArrayList<Cities> cityList) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_country_code);
        rcvCountryCode = (RecyclerView) findViewById(R.id.rcvCountryCode);
        tvCountryDialogTitle = (CustomFontTextView) findViewById(R.id.tvCountryDialogTitle);
        tvCountryDialogTitle.setText(context.getResources().getString(R.string
                .text_select_city));
        rcvCountryCode.setLayoutManager(new LinearLayoutManager(context));
        cityAdapter = new CityAdapter(cityList);
        rcvCountryCode.setAdapter(cityAdapter);
        rcvCountryCode.addItemDecoration(new DividerItemDecoration(context,
                LinearLayoutManager.VERTICAL));
        this.context = context;
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rcvCountryCode.addOnItemTouchListener(new RecyclerTouchListener(context, rcvCountryCode,
                new ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        onSelect(position);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    public abstract void onSelect(int position);


}

package com.edelivery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.edelivery.adapter.VehicleDetailAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.models.datamodels.Vehicle;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.VehicleDetailResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleDetailActivity extends BaseAppCompatActivity {

    public static final String TAG = VehicleDetailActivity.class.getName();
    private RecyclerView rcvVehilceDetail;
    private VehicleDetailAdapter vehicleDetailAdapter;
    private ArrayList<Vehicle> vehicleDetail = new ArrayList<>();
    private FloatingActionButton floatingBtnAddVehicleDetail;
    private boolean isApplicationStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_vehicle_detail));
        findViewById();
        setViewListener();
        getExtraData();
        vehicleDetail = new ArrayList<>();
        initRcvVehicle();
        getVehicleDetail();

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            isApplicationStart = getIntent().getExtras().getBoolean(Const.Tag.HOME_FRAGMENT);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Const.REQUEST_UPDATE_VEHICLE_DETAIL:
                    getVehicleDetail();
                    break;

                default:
                    // do with default
                    break;
            }

        }

    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        rcvVehilceDetail = (RecyclerView) findViewById(R.id.rcvVehicleDetail);
        floatingBtnAddVehicleDetail = (FloatingActionButton) findViewById(R.id
                .floatingBtnAddVehicleDetail);
    }

    @Override
    protected void setViewListener() {
        floatingBtnAddVehicleDetail.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (isApplicationStart && vehicleDetail.isEmpty()) {
            openLogoutDialog();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingBtnAddVehicleDetail:
                goToAddVehicleDetailActivity(null);
                break;

            default:
                // do with default
                break;
        }
    }


    /**
     * this method  call webservice for get bank detail
     */
    private void getVehicleDetail() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID,
                    preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN,
                    preferenceHelper.getSessionToken());

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<VehicleDetailResponse> responseCall = apiInterface.getVehicleDetail(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<VehicleDetailResponse>() {
            @Override
            public void onResponse(Call<VehicleDetailResponse> call, Response<VehicleDetailResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        vehicleDetail.clear();
                        vehicleDetail.addAll(response.body().getVehicleList());
                        vehicleDetailAdapter.notifyDataSetChanged();
                        if (isApplicationStart && preferenceHelper
                                .getIsProviderAllVehicleDocumentsUpload()) {
                            goToHomeActivity();
                        }
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), VehicleDetailActivity
                                .this);
                    }

            }

            @Override
            public void onFailure(Call<VehicleDetailResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });

    }

    /**
     * this method  call webservice for get bank detail
     */
    public void selectVehicle(final int position) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID,
                    preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN,
                    preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.VEHICLE_ID,
                    vehicleDetail.get(position).getId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.selectVehicle(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        for (Vehicle vehicle : vehicleDetail) {
                            vehicle.setSelected(false);
                        }
                        vehicleDetail.get(position).setSelected(true);
                        preferenceHelper.putSelectedVehicleId(vehicleDetail.get(position).getId());
                        vehicleDetailAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), VehicleDetailActivity
                                .this);
                    }
                }


            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });

    }

    private void initRcvVehicle() {
        rcvVehilceDetail.setLayoutManager(new LinearLayoutManager(this));
        vehicleDetailAdapter = new VehicleDetailAdapter(this, vehicleDetail);
        rcvVehilceDetail.setAdapter(vehicleDetailAdapter);
        rcvVehilceDetail.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration
                .VERTICAL));
    }

    public void goToAddVehicleDetailActivity(Vehicle vehicle) {
        Intent intent = new Intent(this, AddVehicleActivity.class);
        intent.putExtra(Const.BUNDLE, vehicle);
        intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        startActivityForResult(intent, Const.REQUEST_UPDATE_VEHICLE_DETAIL);
    }

    private void openLogoutDialog() {
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, getResources()
                .getString(R.string.text_log_out), getResources()
                .getString(R.string.msg_are_you_sure), getResources()
                .getString(R.string.text_cancel), getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                logOut();
                dismiss();
            }
        };
        customDialogAlert.show();
    }
}

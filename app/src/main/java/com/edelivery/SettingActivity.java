package com.edelivery;

import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Bundle;
import androidx.appcompat.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomLanguageDialog;

public class SettingActivity extends BaseAppCompatActivity implements CompoundButton
        .OnCheckedChangeListener {

    CustomFontTextView tvLanguage;
    SwitchCompat switchNewOrderSound, switchOrderPickUpPointSound, switchPushNotificationSound;
    CustomLanguageDialog customLanguageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_settings));
        findViewById();
        setViewListener();
        setLanguageName();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        tvLanguage = (CustomFontTextView) findViewById(R.id.tvLanguage);
        switchNewOrderSound = (SwitchCompat) findViewById(R.id.switchNewOrderSound);
        switchOrderPickUpPointSound = (SwitchCompat) findViewById(R.id.switchOrderPickUpPointSound);
        switchPushNotificationSound = (SwitchCompat) findViewById(R.id.switchPushNotificationSound);
        switchNewOrderSound.setChecked(preferenceHelper.getIsNewOrderSoundOn());
        switchOrderPickUpPointSound.setChecked(preferenceHelper.getIsOrderPickUpPointSoundOn());
        switchPushNotificationSound.setChecked(preferenceHelper.getIsPushNotificationSoundOn());

    }

    @Override
    protected void setViewListener() {
        tvLanguage.setOnClickListener(this);
        switchNewOrderSound.setOnCheckedChangeListener(this);
        switchOrderPickUpPointSound.setOnCheckedChangeListener(this);
        switchPushNotificationSound.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.tvLanguage:
               /* openLanguageDialog();*/
                break;

            default:
                // do with default
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switchNewOrderSound:
                preferenceHelper.putIsNewOrderSoundOn(isChecked);
                break;
            case R.id.switchOrderPickUpPointSound:
                preferenceHelper.putIsOrderPickUpPointSoundOn(isChecked);
                break;
            case R.id.switchPushNotificationSound:
                preferenceHelper.putIsPushNotificationSoundOn(isChecked);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void openLanguageDialog() {
        if (customLanguageDialog != null && customLanguageDialog.isShowing()) {
            return;
        }
        customLanguageDialog = new CustomLanguageDialog(this) {
            @Override
            public void onSelect(String languageName, String languageCode) {
                tvLanguage.setText(languageName);
                if (!TextUtils.equals(preferenceHelper.getLanguageCode(),
                        languageCode)) {
                    preferenceHelper.putLanguageCode(languageCode);
                    finishAffinity();
                    restartApp();
                }


                dismiss();

            }
        };
        customLanguageDialog.show();
    }

    private void setLanguageName() {
        TypedArray array = getResources().obtainTypedArray(R.array.language_code);
        TypedArray array2 = getResources().obtainTypedArray(R.array.language_name);
        int size = array.length();
        for (int i = 0; i < size; i++) {
            if (TextUtils.equals(preferenceHelper.getLanguageCode(), array.getString
                    (i))) {
                tvLanguage.setText(array2.getString(i));
                break;
            }
        }

    }
}

package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Cities;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityResponse{

	@SerializedName("cities")
	@Expose
	private List<Cities> cities;

	@SerializedName("success")
	@Expose
	private boolean success;

	@SerializedName("message")
	@Expose
	private int message;

	public void setCities(List<Cities> cities){
		this.cities = cities;
	}

	public List<Cities> getCities(){
		return cities;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(int message){
		this.message = message;
	}

	public int getMessage(){
		return message;
	}

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode= errorCode;
    }
}
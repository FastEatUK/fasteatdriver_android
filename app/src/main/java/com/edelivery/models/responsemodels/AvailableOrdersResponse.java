package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.AvailableOrder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AvailableOrdersResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private int message;

    @SerializedName("request_list")
    @Expose
    private List<AvailableOrder> availableOrder;

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getMessage() {
        return message;
    }

    public void setAvailableOrder(List<AvailableOrder> availableOrder) {
        this.availableOrder = availableOrder;
    }

    public List<AvailableOrder> getAvailableOrder() {
        return availableOrder;
    }

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
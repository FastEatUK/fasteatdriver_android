package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.OrderPayment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceResponse {
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("payment_gateway_name")
    private String payment;

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    @SerializedName("order_payment")
    @Expose
    private OrderPayment orderPayment;

    @SerializedName("success")
    @Expose
    private boolean success;


    @SerializedName("message")
    @Expose
    private int message;

    public void setOrderPayment(OrderPayment orderPayment) {
        this.orderPayment = orderPayment;
    }

    public OrderPayment getOrderPayment() {
        return orderPayment;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }


    public void setMessage(int message) {
        this.message = message;
    }

    public int getMessage() {
        return message;
    }

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
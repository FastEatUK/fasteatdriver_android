package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.OrderHistory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderHistoryResponse {

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private int message;

	@SerializedName("request_list")
	private List<OrderHistory> orderList;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(int message){
		this.message = message;
	}

	public int getMessage(){
		return message;
	}

	public void setOrderList(List<OrderHistory> orderList){
		this.orderList = orderList;
	}

	public List<OrderHistory> getOrderList(){
		return orderList;
	}

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode= errorCode;
    }
}
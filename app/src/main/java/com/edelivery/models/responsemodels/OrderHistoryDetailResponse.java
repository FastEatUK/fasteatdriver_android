package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.OrderHistoryDetail;
import com.edelivery.models.datamodels.StoreDetail;
import com.edelivery.models.datamodels.UserDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryDetailResponse {

    @SerializedName("currency")
    private String currency;
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private int message;
    @SerializedName("request")
    private OrderHistoryDetail orderDetail;
    @SerializedName("error_code")
    @Expose
    private int errorCode;
    @SerializedName("user_detail")
    private UserDetail user;
    @SerializedName("store_detail")
    private StoreDetail storeDetail;
    @SerializedName("payment_gateway_name")
    private String payment;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public OrderHistoryDetail getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(OrderHistoryDetail orderDetail) {
        this.orderDetail = orderDetail;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public UserDetail getUser() {
        return user;
    }

    public void setUser(UserDetail user) {
        this.user = user;
    }

    public StoreDetail getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
    }
}
package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.UserDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OrderStatusResponse {

    @SerializedName("user_detail")
    @Expose
    private UserDetail userDetail;
    @SerializedName("delivery_status")
    @Expose
    private int deliveryStatus;
    @SerializedName("order_status")
    @Expose
    private int orderStatus;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private int message;
    @SerializedName("error_code")
    @Expose
    private int errorCode;
    @SerializedName("request")
    @Expose
    private Order orderRequest;

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public Order getOrderRequest() {
        return orderRequest;
    }

    public void setOrderRequest(Order orderRequest) {
        this.orderRequest = orderRequest;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }
}
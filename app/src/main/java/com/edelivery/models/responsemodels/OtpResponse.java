package com.edelivery.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpResponse {

	@SerializedName("success")
	@Expose
	private boolean success;

	@SerializedName("otp_for_sms")
	@Expose
	private String otpForSms;

	@SerializedName("otp_for_email")
	@Expose
	private String otpForEmail;

	@SerializedName("message")
	@Expose
	private int message;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setOtpForSms(String otpForSms){
		this.otpForSms = otpForSms;
	}

	public String getOtpForSms(){
		return otpForSms;
	}

	public void setOtpForEmail(String otpForEmail){
		this.otpForEmail = otpForEmail;
	}

	public String getOtpForEmail(){
		return otpForEmail;
	}

	public void setMessage(int message){
		this.message = message;
	}

	public int getMessage(){
		return message;
	}
	@SerializedName("error_code")
	@Expose
	private int errorCode;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode= errorCode;
	}
}
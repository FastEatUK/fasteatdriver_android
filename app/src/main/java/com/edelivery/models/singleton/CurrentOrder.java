package com.edelivery.models.singleton;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by elluminati on 11-Apr-17.
 */

public class CurrentOrder {

    private static CurrentOrder currentOrder = new CurrentOrder();
    private String currency;
    private int availableOrders;
    private String vehiclePin;
    private Bitmap bmVehiclePin;
    private ArrayList<String> providerMessageList;

    private CurrentOrder() {
        providerMessageList = new ArrayList<>();
    }

    public static CurrentOrder getInstance() {
        return currentOrder;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getAvailableOrders() {
        return availableOrders;
    }

    public void setAvailableOrders(int availableOrders) {
        this.availableOrders = availableOrders;
    }

    public String getVehiclePin() {
        return vehiclePin;
    }

    public void setVehiclePin(String vehiclePin) {
        this.vehiclePin = vehiclePin;
    }

    public Bitmap getBmVehiclePin() {
        return bmVehiclePin;
    }

    public void setBmVehiclePin(Bitmap bmVehiclePin) {
        this.bmVehiclePin = bmVehiclePin;
    }

    public ArrayList<String> getProviderMessageList() {
        return providerMessageList;
    }

    public void setProviderMessageList(ArrayList<String> providerMessageList) {
        this.providerMessageList = providerMessageList;
    }

    public void clearCurrentOrder() {
        currency = "";
        availableOrders = 0;
        vehiclePin = "";
        bmVehiclePin = null;
    }
}

package com.edelivery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogDeliveryRequest;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.FontsOverride;
import com.edelivery.utils.LanguageHelper;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.SoundHelper;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseAppCompatActivity extends AppCompatActivity implements View
        .OnClickListener {

    public Toolbar toolbar;
    public PreferenceHelper preferenceHelper;
    public ParseContent parseContent;
    public CustomFontTextView tvTitleToolbar, tvToolRightText;
    public ImageView ivToolbarBack, ivToolbarRightIcon;
    public CurrentOrder currentOrder;
    private ActionBar actionBar;
    private CustomDialogAlert customDialogEnableInternet;
    private CustomDialogDeliveryRequest deliveryRequest;
    private AppReceiver appReceiver = new AppReceiver();
    private NetworkListener networkListener;
    private OrderListener orderListener;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/ClanPro-News.otf");
        preferenceHelper = PreferenceHelper.getInstance(this);
        parseContent = ParseContent.getInstance();
        SoundHelper.getInstance(this);
        parseContent.setContext(this);
        currentOrder = CurrentOrder.getInstance();
        setNetworkListener(new NetworkListener() {
            @Override
            public void onNetworkChange(boolean isEnable) {
                if (isEnable) {
                    closedEnableDialogInternet();
                } else {
                    openInternetDialog(BaseAppCompatActivity.this);
                    Utils.hideCustomProgressDialog();
                }
            }
        });
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.Action.NETWORK_ACTION);
        intentFilter.addAction(Const.Action.ACTION_ADMIN_APPROVED);
        intentFilter.addAction(Const.Action.ACTION_ADMIN_DECLINE);
        intentFilter.addAction(Const.Action.ACTION_ORDER_STATUS);
        intentFilter.addAction(Const.Action.ACTION_NEW_ORDER);
        intentFilter.addAction(Const.Action.ACTION_STORE_CANCELED_REQUEST);
        registerReceiver(appReceiver, intentFilter);

    }


    @Override
    protected void onResume() {
        super.onResume();
        LanguageHelper.wrapper(this, PreferenceHelper.getInstance
                (this)
                .getLanguageCode());

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(appReceiver);
        super.onDestroy();
    }


    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appToolbar);
        tvTitleToolbar = (CustomFontTextView) toolbar.findViewById(R.id.tvToolbarTitle);
        ivToolbarRightIcon = (ImageView) toolbar.findViewById(R.id.ivToolbarRightIcon);
        ivToolbarBack = (ImageView) findViewById(R.id.ivToolbarBack);
        tvToolRightText = (CustomFontTextView) findViewById(R.id.tvToolRightText);
        tvToolRightText.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        ivToolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackNavigation();
            }
        });
    }

    public void setTitleOnToolBar(String title) {
        if (tvTitleToolbar != null) {
            tvTitleToolbar.setText(title);
        }
    }

    protected void IsToolbarNavigationVisible(boolean isVisible) {
        if (isVisible) {
            ivToolbarBack.setVisibility(View.VISIBLE);
        } else {
            ivToolbarBack.setVisibility(View.GONE);
        }

    }

    public void setToolbarRightIcon(int drawableResourcesId, View.OnClickListener
            onClickListener) {
        ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable(this,
                drawableResourcesId));
        ivToolbarRightIcon.setOnClickListener(onClickListener);

    }

    public void hideSoftKeyboard(AppCompatActivity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * this method will help you to check all data filed is valid or not
     *
     * @return true if is valid otherwise false
     */
    protected abstract boolean isValidate();

    /**
     * method used to manage all view address
     */
    protected abstract void findViewById();

    /**
     * method used to manage all interface or listener
     */
    protected abstract void setViewListener();

    /**
     * method used to manage toolbar beck navigation button
     */
    protected abstract void onBackNavigation();


    protected void openInternetDialog(final Activity activity) {
        if (!this.isFinishing()) {
            if (customDialogEnableInternet != null && customDialogEnableInternet.isShowing()) {
                return;
            }

            customDialogEnableInternet = new CustomDialogAlert(this, getString(R.string
                    .text_internet), getString(R.string
                    .msg_internet_enable), getString(R.string.text_cancel), getString(R.string
                    .text_ok)) {

                @Override
                public void onClickLeftButton() {
                    closedEnableDialogInternet();
                    activity.finishAffinity();
                }

                @Override
                public void onClickRightButton() {
                    activity.startActivityForResult(new Intent(Settings
                            .ACTION_SETTINGS), Const.ACTION_SETTINGS);
                }

            };
            customDialogEnableInternet.show();
        }


    }


    protected void closedEnableDialogInternet() {
        if (customDialogEnableInternet != null && customDialogEnableInternet.isShowing()) {
            customDialogEnableInternet.dismiss();
            customDialogEnableInternet = null;

        }
    }

    public void goToLoginActivity() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public void goToHomeActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToDocumentActivity(boolean isApplicationStart) {
        Intent intent = new Intent(this, DocumentActivity.class);
        intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToVehicleDetailActivity(boolean isApplicationStart) {
        Intent intent = new Intent(this, VehicleDetailActivity.class);
        intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected void setNetworkListener(NetworkListener networkListener) {
        this.networkListener = networkListener;
    }

    public void setOrderListener(OrderListener orderListener) {
        this.orderListener = orderListener;
    }


    public void closedDeliveryRequestDialog() {
        if (deliveryRequest != null && deliveryRequest.isShowing()) {
            deliveryRequest.dismiss();
        }

    }

    private void openDeliveryRequestDialog(Intent intent) {
        if (!this.isFinishing() && !TextUtils.isEmpty(preferenceHelper.getSessionToken())) {
            if(deliveryRequest != null && deliveryRequest.isShowing()){
                return;
            }
            Bundle bundle = intent.getBundleExtra(Const.Params.NEW_ORDER);
            if (deliveryRequest != null && deliveryRequest.isShowing()) {
                deliveryRequest.notifyDataSetChange(bundle.getString(Const
                        .Params.PUSH_DATA1));
                return;
            }
            if (Integer.valueOf(bundle.getString(Const.Params.PUSH_DATA2)) > 0) {
                deliveryRequest = new CustomDialogDeliveryRequest
                        (this, bundle.getString(Const
                                .Params.PUSH_DATA1)
                                , bundle.getString(Const.Params.PUSH_DATA2));

                deliveryRequest.show();
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                if(notificationManager !=null) {
                    notificationManager.cancelAll();
                }
            }
        }
    }


    public String getAppVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.handleException(BaseAppCompatActivity.class.getName(), e);
        }
        return null;
    }

    public int checkWitchOtpValidationON() {
        if (checkEmailVerification() && checkPhoneNumberVerification()) {

            return Const.SMS_AND_EMAIL_VERIFICATION_ON;

        } else if (checkPhoneNumberVerification()) {
            return Const.SMS_VERIFICATION_ON;
        } else if (checkEmailVerification()) {
            return Const.EMAIL_VERIFICATION_ON;
        }
        return 0;

    }

    private boolean checkEmailVerification() {
        return preferenceHelper.getIsMailVerification() && !preferenceHelper.getIsEmailVerified();
    }

    private boolean checkPhoneNumberVerification() {
        return preferenceHelper.getIsSmsVerification() && !preferenceHelper
                .getIsPhoneNumberVerified();

    }

    /**
     * this method used to send email to admin email id
     */
    public void contactUsWithAdmin() {
        Uri gmmIntentUri = Uri.parse("mailto:" + preferenceHelper
                .getAdminContactEmail() +
                "?subject=" + "Request to Admin " +
                "&body=" + "Hello sir");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.gm");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_google_mail_app_not_installed), this);
        }
    }

    /**
     * this method call webservice for logout from app
     */
    public void logOut() {

        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, this
                    .preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, this
                    .preferenceHelper.getSessionToken());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.USER_FRAGMENT, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.logOut(ApiClient.makeJSONRequestBody
                (jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        preferenceHelper.logout();
                        goToLoginActivity();

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                BaseAppCompatActivity.this);
                    }
                }


            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(BaseAppCompatActivity.class.getName(), t);
            }
        });
    }

    public void restartApp() {
        startActivity(new Intent(this, SplashScreenActivity.class));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageHelper.wrapper(newBase, PreferenceHelper.getInstance
                (newBase)
                .getLanguageCode()));
    }

    public interface OrderListener {
        void onOrderReceive();

    }


    public interface NetworkListener {
        void onNetworkChange(boolean isEnable);

    }


    public class AppReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(final Context context, final Intent intent) {

            if (intent != null) {
                switch (intent.getAction()) {
                    case Const.Action.NETWORK_ACTION:
                        if (networkListener != null) {
                            networkListener.onNetworkChange(Utils.isInternetConnected(context));
                        }
                        break;
                    case Const.Action.ACTION_ADMIN_APPROVED:
                        preferenceHelper.putIsApproved(true);
                        goToHomeActivity();
                        break;
                    case Const.Action.ACTION_ADMIN_DECLINE:
                        preferenceHelper.putIsApproved(false);
                        goToHomeActivity();
                        break;
                    case Const.Action.ACTION_NEW_ORDER:
                        openDeliveryRequestDialog(intent);
                        break;
                    case Const.Action.ACTION_STORE_CANCELED_REQUEST:
                        closedDeliveryRequestDialog();
                        if (orderListener != null) {
                            orderListener.onOrderReceive();
                        }
                        break;
                    case Const.Action.ACTION_ORDER_STATUS:
                        if (orderListener != null) {
                            orderListener.onOrderReceive();
                        }
                        break;

                }
            }


        }


    }
    public void showSettingsGPSAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.show();
    }
}

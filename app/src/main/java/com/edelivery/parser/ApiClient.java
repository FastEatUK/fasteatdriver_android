package com.edelivery.parser;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.edelivery.BuildConfig;
import com.edelivery.R;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.ImageHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.edelivery.BuildConfig.BASE_URL;

public class ApiClient {

    public static final String Tag = "ApiClient";
    private static final int CONNECTION_TIMEOUT = 30; //seconds
    private static final int READ_TIMEOUT = 20; //seconds
    private static final int WRITE_TIMEOUT = 20; //seconds
    private static MediaType MEDIA_TYPE_TEXT = MediaType.parse("text/plain");
    private static MediaType MEDIA_TYPE_IMAGE = MediaType.parse("image/*");
    private static Retrofit retrofit = null;
    private static Gson gson;

    public static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient okHttpClient;
            if (BuildConfig.DEBUG) {
                // development build
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                okHttpClient = new OkHttpClient().newBuilder().connectTimeout
                        (CONNECTION_TIMEOUT,
                                TimeUnit.SECONDS)
                        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS).writeTimeout(WRITE_TIMEOUT,
                                TimeUnit.SECONDS).addInterceptor(interceptor).build();
            } else {
                // production build
                okHttpClient = new OkHttpClient().newBuilder().connectTimeout
                        (CONNECTION_TIMEOUT,
                                TimeUnit.SECONDS)
                        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS).writeTimeout(WRITE_TIMEOUT,
                                TimeUnit.SECONDS).build();
            }
            retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
        }
        return retrofit;
    }

    @NonNull
    public static MultipartBody.Part makeMultipartRequestBody(Context context, Uri imageUri,
                                                              String photoPath, String
                                                                      partName) {
        Log.i("IMAGE_PATH", getRealPathFromURI(imageUri, context));
        String path = getRealPathFromURI(imageUri, context);
        File file;
        try {
            file = new File(TextUtils.isEmpty(path) ? photoPath : path);
        } catch (NullPointerException e) {
            file = ImageHelper.getFromMediaUriPfd(context, context.getContentResolver(), imageUri);
        }
        RequestBody requestFile = RequestBody.create(MEDIA_TYPE_IMAGE, file);
        return MultipartBody.Part.createFormData(partName, context.getResources().getString(R.string
                        .app_name),
                requestFile);

    }

    @NonNull
    public static MultipartBody.Part makeMultipartRequestBodySocial(Context context, File file,
                                                                    String partName) {
        RequestBody requestFile = RequestBody.create(MEDIA_TYPE_IMAGE, file);
        return MultipartBody.Part.createFormData(partName, context.getResources().getString(R.string
                        .app_name),
                requestFile);

    }

    @NonNull
    public static RequestBody makeTextRequestBody(Object stringData) {
        return RequestBody.create(MEDIA_TYPE_TEXT, String.valueOf(stringData));
    }

    @NonNull
    public static RequestBody makeGSONRequestBody(Object jsonObject) {
        if (gson == null) {
            gson = new Gson();
        }
        return RequestBody.create(MEDIA_TYPE_TEXT, gson.toJson(jsonObject));
    }

    @NonNull
    public static String JSONResponse(Object jsonObject) {
        if (gson == null) {
            gson = new Gson();
        }
        String params = gson.toJson(jsonObject);
        return params;
    }

    @NonNull
    public static RequestBody makeJSONRequestBody(JSONObject jsonObject) {
        String params = jsonObject.toString();
        return RequestBody.create(MEDIA_TYPE_TEXT, params);
    }

    public static JSONObject JSONObject(Object jsonObject) {
        if (gson == null) {
            gson = new Gson();
        }
        try {
            return new JSONObject(String.valueOf(gson.toJsonTree(jsonObject)
                    .getAsJsonObject()));
        } catch (JSONException e) {
            AppLog.handleException(Tag, e);
        }
        return null;
    }

    private static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            try {
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
            } catch (Exception e) {

                AppLog.handleException(Tag, e);

                result = "";
            }
            cursor.close();
        }
        return result;
    }

    public Retrofit changeApiBaseUrl(String newApiBaseUrl) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().connectTimeout
                (CONNECTION_TIMEOUT,
                        TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS).writeTimeout(WRITE_TIMEOUT,
                        TimeUnit.SECONDS).build();

        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(newApiBaseUrl).build();
    }

}
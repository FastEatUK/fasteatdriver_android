package com.edelivery.parser;


import com.edelivery.models.responsemodels.AllDocumentsResponse;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.models.responsemodels.AvailableOrdersResponse;
import com.edelivery.models.responsemodels.BankDetailResponse;
import com.edelivery.models.responsemodels.CardsResponse;
import com.edelivery.models.responsemodels.CityResponse;
import com.edelivery.models.responsemodels.CompleteOrderResponse;
import com.edelivery.models.responsemodels.CountriesResponse;
import com.edelivery.models.responsemodels.DayEarningResponse;
import com.edelivery.models.responsemodels.DocumentResponse;
import com.edelivery.models.responsemodels.InvoiceResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OrderHistoryDetailResponse;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.models.responsemodels.OrderStatusResponse;
import com.edelivery.models.responsemodels.OtpResponse;
import com.edelivery.models.responsemodels.PaymentGatewayResponse;
import com.edelivery.models.responsemodels.ProviderDataResponse;
import com.edelivery.models.responsemodels.VehicleAddResponse;
import com.edelivery.models.responsemodels.VehicleDetailResponse;
import com.edelivery.models.responsemodels.WalletHistoryResponse;
import com.edelivery.models.responsemodels.WalletResponse;
import com.edelivery.models.responsemodels.WalletTransactionResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {


    @Multipart
    @POST("api/provider/register")
    Call<ProviderDataResponse> register(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/login")
    Call<ProviderDataResponse> login(@Body RequestBody requestBody);

    @GET("api/admin/get_country_list")
    Call<CountriesResponse> getCountries();

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_city_list")
    Call<CityResponse> getCities(@Body RequestBody requestBody);

    @Multipart
    @POST("api/provider/update")
    Call<ProviderDataResponse> updateProfile(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/check_app_keys")
    Call<AppSettingDetailResponse>getAppSettingDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/forgot_password")
    Call<IsSuccessResponse> forgotPassword(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/update_device_token")
    Call<IsSuccessResponse> updateDeviceToken(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/logout")
    Call<IsSuccessResponse> logOut(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/otp_verification")
    Call<OtpResponse> getOtpVerify(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/check_referral")
    Call<IsSuccessResponse> getCheckReferral(@Body RequestBody requestBody);

    @GET("api/directions/json")
    Call<ResponseBody> getGoogleDirection(@Query("origin") String originLatLng, @Query
            ("destination") String destinationLatLng, @Query("key") String googleKey);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/update_location")
    Call<IsSuccessResponse> updateProviderLocation(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/change_status")
    Call<IsSuccessResponse> changeProviderOnlineStatus(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_requests")
    Call<AvailableOrdersResponse> getNewOrder(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_active_requests")
    Call<AvailableOrdersResponse> getActiveOrders(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/provider_cancel_or_reject_request")
    Call<IsSuccessResponse> rejectOrCancelDelivery(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/change_request_status")
    Call<OrderStatusResponse> setRequestStatus(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_request_status")
    Call<OrderStatusResponse> getRequestStatus(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/complete_request")
    Call<CompleteOrderResponse> completeOrder(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_document_list")
    Call<AllDocumentsResponse> getAllDocument(@Body RequestBody requestBody);


    @Multipart
    @POST("api/admin/upload_document")
    Call<DocumentResponse> uploadDocument(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/request_history_detail")
    Call<OrderHistoryDetailResponse> getOrderHistoryDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/request_history")
    Call<OrderHistoryResponse> getOrdersHistory(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_bank_detail")
    Call<BankDetailResponse> getBankDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_order_count")
    Call<IsSuccessResponse> getAvailableDeliveryCount(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_detail")
    Call<ProviderDataResponse> getProviderDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/otp_verification")
    Call<IsSuccessResponse> setOtpVerification(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/daily_earning")
    Call<DayEarningResponse> getDailyEarning(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/weekly_earning")
    Call<DayEarningResponse> getWeeklyEarning(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/show_request_invoice")
    Call<IsSuccessResponse> setShowInvoice(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_invoice")
    Call<InvoiceResponse> getInvoice(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/rating_to_store")
    Call<IsSuccessResponse> setFeedbackStore(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/rating_to_user")
    Call<IsSuccessResponse> setFeedbackUser(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/add_bank_detail")
    Call<IsSuccessResponse> getAddBankDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/update_bank_detail")
    Call<IsSuccessResponse> updateBankDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/delete_bank_detail")
    Call<BankDetailResponse> deleteBankDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/select_bank_detail")
    Call<IsSuccessResponse> selectBankDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/create_wallet_request")
    Call<IsSuccessResponse> createWithdrawalRequest(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("admin/cancel_wallet_request")
    Call<IsSuccessResponse> cancelWithdrawalRequest(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_wallet_history")
    Call<WalletHistoryResponse> getWalletHistory(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_wallet_request_list")
    Call<WalletTransactionResponse> getWalletTransaction(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_payment_gateway")
    Call<PaymentGatewayResponse> getPaymentGateway(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/add_card")
    Call<CardsResponse> getAddCreditCard(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/select_card")
    Call<CardsResponse> selectCreditCard(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/delete_card")
    Call<IsSuccessResponse> deleteCreditCard(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_card_list")
    Call<CardsResponse> getAllCreditCards(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/add_wallet_amount")
    Call<WalletResponse> getAddWalletAmount(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/add_vehicle")
    Call<VehicleAddResponse> addVehicle(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/update_vehicle_detail")
    Call<VehicleAddResponse> updateVehicle(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_vehicle_list")
    Call<VehicleDetailResponse> getVehicleDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/select_vehicle")
    Call<IsSuccessResponse> selectVehicle(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/get_request_count")
    Call<IsSuccessResponse> getRequestCount(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider/send_extra_time_push_to_user")
    Call<IsSuccessResponse> sendExtraTimePushToUser(@Body RequestBody requestBody);



}
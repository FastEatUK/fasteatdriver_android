package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomPhotoDialog;
import com.edelivery.models.responsemodels.OtpResponse;
import com.edelivery.models.responsemodels.ProviderDataResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.ImageHelper;
import com.edelivery.utils.Utils;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.ImageHelper.CHOOSE_PHOTO_FROM_GALLERY;
import static com.edelivery.utils.ImageHelper.TAKE_PHOTO_FROM_CAMERA;

public class ProfileActivity extends BaseAppCompatActivity {


    private ImageView ivProfileImage;
    private CustomFontEditTextView etProfileLastName, etProfileFirstName, etProfileEmail,
            etProfileAddress, etProfileMobileNumber,
            etProfileCountryCode, etProfileZipCode, etNewPassword, etConfirmPassword;
    private FrameLayout ivProfileImageSelect;
    private Uri picUri;
    private ImageHelper imageHelper;
    private CustomFontTextView tvChangePassword;
    private TextInputLayout tilProfileAddress, tilProfileZipCode;
    private CustomDialogAlert closedPermissionDialog;
    private LinearLayout llChangePassword;
    private ScrollView scrollView;
    private CustomDialogVerification customDialogVerification;
    private String currentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_profile));
        setToolbarRightIcon(R.drawable.ic_edit_white_20dp, this);
        findViewById();
        setViewListener();
        setProfileData();
        imageHelper = new ImageHelper(this);
        setDataEnable(false);
        updateUiForOptionalFiled(preferenceHelper.getIsShowOptionalFieldInRegister());
        setContactNoLength(preferenceHelper.getMaxPhoneNumberLength());
        updateUiForSocial();
    }

    @Override
    protected boolean isValidate() {
        String msg = null;

        if (TextUtils.isEmpty(etProfileFirstName.getText().toString().trim())) {
            msg = getResources().getString(R.string.msg_please_enter_valid_name);
            etProfileFirstName.setError(msg);
            etProfileFirstName.requestFocus();
        } else if (TextUtils.isEmpty(etProfileLastName.getText().toString().trim())) {
            msg = getResources().getString(R.string.msg_please_enter_valid_name);
            etProfileLastName.setError(msg);
            etProfileLastName.requestFocus();
        } else if (TextUtils.isEmpty(etProfileMobileNumber.getText().toString().trim())) {
            msg = getResources().getString(R.string.msg_please_enter_valid_mobile_number);
            etProfileMobileNumber.setError(msg);
            etProfileMobileNumber.requestFocus();
        } else if (etProfileMobileNumber.getText().toString().trim().length()
                > preferenceHelper.getMaxPhoneNumberLength() || etProfileMobileNumber.getText()
                .toString().trim()
                .length
                        () < preferenceHelper.getMinPhoneNumberLength()) {
            msg = getResources().getString(R.string.msg_please_enter_valid_mobile_number) + " " +
                    "" + preferenceHelper.getMinPhoneNumberLength() + getResources().getString(R
                    .string
                    .text_or)
                    + preferenceHelper.getMaxPhoneNumberLength() + " " + getResources().getString
                    (R.string
                            .text_digits);
            etProfileMobileNumber.setError(msg);
            etProfileMobileNumber.requestFocus();
        } else if (tvChangePassword.getVisibility() == View.VISIBLE && !TextUtils.isEmpty
                (etNewPassword.getText()
                        .toString().trim())
                && etNewPassword
                .getText().toString().trim().length() < 6) {
            msg = getString(R.string.msg_enter_valid_password);
            etNewPassword.setError(msg);
            etNewPassword.requestFocus();
        } else if (tvChangePassword.getVisibility() == View.VISIBLE && !etNewPassword.getText()
                .toString().trim().equalsIgnoreCase(etConfirmPassword
                        .getText().toString().trim())) {
            msg = getString(R.string.msg_incorrect_confirm_password);
            etConfirmPassword.setError(msg);
            etConfirmPassword.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etProfileEmail.getText().toString().trim())
                .matches
                        ()) {
            msg = getResources().getString(R.string.msg_please_enter_valid_email);
            etProfileEmail.setError(msg);
            etProfileEmail.requestFocus();
        }

        return TextUtils.isEmpty(msg);
    }

    @Override
    protected void findViewById() {
        // do something
        etProfileFirstName = (CustomFontEditTextView) findViewById(R.id.etProfileFirstName);
        etProfileLastName = (CustomFontEditTextView) findViewById(R.id.etProfileLastName);
        etProfileEmail = (CustomFontEditTextView) findViewById(R.id.etProfileEmail);
        etProfileAddress = (CustomFontEditTextView) findViewById(R.id.etProfileAddress);
        etProfileMobileNumber = (CustomFontEditTextView) findViewById(R.id.etProfileMobileNumber);
        etProfileCountryCode = (CustomFontEditTextView) findViewById(R.id.etProfileCountryCode);
        etProfileZipCode = (CustomFontEditTextView) findViewById(R.id.etProfileZipCode);
        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);
        ivProfileImageSelect = (FrameLayout) findViewById(R.id.ivProfileImageSelect);
        etNewPassword = (CustomFontEditTextView) findViewById(R.id.etNewPassword);
        etConfirmPassword = (CustomFontEditTextView) findViewById(R.id.etConfirmPassword);
        tilProfileZipCode = (TextInputLayout) findViewById(R.id.tilProfileZipCode);
        tilProfileAddress = (TextInputLayout) findViewById(R.id.tilProfileAddress);
        llChangePassword = (LinearLayout) findViewById(R.id.llChangePassword);
        tvChangePassword = (CustomFontTextView) findViewById(R.id.tvChangePassword);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

    }

    private void setDataEnable(boolean isEnable) {

        etProfileFirstName.setEnabled(isEnable);
        etProfileMobileNumber.setEnabled(isEnable);
        etProfileLastName.setEnabled(isEnable);
        etProfileAddress.setEnabled(isEnable);
        etProfileCountryCode.setEnabled(false);
        etProfileZipCode.setEnabled(isEnable);
        etNewPassword.setEnabled(isEnable);
        etConfirmPassword.setEnabled(isEnable);
        tvChangePassword.setEnabled(isEnable);

        etProfileFirstName.setFocusableInTouchMode(isEnable);
        etProfileMobileNumber.setFocusableInTouchMode(isEnable);
        etProfileLastName.setFocusableInTouchMode(isEnable);
        etProfileAddress.setFocusableInTouchMode(isEnable);
        etProfileCountryCode.setFocusableInTouchMode(isEnable);
        etProfileZipCode.setFocusableInTouchMode(isEnable);
        etNewPassword.setFocusableInTouchMode(isEnable);
        etConfirmPassword.setFocusableInTouchMode(isEnable);
        tvChangePassword.setFocusableInTouchMode(isEnable);

        if (TextUtils.isEmpty(preferenceHelper.getSocialId())) {
            etProfileEmail.setEnabled(isEnable);
            etProfileEmail.setFocusableInTouchMode(isEnable);
        } else {
            etProfileEmail.setEnabled(false);
            etProfileEmail.setFocusableInTouchMode(false);
        }
        if (isEnable) {
            ivProfileImageSelect.setOnClickListener(this);
        } else {
            ivProfileImageSelect.setOnClickListener(null);
        }
    }

    private void setContactNoLength(int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        etProfileMobileNumber.setFilters(FilterArray);
    }

    private void setProfileData() {
        etProfileFirstName.setText(preferenceHelper.getFirstName());
        etProfileMobileNumber.setText(preferenceHelper.getPhoneNumber());
        etProfileLastName.setText(preferenceHelper.getLastName());
        etProfileAddress.setText(preferenceHelper.getAddress());
        etProfileCountryCode.setText(preferenceHelper.getPhoneCountyCodeCode());
        etProfileZipCode.setText(preferenceHelper.getZipCode());
        etProfileEmail.setText(preferenceHelper.getEmail());
        Glide.with(this).load(preferenceHelper.getProfilePic())
                .dontAnimate().placeholder(ResourcesCompat.getDrawable(this
                .getResources(), R.drawable.man_user, null)).fallback
                (ResourcesCompat.getDrawable
                        (getResources(), R
                                .drawable.man_user, null)).into
                (ivProfileImage);
    }

    @Override
    protected void setViewListener() {
        // do something
        tvChangePassword.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        // do something
        Utils.hideSoftKeyboard(this);
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do something
        switch (view.getId()) {
            case R.id.ivProfileImageSelect:
                checkPermission();
                break;
            case R.id.ivToolbarRightIcon:
                editProfile();
                break;
            case R.id.tvChangePassword:
                if (llChangePassword.getVisibility() == View.VISIBLE) {
                    llChangePassword.setVisibility(View.GONE);
                } else {
                    llChangePassword.setVisibility(View.VISIBLE);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                }
                break;
            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void openPhotoSelectDialog() {
        //Do the stuff that requires permission...
        CustomPhotoDialog customPhotoDialog = new CustomPhotoDialog(this, getResources()
                .getString(R.string.text_set_profile_photos)) {
            @Override
            public void clickedOnCamera() {
                takePhotoFromCamera();
                dismiss();
            }

            @Override
            public void clickedOnGallery() {
                choosePhotoFromGallery();
                dismiss();
            }
        };
        customPhotoDialog.show();

    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = imageHelper.createImageFile();
        currentPhotoPath = file.getAbsolutePath();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           // picUri = FileProvider.getUriForFile(this, getPackageName(), file);
            picUri = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), file);

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, TAKE_PHOTO_FROM_CAMERA);
    }

    private void choosePhotoFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, CHOOSE_PHOTO_FROM_GALLERY);
    }

    /**
     * This method is used for crop the placeholder which selected or captured
     */
    public void beginCrop(Uri sourceUri) {

        Uri outputUri = Uri.fromFile(imageHelper.createImageFile());
        Crop.of(sourceUri, outputUri).asSquare().start(this);
    }

    /**
     * This method is used for handel result after select placeholder from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            beginCrop(picUri);
        }
    }

    /**
     * This method is used for handel result after captured placeholder from camera .
     */
    private void onCaptureImageResult() {
        beginCrop(picUri);

    }

    /**
     * This method is used for  handel crop result after crop the placeholder.
     */
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            picUri = Crop.getOutput(result);
            AppLog.Log("PIC", picUri.getPath());
            Glide.with(this).load(picUri).transition(new DrawableTransitionOptions().crossFade())
                    .into(ivProfileImage);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.showToast(Crop.getError(result).getMessage(), this);
        }
    }

    private void goWithCameraAndStoragePermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.CAMERA)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.READ_EXTERNAL_STORAGE)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        } else {
            //
            openPhotoSelectDialog();
        }
    }

    private void openCameraPermissionDialog() {
        if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
            return;
        }
        closedPermissionDialog = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{android
                        .Manifest
                        .permission
                        .CAMERA, android.Manifest
                        .permission
                        .READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

        };
        closedPermissionDialog.show();
    }

    private void closedPermissionDialog() {
        if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
            closedPermissionDialog.dismiss();
            closedPermissionDialog = null;

        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission
                (this, android.Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        } else {
            //
            openPhotoSelectDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    //Do som thing
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppLog.Log(Const.Tag.PROFILE_ACTIVITY, requestCode + "");
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TAKE_PHOTO_FROM_CAMERA:

                    onCaptureImageResult();

                    break;
                case CHOOSE_PHOTO_FROM_GALLERY:
                    onSelectFromGalleryResult(data);
                    break;
                case Crop.REQUEST_CROP:
                    handleCrop(resultCode, data);
                    break;
                default:
                    //Do something
                    break;

            }
        }


    }

    /**
     * this method call a webservice for updateProfile of user
     *
     * @param currentPassword set current password in string
     */
    private void updateProfile(String currentPassword) {

        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put(Const.Params.SERVER_TOKEN, ApiClient.makeTextRequestBody
                (preferenceHelper.getSessionToken()));
        hashMap.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper
                .getProviderId()));
        hashMap.put(Const.Params.FIRST_NAME, ApiClient.makeTextRequestBody
                (etProfileFirstName.getText().toString()));
        hashMap.put(Const.Params.LAST_NAME, ApiClient.makeTextRequestBody
                (etProfileLastName.getText().toString()));
        hashMap.put(Const.Params.PHONE, ApiClient.makeTextRequestBody
                (etProfileMobileNumber.getText().toString()));

        hashMap.put(Const.Params.ADDRESS, ApiClient.makeTextRequestBody
                (etProfileAddress.getText().toString()));
        hashMap.put(Const.Params.ZIP_CODE, ApiClient.makeTextRequestBody
                (etProfileZipCode.getText().toString()));

        hashMap.put(Const.Params.EMAIL, ApiClient.makeTextRequestBody
                (etProfileEmail.getText().toString()));

        hashMap.put(Const.Params.IS_PHONE_NUMBER_VERIFIED, ApiClient.makeTextRequestBody
                (preferenceHelper.getIsPhoneNumberVerified()));
        hashMap.put(Const.Params.IS_EMAIL_VERIFIED, ApiClient.makeTextRequestBody
                (preferenceHelper.getIsEmailVerified()));
        if (TextUtils.isEmpty(preferenceHelper.getSocialId())) {
            hashMap.put(Const.Params.OLD_PASS_WORD, ApiClient.makeTextRequestBody(currentPassword));
            hashMap.put(Const.Params.NEW_PASS_WORD, ApiClient.makeTextRequestBody(etNewPassword
                    .getText().toString()));
            hashMap.put(Const.Params.SOCIAL_ID, ApiClient.makeTextRequestBody(""));
            hashMap.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody
                    (Const.MANUAL));

        } else {
            hashMap.put(Const.Params.OLD_PASS_WORD, ApiClient.makeTextRequestBody(""));
            hashMap.put(Const.Params.NEW_PASS_WORD, ApiClient.makeTextRequestBody(""));
            hashMap.put(Const.Params.SOCIAL_ID, ApiClient.makeTextRequestBody(preferenceHelper
                    .getSocialId()));
            hashMap.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody
                    (Const.SOCIAL));
        }

        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProviderDataResponse> responseCall;
        if (picUri == null) {
            responseCall = apiInterface.updateProfile(null, hashMap);
        } else {
            responseCall = apiInterface.updateProfile(ApiClient
                    .makeMultipartRequestBody
                            (this, picUri, currentPhotoPath, Const.Params
                                    .IMAGE_URL), hashMap);
        }
        responseCall.enqueue(new Callback<ProviderDataResponse>() {
            @Override
            public void onResponse(Call<ProviderDataResponse> call, Response<ProviderDataResponse>
                    response) {

                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.parseUserStorageData(response)) {
                    Utils.showMessageToast(response.body().getMessage(), ProfileActivity.this);
                    goToHomeActivity();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), ProfileActivity.this);
                }


            }

            @Override
            public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });


    }


    private void openVerifyAccountDialog() {
        if (TextUtils.isEmpty(preferenceHelper.getSocialId())) {
            if (customDialogVerification != null && customDialogVerification.isShowing()) {
                return;
            }
            customDialogVerification = new CustomDialogVerification(this,
                    getResources()
                            .getString(R.string
                                    .text_verify_account), getResources()
                    .getString(R.string.msg_enter_password_which_used_in_register),
                    getResources().getString(R.string.text_cancel), getResources()
                    .getString(R.string.text_ok), "", getResources()
                    .getString(R.string
                            .text_password), false, InputType.TYPE_CLASS_TEXT, InputType
                    .TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT) {
                @Override
                public void onClickLeftButton() {
                    dismiss();

                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {
                    if (!etDialogEditTextTwo.getText().toString().isEmpty()) {
                        dismiss();
                        updateProfile(etDialogEditTextTwo.getText().toString());

                    } else {
                        etDialogEditTextTwo.setError(getString(R.string.msg_enter_password));
                    }
                }

            };
            customDialogVerification.show();
        } else {
            updateProfile("");
        }


    }

    /**
     * this method mange edit icon toggle
     */
    private void editProfile() {
        if (!etProfileFirstName.isEnabled()) {
            setDataEnable(true);
            setToolbarRightIcon(R.drawable.ic_check_black_24dp, this);
        } else {
            if (isValidate()) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.ID, preferenceHelper.getProviderId());
                    jsonObject.put(Const.Params.TYPE, String.valueOf(Const.TYPE_PROVIDER));

                    switch (checkProfileWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            jsonObject.put(Const.Params.EMAIL, etProfileEmail.getText().toString());
                            jsonObject.put(Const.Params.PHONE, etProfileMobileNumber.getText()
                                    .toString());
                            jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, etProfileCountryCode
                                    .getText()
                                    .toString());
                            getOtpVerify(jsonObject);
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            jsonObject.put(Const.Params.PHONE, etProfileMobileNumber.getText()
                                    .toString());
                            jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, etProfileCountryCode
                                    .getText()
                                    .toString());
                            getOtpVerify(jsonObject);
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            jsonObject.put(Const.Params.EMAIL, etProfileEmail.getText().toString());
                            getOtpVerify(jsonObject);
                            break;
                        default:
                            openVerifyAccountDialog();
                            break;
                    }
                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
                }
            }
        }
    }

    /**
     * this method call a webservice for get OTP of email or mobile
     *
     * @param jsonObject
     */
    private void getOtpVerify(JSONObject jsonObject) {
        Utils.showCustomProgressDialog(this, false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OtpResponse> otpResponseCall = apiInterface.getOtpVerify(ApiClient
                .makeJSONRequestBody(jsonObject));
        otpResponseCall.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        AppLog.Log("SMS_OTP", response.body().getOtpForSms());
                        AppLog.Log("EMAIL_OTP", response.body().getOtpForEmail());
                        switch (checkProfileWitchOtpValidationON()) {
                            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                                openOTPVerifyDialog(response.body().getOtpForEmail(), response
                                        .body().getOtpForSms(), getResources().getString(R
                                        .string.text_email_otp), getResources().getString(R.string
                                        .text_phone_otp), true);
                                break;
                            case Const.SMS_VERIFICATION_ON:
                                openOTPVerifyDialog(response.body().getOtpForEmail(), response
                                        .body().getOtpForSms(), "", getResources().getString(R
                                        .string
                                        .text_phone_otp), false);
                                break;
                            case Const.EMAIL_VERIFICATION_ON:
                                openOTPVerifyDialog(response.body().getOtpForEmail(), response
                                        .body().getOtpForSms(), "", getResources().getString(R
                                        .string
                                        .text_email_otp), false);
                                break;

                            default:
                                // do with default
                                break;
                        }

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), ProfileActivity.this);
                    }
                }




            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });

    }

    private void updateUiForOptionalFiled(boolean isUpdate) {
        if (isUpdate) {
            tilProfileAddress.setVisibility(View.VISIBLE);
            tilProfileZipCode.setVisibility(View.VISIBLE);
        } else {
            tilProfileAddress.setVisibility(View.GONE);
            tilProfileZipCode.setVisibility(View.GONE);
        }

    }

    private void openOTPVerifyDialog(final String otpEmailVerification, final String
            otpSmsVerification, String editTextOneHint, String ediTextTwoHint, boolean
                                             isEditTextOneVisible) {

        CustomDialogVerification customDialogVerification = new CustomDialogVerification
                (this,
                        getResources().getString(R.string.text_verify_detail), getResources()
                        .getString(R
                                .string.msg_verify_detail), getResources().getString(R.string
                        .text_cancel)
                        , getResources().getString(R.string.text_ok), editTextOneHint,
                        ediTextTwoHint,
                        isEditTextOneVisible,
                        InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_NUMBER) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                           CustomFontEditTextView etDialogEditTextTwo) {


                switch (checkWitchOtpValidationON()) {
                    case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                        if (TextUtils.equals(etDialogEditTextOne.getText().toString(),
                                otpEmailVerification)) {
                            if (TextUtils.equals(etDialogEditTextTwo
                                            .getText().toString(),
                                    otpSmsVerification)) {
                                preferenceHelper.putIsEmailVerified(true);
                                preferenceHelper.putIsPhoneNumberVerified(true);
                                dismiss();
                                openVerifyAccountDialog();
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_sms_otp_wrong));
                            }

                        } else {
                            etDialogEditTextOne.setError(getResources().getString(R.string
                                    .msg_email_otp_wrong));
                        }
                        break;
                    case Const.SMS_VERIFICATION_ON:
                        if (TextUtils.equals(etDialogEditTextTwo
                                        .getText().toString(),
                                otpSmsVerification)) {
                            preferenceHelper.putIsPhoneNumberVerified(true);
                            dismiss();
                            openVerifyAccountDialog();

                        } else {
                            etDialogEditTextTwo.setError(getResources().getString(R.string
                                    .msg_sms_otp_wrong));
                        }
                        break;
                    case Const.EMAIL_VERIFICATION_ON:
                        if (TextUtils.equals(etDialogEditTextTwo.getText().toString(),
                                otpEmailVerification)) {
                            preferenceHelper.putIsEmailVerified(true);
                            dismiss();
                            openVerifyAccountDialog();

                        } else {
                            etDialogEditTextTwo.setError(getResources().getString(R.string
                                    .msg_email_otp_wrong));
                        }
                        break;
                    default:
                        // do with default
                        break;
                }

            }
        };
        customDialogVerification.show();

    }

    /**
     * this method will manage which otp validation is on from admin panel
     *
     * @return get code according for validation
     */
    private int checkProfileWitchOtpValidationON() {
        if (checkEmailVerification() && checkPhoneNumberVerification()) {

            return Const.SMS_AND_EMAIL_VERIFICATION_ON;

        } else if (checkPhoneNumberVerification()) {
            return Const.SMS_VERIFICATION_ON;
        } else if (checkEmailVerification()) {
            return Const.EMAIL_VERIFICATION_ON;
        }
        return 0;

    }

    private boolean checkPhoneNumberVerification() {
        return preferenceHelper.getIsSmsVerification() && !TextUtils.equals(etProfileMobileNumber
                .getText().toString(), preferenceHelper.getPhoneNumber());

    }

    private boolean checkEmailVerification() {
        return preferenceHelper.getIsMailVerification() && !TextUtils.equals(etProfileEmail
                .getText().toString(), preferenceHelper.getEmail());

    }

    private void updateUiForSocial() {
        if (TextUtils.isEmpty(preferenceHelper.getSocialId())) {
            tvChangePassword.setVisibility(View.VISIBLE);
        } else {
            tvChangePassword.setVisibility(View.GONE);
        }
    }
}

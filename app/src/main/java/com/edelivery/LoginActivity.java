package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;


import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;

import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.LoginFragment;
import com.edelivery.fragments.RegisterFragment;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.os.Bundle;

public class LoginActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived {

    public LocationHelper locationHelper;
    public CallbackManager callbackManager;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private TwitterAuthClient twitterAuthClient;
    private RegisterFragment registerFragment;
    private LoginFragment loginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initTwitter();
        CurrentOrder.getInstance().clearCurrentOrder();
        findViewById();
        setViewListener();
        preferenceHelper.clearVerification();
        preferenceHelper.logout();
        locationHelper = new LocationHelper(getApplicationContext());
        locationHelper.setLocationReceivedLister(this);
        callbackManager = CallbackManager.Factory.create();
        twitterAuthClient = new TwitterAuthClient();



    }

    @Override
    protected boolean isValidate() {
        // do somethings

        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        tabLayout = (TabLayout) findViewById(R.id.loginTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.loginViewpager);

         initTabLayout(viewPager);
    }

    @Override
    protected void setViewListener() {
        // do somethings

    }

    @Override
    protected void onBackNavigation() {
        // do somethings

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.GOOGLE_SIGN_IN:
                // result from google
                getGoogleSignInResult(data);
                break;
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        twitterAuthClient.onActivityResult(requestCode, resultCode, data);


    }

    private void initTabLayout(ViewPager viewPager) {
    //unhide the all code if want to use register feture with view pager
        try {
            if (adapter == null) {
                adapter = new ViewPagerAdapter(getSupportFragmentManager());
                adapter.addFragment(new LoginFragment(), getString(R.string.text_login));
               // adapter.addFragment(new RegisterFragment(), getString(R.string.text_register));
                viewPager.setAdapter(adapter);
                tabLayout.setupWithViewPager(viewPager);
              /*  tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        int position = tab.getPosition();
                        // hide keyboard when scrol and click tab
                        Utils.hideSoftKeyboard(LoginActivity.this);
                        switch (position) {
                            case 0:
                                LoginFragment loginFragment = (LoginFragment) adapter.getItem(position);
                                loginFragment.clearError();
                                break;
                            case 1:
                                RegisterFragment registerFragment = (RegisterFragment) adapter
                                        .getItem(position);
                                registerFragment.clearError();
                                break;
                            default:
                                // do with default
                                break;
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });*/
            }
            loginFragment = (LoginFragment) adapter.getItem(0);
          //  registerFragment = (RegisterFragment) adapter.getItem(1);

        } catch (Exception e) {
            Log.e("initTabLayout() " , e.getMessage());
        }

    }

    @Override
    public void onClick(View view) {
        // do somethings

    }


    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
    }

    @Override
    public void onLocationChanged(Location location) {
        //Do som thing

    }

    @Override
    public void onConnected(Bundle bundle) {
        //Do som thing
        //initTabLayout(viewPager);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Do som thing

    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do som thing

    }

    private void initTwitter() {

        try {
            TwitterConfig config = new TwitterConfig.Builder(this)
                    .logger(new DefaultLogger(Log.DEBUG))
                    .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string
                            .TWITTER_CONSUMER_KEY),
                            getResources().getString(R.string.TWITTER_CONSUMER_SECRET)))
                    .debug(BuildConfig.DEBUG)
                    .build();
            Twitter.initialize(config);
        } catch (Exception e) {
            Log.e("initTwitter() " , e.getMessage());
        }

    }

    /**
     * Login with Twitter
     */
    public void loginWithTwitter() {

        try {
            TwitterCore.getInstance().getSessionManager().clearActiveSession();

            twitterAuthClient.authorize(LoginActivity.this, new com.twitter.sdk.android.core
                    .Callback<TwitterSession>
                    () {


                @Override
                public void success(final Result<TwitterSession> result1) {
                    AppLog.Log("TWITTER_LOGIN", "success");


                    Utils.showCustomProgressDialog(LoginActivity.this, false);
                    twitterAuthClient.requestEmail(result1.data, new com.twitter.sdk.android.core
                            .Callback<String>() {


                        @Override
                        public void success(Result<String> result2) {
                            Utils.hideCustomProgressDialog();
                            if (isLoginTabSelect()) {
                                loginFragment.login(String.valueOf(result1.data.getUserId()),result2.data );
                            } else {
                                registerFragment.updateUiForSocialLogin(result2.data, String
                                                .valueOf(result1.data
                                                        .getUserId
                                                                ()),
                                        result1.data.getUserName(), "", null);
                            }
                        }

                        @Override
                        public void failure(TwitterException exception) {
                            Utils.hideCustomProgressDialog();
                            AppLog.handleException("TWITTER_LOGIN", exception);
                        }
                    });
                }
                @Override
                public void failure(TwitterException exception) {
                    AppLog.handleException("TWITTER_LOGIN", exception);
                }
            });


        } catch (Exception e) {
            Log.e("loginWithTwitter() " , e.getMessage());
        }
    }

    /**
     * Login with Facebook
     */
    public void loginWithFacebook() {
        try {

            LoginManager.getInstance().logOut();
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            // App code

                            if (isLoginTabSelect()) {
                                try {
                                    GraphRequest data_request = GraphRequest.newMeRequest(
                                            loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                                @Override
                                                public void onCompleted(
                                                        JSONObject json_object,
                                                        GraphResponse response) {
                                                    try {
                                                        if (Profile.getCurrentProfile() != null) {
                                                            registerFragment.updateUiForSocialLogin(json_object.getString(Const
                                                                            .Facebook.EMAIL),
                                                                    Profile.getCurrentProfile().getId(), Profile.getCurrentProfile
                                                                            ().getFirstName(), Profile.getCurrentProfile()
                                                                            .getLastName(), ImageRequest.getProfilePictureUri
                                                                            (Profile.getCurrentProfile().getId(), 250,
                                                                                    250));
                                                        } else {
                                                            Log.e("aaa", "FB==" + Profile.getCurrentProfile());
                                                        }

                                                    } catch (JSONException e) {
                                                        AppLog.handleException(Const.Tag.REGISTER_FRAGMENT, e);
                                                    }

                                                }

                                            });
                                    Bundle permission_param = new Bundle();
                                    permission_param.putString("fields", "id,name,email,picture");
                                    data_request.setParameters(permission_param);
                                    data_request.executeAsync();
                                } catch (Exception e) {
                                    Log.e("getFacebookSignInResult() " , e.getMessage());
                                }
                                loginFragment.login(loginResult.getAccessToken().getUserId(), Const
                                        .Facebook.EMAIL);
                            } else {
                                getFacebookSignInResult(loginResult);
                            }
                        }

                        @Override
                        public void onCancel() {
                            // App code
                            AppLog.Log("FB_RESULT", "failed");
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            // App code
                            AppLog.Log("FB_RESULT", exception.getMessage());
                        }
                    });
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(Const.Facebook
                    .PUBLIC_PROFILE, Const.Facebook.EMAIL));

        } catch (Exception e) {
            Log.e("loginWithFacebook() " , e.getMessage());
        }
    }

    /**
     * Login with Google
     */
    public void loginWithGoogle() {
        try {

            Auth.GoogleSignInApi.revokeAccess(locationHelper.googleApiClient)
                    .setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    AppLog.Log("REVOKE_ACCESS_GOOGLE", status.toString());
                                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent
                                            (locationHelper
                                                    .googleApiClient);
                                    startActivityForResult(signInIntent, Const.GOOGLE_SIGN_IN);
                                }
                            });
        } catch (Exception e) {
            Log.e("loginWithGoogle() " , e.getMessage());
        }

    }

    private void getFacebookSignInResult(LoginResult loginResult) {
        try {
            GraphRequest data_request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject json_object,
                                GraphResponse response) {
                            try {
                                if (Profile.getCurrentProfile() != null) {
                                    registerFragment.updateUiForSocialLogin(json_object.getString(Const
                                                    .Facebook.EMAIL),
                                            Profile.getCurrentProfile().getId(), Profile.getCurrentProfile
                                                    ().getFirstName(), Profile.getCurrentProfile()
                                                    .getLastName(), ImageRequest.getProfilePictureUri
                                                    (Profile.getCurrentProfile().getId(), 250,
                                                            250));
                                } else {
                                    Log.e("aaa", "FB==" + Profile.getCurrentProfile());
                                }

                            } catch (JSONException e) {
                                AppLog.handleException(Const.Tag.REGISTER_FRAGMENT, e);
                            }

                        }

                    });
            Bundle permission_param = new Bundle();
            permission_param.putString("fields", "id,name,email,picture");
            data_request.setParameters(permission_param);
            data_request.executeAsync();
        } catch (Exception e) {
            Log.e("getFacebookSignInResult() " , e.getMessage());
        }

    }

    private void getGoogleSignInResult(Intent data) {
        try {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent
                    (data);
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();

                if (isLoginTabSelect())
                {
                    loginFragment.login(acct.getId(),acct.getEmail());
                } else {
                    if (acct != null) {
                        String firstName;
                        String lastName = "";
                        if (acct.getDisplayName().contains(" ")) {
                            String[] strings = acct.getDisplayName().split(" ");
                            firstName = strings[0];
                            lastName = strings[1];
                        } else {
                            firstName = acct.getDisplayName();
                        }

                        registerFragment.updateUiForSocialLogin(acct.getEmail(), acct.getId(),
                                firstName,
                                lastName, acct
                                        .getPhotoUrl());
                    }
                }


            } else {
                // Signed out, show unauthenticated UI.
                AppLog.Log("GOOGLE_RESULT", "failed");
            }
        } catch (Exception e) {
            Log.e("getGoogleSignInResult() " , e.getMessage());
        }

    }


    private boolean isLoginTabSelect() {

        return tabLayout.getSelectedTabPosition() == 0;
    }
}

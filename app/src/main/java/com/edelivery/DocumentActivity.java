package com.edelivery;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.edelivery.adapter.DocumentAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomPhotoDialog;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Documents;
import com.edelivery.models.responsemodels.AllDocumentsResponse;
import com.edelivery.models.responsemodels.DocumentResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.ImageHelper;
import com.edelivery.utils.Utils;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.BuildConfig.BASE_URL;
import static com.edelivery.utils.ImageHelper.CHOOSE_PHOTO_FROM_GALLERY;
import static com.edelivery.utils.ImageHelper.TAKE_PHOTO_FROM_CAMERA;

public class DocumentActivity extends BaseAppCompatActivity {

    private Dialog documentDialog;
    private Uri picUri;
    private ImageHelper imageHelper;
    private ImageView ivDocumentImage;
    private TextInputLayout tilExpireDate, tilNumberId;
    private CustomFontEditTextView etIdNumber, etExpireDate;
    private CustomFontTextView tvDocumentTitle;
    private RecyclerView rcvDocument;
    private CustomFontButton btnDocumentSubmit;
    private String expireDate;
    private List<Documents> documentList = new ArrayList<>();
    private CustomDialogAlert closedPermissionDialog;
    private boolean isApplicationStart;
    private DocumentAdapter documentAdapter;
    private String currentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_documents));
        findViewById();
        setViewListener();
        getAllDocument();
        imageHelper = new ImageHelper(this);
        documentList = new ArrayList<>();
        getExtraData();
    }

    @Override
    protected boolean isValidate() {
        //do something

        return false;
    }

    @Override
    protected void findViewById() {
        //do something
        rcvDocument = (RecyclerView) findViewById(R.id.rcvDocument);
        btnDocumentSubmit = (CustomFontButton) findViewById(R.id.btnDocumentSubmit);

    }

    @Override
    protected void setViewListener() {
        //do something
        btnDocumentSubmit.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        //do something
        Utils.hideSoftKeyboard(this);
        onBackPressed();

    }

    @Override
    public void onClick(View view) {
        //do something
        switch (view.getId()) {
            case R.id.btnDocumentSubmit:
                submitDocument();
                break;
            default:
                // do with default
                break;
        }
    }

    private void takePhotoFromCamera() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = imageHelper.createImageFile();
        currentPhotoPath = file.getAbsolutePath();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

           // picUri = FileProvider.getUriForFile(this, getPackageName(), file);
            picUri = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), file);

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, TAKE_PHOTO_FROM_CAMERA);
    }

    private void choosePhotoFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, CHOOSE_PHOTO_FROM_GALLERY);
    }

    /**
     * This method is used for crop the placeholder which selected or captured
     */
    public void beginCrop(Uri sourceUri) {

        Uri outputUri = Uri.fromFile(imageHelper.createImageFile());
        Crop.of(sourceUri, outputUri).asSquare().start(this);
    }

    /**
     * This method is used for handel result after select placeholder from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            setWithOutCropImage();
        }
    }


    private void setWithOutCropImage() {
        AppLog.Log("PIC", picUri.getPath());
        if (documentDialog != null && documentDialog.isShowing()) {
            Glide.with(this).load(picUri).transition(new DrawableTransitionOptions().crossFade())
                    .into(ivDocumentImage);

        }
    }

    /**
     * This method is used for  handel crop result after crop the placeholder.
     */
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            picUri = Crop.getOutput(result);
            AppLog.Log("PIC", picUri.getPath());
            if (documentDialog != null && documentDialog.isShowing()) {
                Glide.with(this).load(picUri).transition(new DrawableTransitionOptions().crossFade())
                        .transition(new DrawableTransitionOptions().crossFade())
                        .into(ivDocumentImage);
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.showToast(Crop.getError(result).getMessage(), this);
        }
    }

    /**
     * this method will make decision according to permission result
     *
     * @param grantResults set result from system or OS
     */
    private void goWithCameraAndStoragePermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.CAMERA)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.READ_EXTERNAL_STORAGE)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        } else {
            //
            openPhotoSelectDialog();
        }
    }

    private void openCameraPermissionDialog() {
        if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
            return;
        }
        closedPermissionDialog = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(DocumentActivity.this, new String[]{android
                        .Manifest
                        .permission
                        .CAMERA, android.Manifest
                        .permission
                        .READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

        };
        closedPermissionDialog.show();
    }

    private void closedPermissionDialog() {
        if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
            closedPermissionDialog.dismiss();
            closedPermissionDialog = null;

        }
    }

    public void openPhotoSelectDialog() {
        //Do the stuff that requires permission...
        CustomPhotoDialog customPhotoDialog = new CustomPhotoDialog(this, getResources()
                .getString(R.string.text_set_profile_photos)) {
            @Override
            public void clickedOnCamera() {
                takePhotoFromCamera();
                dismiss();
            }

            @Override
            public void clickedOnGallery() {
                choosePhotoFromGallery();
                dismiss();
            }
        };
        customPhotoDialog.show();

    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission
                (this, android.Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        } else {
            //
            openPhotoSelectDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    //Do som thing
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppLog.Log(Const.Tag.PROFILE_ACTIVITY, requestCode + "");
        switch (requestCode) {
            case TAKE_PHOTO_FROM_CAMERA:
                if (resultCode == RESULT_OK) {
                    picUri = imageHelper.checkExifRotation(picUri);
                    setWithOutCropImage();
                }
                break;
            case CHOOSE_PHOTO_FROM_GALLERY:
                onSelectFromGalleryResult(data);
                break;
            case Crop.REQUEST_CROP:
                handleCrop(resultCode, data);
                break;
            default:
                //Do something
                break;

        }


    }


    private void openDocumentUploadDialog(final int position) {


        if (documentDialog != null && documentDialog.isShowing()) {
            return;
        }

        final Documents document = documentList.get(position);

        documentDialog = new Dialog(this);
        documentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        documentDialog.setContentView(R.layout.dialog_document_upload);
        ivDocumentImage = (ImageView) documentDialog.findViewById(R.id.ivDialogDocumentImage);
        etIdNumber = (CustomFontEditTextView) documentDialog.findViewById(R.id
                .etIdNumber);
        etExpireDate = (CustomFontEditTextView) documentDialog.findViewById(R.id.etExpireDate);
        tilExpireDate = (TextInputLayout) documentDialog.findViewById(R.id.tilExpireDate);
        tilNumberId = (TextInputLayout) documentDialog.findViewById(R.id.tilNumberId);
        tvDocumentTitle = (CustomFontTextView) documentDialog.findViewById(
                R.id.tvDialogDocumentTitle);
        tvDocumentTitle.setText(document.getDocumentDetails().getDocumentName());
        Glide.with(this).load(BASE_URL + document.getImageUrl())
                .dontAnimate().placeholder
                (ResourcesCompat.getDrawable(this
                        .getResources(), R.drawable.uploading, null)).fallback(ResourcesCompat
                .getDrawable
                        (this.getResources(), R.drawable.uploading, null)).into
                (ivDocumentImage);
        expireDate = "";
        if (document.getDocumentDetails().isIsExpiredDate()) {
            tilExpireDate.setVisibility(View.VISIBLE);
            String date = "";
            try {
                if (!TextUtils.isEmpty(document.getExpiredDate())) {
                    expireDate = document.getExpiredDate();
                    date = parseContent.dateFormat.format(parseContent
                            .webFormat
                            .parse(document.getExpiredDate()));
                    etExpireDate.setText(date);
                } else {
                    etExpireDate.setText(date);
                }

            } catch (ParseException e) {
                AppLog.handleException(DocumentAdapter.class.getName(), e);
            }

        } else {
            tilExpireDate.setVisibility(View.GONE);
        }
        if (document.getDocumentDetails().isIsUniqueCode()) {
            tilNumberId.setVisibility(View.VISIBLE);
            etIdNumber.setText(document.getUniqueCode());
        } else {
            tilNumberId.setVisibility(View.GONE);
        }

        documentDialog.findViewById(R.id.btnDialogDocumentSubmit).setOnClickListener(new View
                .OnClickListener() {

            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(etExpireDate.getText().toString()) && document
                        .getDocumentDetails().isIsExpiredDate()) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_document_expire_date), DocumentActivity.this);

                } else if (TextUtils.isEmpty(etIdNumber.getText().toString().trim()) &&
                        document.getDocumentDetails().isIsUniqueCode()) {
                    etIdNumber.setError(getResources().getString(R.string
                            .msg_plz_enter_document_unique_code));
//                    Utils.showToast(getResources().getString(R.string
//                            .msg_plz_enter_document_unique_code), DocumentActivity.this);

                } else if (TextUtils.isEmpty(BASE_URL + document.getImageUrl())) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_select_document_image), DocumentActivity.this);
                } else {
                    documentDialog.dismiss();
                    documentUpload(position);
                }


            }
        });

        documentDialog.findViewById(R.id.btnDialogDocumentCancel).setOnClickListener(new View
                .OnClickListener() {


            @Override
            public void onClick(View view) {
                documentDialog.dismiss();
            }
        });

        ivDocumentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });
        etExpireDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePickerDialog();
            }
        });
        WindowManager.LayoutParams params = documentDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        documentDialog.getWindow().setAttributes(params);
        documentDialog.setCancelable(false);
        documentDialog.show();

    }

    private void openDatePickerDialog() {


        final Calendar calendar = Calendar.getInstance();
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDate = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog
                .OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        };
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                onDateSetListener, currentYear,
                currentMonth,
                currentDate);
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, this
                .getResources()
                .getString(R.string.text_select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (documentDialog != null && datePickerDialog.isShowing()) {

                    calendar.set(Calendar.YEAR, datePickerDialog.getDatePicker().getYear());
                    calendar.set(Calendar.MONTH, datePickerDialog.getDatePicker().getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePickerDialog.getDatePicker()
                            .getDayOfMonth());
                    etExpireDate.setText(parseContent.dateFormat.format(calendar.getTime()));
                    expireDate = parseContent.webFormat.format(calendar.getTime());
                }
            }
        });
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();

    }

    /**
     * this method call webservice for get all required document list
     */
    private void getAllDocument() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.DOCUMENT_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllDocumentsResponse> responseCall = apiInterface.getAllDocument(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<AllDocumentsResponse>() {
            @Override
            public void onResponse(Call<AllDocumentsResponse> call, Response<AllDocumentsResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        documentList.addAll(response.body().getDocuments());
                        preferenceHelper.putIsProviderAllDocumentsUpload(response.body()
                                .isDocumentUploaded());
                        initRcvDocument();
                        if (documentList.isEmpty()) {
                            preferenceHelper.putIsProviderAllDocumentsUpload(true);
                            submitDocument();
                        }
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), DocumentActivity.this);
                    }
                    btnDocumentSubmit.setVisibility(documentList.isEmpty() ? View.GONE : View
                            .VISIBLE);
                }


            @Override
            public void onFailure(Call<AllDocumentsResponse> call, Throwable t) {

            }
        });
    }

    private void initRcvDocument() {
        rcvDocument.setLayoutManager(new LinearLayoutManager(this));
        documentAdapter = new DocumentAdapter(documentList);
        rcvDocument.setAdapter(documentAdapter);
        rcvDocument.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration
                .VERTICAL));
        rcvDocument.addOnItemTouchListener(new RecyclerTouchListener(this, rcvDocument, new
                ClickListener() {

                    @Override
                    public void onClick(View view, int position) {
                        openDocumentUploadDialog(position);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    /**
     * this method call webservice for upload document
     *
     * @param position
     */

    private void documentUpload(final int position) {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put(Const.Params.DOCUMENT_ID, ApiClient.makeTextRequestBody
                (documentList.get(position).getId()));
        if (documentList.get(position).getDocumentDetails().isIsExpiredDate()) {
            hashMap.put(Const.Params.EXPIRED_DATE, ApiClient.makeTextRequestBody
                    (expireDate));
        }
        hashMap.put(Const.Params.ID, ApiClient.makeTextRequestBody
                (preferenceHelper.getProviderId()));
        hashMap.put(Const.Params.UNIQUE_CODE, ApiClient.makeTextRequestBody
                (etIdNumber.getText().toString()));
        hashMap.put(Const.Params.TYPE, ApiClient.makeTextRequestBody
                (Const.TYPE_PROVIDER));
        hashMap.put(Const.Params.SERVER_TOKEN, ApiClient.makeTextRequestBody
                (preferenceHelper.getSessionToken()));
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<DocumentResponse> responseCall;
        if (picUri == null) {
            responseCall = apiInterface.uploadDocument(null, hashMap);
        } else {
            responseCall = apiInterface.uploadDocument(ApiClient.makeMultipartRequestBody
                    (this, picUri, currentPhotoPath, Const.Params
                            .IMAGE_URL), hashMap);
        }
        responseCall.enqueue(new Callback<DocumentResponse>() {
            @Override
            public void onResponse(Call<DocumentResponse> call, Response<DocumentResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        picUri = null;
                        AppLog.Log("DOCUMENT", ApiClient.JSONResponse(response.body()));
                        Documents documents = documentList.get(position);
                        documents.setImageUrl(response.body().getImageUrl());
                        documents.setUniqueCode(response.body().getUniqueCode());
                        documents.setExpiredDate(response.body().getExpiredDate());
                        documentList.set(position, documents);
                        preferenceHelper.putIsProviderAllDocumentsUpload(response.body()
                                .isIsDocumentUploaded());
                        documentAdapter.notifyDataSetChanged();
                        Utils.showMessageToast(response.body().getMessage(), DocumentActivity.this);
                    } else {
                        picUri = null;
                        Utils.showErrorToast(response.body().getErrorCode(), DocumentActivity.this);
                    }
                }



            @Override
            public void onFailure(Call<DocumentResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                picUri = null;

            }
        });

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            isApplicationStart = getIntent().getExtras().getBoolean(Const.Tag.HOME_FRAGMENT);
        }

    }


    @Override
    public void onBackPressed() {

        if (isApplicationStart) {
            openLogoutDialog();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    private void openLogoutDialog() {
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, getResources()
                .getString(R.string.text_log_out), getResources()
                .getString(R.string.msg_are_you_sure), getResources()
                .getString(R.string.text_cancel), getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                logOut();
                dismiss();
            }
        };
        customDialogAlert.show();
    }

    private void submitDocument() {
        if (preferenceHelper.getIsProviderAllDocumentsUpload()) {
            goToHomeActivity();
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_plz_upload_document), this);
        }
    }

}

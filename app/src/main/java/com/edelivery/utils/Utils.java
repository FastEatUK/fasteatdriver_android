package com.edelivery.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.edelivery.LoginActivity;
import com.edelivery.R;
import com.edelivery.component.CustomCircularProgressView;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by elluminati on 02-Feb-2017.
 */
public class Utils {

    public static final String TAG = "Utils";
    private static Dialog dialog;
    private static CustomCircularProgressView ivProgressBar;

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static void showErrorToast(int code, Context context) {
        String msg;
        String errorCode = Const.ERROR_CODE_PREFIX + code;
        try {
            msg = context.getResources().getString(
                    context.getResources().getIdentifier(errorCode, Const.STRING,
                            context.getPackageName()));
            if (Const.INVALID_TOKEN == code || Const.PROVIDER_DATA_NOT_FOUND == code) {
                goToLoginActivity(context);
            }
        } catch (Resources.NotFoundException e) {
            msg = errorCode;
            AppLog.Log(TAG, msg);
            AppLog.handleException(TAG, e);
        }
        showToast(msg, context);


    }

    public static void showMessageToast(int code, Context context) {
        String msg;
        String messageCode = Const.MESSAGE_CODE_PREFIX + code;
        try {
            msg = context.getResources().getString(
                    context.getResources().getIdentifier(messageCode, Const.STRING,
                            context.getPackageName()));

        } catch (Resources.NotFoundException e) {
            msg = messageCode;
            AppLog.Log(TAG, msg);
            AppLog.handleException(TAG, e);
        }
        showToast(msg, context);
    }

    public static void showHttpErrorToast(int code, Context context) {
        String msg;
        String errorCode = Const.HTTP_ERROR_CODE_PREFIX + code;
        try {
            msg = context.getResources().getString(
                    context.getResources().getIdentifier(errorCode, Const.STRING,
                            context.getPackageName()));
            showToast(msg, context);
        } catch (Resources.NotFoundException e) {
            msg = errorCode;
            AppLog.Log(TAG, msg);
            AppLog.handleException(TAG, e);
        }


    }

    public static void showCustomProgressDialog(Context context, boolean isCancel) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        if (isInternetConnected(context) && !((AppCompatActivity) context).isFinishing()) {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.circuler_progerss_bar_two);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ivProgressBar = (CustomCircularProgressView) dialog.findViewById(R.id.ivProgressBarTwo);
            ivProgressBar.startAnimation();
            dialog.setCancelable(isCancel);
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setAttributes(params);
            dialog.getWindow().setDimAmount(0);
            dialog.show();
        }

    }

    public static boolean hasGpsHardware(Context context) {
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    public static void hideCustomProgressDialog() {
        try {
            if (dialog != null && ivProgressBar != null) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            AppLog.handleException(TAG, e);
        }
    }


    public static void  isHashkey(Context context) {

        PackageInfo info;
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key -->  ", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();

    }

    public static boolean isGpsEnable(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context
                .LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap
            // will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable
                    .getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return n + "th";
        }
        switch (n % 10) {
            case 1:
                return n + "st";
            case 2:
                return n + "nd";
            case 3:
                return n + "rd";
            default:
                return n + "th";
        }
    }

    private static void goToLoginActivity(Context context) {
        Intent loginIntent = new Intent(context, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PreferenceHelper.getInstance(context).logout();
        context.startActivity(loginIntent);
    }


    public static String    secondsToHoursMinutesSeconds(long seconds)
    {
     ///   return (seconds / 3600) + " hr" + " " + (seconds % 3600) / 60 + " min";

        int total_second = (int) seconds;
        Log.d("","======== Total Second INT ========> "+total_second);
        int hours = (int) Math.floor(total_second / 3600);
        int minute = (int) Math.floor((total_second % 3600) / 60);
        int seconds11 = (int) Math.abs(Math.floor((minute * 60) - total_second));
        String finalTime  = String.format("%02d:%02d", hours, minute);
        Log.d("response","======== Final Formate ========> "+finalTime);
        return finalTime;
    }

   /* public static String minuteToHoursMinutesSeconds(double minute) {
        Log.e("seconds", String.valueOf(minute));
        double seconds = (minute * 60);
        Log.e("seconds", String.valueOf(seconds));
        Log.e("seconds", "hour is "+ String.valueOf((seconds / 3600)));
        Log.e("seconds", "min is "+ String.valueOf(((seconds % 3600) / 60)));
        int hours = (int) seconds / 3600;

        String myStrMinutes = String.valueOf(minute).substring(String.valueOf(minute).indexOf(".") + 1);
        String finalTime = String.format("%02d", hours) + ":" + myStrMinutes;
        Log.e("seconds", "hh:mm is "+ finalTime);
        return finalTime;
    }*/

   /* public static String CalculateTImeSplit(double minute)
    {
        String str_input= String.valueOf(minute);
        String str_splite=str_input.replace(".",":");
        String str_arry[]=str_splite.split(":");
        String str_hour="",str_minut="",str_second="",str_answer = null;
        if(str_arry.length>0)
        {
            if(str_arry.length>2)
            {
                str_second=str_arry[2];
                str_minut=str_arry[1];
                str_hour=str_arry[0];
                str_answer=str_hour+":"+str_minut+":"+str_second;
            }
            else {
                str_minut=str_arry[0];
                str_second=str_arry[1];
                str_answer=str_minut+":"+str_second;
            }
            Log.d("call","===== Result ======"+str_answer);
        }
        return str_answer;
    }*/


    //Utils.convertToHHMMSSFormate(339.1);
    public static String convertToHHMMSSFormate(double second)
    {
        int total_second = (int) second;
        Log.d("","======== Total Second INT ========> "+total_second);
        int hours = (int) Math.floor(total_second / 3600);
        int minute = (int) Math.floor((total_second % 3600) / 60);
        int seconds = (int) Math.abs(Math.floor((minute * 60) - total_second));
        String finalTime  = String.format("%02d:%02d", hours, minute);
        Log.d("response","======== Final Formate ========> "+finalTime);
        return finalTime;
    }

    public static void hideSoftKeyboard(AppCompatActivity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

   /* public static String secondToHoursMinutesSeconds(double second) {
        int p1 = (int) (second % 60);
        int p2 = (int) (second / 60);
        int p3 = p2 % 60;
        p2 = p2 / 60;
        String finalTime  = String.format("%02d:%02d:%02d", p2, p3, p1);
        return finalTime;
    }
*/
    public static void setTagBackgroundRtlView(Context context, View view) {
        if (context.getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            view.setBackground(AppCompatResources.getDrawable(context, R.drawable
                    .selector_round_rect_shape_red_rtl));
        } else {
            view.setBackground(AppCompatResources.getDrawable(context, R.drawable
                    .selector_round_rect_shape_red));
        }

    }

    public static boolean hasAnyPrefix(String number, String... prefixes) {
        if (number == null) {
            return false;
        }
        for (String prefix : prefixes) {
            if (number.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBlank(String value) {
        return value == null || value.trim().length() == 0;
    }

    public static boolean isDecimalAndGraterThenZero(String data) {
        try {
            if (Double.valueOf(data) <= 0) {

                return false;
            }
        } catch (NumberFormatException e) {

            return false;
        }
        return true;
    }

    /**
     * This method is used for convert bitmap to base64 format.
     */
    public static String convertImageToBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.URL_SAFE);

        return encodedImage;
    }
}


package com.edelivery.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by elluminati on 30-01-2017.
 */
public class PreferenceHelper {

    /**
     * Preference Const
     */
    private static final String PREF_NAME = "EDelivery";
    private static SharedPreferences app_preferences;
    private static PreferenceHelper preferenceHelper = new PreferenceHelper();
    private final String DEVICE_TOKEN = "device_token";
    private final String GOOGLE_KEY = "google_key";
    private final String IS_PROFILE_PICTURE_REQUIRED = "is_profile_picture_required";
    private final String IS_SMS_VERIFICATION = "is_sms_verification";
    private final String IS_MAIL_VERIFICATION = "is_mail_verification";
    private final String IS_UPLOAD_DOCUMENTS = "is_upload_documents";
    private final String IS_HIDE_OPTIONAL_FIELD_IN_REGISTER = "is_hide_optional_field_in_register";
    private final String PROVIDER_ID = "provider_id";
    private final String SESSION_TOKEN = "session_token";
    private final String FIRST_NAME = "first_name";
    private final String LAST_NAME = "last_name";
    private final String BIO = "bio";
    private final String ADDRESS = "address";
    private final String ZIP_CODE = "zip_code";
    private final String PROFILE_PIC = "profile_pic";
    private final String PHONE_NUMBER = "phone_number";
    private final String PHONE_COUNTRY_CODE = "phone_country_code";
    private final String EMAIL = "email";
    private final String IS_ACTIVE_FOR_JOB = "is_active_for_job";
    private final String IS_PROVIDER_ONLINE = "is_provider_online";
    private final String HOT_LINE_APP_ID = "hot_line_app_id";
    private final String HOT_LINE_APP_KEY = "hot_line_app_key";
    private final String IS_PUSH_SOUND_ON = "is_push_sound_on";
    private final String IS_NEW_ORDER_SOUND_ON = "is_new_order_sound_on";
    private final String IS_PICK_UP_SOUND_ON = "is_pick_up_sound_on";
    private final String IS_APPROVED = "is_approved";
    private final String IS_PATH_DRAW = "is_path_draw";
    private final String IS_LOGIN_BY_EMAIL = "is_login_by_email";
    private final String IS_LOGIN_BY_PHONE = "is_login_by_phone";
    private final String IS_ADMIN_DOCUMENT_MANDATORY = "is_admin_document_mandatory";
    private final String IS_MAIL_VERIFIED = "is_mail_verified";
    private final String IS_PHONE_NUMBER_VERIFIED = "is_phone_number_verified";
    private final String ADMIN_EMAIL = "admin_email";
    private final String MAX_LENGTH = "max_length";
    private final String MIN_LENGTH = "min_length";
    private final String LANGUAGE = "language";
    private final String IS_REFERRAL_ON = "is_referral_on";
    private final String WALLET_CURRENCY_CODE = "wallet_currency_code";
    private final String WALLET_AMOUNT = "wallet_amount";
    private final String REFERRAL = "referral";
    private final String SOCIAL_ID = "social_id";
    private final String IS_LOGIN_BY_SOCIAL = "is_login_by_social";

    private final String ADMIN_CONTACT = "admin_contact";
    private final String T_AND_C = "t_and_c";
    private final String POLICY = "policy";
    private final String CITY_ID = "city_id";
    private final String IS_VEHICLE_UPLOAD_DOCUMENTS = "is_vehicle_upload_documents";
    private final String SELECTED_VEHICLE_ID = "selected_vehicle_id";
    private final String UNIQUE_ID = "unique_id";
    private final String IS_PROVIDER_CAN_SEND_BUSY_MESSAGE = "is_provider_can_send_busy_message";

    private PreferenceHelper() {
    }

    public static PreferenceHelper getInstance(Context context) {
        app_preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return preferenceHelper;
    }

    public void putDeviceToken(String deviceToken) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putString(DEVICE_TOKEN, deviceToken);
        editor.commit();
    }

    public String getDeviceToken() {
        return app_preferences.getString(DEVICE_TOKEN, null);
    }

    public void putGoogleKey(String key) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putString(GOOGLE_KEY, key.trim());
        editor.commit();
    }

    public String getGoogleKey() {
        return app_preferences.getString(GOOGLE_KEY, null);
    }

    public void putIsProfilePictureRequired(boolean isRequired) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_PROFILE_PICTURE_REQUIRED, isRequired);
        editor.commit();
    }

    public boolean getIsProfilePictureRequired() {
        return app_preferences.getBoolean(IS_PROFILE_PICTURE_REQUIRED, false);
    }

    public void putIsSmsVerification(boolean isRequired) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_SMS_VERIFICATION, isRequired);
        editor.commit();
    }

    public boolean getIsSmsVerification() {
        return app_preferences.getBoolean(IS_SMS_VERIFICATION, false);
    }

    public void putIsMailVerification(boolean isRequired) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_MAIL_VERIFICATION, isRequired);
        editor.commit();
    }

    public boolean getIsMailVerification() {
        return app_preferences.getBoolean(IS_MAIL_VERIFICATION, false);
    }


    public void putIsProviderAllDocumentsUpload(boolean isRequired) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_UPLOAD_DOCUMENTS, isRequired);
        editor.commit();
    }

    public boolean getIsProviderAllDocumentsUpload() {
        return app_preferences.getBoolean(IS_UPLOAD_DOCUMENTS, false);
    }

    public void putIsShowOptionalFieldInRegister(boolean isRequired) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_HIDE_OPTIONAL_FIELD_IN_REGISTER, isRequired);
        editor.commit();
    }

    public boolean getIsShowOptionalFieldInRegister() {
        return app_preferences.getBoolean(IS_HIDE_OPTIONAL_FIELD_IN_REGISTER, false);
    }


    public void putSessionToken(String sessionToken) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(SESSION_TOKEN, sessionToken);
        edit.commit();
    }

    public String getSessionToken() {
        return app_preferences.getString(SESSION_TOKEN, "");

    }

    public void putProviderId(String userId) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(PROVIDER_ID, userId);
        edit.commit();
    }

    public String getProviderId() {
        return app_preferences.getString(PROVIDER_ID, null);
    }

    public void putFirstName(String firstName) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(FIRST_NAME, firstName);
        edit.commit();
    }

    public String getFirstName() {
        return app_preferences.getString(FIRST_NAME, null);

    }

    public void putLastName(String lastName) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(LAST_NAME, lastName);
        edit.commit();
    }

    public String getLastName() {
        return app_preferences.getString(LAST_NAME, null);

    }

    public void putProfilePic(String profilePic) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(PROFILE_PIC, profilePic);
        edit.commit();
    }

    public String getProfilePic() {
        return app_preferences.getString(PROFILE_PIC, null);

    }

    public void putBio(String bio) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(BIO, bio);
        edit.commit();
    }

    public String getBio() {
        return app_preferences.getString(BIO, null);

    }

    public void putAddress(String address) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(ADDRESS, address);
        edit.commit();
    }

    public String getAddress() {
        return app_preferences.getString(ADDRESS, null);

    }

    public void putZipCode(String zipCode) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(ZIP_CODE, zipCode);
        edit.commit();
    }

    public String getZipCode() {
        return app_preferences.getString(ZIP_CODE, null);

    }

    public void putPhoneCountyCodeCode(String phoneCountryCode) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(PHONE_COUNTRY_CODE, phoneCountryCode);
        edit.commit();
    }

    public String getPhoneCountyCodeCode() {
        return app_preferences.getString(PHONE_COUNTRY_CODE, null);

    }

    public void putPhoneNumber(String phoneNumber) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(PHONE_NUMBER, phoneNumber);
        edit.commit();
    }

    public String getPhoneNumber() {
        return app_preferences.getString(PHONE_NUMBER, null);

    }

    public void putEmail(String providerEmail) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(EMAIL, providerEmail);
        edit.commit();
    }

    public String getEmail() {
        return app_preferences.getString(EMAIL, null);

    }

    public void putIsProviderOnline(boolean isProviderOnline) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_PROVIDER_ONLINE, isProviderOnline);
        edit.commit();
    }

    public boolean getIsProviderOnline() {
        return app_preferences.getBoolean(IS_PROVIDER_ONLINE, false);
    }

    public void putIsProviderActiveForJob(boolean isActiveForJob) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_ACTIVE_FOR_JOB, isActiveForJob);
        edit.commit();
    }

    public boolean getIsProviderActiveForJob() {
        return app_preferences.getBoolean(IS_ACTIVE_FOR_JOB, false);
    }

    public void putIsPathDraw(boolean isPthDraw) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_PATH_DRAW, isPthDraw);
        edit.commit();
    }

    public boolean getIsPathDraw() {
        return app_preferences.getBoolean(IS_PATH_DRAW, true);
    }

    public void putHotLineAppId(String appId) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(HOT_LINE_APP_ID, appId);
        edit.commit();
    }

    public String getHotLineAppId() {
        return app_preferences.getString(HOT_LINE_APP_ID, null);
    }

    public void putHotLineAppKey(String appKey) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(HOT_LINE_APP_KEY, appKey);
        edit.commit();
    }

    public String getHotLineAppKey() {
        return app_preferences.getString(HOT_LINE_APP_KEY, null);
    }

    public void putIsPushNotificationSoundOn(boolean isSoundOn) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_PUSH_SOUND_ON, isSoundOn);
        edit.commit();

    }

    public boolean getIsPushNotificationSoundOn() {

        return app_preferences.getBoolean(IS_PUSH_SOUND_ON, true);
    }

    public void putIsOrderPickUpPointSoundOn(boolean isSoundOn) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_PICK_UP_SOUND_ON, isSoundOn);
        edit.commit();

    }

    public boolean getIsOrderPickUpPointSoundOn() {

        return app_preferences.getBoolean(IS_PICK_UP_SOUND_ON, true);
    }

    public void putIsNewOrderSoundOn(boolean isSoundOn) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_NEW_ORDER_SOUND_ON, isSoundOn);
        edit.commit();

    }

    public boolean getIsNewOrderSoundOn() {

        return app_preferences.getBoolean(IS_NEW_ORDER_SOUND_ON, true);
    }

    public void putIsApproved(boolean is_approved) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_APPROVED, is_approved);
        edit.commit();
    }

    public boolean getIsApproved() {
        return app_preferences.getBoolean(IS_APPROVED, false);
    }


    public void putIsLoginByEmail(boolean isLoginEmail) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_LOGIN_BY_EMAIL, isLoginEmail);
        editor.commit();
    }

    public boolean getIsLoginByEmail() {
        return app_preferences.getBoolean(IS_LOGIN_BY_EMAIL, false);
    }

    public void putIsLoginByPhone(boolean isLoginPhone) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_LOGIN_BY_PHONE, isLoginPhone);
        editor.commit();
    }

    public boolean getIsLoginByPhone() {
        return app_preferences.getBoolean(IS_LOGIN_BY_PHONE, false);
    }


    public void putIsAdminDocumentMandatory(boolean isUpload) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_ADMIN_DOCUMENT_MANDATORY, isUpload);
        editor.commit();
    }

    public boolean getIsAdminDocumentMandatory() {
        return app_preferences.getBoolean(IS_ADMIN_DOCUMENT_MANDATORY, false);
    }

    public void putIsEmailVerified(boolean isVerified) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_MAIL_VERIFIED, isVerified);
        editor.commit();
    }

    public boolean getIsEmailVerified() {
        return app_preferences.getBoolean(IS_MAIL_VERIFIED, false);
    }

    public void putIsPhoneNumberVerified(boolean isVerified) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_PHONE_NUMBER_VERIFIED, isVerified);
        editor.commit();
    }

    public boolean getIsPhoneNumberVerified() {
        return app_preferences.getBoolean(IS_PHONE_NUMBER_VERIFIED, false);
    }


    public void putAdminContactEmail(String email) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(ADMIN_EMAIL, email);
        edit.commit();
    }

    public String getAdminContactEmail() {
        return app_preferences.getString(ADMIN_EMAIL, null);

    }

    public void putMaxPhoneNumberLength(int length) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putInt(MAX_LENGTH, length);
        editor.commit();
    }

    public int getMaxPhoneNumberLength() {
        return app_preferences.getInt(MAX_LENGTH, 10);
    }

    public void putMinPhoneNumberLength(int length) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putInt(MIN_LENGTH, length);
        editor.commit();
    }

    public int getMinPhoneNumberLength() {
        return app_preferences.getInt(MIN_LENGTH, 9);
    }

    public void putLanguageCode(String code) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(LANGUAGE, code);
        edit.commit();
    }

    public String getLanguageCode() {
        return app_preferences.getString(LANGUAGE, "en");

    }

    public void putIsReferralOn(boolean isReferralOn) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_REFERRAL_ON, isReferralOn);
        edit.commit();
    }

    public boolean getIsReferralOn() {
        return app_preferences.getBoolean(IS_REFERRAL_ON, false);
    }

    public void putWalletCurrencyCode(String code) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(WALLET_CURRENCY_CODE, code);
        edit.commit();
    }

    public String getWalletCurrencyCode() {
        return app_preferences.getString(WALLET_CURRENCY_CODE, "--");
    }

    public void putWalletAmount(float amount) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putFloat(WALLET_AMOUNT, amount);
        edit.commit();
    }

    public float getWalletAmount() {
        return app_preferences.getFloat(WALLET_AMOUNT, 0);
    }

    public void putReferral(String referral) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(REFERRAL, referral);
        edit.commit();
    }

    public String getReferral() {
        return app_preferences.getString(REFERRAL, null);

    }

    public void putSocialId(String id) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(SOCIAL_ID, id);
        edit.commit();
    }

    public String getSocialId() {
        return app_preferences.getString(SOCIAL_ID, "");

    }

    public void putIsLoginBySocial(boolean isLogin) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_LOGIN_BY_SOCIAL, isLogin);
        editor.commit();
    }

    public boolean getIsLoginBySocial() {
        return app_preferences.getBoolean(IS_LOGIN_BY_SOCIAL, false);
    }

    public void putAdminContact(String contact) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(ADMIN_CONTACT, contact);
        edit.commit();
    }

    public String getAdminContact() {
        return app_preferences.getString(ADMIN_CONTACT, null);

    }


    public void putTermsANdConditions(String tandc) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(T_AND_C, tandc);
        edit.commit();
    }

    public String getTermsANdConditions() {
        return app_preferences.getString(T_AND_C, null);

    }


    public void putPolicy(String policy) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(POLICY, policy);
        edit.commit();
    }

    public String getPolicy() {
        return app_preferences.getString(POLICY, null);

    }

    public void putCityId(String cityId) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(CITY_ID, cityId);
        edit.commit();
    }

    public String getCityId() {
        return app_preferences.getString(CITY_ID, null);

    }


    public void putIsProviderAllVehicleDocumentsUpload(boolean isRequired) {
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean(IS_VEHICLE_UPLOAD_DOCUMENTS, isRequired);
        editor.commit();
    }

    public boolean getIsProviderAllVehicleDocumentsUpload() {
        return app_preferences.getBoolean(IS_VEHICLE_UPLOAD_DOCUMENTS, false);
    }

    public void putSelectedVehicleId(String contact) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putString(SELECTED_VEHICLE_ID, contact);
        edit.commit();
    }

    public String getSelectedVehicleId() {
        return app_preferences.getString(SELECTED_VEHICLE_ID, "");

    }

    public void putUniqueId(int id) {
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putInt(UNIQUE_ID, id);
        edit.commit();
    }

    public int getUniqueId() {
        return app_preferences.getInt(UNIQUE_ID, 0);

    }

    public void clearVerification() {
        putIsEmailVerified(false);
        putIsPhoneNumberVerified(false);
    }

    public void logout() {
        putSessionToken("");
        putProviderId("");
    }

    public void putIsProviderSendBusyMessage(boolean isProviderSendBusyMessage){
        SharedPreferences.Editor edit = app_preferences.edit();
        edit.putBoolean(IS_PROVIDER_CAN_SEND_BUSY_MESSAGE,isProviderSendBusyMessage);
        edit.commit();
    }

    public boolean getIsProviderSendBusyMessage(){
        return app_preferences.getBoolean(IS_PROVIDER_CAN_SEND_BUSY_MESSAGE,false);
    }

}

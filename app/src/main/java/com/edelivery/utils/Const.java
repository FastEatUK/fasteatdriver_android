package com.edelivery.utils;

/**
 * Created by elluminati on 19-Jan-17.
 */

public class Const {


    /***
     * Google url
     */
    public static final String GOOGLE_API_URL = "https://maps.googleapis.com/maps/";
    /**
     * Timer Scheduled in Second
     */
    public static final long LOCATION_SCHEDULED_SECONDS = 15;//seconds
    public static final long AVAILABLE_DELIVER_SCHEDULED_SECONDS = 10;//seconds

    /**
     * Permission requestCode
     */
    public static final int PERMISSION_FOR_CALL = 4;
    public static final int PERMISSION_FOR_LOCATION = 2;
    public static final int PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE = 3;
    public static final int REQUEST_CHECK_SETTINGS = 32;
    public static final int REQUEST_UPDATE_BANK_DETAIL = 34;
    public static final int REQUEST_UPDATE_VEHICLE_DETAIL = 35;
    /**
     * App intentId
     */
    public static final int ORDER_ACTIVITY = 3;
    public static final int LOGIN_ACTIVITY = 4;
    public static final int HOME_ACTIVITY = 5;
    public static final int GOOGLE_SIGN_IN = 21;
    /**
     * App result
     */
    public static final int ACTION_SETTINGS = 4;
    /**
     * AppGeneral
     */
    public static final int FOREGROUND_NOTIFICATION_ID = 2568;
    public static final String BUNDLE = "BUNDLE";
    public static final String BACK_TO_ACTIVE_DELIVERY = "BACK_TO_ACTIVE_DELIVERY";
    public static final String GO_TO_INVOICE = "GO_TO_INVOICE";
    public static final String ORDER_PAYMENT = "ORDER_PAYMENT";
    public static final String PAYMENT = "PAYMENT";
    public static final String USER_DETAIL = "USER_DETAIL";
    public static final String STORE_DETAIL = "STORE_DETAIL";
    public static final int INVALID_TOKEN = 999;
    public static final int PROVIDER_DATA_NOT_FOUND=424;
    public static final int SMS_VERIFICATION_ON = 1;
    public static final int EMAIL_VERIFICATION_ON = 2;
    public static final int SMS_AND_EMAIL_VERIFICATION_ON = 3;
    public static final int TYPE_PROVIDER = 8;
    public static final int TYPE_PROVIDER_VEHICLE = 9;
    public static final String ERROR_CODE_PREFIX = "error_code_";
    public static final String MESSAGE_CODE_PREFIX = "message_code_";
    public static final String PUSH_MESSAGE_PREFIX = "push_message_";
    public static final String HTTP_ERROR_CODE_PREFIX = "http_error_";
    public static final String STRING = "string";
    public static final String ANDROID = "android";
    public static final String MANUAL = "manual";
    public static final String SOCIAL = "social";
    public static final String DATE_TIME_FORMAT_WEB = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String TIME_FORMAT_AM = "h:mm a";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_MONTH = "MMM yyyy";
    public static final String DAY = "d";
    public static final String DATE_FORMAT_3 = "dd MMM yy";
    public static final String DATE_FORMAT_2 = "MM-dd-yyyy";
    public static final String WEEK_DAY = "EEE, dd MMMM";
    public static final String DATE_TIME_FORMAT_AM = "yyyy-MM-dd h:mm a";


    public class Params {

        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String EMAIL = "email";
        public static final String PASS_WORD = "password";
        public static final String LOGIN_BY = "login_by";
        public static final String COUNTRY_PHONE_CODE = "country_phone_code";
        public static final String PHONE = "phone";
        public static final String ADDRESS = "address";
        public static final String ZIP_CODE = "zipcode";
        public static final String COUNTRY_ID = "country_id";
        public static final String CITY_ID = "city_id";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DEVICE_TYPE = "device_type";
        public static final String IMAGE_URL = "image_url";
        public static final String TYPE = "type";
        public static final String USER_TYPE_ID = "user_type_id";
        public static final String SERVER_TOKEN = "server_token";
        public static final String USER_ID = "user_id";
        public static final String PROVIDER_ID = "provider_id";
        public static final String IS_ONLINE = "is_online";
        public static final String OLD_PASS_WORD = "old_password";
        public static final String NEW_PASS_WORD = "new_password";
        public static final String BEARING = "bearing";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ORDER_ID = "order_id";
        public static final String UNIQUE_CODE = "unique_code";
        public static final String PUSH_DATA1 = "push_data1";
        public static final String PUSH_DATA2 = "push_data2";
        public static final String NEW_ORDER = "new_order";
        public static final String ID = "id";
        public static final String DOCUMENT_ID = "document_id";
        public static final String EXPIRED_DATE = "expired_date";
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";
        public static final String CANCEL_REASONS = "cancel_reasons";
        public static final String RATING = "rating";
        public static final String REVIEW = "review";
        public static final String IS_ACTIVE_FOR_JOB = "is_active_for_job";
        public static final String IS_PHONE_NUMBER_VERIFIED = "is_phone_number_verified";
        public static final String IS_EMAIL_VERIFIED = "is_email_verified";
        public static final String APP_VERSION = "app_version";
        public static final String IS_PROVIDER_SHOW_INVOICE = "is_provider_show_invoice";
        public static final String PROVIDER_RATING_TO_USER = "provider_rating_to_user";
        public static final String PROVIDER_REVIEW_TO_USER = "provider_review_to_user";
        public static final String PROVIDER_RATING_TO_STORE = "provider_rating_to_store";
        public static final String PROVIDER_REVIEW_TO_STORE = "provider_review_to_store";
        public static final String REFERRAL_CODE = "referral_code";
        public static final String SOCIAL_ID = "social_id";
        public static final String WALLET_STATUS = "wallet_status ";
        public static final String PAYMENT_TOKEN = "payment_token";
        public static final String CARD_ID = "card_id";
        public static final String PAYMENT_ID = "payment_id";
        public static final String LAST_FOUR = "last_four";
        public static final String CARD_TYPE = "card_type";
        public static final String WALLET = "wallet";
        public static final String VEHICLE_ID = "vehicle_id";
        public static final String DELIVERY_STATUS = "delivery_status";
        public static final String REQUEST_ID = "request_id";
        public static final String CARD_EXPIRY_DATE = "card_expiry_date";
        public static final String CARD_HOLDER_NAME = "card_holder_name";
        public static final String MESSAGE = "message";

    }

    /**
     * all activity and fragment TAG for log
     */
    public class Tag {
        public static final String LOG_IN_FRAGMENT = "LOG_IN_FRAGMENT";
        public static final String HOME_FRAGMENT = "HOME_FRAGMENT";
        public static final String SPLASH_SCREEN_ACTIVITY = "SPLASH_SCREEN_ACTIVITY";
        public static final String REGISTER_FRAGMENT = "REGISTER_FRAGMENT";
        public static final String USER_FRAGMENT = "USER_FRAGMENT";
        public static final String PROFILE_ACTIVITY = "PROFILE_ACTIVITY";
        public static final String ACTIVE_DELIVERY_ACTIVITY = "ACTIVE_DELIVERY_ACTIVITY";
        public static final String INVOICE_FRAGMENT = "INVOICE_FRAGMENT";
        public static final String FEEDBACK_FRAGMENT = "FEEDBACK_FRAGMENT";
        public static final String DOCUMENT_ACTIVITY = "DOCUMENT_ACTIVITY";
        public static final String HISTORY_FRAGMENT = "HISTORY_FRAGMENT";
        public static final String BANK_DETAIL_ACTIVITY = "BANK_DETAIL_ACTIVITY";
    }

    public class ProviderStatus {
        public static final int DELIVERY_MAN_NEW_DELIVERY = 9;
        public static final int DELIVERY_MAN_ACCEPTED = 11;
        public static final int DELIVERY_MAN_COMING = 13;
        public static final int DELIVERY_MAN_ARRIVED = 15;
        public static final int DELIVERY_MAN_PICKED_ORDER = 17;
        public static final int DELIVERY_MAN_STARTED_DELIVERY = 19;
        public static final int DELIVERY_MAN_ARRIVED_AT_DESTINATION = 21;
        public static final int DELIVERY_MAN_COMPLETE_DELIVERY = 23;
        public static final int FINAL_ORDER_COMPLETED = 25;
        public static final int DELIVERY_MAN_REJECTED = 111;
        public static final int DELIVERY_MAN_CANCELLED = 112;
        public static final int STORE_CANCELLED_REQUEST = 105;


    }

    public class Wallet {
        public static final int ADDED_BY_ADMIN = 1;
        public static final int ADDED_BY_CARD = 2;
        public static final int ADDED_BY_REFERRAL = 3;
        public static final int ORDER_CHARGED = 4;
        public static final int ORDER_REFUND = 5;
        public static final int ORDER_PROFIT = 6;
        public static final int ORDER_CANCELLATION_CHARGE = 7;
        public static final int WALLET_REQUEST_CHARGE = 8;
        public static final int SET_WEEKLY_PAYMENT_BY_ADMIN = 9;


        public static final int WALLET_STATUS_CREATED = 1;
        public static final int WALLET_STATUS_ACCEPTED = 2;
        public static final int WALLET_STATUS_TRANSFERRED = 3;
        public static final int WALLET_STATUS_COMPLETED = 4;
        public static final int WALLET_STATUS_CANCELLED = 5;

        public static final int ADD_WALLET_AMOUNT = 1;
        public static final int REMOVE_WALLET_AMOUNT = 2;

    }

    public class Payment {
        public static final String CASH = "0";
        public static final String STRIPE = "586f7db95847c8704f537bd5";
        public static final String PAY_PAL = "586f7db95847c8704f537bd6";
        public static final String PAY_U_MONEY = "586f7db95847c8704f537bd";
    }

    /**
     * App Receiver
     */

    public class Action {

        public static final String NETWORK_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
        public static final String GPS_ACTION = "android.location.PROVIDERS_CHANGED";

        public static final String ACTION_NEW_ORDER = "edelivery.provider" +
                ".NEW_ORDER";
        public static final String ACTION_ADMIN_DECLINE = "edelivery.provider" +
                ".ACTION_ADMIN_DECLINE";
        public static final String ACTION_ADMIN_APPROVED = "edelivery.provider" +
                ".ACTION_ADMIN_APPROVED";
        public static final String ACTION_ORDER_STATUS = "edelivery.provider" +
                ".ORDER_STATUS";
        public static final String ACTION_STORE_CANCELED_REQUEST = "edelivery.provider" +
                ".STORE_CANCELED_REQUEST";
        public static final String ACTION_START_UPDATE_LOCATION_SERVICE = "edelivery.provider" +
                ".START_UPDATE_LOCATION_SERVICE";
        public static final String START_FOREGROUND_ACTION = "edelivery.provider" +
                ".startforeground";
        public static final String STOP_FOREGROUND_ACTION = "edelivery.provider" +
                ".stopforeground";

    }

    public class Bank {
        public static final String BANK_ACCOUNT_HOLDER_TYPE = "individual";
    }

    /**
     * Google params
     */

    public class google {
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String ROUTES = "routes";
        public static final String LEGS = "legs";
        public static final String STEPS = "steps";
        public static final String POLYLINE = "polyline";
        public static final String POINTS = "points";
    }

    public class Facebook {
        public static final String EMAIL = "email";
        public static final String PUBLIC_PROFILE = "public_profile";
    }


}

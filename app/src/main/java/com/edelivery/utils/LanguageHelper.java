package com.edelivery.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

public class LanguageHelper extends ContextWrapper {


    public LanguageHelper(Context base) {
        super(base);
    }

    @SuppressWarnings("deprecation")
    public static ContextWrapper wrapper(Context context, String language) {
        Resources res = context.getResources();
        Configuration config = res.getConfiguration();
        Locale sysLocale = config.locale;
        if (!language.equals("") && !sysLocale.getLanguage().equals(language)) {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            config.locale = locale;
            config.setLayoutDirection(locale);
            res.updateConfiguration(config, res
                    .getDisplayMetrics());
        }
        return new LanguageHelper(context);
    }
}
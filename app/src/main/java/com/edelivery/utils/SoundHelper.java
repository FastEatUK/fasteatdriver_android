package com.edelivery.utils;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import com.edelivery.R;

/**
 * Created by elluminati on 30-May-17.
 */

public class SoundHelper {
    private SoundPool soundPool;
    private boolean loaded, plays, playAlert;
    private int tripRequestSoundId, pickupAlertSoundId;
    private int streamID1, streamID2;

    private static SoundHelper soundHelper;

    private SoundHelper(Context context) {
        initializeSoundPool(context);
    }


    private SoundHelper() {

    }

    public static SoundHelper getInstance(Context context) {
        if (soundHelper == null) {
            soundHelper = new SoundHelper(context);
        }
        return soundHelper;
    }

    /**
     * this method is used to init sound pool for play sound file
     */
    private void initializeSoundPool(Context context) {

        if (soundPool == null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                        .build();
                soundPool = new SoundPool.Builder()
                        .setMaxStreams(1)
                        .setAudioAttributes(audioAttributes)
                        .build();
                soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                    @Override
                    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                        loaded = true;
                    }
                });
                tripRequestSoundId = soundPool.load(context, R.raw.beep, 1);
                pickupAlertSoundId = soundPool.load(context, R.raw
                        .driver_notify_before_pickup, 1);

            } else {
                soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 1);
                soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                    @Override
                    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                        loaded = true;
                    }
                });
                tripRequestSoundId = soundPool.load(context, R.raw.beep, 1);
                pickupAlertSoundId = soundPool.load(context, R.raw
                        .driver_notify_before_pickup, 1);
            }
        }
    }

    public void playWhenNewOrderSound() {
        // Is the sound loaded does it already play?
        if (loaded && !plays) {
            // the sound will play for ever if we put the loop parameter -1
            streamID1 = soundPool.play(tripRequestSoundId, 1, 1, 1, -1, 0.5f);
            plays = true;
        }
    }

    public void stopWhenNewOrderSound(Context context) {
        if (plays) {
            soundPool.stop(streamID1);
            tripRequestSoundId = soundPool.load(context, R.raw.beep, 1);
            plays = false;
        }
    }

    public void playSoundBeforePickup() {
        // Is the soniund loaded does it already play?
        if (loaded && !playAlert) {
            // the sound will play for ever if we put the loop parameter -1
            streamID2 = soundPool.play(pickupAlertSoundId, 1, 1, 1, 0, 1f);
            playAlert = true;
        }
    }

    public void stopSoundBeforePickup(Context context) {
        if (playAlert) {
            soundPool.stop(streamID2);
            pickupAlertSoundId = soundPool.load(context, R.raw
                    .driver_notify_before_pickup, 1);
            playAlert = false;
        }
    }
}

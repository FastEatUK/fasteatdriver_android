package com.edelivery.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.edelivery.HomeActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;


/**
 * Created by elluminati on 20-06-2016.
 */
public class LocationHelper implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient
        .ConnectionCallbacks, LocationListener {

    private static final long INTERVAL = 5000;// millisecond
    private static final long FASTEST_INTERVAL = 4000;// millisecond
    public static final float DISPLACEMENT = 5; //miter
    private final String Tag = "LOCATION_HELPER";
    public GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Context context;
    private OnLocationReceived locationReceived;
    private Location currentLocation;
    private LocationSettingsRequest locationSettingsRequest;
    private SettingsClient client;
    private boolean isOpenGpsDialog = false;
    public ProgressDialog dialog;


    public LocationHelper(Context context) {

        this.context = context;
        dialog=new ProgressDialog(this.context);
        getLocationRequest();
        getGoogleApiClientConnect();
    }

    public void setLocationReceivedLister(OnLocationReceived locationReceived) {
        this.locationReceived = locationReceived;
    }

    private void getGoogleApiClientConnect() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder
                (GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).addApi(Auth
                        .GOOGLE_SIGN_IN_API, googleSignInOptions)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
    }

    private void getLocationRequest() {

        locationRequest = new LocationRequest();
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        locationRequest.setSmallestDisplacement(DISPLACEMENT);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();
        client = LocationServices.getSettingsClient(context);

    }

    public void stopLocationUpdate() {
        AppLog.Log(Tag, "Stop location update");


        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    public void startLocationUpdate() {
       /* dialog.setMessage("Gathering your current location.Please wait....");
        dialog.show();*/


        AppLog.Log(Tag, "Start location update");
        /*if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
        }else{*/
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                    locationRequest, this);



    }

    public Location getLastLocation() {
        Location location = null;
        if (googleApiClient.isConnected()) {

        /*    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
            }else{
        */        location = LocationServices.FusedLocationApi
                        .getLastLocation(googleApiClient);
//            }

        }
        return location;

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        AppLog.Log(Tag, "GoogleApiClient is Connected Successfully");
        locationReceived.onConnected(bundle);

    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLog.Log(Tag, "GoogleApiClient is Connection Suspended ");

        locationReceived.onConnectionSuspended(i);

    }

    @Override
    public void onLocationChanged(Location location) {




        AppLog.Log(Tag, "Location Changed");
        currentLocation = location;
        if (currentLocation != null) {
            locationReceived.onLocationChanged(currentLocation);
        }


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(Tag, "GoogleApiClient is Failed to Connect ");
        locationReceived.onConnectionFailed(connectionResult);

    }

    public void onStart() {
        if (googleApiClient != null && !googleApiClient.isConnected()) {
            googleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
        } else {
            startLocationUpdate();
        }
    }

    public void onStop() {
        if (googleApiClient.isConnected()) {
            stopLocationUpdate();
        }

        googleApiClient.disconnect();
    }

    public void setLocationSettingRequest(final AppCompatActivity activity, final int requestCode,
                                          OnSuccessListener onSuccessListener,
                                          final NoGPSDeviceFoundListener noGPSDeviceFoundListener) {

        Task<LocationSettingsResponse> task = client.checkLocationSettings(locationSettingsRequest);
        task.addOnFailureListener(activity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(activity,
                                    requestCode);

                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        if (noGPSDeviceFoundListener != null) {
                            noGPSDeviceFoundListener.noFound();
                        }
                        break;
                }
            }
        });
        task.addOnSuccessListener(activity, onSuccessListener);


    }

    public interface OnLocationReceived {


        /**
         * this method will help to get location at particular time interval
         *
         * @param location location given by device
         */
        public void onLocationChanged(Location location);

        /**
         * this method called when google client is connected successfully
         *
         * @param bundle
         */
        public void onConnected(Bundle bundle);

        /**
         * this method called when google client is connected failed
         *
         * @param connectionResult
         */
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult);

        /**
         * this method called when google client is connected suspended
         *
         * @param i
         */
        public void onConnectionSuspended(int i);


    }

    public interface NoGPSDeviceFoundListener {
        void noFound();
    }

    public boolean isOpenGpsDialog() {
        return isOpenGpsDialog;
    }

    public void setOpenGpsDialog(boolean openGpsDialog) {
        isOpenGpsDialog = openGpsDialog;
    }



}

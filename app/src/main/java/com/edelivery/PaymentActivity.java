package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.PayPalFragment;
import com.edelivery.fragments.StripeFragment;
import com.edelivery.models.datamodels.PaymentGateway;
import com.edelivery.models.responsemodels.PaymentGatewayResponse;
import com.edelivery.models.responsemodels.WalletResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends BaseAppCompatActivity {
    public static final String TAG = PaymentActivity.class.getName();
    public List<PaymentGateway> paymentGateways = new ArrayList<>();
    public PaymentGateway gatewayItem;
    public CustomFontEditTextView etWalletAmount;
    private ImageView ivWithdrawal;
    private CustomFontTextView tvWalletAmount, tvAddWalletAmount;
    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        /*setToolbarRightIcon(R.drawable.ic_history, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToWalletTransaction();
            }
        });*/
        setTitleOnToolBar(getResources().getString(R.string.text_payments));
        findViewById();
        setViewListener();
        getPaymentGateWays();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        ivWithdrawal = (ImageView) findViewById(R.id.ivWithdrawal);
        tvWalletAmount = (CustomFontTextView) findViewById(R.id.tvWalletAmount);
        Utils.setTagBackgroundRtlView(this, findViewById(R.id.tvWallet));
        Utils.setTagBackgroundRtlView(this, findViewById(R.id.tvStoreMethod));
        tabLayout = (TabLayout) findViewById(R.id.paymentTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.paymentViewpager);
        tvAddWalletAmount = (CustomFontTextView) findViewById(R.id.tvAddWalletAmount);
        etWalletAmount = (CustomFontEditTextView) findViewById(R.id.etWalletAmount);
    }

    @Override
    protected void setViewListener() {
        ivWithdrawal.setOnClickListener(this);
        tvAddWalletAmount.setOnClickListener(this);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setSelectedPaymentGateway(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                // do somethings
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // do somethings

            }
        });
        etWalletAmount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() ==
                        KeyEvent.ACTION_UP) {
                    updateUiForWallet(false);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivWithdrawal:
                goToWithdrawalActivity();
                break;
            case R.id.tvAddWalletAmount:
                selectPaymentGatewayForAddWalletAmount();
                break;

            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void goToWithdrawalActivity() {
        Intent intent = new Intent(this, WithdrawalActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToWalletTransaction() {
        Intent intent = new Intent(this, WalletTransactionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void initTabLayout(ViewPager viewPager) {
        if (!paymentGateways.isEmpty()) {
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

            for (PaymentGateway paymentGateway : paymentGateways) {
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.STRIPE)) {
                    viewPagerAdapter.addFragment(new StripeFragment(), paymentGateway.getName
                            ());
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.PAY_PAL)) {
                    viewPagerAdapter.addFragment(new PayPalFragment(), paymentGateway.getName
                            ());
                }
            }
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    /**
     * this method called a webservice to get All Payment Gateway witch is available from admin
     * panel
     */
    private void getPaymentGateWays() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
            jsonObject.put(Const.Params.CITY_ID, preferenceHelper.getCityId());

        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentGatewayResponse> responseCall = apiInterface.getPaymentGateway(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<PaymentGatewayResponse>() {
            @Override
            public void onResponse(Call<PaymentGatewayResponse> call,
                                   Response<PaymentGatewayResponse> response) {
                    Utils.hideCustomProgressDialog();
                    AppLog.Log("PAYMENT_GATEWAY", ApiClient.JSONResponse(response.body()));
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        setWalletAmount(response.body().getWalletAmount(), response.body()
                                .getWalletCurrencyCode());
                        paymentGateways = new ArrayList<>();
                        if (response.body().getPaymentGateway() != null) {
                            paymentGateways.addAll(response.body().getPaymentGateway());
                        }
                        initTabLayout(viewPager);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity
                                .this);
                    }
                }


            @Override
            public void onFailure(Call<PaymentGatewayResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });
    }

    private void setSelectedPaymentGateway(String paymentName) {
        for (PaymentGateway gatewayItem : paymentGateways) {
            if (TextUtils.equals(gatewayItem.getName(), paymentName)) {
                this.gatewayItem = gatewayItem;
                return;
            }

        }
    }

    private void selectPaymentGatewayForAddWalletAmount() {


        if (gatewayItem != null) {
            if (etWalletAmount.getVisibility() == View.GONE) {
                updateUiForWallet(true);
            } else {

                if (!Utils.isDecimalAndGraterThenZero(etWalletAmount.getText().toString())) {
                    Utils.showToast(getResources().getString(R.string
                                    .msg_plz_enter_valid_amount)
                            , this);
                    return;
                }
                switch (gatewayItem.getId()) {
                    case Const.Payment.STRIPE:
                        StripeFragment stripeFragment = (StripeFragment) viewPagerAdapter.getItem
                                (tabLayout.getSelectedTabPosition());
                        stripeFragment.addWalletAmount();
                        break;
                    case Const.Payment.PAY_PAL:

                        break;
                    case Const.Payment.PAY_U_MONEY:
                        break;
                    case Const.Payment.CASH:
                        Utils.showToast(getResources().getString(R.string
                                .msg_plz_select_other_payment_gateway), this);
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        }
    }


    private void setWalletAmount(double amount, String currency) {
        preferenceHelper.putWalletAmount((float) amount);
        preferenceHelper.putWalletCurrencyCode(currency);
        tvWalletAmount.setText(parseContent.decimalTwoDigitFormat.format(amount) + " " + currency);
    }


    private void updateUiForWallet(boolean isUpdate) {
        if (isUpdate) {
            tvWalletAmount.setVisibility(View.GONE);
            etWalletAmount.setVisibility(View.VISIBLE);
            etWalletAmount.requestFocus();
            tvAddWalletAmount.setText(getResources().getString(R.string.text_submit));
        } else {
            etWalletAmount.getText().clear();
            etWalletAmount.setVisibility(View.GONE);
            tvWalletAmount.setVisibility(View.VISIBLE);
            tvAddWalletAmount.setText(getResources().getString(R.string.text_add));
        }

    }

    /**
     * this method called a webservice for  add wallet amount
     */
    public void addWalletAmount(String cardId, String paymentId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
            jsonObject.put(Const.Params.CARD_ID, cardId);
            jsonObject.put(Const.Params.PAYMENT_ID, paymentId);
            jsonObject.put(Const.Params.WALLET, Double.valueOf(etWalletAmount.getText().toString
                    ()));

            Utils.showCustomProgressDialog(this, false);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<WalletResponse> responseCall = apiInterface.getAddWalletAmount(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<WalletResponse>() {
                @Override
                public void onResponse(Call<WalletResponse> call, Response<WalletResponse>
                        response) {
                        Utils.hideCustomProgressDialog();
                        if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                            updateUiForWallet(false);
                            setWalletAmount(response.body().getWallet(), response.body()
                                    .getWalletCurrencyCode());
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity
                                    .this);
                        }
                    }


                @Override
                public void onFailure(Call<WalletResponse> call, Throwable t) {
                    AppLog.handleThrowable(TAG, t);
                }
            });

        } catch (NumberFormatException e) {

            etWalletAmount.setError(getResources().getString(R.string.msg_enter_valid_amount));
        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }

    }
}

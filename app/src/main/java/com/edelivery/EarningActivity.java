package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.DayEarningFragment;
import com.edelivery.fragments.WeekEarningFragment;

public class EarningActivity extends BaseAppCompatActivity {
    public ViewPager earningViewpager;
    public ViewPagerAdapter adapter;
    private TabLayout earningTabsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        findViewById();
        setViewListener();
        initTabLayout();
        setTitleOnToolBar(getResources().getString(R.string.text_Earn));
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(getResources().getDimensionPixelSize(R.dimen
                    .dimen_app_tab_elevation));
        }
        earningTabsLayout = (TabLayout) findViewById(R.id.earningTabsLayout);
        earningViewpager = (ViewPager) findViewById(R.id.earningViewpager);

    }

    @Override
    protected void setViewListener() {
        // do somethings

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings

    }


    private void initTabLayout() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DayEarningFragment(), getResources().getString(R.string.text_day));
        adapter.addFragment(new WeekEarningFragment(), getResources().getString(R.string
                .text_week));
        earningViewpager.setAdapter(adapter);
        earningTabsLayout.setupWithViewPager(earningViewpager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

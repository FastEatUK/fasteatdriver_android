package com.edelivery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.IdRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.edelivery.adapter.BankSpinnerAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.BankDetail;
import com.edelivery.models.datamodels.Withdrawal;
import com.edelivery.models.responsemodels.BankDetailResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawalActivity extends BaseAppCompatActivity {
    private ArrayList<BankDetail> bankDetailArrayList = new ArrayList<>();
    private RadioButton rbBankAccount, rbCash;
    private RadioGroup radioGroup2;
    private CustomFontEditTextView etAmount, etDescription;
    private Spinner spinnerBank;
    private CustomFontTextView tvAddBankAccount;
    private BankSpinnerAdapter bankSpinnerAdapter;
    private LinearLayout llSelectBank;
    private boolean isPaymentModeCash;
    private CustomFontButton btnWithdrawal;
    private BankDetail bankDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bankDetailArrayList = new ArrayList<>();
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_withdrawal));
        findViewById();
        setViewListener();
        initSpinnerBank();
        getBankDetail();

    }

    @Override
    protected boolean isValidate() {
        String msg = null;

        if (!Utils.isDecimalAndGraterThenZero(etAmount.getText().toString())) {
            msg = (getResources().getString(R.string
                    .msg_plz_enter_valid_amount));
            etAmount.setError(msg);
            etAmount.requestFocus();
        } else if (TextUtils.isEmpty(etDescription.getText().toString())) {
            msg = (getResources().getString(R.string
                    .msg_enter_description));
            etDescription.setError(msg);
            etDescription.requestFocus();
        } else if (isPaymentModeCash) {
            return true;
        } else if (bankDetail == null) {
            msg = getResources().getString(R.string.msg_plz_add_bank_detail);
            Utils.showToast(msg, WithdrawalActivity.this);
        }
        return TextUtils.isEmpty(msg);
    }

    @Override
    protected void findViewById() {
        rbBankAccount = (RadioButton) findViewById(R.id.rbBankAccount);
        rbCash = (RadioButton) findViewById(R.id.rbCash);
        radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        etAmount = (CustomFontEditTextView) findViewById(R.id.etAmount);
        etDescription = (CustomFontEditTextView) findViewById(R.id.etDescription);
        spinnerBank = (Spinner) findViewById(R.id.spinnerBank);
        llSelectBank = (LinearLayout) findViewById(R.id.llSelectBank);
        btnWithdrawal = (CustomFontButton) findViewById(R.id.btnWithdrawal);
        tvAddBankAccount = (CustomFontTextView) findViewById(R.id.tvAddBankAccount);
        Utils.setTagBackgroundRtlView(this, findViewById(R.id.tvSelectMethod));
    }

    @Override
    protected void setViewListener() {
        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rbBankAccount:
                        isPaymentModeCash = false;
                        llSelectBank.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rbCash:
                        isPaymentModeCash = true;
                        llSelectBank.setVisibility(View.GONE);
                        break;

                    default:
                        // do with default
                        break;
                }
            }
        });
        rbBankAccount.setChecked(true);
        btnWithdrawal.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnWithdrawal:
                if (isValidate()) {
                    createWithdrawalRequest();
                }
                break;

            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /**
     * this method  call webservice for get bank detail
     */
    private void getBankDetail() {
        Utils.showCustomProgressDialog(this, false);
        final BankDetail bankDetail = new BankDetail();
        bankDetail.setBankHolderId(preferenceHelper.getProviderId());
        bankDetail.setBankHolderType(Const.TYPE_PROVIDER);
        bankDetail.setServerToken(preferenceHelper.getSessionToken());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<BankDetailResponse> responseCall = apiInterface.getBankDetail(ApiClient
                .makeGSONRequestBody(bankDetail));
        responseCall.enqueue(new Callback<BankDetailResponse>() {
            @Override
            public void onResponse(Call<BankDetailResponse> call, Response<BankDetailResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        bankDetailArrayList.clear();
                        bankDetailArrayList.addAll(response.body().getBankDetail());
                        bankSpinnerAdapter.notifyDataSetChanged();


                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), WithdrawalActivity
                                .this);
                    }
                    updateUiBankAccount(bankDetailArrayList.isEmpty());
                }


            @Override
            public void onFailure(Call<BankDetailResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.BANK_DETAIL_ACTIVITY, t);
            }
        });

    }

    private void initSpinnerBank() {
        bankSpinnerAdapter = new BankSpinnerAdapter(this, R.layout.item_spinner_bank,
                bankDetailArrayList);
        spinnerBank.setAdapter(bankSpinnerAdapter);
        spinnerBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankDetail = bankDetailArrayList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void createWithdrawalRequest() {
        Utils.showCustomProgressDialog(this, false);
        final Withdrawal withdrawal = new Withdrawal();
        withdrawal.setProviderId(preferenceHelper.getProviderId());
        withdrawal.setServerToken(preferenceHelper.getSessionToken());
        withdrawal.setBankDetail(bankDetail);
        withdrawal.setIsPaymentModeCash(isPaymentModeCash);
        withdrawal.setDescriptionForRequestWalletAmount(etDescription.getText().toString().trim());
        withdrawal.setType(Const.TYPE_PROVIDER);
        withdrawal.setRequestedWalletAmount(Double.valueOf(etAmount.getText().toString()));


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.createWithdrawalRequest
                (ApiClient
                        .makeGSONRequestBody(withdrawal));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call,
                                   Response<IsSuccessResponse>
                                           response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        onBackPressed();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), WithdrawalActivity
                                .this);
                    }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.BANK_DETAIL_ACTIVITY, t);
            }
        });

    }

    private void updateUiBankAccount(boolean isUpdate) {
        if (isUpdate) {
            spinnerBank.setVisibility(View.GONE);
            tvAddBankAccount.setVisibility(View.VISIBLE);
            tvAddBankAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(WithdrawalActivity.this, AddBankDetailActivity
                            .class);
                    startActivityForResult(intent, Const.REQUEST_UPDATE_BANK_DETAIL);
                }
            });
        } else {
            spinnerBank.setVisibility(View.VISIBLE);
            tvAddBankAccount.setVisibility(View.GONE);
            tvAddBankAccount.setOnClickListener(null);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Const.REQUEST_UPDATE_BANK_DETAIL:
                    getBankDetail();
                    break;

                default:
                    // do with default
                    break;
            }

        }
    }
}


package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.adapter.HistoryDetailAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.fragments.HistoryFragment;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.models.datamodels.OrderHistoryDetail;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.StoreDetail;
import com.edelivery.models.datamodels.UserDetail;
import com.edelivery.models.responsemodels.OrderHistoryDetailResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.BuildConfig.BASE_URL;

public class HistoryDetailActivity extends BaseAppCompatActivity {

    private ImageView ivHistoryStoreImage, ivProviderImage;
    private RecyclerView rcvOrderItem;
    private CustomFontTextView tvHistoryOderTotal,
            tvAddressOne, tvAddressTwo, tvDeliveryDate;
    private CustomFontTextViewTitle tvHistoryOrderName, tvProviderName;
    private OrderHistoryDetail orderDetail;
    private StoreDetail storesItem;
    private UserDetail user;
    private String payment;
    private CustomFontTextView tvEstTime, tvEstDistance, tvHistoryDetailOrderNumber;
    private OrderPayment orderPayment;
    private CustomFontTextView tvStoreRatings, tvRatings;
    private LinearLayout llCartDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_history_detail));
        findViewById();
        setViewListener();
        //setInvoiceButton();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadExtraData();
    }

    @Override
    protected boolean isValidate() {
        return false;
        // do somethings
    }

    @Override
    protected void findViewById() {
        // do somethings
        tvHistoryOderTotal = (CustomFontTextView) findViewById(R.id.tvHistoryOderTotal);
        tvHistoryOrderName = (CustomFontTextViewTitle) findViewById(R.id.tvHistoryOrderName);
        tvProviderName = (CustomFontTextViewTitle) findViewById(R.id.tvCustomerName);
        tvAddressOne = (CustomFontTextView) findViewById(R.id.tvAddressOne);
        tvAddressTwo = (CustomFontTextView) findViewById(R.id.tvAddressTwo);
        tvDeliveryDate = (CustomFontTextView) findViewById(R.id.tvDetailDate);
        rcvOrderItem = (RecyclerView) findViewById(R.id.rcvOrderItem);
        ivHistoryStoreImage = (ImageView) findViewById(R.id.ivHistoryStoreImage);
        ivProviderImage = (ImageView) findViewById(R.id.ivCustomerImage);
        findViewById(R.id.tvOrderNumber).setVisibility(View.GONE);
        findViewById(R.id.tvDeliveryDate).setVisibility(View.GONE);
        findViewById(R.id.tvDeliveryStatus).setVisibility(View.GONE);
        tvEstDistance = (CustomFontTextView) findViewById(R.id.tvEstDistance);
        tvEstTime = (CustomFontTextView) findViewById(R.id.tvEstTime);
        tvStoreRatings = (CustomFontTextView) findViewById(R.id.tvStoreRatings);
        tvRatings = (CustomFontTextView) findViewById(R.id.tvRatings);
        tvHistoryDetailOrderNumber = (CustomFontTextView) findViewById(R.id
                .tvHistoryDetailOrderNumber);
        findViewById(R.id.llProfit).setVisibility(View.GONE);
        findViewById(R.id.llEstProfit).setVisibility(View.GONE);
        llCartDetail = findViewById(R.id.llCartDetail);
    }

    @Override
    protected void setViewListener() {
        // do somethings
        tvRatings.setOnClickListener(this);
        tvStoreRatings.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();

    }

    private void loadExtraData() {
        if (getIntent().getExtras() != null) {
            getOrderHistoryDetail(getIntent().getExtras().getString(Const.Params.ORDER_ID));
        }
    }

    @Override
    public void onClick(View view) {
        // do somethings
        //do something
        switch (view.getId()) {
            case R.id.tvStoreRatings:
                goToCompleteOrderActivity(orderPayment, payment, null, storesItem, orderDetail
                        .getId(), false, false);
                break;
            case R.id.tvRatings:
                goToCompleteOrderActivity(orderPayment, payment, user, null, orderDetail
                        .getId(), false, false);
                break;
            default:
                // do with default
                break;
        }
    }

    /**
     * this method called a webservice for get order history detail
     *
     * @param orderId order id in string
     */
    private void getOrderHistoryDetail(String orderId) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.REQUEST_ID, orderId);
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderHistoryDetailResponse> responseCall = apiInterface.getOrderHistoryDetail(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderHistoryDetailResponse>() {
            @Override
            public void onResponse(Call<OrderHistoryDetailResponse> call,
                                   Response<OrderHistoryDetailResponse> response) {
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) &&response.body().isSuccess()) {
                        CurrentOrder.getInstance().setCurrency(response.body()
                                .getCurrency());
                        orderPayment = response.body().getOrderDetail().getOrderPaymentDetail();
                        orderDetail = response.body().getOrderDetail();
                        String orderNumber = getResources().getString(R.string.text_request_number)
                                + " " + "#" + orderDetail.getUniqueId();
                        setTitleOnToolBar(orderNumber);
                        storesItem = response.body().getStoreDetail();
                        user = response.body().getUser();
                        payment = response.body().getPayment();
                        initRcvOrder();
                        loadOrderData();
                        updateUi();
                        Utils.hideCustomProgressDialog();


                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                HistoryDetailActivity.this);
                    }


            }

            @Override
            public void onFailure(Call<OrderHistoryDetailResponse> call, Throwable t) {
                AppLog.handleThrowable(HistoryFragment.class.getName(), t);
            }
        });
    }

    /**
     * this method load order data in view
     */

    private void loadOrderData() {
        tvHistoryOderTotal.setText(CurrentOrder.getInstance().getCurrency() + parseContent
                .decimalTwoDigitFormat.format(orderDetail
                        .getTotalOrderPrice()));
        tvAddressOne.setText(orderDetail.getCartDetail().getPickupAddresses().get(0).getAddress());
        tvAddressTwo.setText(orderDetail.getCartDetail().getDestinationAddresses().get(0)
                .getAddress());
        try {
            Date date = parseContent.webFormat.parse(orderDetail.getCompletedAt());
            String dateCreated = parseContent.dateFormat.format(date);
            String currentDate = parseContent.dateFormat.format(new Date());

            if (dateCreated.equals(currentDate)) {
                tvDeliveryDate.setText(this
                        .getString(R.string.text_today));
            } else if (dateCreated
                    .equals(getYesterdayDateString())) {
                tvDeliveryDate.setText(this
                        .getString(R.string.text_yesterday));
            } else {
                tvDeliveryDate.setText(Utils.getDayOfMonthSuffix(Integer.valueOf(parseContent.day
                        .format(date))) + " " +
                        "" + parseContent.dateFormatMonth.format(date));
            }

        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        tvHistoryOrderName.setText(storesItem.getName());
        tvProviderName.setText(user.getFirstName() + " " + user.getLastName());
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + orderDetail.getOrderDetail().getUniqueId();
        tvHistoryDetailOrderNumber.setText(orderNumber);
        Glide.with(this).load(BASE_URL + storesItem.getImageUrl()).dontAnimate().placeholder
                (ResourcesCompat.getDrawable(this
                        .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                .getDrawable
                        (this
                                .getResources(), R.drawable.placeholder, null)).into
                (ivHistoryStoreImage);
        Glide.with(this).load(BASE_URL + user.getImageUrl()).dontAnimate().placeholder
                (ResourcesCompat.getDrawable(this
                        .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                .getDrawable
                        (this
                                .getResources(), R.drawable.placeholder, null)).into
                (ivProviderImage);
       //  tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(orderPayment.getTotalTime()));

       /* String str_TotalTime= String.valueOf(orderPayment.getTotalTime());
        if(!str_TotalTime.isEmpty())
        {
            tvEstTime.setText(Utils.secondToHoursMinutesSeconds(orderPayment.getTotalTime()));
        }*/



        String str_TotalTime= String.valueOf(orderPayment.getTotal_sec());
        if(str_TotalTime!=null && !str_TotalTime.equals(""))
        {
            Log.d("formate","=========== Formate ========="+Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime)));
            tvEstTime.setText(Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime)));
        }


       String unit;
        if(orderPayment.isIs_distance_unit_mile())
        {
            unit="mile";
        }
        else {
            unit = "km";
        }
        Log.d("call","=====> "+orderPayment.isIs_distance_unit_mile());


        String str_TotalDistance= String.valueOf(orderPayment
                .getTotalDistance());
        if(str_TotalDistance!=null && !str_TotalDistance.equals(""))
        {
            tvEstDistance.setText(parseContent.decimalTwoDigitFormat.format(orderPayment
                    .getTotalDistance()) + unit);

        }
        }

    /**
     * this method manage invoice button view
     */
    private void setInvoiceButton() {
        ivToolbarRightIcon.setImageDrawable(null);
        tvToolRightText.setBackground(AppCompatResources.getDrawable(this,
                R.drawable.selector_round_shape_red));
        tvToolRightText.setText(getResources().getString(R.string.text_invoice));
        tvToolRightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCompleteOrderActivity(orderPayment, payment, null, null, orderPayment
                        .getOrderId(), true, false);
            }
        });
        tvToolRightText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initRcvOrder() {
        if (orderDetail.getCartDetail().getOrderDetails() == null || orderDetail.getCartDetail()
                .getOrderDetails().isEmpty()) {
            llCartDetail.setVisibility(View.GONE);
        } else {
            //llCartDetail.setVisibility(View.VISIBLE);
        }

        rcvOrderItem.setLayoutManager(new LinearLayoutManager(this));
        rcvOrderItem.setAdapter(new HistoryDetailAdapter(orderDetail.getCartDetail()
                .getOrderDetails()));
        rcvOrderItem.setNestedScrollingEnabled(false);
    }


    private String appendString(String string, Double value) {
        return string + parseContent.decimalTwoDigitFormat.format(value);
    }

    private String appendString(Double value, String unit) {
        return parseContent.decimalTwoDigitFormat.format(value) +
                unit;
    }

    private String getYesterdayDateString() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return parseContent.dateFormat.format(cal.getTime());
    }


    private void updateUi() {
        if (orderDetail.getOrderDetail().isProviderRatedToStore()) {
            tvStoreRatings.setVisibility(View.GONE);
        } else {
          //  tvStoreRatings.setVisibility(View.VISIBLE);
        }

        if (orderDetail.getOrderDetail().isProviderRatedToUser()) {
            tvRatings.setVisibility(View.GONE);
        } else {
          //  tvRatings.setVisibility(View.VISIBLE);
        }

    }

    /**
     * this method open Complete order activity witch is contain two fragment
     * Invoice Fragment and Feedback Fragment
     *
     * @param goToInvoiceFirst             set true when you want to open invoice fragment when
     *                                     activity open
     * @param backToActiveDeliveryActivity set true when you want
     *                                     goToActiveDeliveryActivity when press back
     *                                     button
     */
    private void goToCompleteOrderActivity(OrderPayment orderPayment, String payment, UserDetail
            userDetail, StoreDetail storeDetail, String requestId, boolean goToInvoiceFirst, boolean
                                                   backToActiveDeliveryActivity) {
        Intent intent = new Intent(this, CompleteOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Const.GO_TO_INVOICE, goToInvoiceFirst);
        bundle.putParcelable(Const.ORDER_PAYMENT, orderPayment);
        bundle.putString(Const.PAYMENT, payment);
        bundle.putParcelable(Const.USER_DETAIL, userDetail);
        bundle.putString(Const.Params.REQUEST_ID, requestId);
        bundle.putParcelable(Const.STORE_DETAIL, storeDetail);
        intent.putExtra(Const.BUNDLE, bundle);
        intent.putExtra(Const.BACK_TO_ACTIVE_DELIVERY, backToActiveDeliveryActivity);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim
                .slide_out_left);
    }

}

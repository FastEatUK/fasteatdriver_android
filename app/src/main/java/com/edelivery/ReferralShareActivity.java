package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.utils.Utils;

public class ReferralShareActivity extends BaseAppCompatActivity {

    private CustomFontTextView tvWalletAmount;
    private CustomFontTextViewTitle tvReferralCode;
    private CustomFontButton btnShareReferral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_share);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_share));
        findViewById();
        setViewListener();


    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        tvWalletAmount = (CustomFontTextView) findViewById(R.id.tvWalletAmount);
        tvReferralCode = (CustomFontTextViewTitle) findViewById(R.id.tvReferralCode);
        btnShareReferral = (CustomFontButton) findViewById(R.id.btnShareReferral);
        Utils.setTagBackgroundRtlView(this, findViewById(R.id.tvWallet));
        Utils.setTagBackgroundRtlView(this, findViewById(R.id.tvShare));
        tvWalletAmount.setText(parseContent.decimalTwoDigitFormat.format(preferenceHelper
                .getWalletAmount()) + " " + preferenceHelper.getWalletCurrencyCode());
        tvReferralCode.setText(preferenceHelper.getReferral());
    }

    @Override
    protected void setViewListener() {
        btnShareReferral.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShareReferral:
                shareAppAndReferral();
                break;
            default:
                // do with default
                break;
        }
    }

    private void shareAppAndReferral() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString
                (R.string.msg_use_referral_code) + " " + preferenceHelper
                .getReferral());
        startActivity(Intent.createChooser(sharingIntent, getResources().getString
                (R.string.msg_share_referral)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

/*
package com.edelivery;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bumptech.glide.Glide;
import com.edelivery.adapter.OrderDetailsAdapter;
import com.edelivery.adapter.ProviderMessageAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomEventMapView;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.component.CustomPhotoDialog;
import com.edelivery.interfaces.LatLngInterpolator;
import com.edelivery.models.datamodels.CancelReasons;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.UserDetail;
import com.edelivery.models.responsemodels.CompleteOrderResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OrderStatusResponse;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.SoundHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.BuildConfig.BASE_URL;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

public class ActiveDeliveryActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived, OnMapReadyCallback {

    private LocationHelper locationHelper;
    private CustomEventMapView mapView;
    private CustomFontTextView tvOrderTimer, tvEstTime, tvEstDistance, tvAddressTwo,
            tvDeliveryDate, tvDeliveryStatus, tvAddressOne, tvOrderNumber, tvProviderProfit,
            tvOrderId;
    private CustomFontTextViewTitle tvCustomerName;
    private CustomFontButton btnRejectOrder, btnAcceptOrder, btnJobStatus;
    private ImageView ivNavigate, ivCustomerImage, ivActiveTargetLocation, ivCall, ivCart,ivMessage;
    private GoogleMap googleMap;
    private String requestId;
    private int orderStatus = Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY;
    private boolean isCountDownTimerStart;
    private CountDownTimer countDownTimer;
    private FrameLayout flAccept;
    private LinearLayout llUpdateStatus, llAddressOne;
    private Order order;
    private UserDetail userDetail;
    private Location lastLocation, currentLocation;
    private LatLng currentLatLng, pickUpLatLng, deliveryLatLng;
    private ArrayList<LatLng> markerList = new ArrayList<>();
    private Marker providerMarker, pickUpMarker, destinationMarker;
    private String cancelReason,providerMessage;
    private SoundHelper soundHelper;
    private CustomDialogAlert noteDialog, orderItemConfirmDialog;
    private CustomPhotoDialog mapDialog;
    private Dialog orderCancelDialog, cartDetailDialog,providerMessageDialog;
    private CustomDialogVerification pickUpDeliveryDialog, completeDeliveryDialog;
    private OrderStatusReceiver orderStatusReceiver;
    private String call;
    private boolean isCameraIdeal = true;

    private CustomFontTextView tvOrderDetailNumber,tvDeliveryNote;
    private RecyclerView rcvOrderDetailList;
    private LinearLayout llOrderDetail;
    private ProviderMessageAdapter providerMessageAdapter;
    private ArrayList<String> messageList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_delivery);
        getExtraData();
        initToolBar();
        findViewById();
        setViewListener();
        setTitleOnToolBar(getResources().getString(R.string.text_active_delivery));
        initLocationHelper();
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        markerList = new ArrayList<>();
        soundHelper = SoundHelper.getInstance(this);
        orderStatusReceiver = new OrderStatusReceiver();
        messageList = new ArrayList<>();

    }

    @Override
    public void onStart() {
        super.onStart();

        registerReceiver(orderStatusReceiver, new IntentFilter(Const.Action
                .ACTION_STORE_CANCELED_REQUEST));
    }


    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        locationHelper.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mapView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);

    }


    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
        locationHelper.onStop();
    }

    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        mapView = (CustomEventMapView) findViewById(R.id.mapViewActiveDelivery);
        tvOrderTimer = (CustomFontTextView) findViewById(R.id.tvOrderAcceptTimer);
        btnAcceptOrder = (CustomFontButton) findViewById(R.id.btnAcceptOrder);
        btnRejectOrder = (CustomFontButton) findViewById(R.id.btnRejectOrder);
        tvEstDistance = (CustomFontTextView) findViewById(R.id.tvEstDistance);
        tvEstTime = (CustomFontTextView) findViewById(R.id.tvEstTime);
        flAccept = (FrameLayout) findViewById(R.id.flAccept);
        llUpdateStatus = (LinearLayout) findViewById(R.id.llUpdateStatus);
        btnJobStatus = (CustomFontButton) findViewById(R.id.btnJobStatus);
        ivNavigate = (ImageView) findViewById(R.id.ivNavigate);
        ivCustomerImage = (ImageView) findViewById(R.id.ivCustomerImage);
        tvAddressTwo = (CustomFontTextView) findViewById(R.id.tvAddressTwo);
        tvCustomerName = (CustomFontTextViewTitle) findViewById(R.id.tvCustomerName);
        tvDeliveryDate = (CustomFontTextView) findViewById(R.id.tvDeliveryDate);
        tvDeliveryStatus = (CustomFontTextView) findViewById(R.id.tvDeliveryStatus);
        tvAddressOne = (CustomFontTextView) findViewById(R.id.tvAddressOne);
        tvDeliveryDate.setVisibility(View.GONE);
        tvDeliveryStatus.setVisibility(View.GONE);
        ivActiveTargetLocation = (ImageView) findViewById(R.id.ivActiveTargetLocation);
        llAddressOne = (LinearLayout) findViewById(R.id.llAddressOne);
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        tvOrderId = findViewById(R.id.tvOrderId);
        tvProviderProfit = (CustomFontTextView) findViewById(R.id.tvProviderProfit);
        ivCall = (ImageView) findViewById(R.id.ivCall);
        ivCall.setVisibility(View.VISIBLE);
        ivCart = (ImageView) findViewById(R.id.ivCart);
        ivMessage = findViewById(R.id.ivMessage);
        if(preferenceHelper.getIsProviderSendBusyMessage()) {
            ivMessage.setVisibility(View.VISIBLE);
        }else{
            ivMessage.setVisibility(View.GONE);
        }
     //   tvOrderDetailNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        tvDeliveryNote = (CustomFontTextView) findViewById(R.id.tvDeliveryNote);
        llOrderDetail = (LinearLayout) findViewById(R.id.llOrderCartDetail);
        rcvOrderDetailList = (RecyclerView) findViewById(R.id.rcvOrderProductItem);
    }

    @Override
    protected void setViewListener() {
        // do somethings
        btnRejectOrder.setOnClickListener(this);
        btnAcceptOrder.setOnClickListener(this);
        btnJobStatus.setOnClickListener(this);
        ivNavigate.setOnClickListener(this);
        ivActiveTargetLocation.setOnClickListener(this);
        tvDeliveryStatus.setVisibility(View.GONE);
        ivCall.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        if (lastLocation == null) {
            lastLocation = currentLocation;
        }
        setMarkerOnLocation(currentLatLng, pickUpLatLng, deliveryLatLng);
        lastLocation = currentLocation;
    }

    @Override
    public void onConnected(Bundle bundle) {
        AppLog.Log(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, "GoogleClientConnected");
        moveCameraFirstMyLocation(false);
        locationHelper.startLocationUpdate();
        getRequestStatus();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, "GoogleClientConnectionFailed");
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLog.Log(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, "GoogleConnectionSuspended");
    }

    private void initLocationHelper() {
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setUpMap();
        locationHelper.onStart();
    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnAcceptOrder:
                setOrderStatus(Const.ProviderStatus.DELIVERY_MAN_ACCEPTED);
                break;
            case R.id.btnRejectOrder:
                rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED, false);
                break;
            case R.id.ivActiveTargetLocation:
                if (!markerList.isEmpty()) {
                    setLocationBounds(false, markerList);
                }
                break;
            case R.id.btnJobStatus:
                if (orderStatus == Const.ProviderStatus.FINAL_ORDER_COMPLETED) {
                    openCompleteDeliveryDialog();
                } else if (orderStatus == Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER) {
                    openPickupDeliveryDialog();
                   // openDeliveryOrderItemConfirm();
                } else {
                    setOrderStatus(orderStatus);
                }
                break;
            case R.id.ivToolbarRightIcon:
                switch (orderStatus) {
                    case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
                    case Const.ProviderStatus.DELIVERY_MAN_COMING:
                    case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
                    case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                        openCancelOrderDialog();
                        break;
                    default:
                        openOrderNoteDialog();
                        break;
                }

                break;
            case R.id.ivNavigate:
                openPhotoMapDialog();
                break;
            case R.id.ivCall:
                makePhoneCall(call);
                break;
            case R.id.ivCart:
                openCartDetailDialog();
                break;
            case R.id.ivMessage:
               // if(!currentOrder.getProviderMessageList().isEmpty()) {
                    openProviderMessageDailog();
              //  }
                Log.d("aaa","ProvideMessageList=="+currentOrder.getProviderMessageList().size());
                break;
            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void setUpMap() {
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    private void getExtraData() {
        if (getIntent() != null) {
            requestId = getIntent().getExtras().getString(Const.Params.ORDER_ID);
        }
    }

    */
/**
 * this method call webservice for get current active order status
 *//*

    private void getRequestStatus() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderStatusResponse> responseCall = apiInterface.getRequestStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse>
                    response) {

                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) &&response.body().isSuccess()) {

                        Gson gson = new Gson();
                        AppLog.Log("GET_ORDER_STATUS", gson.toJson(response.body()));
                        order = response.body().getOrderRequest();
                        userDetail = response.body().getUserDetail();
                        loadOrderData(order.getDeliveryStatus());
                        tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(order.getTotalTime()));
                        tvEstDistance.setText(parseContent.decimalTwoDigitFormat.format(order
                                .getTotalDistance()));
                        tvProviderProfit.setText(order.getCurrency() + order
                                .getTotalProviderIncome());
                        checkCurrentOderStatus(order.getDeliveryStatus());
                        pickUpLatLng = new LatLng(order.getPickupAddresses().get(0).getLocation()
                                .get(0), order.getPickupAddresses().get(0).getLocation()
                                .get(1));
                        deliveryLatLng = new LatLng(order.getDestinationAddresses().get(0)
                                .getLocation()
                                .get(0), order.getDestinationAddresses().get(0).getLocation()
                                .get(1));
                        setMarkerOnLocation(currentLatLng, pickUpLatLng, deliveryLatLng);


                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ActiveDeliveryActivity.this);
                        goToHomeActivity();
                    }

            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);
            }
        });
    }

    */
/**
 * this method used to manage UI according to order status
 *
 * @param orderStatus int
 *//*

    private void checkCurrentOderStatus(int orderStatus) {
        switch (orderStatus) {
            case Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY:
                updateUiCancelOrder(false);
                updateUiOrderStatus(false);
                startCountDownTimer(order.getTimeLeftToRespondsTrip());
                break;
            case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
                */
/*this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_COMING;
                updateUiOrderStatus(true);
                updateUiCancelOrder(true);
                break;*//*

            case Const.ProviderStatus.DELIVERY_MAN_COMING:
                */
/*this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_ARRIVED;
                btnJobStatus.setText(getResources().getString(R.string.text_tap_here_to_arrive));
                updateUiOrderStatus(true);
                updateUiCancelOrder(true);
                break;*//*

            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER;
                stopCountDownTimer();
                //openCartDetailDialog();
                setOrderDetail();
              //  btnJobStatus.setText(getResources().getString(R.string.text_tap_here_to_pickup));
                updateUiOrderStatus(true);
                updateUiCancelOrder(true);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
                this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY;
                mapView.setVisibility(View.VISIBLE);
                llOrderDetail.setVisibility(View.GONE);
                btnJobStatus.setText(getResources().getString(R.string.text_tap_here_to_start));
                updateUiOrderStatus(true);
                updateUiCancelOrder(false);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
                this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION;
                btnJobStatus.setText(getResources().getString(R.string
                        .text_tap_here_to_arrive_destination));
                updateUiOrderStatus(true);
                updateUiCancelOrder(false);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                this.orderStatus = Const.ProviderStatus.FINAL_ORDER_COMPLETED;
                btnJobStatus.setText(getResources().getString(R.string
                        .text_tap_here_to_end));
                updateUiOrderStatus(true);
                updateUiCancelOrder(false);
                break;
            case Const.ProviderStatus.STORE_CANCELLED_REQUEST:
                goToHomeActivity();
                break;
            default:
                // do with default
                break;

        }
        showPickupTimeAndDate();
    }



    private void showPickupTimeAndDate() {
        if (!TextUtils.isEmpty(order.getEstimatedTimeForReadyOrder())
                && (orderStatus == Const.ProviderStatus
                .DELIVERY_MAN_NEW_DELIVERY || orderStatus == Const.ProviderStatus
                .DELIVERY_MAN_ACCEPTED || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_COMING || orderStatus
                == Const.ProviderStatus.DELIVERY_MAN_ARRIVED)) {
            try {
                Date date = parseContent.webFormat.parse(order
                        .getEstimatedTimeForReadyOrder
                                ());
                tvDeliveryDate.setText(getResources().getString(R.string
                        .text_pick_up_order_after) + " " + parseContent.dateTimeFormat.format
                        (date));
                tvDeliveryDate.setVisibility(View.VISIBLE);
            } catch (ParseException e) {
                AppLog.handleException(ActiveDeliveryActivity.class.getName(), e);
            }

        } else {
            tvDeliveryDate.setVisibility(View.GONE);
        }
    }

    */
/**
 * this method call webservice for reject or cancel active order
 *
 * @param orderStatus in int
 * @param isCanceled  true for cancel order
 *//*

    private void rejectOrCancelDeliveryOrder(int orderStatus, boolean isCanceled) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
            jsonObject.put(Const.Params.DELIVERY_STATUS, orderStatus);
            if (isCanceled) {
                CancelReasons cancelReasons = new CancelReasons();
                cancelReasons.setCancelledAt(parseContent.webFormat.format(new Date()));
                cancelReasons.setCancelReason(cancelReason);
                UserDetail userDetail = new UserDetail();
                userDetail.setName(preferenceHelper.getFirstName() + " " + preferenceHelper
                        .getLastName());
                userDetail.setPhone(preferenceHelper.getPhoneNumber());
                userDetail.setCountryPhoneCode(preferenceHelper.getPhoneCountyCodeCode());
                userDetail.setId(preferenceHelper.getProviderId());
                userDetail.setEmail(preferenceHelper.getEmail());
                userDetail.setUniqueId(preferenceHelper.getUniqueId());
                cancelReasons.setUserDetail(userDetail);
                jsonObject.put(Const.Params.CANCEL_REASONS, ApiClient.JSONObject(cancelReasons));
            }

            AppLog.Log("REJECT_OR_CANCEL", jsonObject.toString());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.rejectOrCancelDelivery(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        onBackPressed();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ActiveDeliveryActivity.this);
                        goToHomeActivity();
                    }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });
    }

    */
/**
 * this method call webservice for set order status by provider
 *
 * @param orderStatus in int
 *//*

    private void setOrderStatus(int orderStatus) {

        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
            jsonObject.put(Const.Params.DELIVERY_STATUS, orderStatus);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderStatusResponse> responseCall = apiInterface.setRequestStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        int orderStatus = response.body().getDeliveryStatus();
                        AppLog.Log("ORDER_STATUS", orderStatus + "");
                        checkCurrentOderStatus(orderStatus);
                        loadOrderData(orderStatus);
                    } else {

                        Utils.showErrorToast(response.body().getErrorCode(),
                                ActiveDeliveryActivity.this);
                    }


            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });

    }

    */
/**
 * this method is set countDown timer for count a order accepting time
 *
 * @param seconds
 *//*

    private void startCountDownTimer(int seconds) {
        AppLog.Log("CountDownTimer", "Start");
        if (seconds <= 0) {
            rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED, false);
            return;
        }
        if (!isCountDownTimerStart) {
            isCountDownTimerStart = true;
            final long milliSecond = 1000;
            long millisUntilFinished = seconds * milliSecond;
            countDownTimer = null;
            if (preferenceHelper.getIsNewOrderSoundOn()) {
                soundHelper.playWhenNewOrderSound();
            } else {
                soundHelper.stopWhenNewOrderSound(ActiveDeliveryActivity.this);
            }
            countDownTimer = new CountDownTimer(millisUntilFinished, milliSecond) {

                public void onTick(long millisUntilFinished) {

                    final long seconds = millisUntilFinished / milliSecond;
                    tvOrderTimer.setText(String.valueOf(seconds) + "s");


                }

                public void onFinish() {
                    rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED, false);
                    isCountDownTimerStart = false;
                    soundHelper.stopWhenNewOrderSound(ActiveDeliveryActivity.this);
                }

            }.start();
        }
    }

    private void stopCountDownTimer() {
        AppLog.Log("CountDownTimer", "Stop");
        if (isCountDownTimerStart) {
            isCountDownTimerStart = false;
            countDownTimer.cancel();
            tvOrderTimer.setText("");
        }
        soundHelper.stopWhenNewOrderSound(ActiveDeliveryActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopCountDownTimer();
        unregisterReceiver(orderStatusReceiver);
    }


    private void updateUiOrderStatus(boolean isUpdate) {
        if (isUpdate) {
            llUpdateStatus.setVisibility(View.VISIBLE);
            flAccept.setVisibility(View.GONE);
        } else {
            llUpdateStatus.setVisibility(View.GONE);
            flAccept.setVisibility(View.VISIBLE);
        }
    }


    */
/**
 * this method is used to set marker on map
 *
 * @param currentLatLng LatLng
 * @param pickUpLatLng  LatLng
 * @param destLatLng    LatLng
 *//*

    private void setMarkerOnLocation(LatLng currentLatLng, LatLng pickUpLatLng, LatLng
            destLatLng) {
        BitmapDescriptor bitmapDescriptor;
        markerList.clear();
        boolean isBounce = false;
        if (currentLatLng != null) {
            if (providerMarker == null) {
              */
/*  providerMarker = googleMap.addMarker(new MarkerOptions().position(currentLatLng)
                        .title(getResources().getString(R.string
                                .text_my_location)
                        ));*//*

              //  providerMarker.setAnchor(0.5f, 0.5f);
             //   parseContent.downloadVehiclePin(this, providerMarker);
                isBounce = true;
            } else {
                animateMarkerToGB(providerMarker, currentLatLng, new LatLngInterpolator.Linear());
            }
            markerList.add(currentLatLng);

            if (pickUpLatLng != null) {
                if (pickUpMarker == null) {
                    */
/*bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(Utils
                            .drawableToBitmap
                                    (AppCompatResources.getDrawable(this, R
                                            .drawable.ic_pin_pickup)));
                    pickUpMarker = googleMap.addMarker(new MarkerOptions().position(pickUpLatLng)
                            .title(getResources().getString(R.string
                                    .text_pick_up_loc))
                            .icon(bitmapDescriptor));*//*

                } else {
                    pickUpMarker.setPosition(pickUpLatLng);
                }
            }
            if (destLatLng != null) {
                if (destinationMarker == null) {
                   */
/* bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(Utils
                            .drawableToBitmap
                                    (AppCompatResources.getDrawable(this, R
                                            .drawable.ic_pin_delivery)));
                    destinationMarker = googleMap.addMarker(new MarkerOptions().position(destLatLng)
                            .title(getResources().getString(R.string
                                    .text_drop_location))
                            .icon(bitmapDescriptor));*//*

                } else {

                    destinationMarker.setPosition(destLatLng);
                }
            }

            if (orderStatus == Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY) {
                if (destLatLng != null) {
                    markerList.add(destLatLng);
                }
                if (pickUpLatLng != null) {
                    markerList.add(pickUpLatLng);
                }
            } else if (orderStatus <= Const.ProviderStatus.DELIVERY_MAN_COMING) {
                if (pickUpLatLng != null) {
                    markerList.add(pickUpLatLng);
                }
            } else {
                if (destLatLng != null) {
                    markerList.add(destLatLng);
                }

            }
            if (isBounce) {
                try {
                    setLocationBounds(false, markerList);
                } catch (Exception e) {
                    AppLog.handleException(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);

                }
            }

        }

    }

    */
/**
 * this method used to animate marker on map smoothly
 *
 * @param marker             Marker
 * @param finalPosition      LatLag
 * @param latLngInterpolator LatLngInterpolator
 *//*

    private void animateMarkerToGB(final Marker marker, final LatLng finalPosition, final
    LatLngInterpolator latLngInterpolator) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(finalPosition.getLatitude(), finalPosition
                    .getLongitude());

     //       final float startRotation = marker.getRotation();
            final LatLngInterpolator interpolator = new LatLngInterpolator.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition,
                                endPosition);
                        marker.setPosition(newPosition);
                       // marker.setAnchor(0.5f, 0.5f);
                        if (getDistanceBetweenTwoLatLng(startPosition,
                                finalPosition) > LocationHelper.DISPLACEMENT) {
                            updateCamera(getBearing(startPosition, new LatLng(finalPosition
                                            .getLatitude(), finalPosition.getLongitude())),
                                    newPosition);
                        }


                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                }
            });
            valueAnimator.start();
        }
    }

    */
/**
 * this method update camera for map
 *
 * @param bearing        float
 * @param positionLatLng LatLng
 *//*

    private void updateCamera(float bearing, LatLng positionLatLng) {
        if (positionLatLng != null && isCameraIdeal) {
            isCameraIdeal = false;
          */
/*  CameraPosition oldPos = googleMap.getCameraPosition();
            CameraPosition pos = CameraPosition.builder(oldPos).bearing(bearing).target
                    (positionLatLng).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos), 3000,
                    new GoogleMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            isCameraIdeal = true;
                            AppLog.Log("CAMERA", "FINISH");

                        }

                        @Override
                        public void onCancel() {
                            isCameraIdeal = true;
                            AppLog.Log("CAMERA", "cancelling camera");

                        }
                    });*//*

        }
    }

    private void setLocationBounds(boolean isCameraAnim, ArrayList<LatLng> markerList) {
        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        int driverListSize = markerList.size();
        for (int i = 0; i < driverListSize; i++) {
            bounds.include(markerList.get(i));
        }
        //Change the padding as per needed
      */
/*  CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds.build(), getResources()
                .getDimensionPixelSize(R.dimen.dimen_map_bounce));

        if (isCameraAnim) {
            googleMap.animateCamera(cu);
        } else {
            googleMap.moveCamera(cu);
        }*//*

    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.getLatitude() - end.getLatitude());
        double lng = Math.abs(begin.getLongitude() - end.getLongitude());

        if (begin.getLatitude() < end.getLatitude() && begin.getLongitude() < end.getLongitude())
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.getLatitude() >= end.getLatitude() && begin.getLongitude() < end.getLongitude())
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.getLatitude() >= end.getLatitude() && begin.getLongitude() >= end.getLongitude())
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.getLatitude() < end.getLatitude() && begin.getLongitude() >= end.getLongitude())
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private float getDistanceBetweenTwoLatLng(LatLng startLatLng, LatLng endLatLang) {
        Location startLocation = new Location("start");
        Location endlocation = new Location("end");
        endlocation.setLatitude(endLatLang.getLatitude());
        endlocation.setLongitude(endLatLang.getLongitude());
        startLocation.setLatitude(startLatLng.getLatitude());
        startLocation.setLongitude(startLatLng.getLongitude());
        float distance = startLocation.distanceTo(endlocation);
        AppLog.Log("DISTANCE", distance + "in meters");
        return distance;

    }

    */
/**
 * this method set map camera at current location
 *
 * @param isAnimate ture for animate camera in map
 *//*

    public void moveCameraFirstMyLocation(boolean isAnimate) {

        currentLocation = locationHelper.getLastLocation();
        if (currentLocation != null) {
            currentLatLng = new LatLng(currentLocation.getLatitude(),
                    currentLocation.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentLatLng).zoom(17).build();
           */
/* if (isAnimate) {
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            } else {
                googleMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }*//*

        }
    }

    */
/**
 * this method call webservice for complete a active or running  order
 *//*

    private void completeOrder() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CompleteOrderResponse> responseCall = apiInterface.completeOrder(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CompleteOrderResponse>() {
            @Override
            public void onResponse(Call<CompleteOrderResponse> call,
                                   Response<CompleteOrderResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        Utils.showMessageToast(response.body().getMessage(),
                                ActiveDeliveryActivity.this);
                        CurrentOrder.getInstance().setCurrency(response.body().getCurrency());
                        */
/*goToCompleteOrderActivity(response.body().getOrderPayment(), response.body()
                                .getPaymentGatewayName(), userDetail, requestId, true, true);*//*

                        goToHistoryActivity();


                    } else {

                        Utils.showErrorToast(response.body().getErrorCode(),
                                ActiveDeliveryActivity.this);
                    }



            }

            @Override
            public void onFailure(Call<CompleteOrderResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);
            }
        });

    }

    private void openPickupDeliveryDialog() {
        if (order.isConfirmationCodeRequiredAtPickupDelivery()) {


            if (pickUpDeliveryDialog != null && pickUpDeliveryDialog.isShowing()) {
                return;
            }
            pickUpDeliveryDialog = new CustomDialogVerification
                    (this, getResources().getString(R.string.text_pickup_delivery),
                            getResources().getString(R.string.msg_enter_delivery_pick_up_code),
                            getResources()
                                    .getString(R.string.text_cancel), getResources().getString(R
                            .string.text_submit), null, getResources().getString(R.string
                            .text_confirmation_code), false, InputType.TYPE_CLASS_TEXT, InputType
                            .TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {


                    if (TextUtils.equals(etDialogEditTextTwo.getText().toString(), order
                            .getConfirmationCodeForPickUp())) {
                        setOrderStatus(orderStatus);
                        dismiss();
                    } else {
                        etDialogEditTextTwo.setError(getResources().getString(R.string
                                .error_code_660));
                        etDialogEditTextTwo.requestFocus();
                    }


                }
            };
            pickUpDeliveryDialog.show();
        } else {
            setOrderStatus(orderStatus);
        }

    }

    private void goToHistoryActivity(){
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void openCompleteDeliveryDialog() {
        if (order.isConfirmationCodeRequiredAtCompleteDelivery()) {

            if (completeDeliveryDialog != null && completeDeliveryDialog.isShowing()) {
                return;
            }

            completeDeliveryDialog = new CustomDialogVerification
                    (this, getResources().getString(R.string.text_complete_delivery),
                            getResources().getString(R.string.msg_enter_delivery_code),
                            getResources()

                                    .getString(R.string.text_cancel), getResources().getString(R
                            .string.text_submit), null, getResources().getString(R.string
                            .text_confirmation_code), false, InputType.TYPE_CLASS_TEXT, InputType
                            .TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {

                    if (TextUtils.equals(etDialogEditTextTwo.getText().toString(), order
                            .getConfirmationCodeForCompleteDelivery())) {
                        completeOrder();
                        dismiss();
                    } else {
                        etDialogEditTextTwo.setError(getResources().getString(R.string
                                .error_code_660));
                        etDialogEditTextTwo.requestFocus();
                    }

                }
            };
            completeDeliveryDialog.show();
        } else {
            completeOrder();
        }

    }

    */
/**
 * this method open Complete order activity witch is contain two fragment
 * Invoice Fragment and Feedback Fragment
 *
 * @param goToInvoiceFirst           set true when you want to open invoice fragment when
 *                                   activity open
 * @param backToActiveDeliveryActivity set true when you want
 *                                   goToActiveDeliveryActivity when press back
 *                                   button
 *//*

    private void goToCompleteOrderActivity(OrderPayment orderPayment, String payment, UserDetail
            userDetail, String requestId, boolean goToInvoiceFirst, boolean
                                                   backToActiveDeliveryActivity) {
        Intent intent = new Intent(this, CompleteOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Const.GO_TO_INVOICE, goToInvoiceFirst);
        bundle.putParcelable(Const.ORDER_PAYMENT, orderPayment);
        bundle.putString(Const.PAYMENT, payment);
        bundle.putParcelable(Const.USER_DETAIL, userDetail);
        bundle.putString(Const.Params.REQUEST_ID, requestId);
        intent.putExtra(Const.BUNDLE, bundle);
        intent.putExtra(Const.BACK_TO_ACTIVE_DELIVERY, backToActiveDeliveryActivity);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private boolean isOrderPickedUp(int orderStatus) {
        switch (orderStatus) {
            case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
            case Const.ProviderStatus.DELIVERY_MAN_COMING:
            case Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY:
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                return false;
            case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
            case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                return true;
            default:

                // do with default
                break;

        }
        return false;
    }

    private void loadOrderData(int orderStatus) {
        String name, image;
        if (isOrderPickedUp(orderStatus)) {
            name = userDetail.getFirstName() + " " + userDetail.getLastName();
            image = BASE_URL + userDetail.getImageUrl();
            tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
            llAddressOne.setVisibility(View.GONE);
            call = userDetail.getCountryPhoneCode() + userDetail.getPhone();
        } else {
            name = order.getStoreName();
            image = BASE_URL + order.getStoreImage();
            tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
            tvAddressOne.setText(order.getPickupAddresses().get(0).getAddress());
            llAddressOne.setVisibility(View.VISIBLE);
            call = order.getPickupAddresses().get(0).getUserDetails().getCountryPhoneCode() + order
                    .getPickupAddresses().get(0).getUserDetails().getPhone();
        }

        String requestNumber = getResources().getString(R.string.text_request_number)
                + " " + "#" + order.getRequestUniqueId();
        tvOrderNumber.setText(requestNumber);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        tvOrderId.setText(orderNumber);
        tvOrderId.setVisibility(View.VISIBLE);

        tvCustomerName.setText(name);
        Glide.with(ActiveDeliveryActivity.this).load(image).dontAnimate().placeholder
                (ResourcesCompat.getDrawable(
                        getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat.getDrawable(
                        getResources(), R.drawable.placeholder, null)).into
                (ivCustomerImage);
        ivCart.setVisibility(order.getOrderDetails().isEmpty() ? View.GONE : View.VISIBLE);
    }

    private void updateUiCancelOrder(boolean isEnable) {
        if (isEnable) {
            setToolbarRightIcon(R.drawable.ic_cancel, this);
        } else {
           */
/* if (TextUtils.isEmpty(order.getDestinationAddresses().get(0).getNote())) {
                ivToolbarRightIcon.setImageDrawable(null);
            } else {
                setToolbarRightIcon(R.drawable.ic_notepad, this);
            }*//*

            ivToolbarRightIcon.setImageDrawable(null);

        }

    }

    private void openProviderMessageDailog(){
        if(providerMessageDialog != null && providerMessageDialog.isShowing()){
            return;
        }
        RecyclerView rcvProviderMessage;
        final CustomFontTextView tvOther;
        final CustomFontEditTextView etOthersReason;

        providerMessageDialog = new Dialog(this);
        providerMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        providerMessageDialog.setContentView(R.layout.dialog_proider_message);
        tvOther = providerMessageDialog.findViewById(R.id.tvOther);
        etOthersReason = providerMessageDialog.findViewById(R.id.etOthersReason);
        rcvProviderMessage = providerMessageDialog.findViewById(R.id.rcvProviderMessage);


        providerMessageAdapter = new ProviderMessageAdapter(currentOrder.getProviderMessageList(),this) {
            @Override
            public void onMessageClick(int position) {
                providerMessage = currentOrder.getProviderMessageList().get(position);
                Log.e("aaa","proivderMesssge="+providerMessage);
                etOthersReason.setVisibility(View.VISIBLE);
                etOthersReason.setText(providerMessage);
            }
        };
        rcvProviderMessage.setLayoutManager(new LinearLayoutManager
                (this));
        rcvProviderMessage.setAdapter(providerMessageAdapter);
        rcvProviderMessage.addItemDecoration(new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL));

        tvOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etOthersReason.setVisibility(View.VISIBLE);
                etOthersReason.setText("");
            }
        });
        providerMessageDialog.findViewById(R.id.btnDialogAlertRight).setOnClickListener(new View
                .OnClickListener
                () {
            @Override
            public void onClick(View view) {

               providerMessage = etOthersReason.getText().toString();
               if(!TextUtils.isEmpty(providerMessage.trim())){
                   SendPushToUser();
               }else{
                   Utils.showToast(getResources().getString(R.string.msg_select_push_message), ActiveDeliveryActivity.this);
               }

            }
        });
        providerMessageDialog.findViewById(R.id.btnDialogAlertLeft).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View view) {
                providerMessageDialog.dismiss();
                providerMessage = "";
            }
        });



        WindowManager.LayoutParams params = providerMessageDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        providerMessageDialog.setCancelable(false);
        providerMessageDialog.show();

    }

    private void SendPushToUser() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
            jsonObject.put(Const.Params.MESSAGE, providerMessage);

            AppLog.Log("SEND_PUSH_TO_USER", jsonObject.toString());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.sendExtraTimePushToUser(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        providerMessageDialog.dismiss();

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ActiveDeliveryActivity.this);
                    }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });
    }

    private void openCancelOrderDialog() {
        if (orderCancelDialog != null && orderCancelDialog.isShowing()) {
            return;
        }
        RadioGroup radioGroup;
        final RadioButton rbReasonOne, rbReasonTwo, rbReasonOther;
        final CustomFontEditTextView etOtherReason;
        orderCancelDialog = new Dialog(this);
        orderCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        orderCancelDialog.setContentView(R.layout.dialog_cancel_order);
        rbReasonOne = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonOne);
        rbReasonTwo = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonTwo);
        rbReasonOther = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonOthers);
        radioGroup = (RadioGroup) orderCancelDialog.findViewById(R.id.radioGroup);
        etOtherReason = (CustomFontEditTextView) orderCancelDialog.findViewById(R.id
                .etOthersReason);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rbReasonOne:
                        etOtherReason.setVisibility(View.GONE);
                        cancelReason = rbReasonOne.getText().toString();
                        break;
                    case R.id.rbReasonTwo:
                        etOtherReason.setVisibility(View.GONE);
                        cancelReason = rbReasonTwo.getText().toString();
                        break;
                    case R.id.rbReasonOthers:
                        etOtherReason.setVisibility(View.VISIBLE);
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        });
        orderCancelDialog.findViewById(R.id.btnDialogAlertRight).setOnClickListener(new View
                .OnClickListener
                () {
            @Override
            public void onClick(View view) {
                if (rbReasonOther.isChecked()) {
                    cancelReason = etOtherReason.getText().toString();
                }
                if (!TextUtils.isEmpty(cancelReason)) {
                    rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_CANCELLED, true);
                    orderCancelDialog.dismiss();
                } else {
                    Utils.showToast(getResources().getString(R
                            .string.msg_plz_give_valid_reason), ActiveDeliveryActivity.this);
                }
            }
        });
        orderCancelDialog.findViewById(R.id.btnDialogAlertLeft).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View view) {
                orderCancelDialog.dismiss();
                cancelReason = "";
            }
        });
        WindowManager.LayoutParams params = orderCancelDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        orderCancelDialog.setCancelable(false);
        orderCancelDialog.show();

    }

    private List<Double> getLocationAsPerStatus(int orderStatus) {
        if (orderStatus == Const.ProviderStatus.DELIVERY_MAN_ACCEPTED || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_COMING || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_ARRIVED || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_NEW_DELIVERY) {
            return order.getPickupAddresses().get(0).getLocation();
        } else {
            return order.getDestinationAddresses().get(0).getLocation();
        }
    }

    */
/**
 * this method is used to open Google Map app whit given LatLng
 *//*

    private void goToGoogleMapApp(List<Double> locationList) {

        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + locationList.get
                (0) + "," + locationList.get(1));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_google_app_not_installed), this);
        }


    }

    */
/**
 * this method is used to open Waze Map app whit given LatLng
 *//*

    private void goToWazeMapApp(List<Double> locationList) {
        try {
            String url = "waze://?ll=" + locationList.get
                    (0) + "," + locationList.get(1) + "&navigate=yes";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Intent intent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
            Utils.showToast(getResources().getString(R.string.waze_map_msg), this);
        }
    }

    public void openPhotoMapDialog() {
        //Do the stuff that requires permission...
        if (mapDialog != null && mapDialog.isShowing()) {
            return;
        }
        mapDialog = new CustomPhotoDialog(this, getResources()
                .getString(R.string.text_choose_map), getResources()
                .getString(R.string.text_google_map), getResources()
                .getString(R.string.text_waze_map)) {
            @Override
            public void clickedOnCamera() {
                goToGoogleMapApp(getLocationAsPerStatus(orderStatus));
                dismiss();
            }

            @Override
            public void clickedOnGallery() {
                goToWazeMapApp(getLocationAsPerStatus(orderStatus));
                dismiss();
            }
        };
        mapDialog.show();

    }

    public void openOrderNoteDialog() {
        if (noteDialog != null && noteDialog.isShowing()) {
            return;
        }

        noteDialog = new CustomDialogAlert(this, getResources().getString(R.string.text_note),
                order.getDestinationAddresses().get(0).getNote(), "",
                getResources().getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        noteDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        locationHelper.setOpenGpsDialog(false);
                        moveCameraFirstMyLocation(true);
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        locationHelper.setOpenGpsDialog(false);
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    public void makePhoneCall(String phone) {
       */
/* if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission
                    .CALL_PHONE}, Const
                    .PERMISSION_FOR_CALL);
        } else {*//*

            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phone));
            startActivity(intent);
      //  }


    }

    private void openCallPermissionDialog() {

        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(ActiveDeliveryActivity.this, new
                        String[]{android.Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                dismiss();
            }

        };
        customDialogAlert.show();
    }

    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            makePhoneCall(call);

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ActiveDeliveryActivity.this, android.Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                default:
                    //do with default
                    break;
            }
        }

    }

    private void setOrderDetail() {
        mapView.setVisibility(View.GONE);
        llOrderDetail.setVisibility(View.VISIBLE);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        String deliveryNote = getResources().getString(R.string.text_order_delivery_note)
                + " " + order.getDestinationAddresses().get(0).getNote();
       // tvOrderDetailNumber.setText(orderNumber);
        if (TextUtils.isEmpty(order.getDestinationAddresses().get(0).getNote())) {
            tvDeliveryNote.setVisibility(View.GONE);
        }else{
            tvDeliveryNote.setText(deliveryNote);
        }
        rcvOrderDetailList.setLayoutManager(new LinearLayoutManager(this));
        OrderDetailsAdapter itemAdapter = new OrderDetailsAdapter(this, order.getOrderDetails(),
                order.getCurrency(), false);
        rcvOrderDetailList.setAdapter(itemAdapter);
        rcvOrderDetailList.addItemDecoration(new PinnedHeaderItemDecoration());
    }

    private void openCartDetailDialog() {
        if (cartDetailDialog != null && cartDetailDialog.isShowing()) {
            return;
        }

        cartDetailDialog = new Dialog(this);
        cartDetailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cartDetailDialog.setContentView(R.layout.dialog_cart_detail);
        CustomFontTextView tvOrderNumber = (CustomFontTextView) cartDetailDialog.findViewById(R
                .id.tvOrderNumber);
        CustomFontTextView tvDeliveryNote = (CustomFontTextView) cartDetailDialog.findViewById(R
                .id.tvDeliveryNote);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        String deliveryNote = getResources().getString(R.string.text_order_delivery_note)
                + " " + order.getDestinationAddresses().get(0).getNote();
        tvOrderNumber.setText(orderNumber);
        if (TextUtils.isEmpty(order.getDestinationAddresses().get(0).getNote())) {
            tvDeliveryNote.setVisibility(View.GONE);
        }else{
            tvDeliveryNote.setText(deliveryNote);
        }
        RecyclerView rcvOrderProductItem = (RecyclerView) cartDetailDialog.findViewById(R.id
                .rcvOrderProductItem);
        rcvOrderProductItem.setLayoutManager(new LinearLayoutManager(this));
        OrderDetailsAdapter itemAdapter = new OrderDetailsAdapter(this, order.getOrderDetails(),
                order.getCurrency(), false);
        rcvOrderProductItem.setAdapter(itemAdapter);
        rcvOrderProductItem.addItemDecoration(new PinnedHeaderItemDecoration());
        cartDetailDialog.findViewById(R.id.btnDone).setOnClickListener(new View
                .OnClickListener() {

            @Override
            public void onClick(View v) {
                cartDetailDialog.dismiss();
            }
        });


        WindowManager.LayoutParams params = cartDetailDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        cartDetailDialog.setCancelable(false);
        cartDetailDialog.show();

    }

    protected void openDeliveryOrderItemConfirm() {
        if (orderItemConfirmDialog != null && orderItemConfirmDialog.isShowing()) {
            return;
        }
        orderItemConfirmDialog = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_order_item_confirm), this.getResources()
                .getString(R.string.msg_order_item_confirm), this.getResources()
                .getString(R.string.text_cancel), this.getResources()
                .getString(R.string.text_confirm)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                dismiss();
                openPickupDeliveryDialog();
            }
        };
        orderItemConfirmDialog.show();
    }

    private class OrderStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            goToHomeActivity();
        }
    }

    private void goToAvailableDelivery() {
        Intent intent = new Intent(this, AvailableDeliveryActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
*/


package com.edelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.edelivery.adapter.OrderDetailsAdapter;
import com.edelivery.adapter.ProviderMessageAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomEventMapView;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.component.CustomMapdialog;
import com.edelivery.component.MapBoxMapReady;
import com.edelivery.interfaces.LatLngInterpolator;
import com.edelivery.models.datamodels.CancelReasons;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.UserDetail;
import com.edelivery.models.responsemodels.CompleteOrderResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OrderStatusResponse;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.SoundHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.gson.Gson;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapOverlay;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.BuildConfig.BASE_URL;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

public class ActiveDeliveryActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived, PermissionsListener, OnMapReadyCallback, MapBoxMapReady {

    private LocationHelper locationHelper;
    private CustomEventMapView mapView;
    private CustomFontTextView tvOrderTimer, tvEstTime, tvEstDistance, tvAddressTwo,
            tvDeliveryDate, tvDeliveryStatus, tvAddressOne, tvOrderNumber, tvProviderProfit,
            tvOrderId;
    private CustomFontTextViewTitle tvCustomerName;
    private CustomFontButton btnRejectOrder, btnAcceptOrder, btnJobStatus;
    private ImageView ivNavigate, ivCustomerImage, ivActiveTargetLocation, ivCall, ivCart,ivMessage;
    //private GoogleMap googleMap;
    private String requestId;
    private int orderStatus = Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY;
    private boolean isCountDownTimerStart;
    private CountDownTimer countDownTimer;
    private FrameLayout flAccept;
    private LinearLayout llUpdateStatus, llAddressOne;
    private Order order;
    private UserDetail userDetail;
    private Location lastLocation, currentLocation;
    private LatLng currentLatLng, pickUpLatLng, deliveryLatLng;
    private ArrayList<LatLng> markerList = new ArrayList<>();
    private Marker providerMarker, pickUpMarker, destinationMarker;

    private String cancelReason,providerMessage;
    private SoundHelper soundHelper;
    private CustomDialogAlert noteDialog, orderItemConfirmDialog;
    private CustomMapdialog mapDialog;
    private Dialog orderCancelDialog, cartDetailDialog,providerMessageDialog;
    private CustomDialogVerification pickUpDeliveryDialog, completeDeliveryDialog;
    private OrderStatusReceiver orderStatusReceiver;
    private String call;
    private boolean isCameraIdeal = true;

    private CustomFontTextView tvOrderDetailNumber,tvDeliveryNote,tvDialogAlertTitle;
    private RecyclerView rcvOrderDetailList;
    private LinearLayout llOrderDetail;
    private ProviderMessageAdapter providerMessageAdapter;
    private ArrayList<String> messageList = new ArrayList<>();





    private MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    public ActiveDeliveryActivity.MainActivityLocationCallback callback = new ActiveDeliveryActivity.MainActivityLocationCallback(ActiveDeliveryActivity.this);
    private MapView mapbox;
    public com.mapbox.mapboxsdk.geometry.LatLng storeLatLng;
    private boolean isCurrentLocationGet = false;
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    String str_subAdminarea11 = null;

    //for here map
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private AndroidXMapFragment mapFragment = null;
    private Map map = null;
    RelativeLayout relativeLayout;
    MapGesture.OnGestureListener listener;
    private MapOverlay mapOverlay;
    View myview;

    String TAG="ActiveDeliveryActivity";

    MapMarker providermarkerh,pickupmarkerh,destinationmarkerh;

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapview);
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     /*   Mapbox.getInstance(ActiveDeliveryActivity.this, getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));*/
        setContentView(R.layout.activity_active_delivery);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /*mapbox = findViewById(R.id.mapBoxView);
        mapbox.onCreate(savedInstanceState);
        new CustomEventMapBoxView(ActiveDeliveryActivity.this, mapbox, this);*/
        Log.i("setheremp","ok");
        checkPermissions();
        getExtraData();
        initToolBar();
        findViewById();
        setViewListener();
        setTitleOnToolBar(getResources().getString(R.string.text_active_delivery));
        initLocationHelper();


     /*   mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);*/
        markerList = new ArrayList<>();
        soundHelper = SoundHelper.getInstance(this);
        orderStatusReceiver = new OrderStatusReceiver();
        messageList = new ArrayList<>();

    }

    @Override
    public void onStart() {
        super.onStart();
        //mapbox.onStart();
        moveCameraFirstMyLocation();
        registerReceiver(orderStatusReceiver, new IntentFilter(Const.Action
                .ACTION_STORE_CANCELED_REQUEST));
    }


    @Override
    protected void onResume() {
        super.onResume();
        // mapbox.onResume();
        moveCameraFirstMyLocation();
        locationHelper.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // mapbox.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mapbox.onLowMemory();
    }



    @Override
    protected void onPause() {
        // mapbox.onPause();
        super.onPause();
        locationHelper.onStop();
    }

    @Override
    protected void onDestroy() {
        // mapbox.onDestroy();
        super.onDestroy();
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        //   mapView = (CustomEventMapView) findViewById(R.id.mapViewActiveDelivery);
        tvOrderTimer = (CustomFontTextView) findViewById(R.id.tvOrderAcceptTimer);
        btnAcceptOrder = (CustomFontButton) findViewById(R.id.btnAcceptOrder);
        btnRejectOrder = (CustomFontButton) findViewById(R.id.btnRejectOrder);
        tvEstDistance = (CustomFontTextView) findViewById(R.id.tvEstDistance);
        tvEstTime = (CustomFontTextView) findViewById(R.id.tvEstTime);
        flAccept = (FrameLayout) findViewById(R.id.flAccept);
        llUpdateStatus = (LinearLayout) findViewById(R.id.llUpdateStatus);

        btnJobStatus = (CustomFontButton) findViewById(R.id.btnJobStatus);
        ivNavigate = (ImageView) findViewById(R.id.ivNavigate);
        ivCustomerImage = (ImageView) findViewById(R.id.ivCustomerImage);
        tvAddressTwo = (CustomFontTextView) findViewById(R.id.tvAddressTwo);
        tvCustomerName = (CustomFontTextViewTitle) findViewById(R.id.tvCustomerName);
        tvDeliveryDate = (CustomFontTextView) findViewById(R.id.tvDeliveryDate);
        tvDeliveryStatus = (CustomFontTextView) findViewById(R.id.tvDeliveryStatus);
        tvAddressOne = (CustomFontTextView) findViewById(R.id.tvAddressOne);
        tvDeliveryDate.setVisibility(View.GONE);
        tvDeliveryStatus.setVisibility(View.GONE);
        ivActiveTargetLocation = (ImageView) findViewById(R.id.ivActiveTargetLocation);
        llAddressOne = (LinearLayout) findViewById(R.id.llAddressOne);
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        tvOrderId = findViewById(R.id.tvOrderId);
        tvProviderProfit = (CustomFontTextView) findViewById(R.id.tvProviderProfit);
        ivCall = (ImageView) findViewById(R.id.ivCall);
        ivCall.setVisibility(View.VISIBLE);
        ivCart = (ImageView) findViewById(R.id.ivCart);
        ivMessage = findViewById(R.id.ivMessage);
        if(preferenceHelper.getIsProviderSendBusyMessage()) {
            ivMessage.setVisibility(View.VISIBLE);
        }else{
            ivMessage.setVisibility(View.GONE);
        }
        //   tvOrderDetailNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        tvDeliveryNote = (CustomFontTextView) findViewById(R.id.tvDeliveryNote);
        tvDialogAlertTitle = (CustomFontTextView)findViewById(R.id.tvDialogAlertTitle);
        llOrderDetail = (LinearLayout) findViewById(R.id.llOrderCartDetail);
        rcvOrderDetailList = (RecyclerView) findViewById(R.id.rcvOrderProductItem);
    }

    @Override
    protected void setViewListener() {
        // do somethings
        btnRejectOrder.setOnClickListener(this);
        btnAcceptOrder.setOnClickListener(this);
        btnJobStatus.setOnClickListener(this);
        ivNavigate.setOnClickListener(this);
        ivActiveTargetLocation.setOnClickListener(this);
        tvDeliveryStatus.setVisibility(View.GONE);
        ivCall.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        if (lastLocation == null) {
            lastLocation = currentLocation;
        }


        if(currentLatLng!=null)
        {
           // setMarkerOnLocation(currentLatLng, pickUpLatLng, deliveryLatLng);
            getRequestStatus();
        }
        lastLocation = currentLocation;
    }

    @Override
    public void onConnected(Bundle bundle) {
        AppLog.Log(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, "GoogleClientConnected");
        moveCameraFirstMyLocation();

        locationHelper.startLocationUpdate();
        getRequestStatus();

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, "GoogleClientConnectionFailed");
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLog.Log(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, "GoogleConnectionSuspended");
    }

    private void initLocationHelper() {
        locationHelper = new LocationHelper(getApplicationContext());
        locationHelper.setLocationReceivedLister(this);
    }

   /* @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setUpMap();
        locationHelper.onStart();
    }*/

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnAcceptOrder:
                setOrderStatus(Const.ProviderStatus.DELIVERY_MAN_ACCEPTED);
                break;
            case R.id.btnRejectOrder:
                rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED, false);
                break;
            case R.id.ivActiveTargetLocation:
               /* if (!markerList.isEmpty()) {
                    setLocationBounds(false, markerList);
                }*/
                /*if (!locationHelper.isOpenGpsDialog()) {
                    locationHelper.setOpenGpsDialog(true);
                    locationHelper.setLocationSettingRequest(ActiveDeliveryActivity.this, REQUEST_CHECK_SETTINGS,
                            new OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    moveCameraFirstMyLocation();
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                                @Override
                                public void noFound() {
                                    moveCameraFirstMyLocation();
                                }
                            });
                }*/

                locationHelper.setOpenGpsDialog(true);
                moveCameraFirstMyLocation();

                break;
            case R.id.btnJobStatus:
                if (orderStatus == Const.ProviderStatus.FINAL_ORDER_COMPLETED) {
                    openCompleteDeliveryDialog();
                } else if (orderStatus == Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER) {
                    openPickupDeliveryDialog();
                    // openDeliveryOrderItemConfirm();
                } else {
                    setOrderStatus(orderStatus);
                }
                break;
            case R.id.ivToolbarRightIcon:
                switch (orderStatus) {
                    case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
                    case Const.ProviderStatus.DELIVERY_MAN_COMING:
                    case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
                    case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                        openCancelOrderDialog();
                        break;
                    default:
                        openOrderNoteDialog();
                        break;
                }

                break;
            case R.id.ivNavigate:
                openPhotoMapDialog();
                break;

            case R.id.ivCall:
                makePhoneCall(call);
                break;
            case R.id.ivCart:
                openCartDetailDialog();
                break;
            case R.id.ivMessage:
                // if(!currentOrder.getProviderMessageList().isEmpty()) {
                openProviderMessageDailog();
                //  }
                Log.d("aaa","ProvideMessageList=="+currentOrder.getProviderMessageList().size());
                break;
            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
/*
    private void setUpMap() {
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }*/

    private void getExtraData() {
        if (getIntent() != null) {
            requestId = getIntent().getExtras().getString(Const.Params.ORDER_ID);
        }
    }

    /**
     * this method call webservice for get current active order status
     */
    private void getRequestStatus() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID,requestId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderStatusResponse> responseCall = apiInterface.getRequestStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse>
                    response) {

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) &&response.body().isSuccess())
                {

                    Gson gson = new Gson();
                    AppLog.Log("GET_ORDER_STATUS", gson.toJson(response.body()));
                    order = response.body().getOrderRequest();
                    userDetail = response.body().getUserDetail();
                    loadOrderData(order.getDeliveryStatus());

                 /*   String str_TotalTime= String.valueOf(order.getTotalTime());
                    if(!str_TotalTime.isEmpty())
                    {
                        tvEstTime.setText(Utils.CalculateTImeSplit(order.getTotalTime()));
                       // tvEstTime.setText(Utils.secondToHoursMinutesSeconds(Double.parseDouble(str_TotalTime)));
                    }
*/





                    String str_TotalTime= String.valueOf(order.getTotal_sec());
                    if(str_TotalTime!=null && !str_TotalTime.equals(""))
                    {
                        Log.d("formate","=========== Formate ========="+Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime)));
                        tvEstTime.setText(Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime)));
                    }






                    String unit;
                    if(order.isIs_distance_unit_mile())
                    {
                        unit="mile";
                    }
                    else {
                        unit = "mile";
                    }
                    Log.d("call","=====> "+order.isIs_distance_unit_mile());

                    String str_TotalDistance= String.valueOf(order.getTotalDistance());
                    if(str_TotalDistance!=null && !str_TotalDistance.equals(""))
                    {
                        tvEstDistance.setText(parseContent.decimalTwoDigitFormat.format(order
                                .getTotalDistance())+unit);
                    }




                    tvProviderProfit.setText(order.getCurrency() + parseContent.decimalTwoDigitFormat.format(order.getTotalProviderIncome()));

                    Log.d("profit","--- Profit ------> "+order.getTotalProviderIncome());
                    checkCurrentOderStatus(order.getDeliveryStatus());
                    pickUpLatLng = new LatLng(order.getPickupAddresses().get(0).getLocation()
                            .get(0), order.getPickupAddresses().get(0).getLocation()
                            .get(1));
                    deliveryLatLng = new LatLng(order.getDestinationAddresses().get(0)
                            .getLocation()
                            .get(0), order.getDestinationAddresses().get(0).getLocation()
                            .get(1));
                    setMarkerOnLocation(currentLatLng, pickUpLatLng, deliveryLatLng);

                } else {
                    if(response.body()!=null)
                    {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ActiveDeliveryActivity.this);
                        goToHomeActivity();
                    }else {
                       /* Utils.showToast(getString(R.string.something_went_wrong),ActiveDeliveryActivity.this);*/
                    }


                }

            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);
            }
        });
    }

    /**
     * this method used to manage UI according to order status
     *
     * @param orderStatus int
     */
    private void checkCurrentOderStatus(int orderStatus) {
        switch (orderStatus) {
            case Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY:
                updateUiCancelOrder(false);
                updateUiOrderStatus(false);
                startCountDownTimer(order.getTimeLeftToRespondsTrip());

                break;
            case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
                /*this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_COMING;
                updateUiOrderStatus(true);
                updateUiCancelOrder(true);
                break;*/
            case Const.ProviderStatus.DELIVERY_MAN_COMING:
                /*this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_ARRIVED;
                btnJobStatus.setText(getResources().getString(R.string.text_tap_here_to_arrive));
                updateUiOrderStatus(true);
                updateUiCancelOrder(true);
                break;*/
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER;
                stopCountDownTimer();
                //openCartDetailDialog();
                setOrderDetail();
                //  btnJobStatus.setText(getResources().getString(R.string.text_tap_here_to_pickup));
                updateUiOrderStatus(true);
                updateUiCancelOrder(true);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
                this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY;
                // mapbox.setVisibility(View.VISIBLE);
                moveCameraFirstMyLocation();
                moveCameraFirstMyLocation();

                llOrderDetail.setVisibility(View.GONE);
                tvDialogAlertTitle.setVisibility(View.GONE);
                tvDeliveryNote.setVisibility(View.GONE);
                btnJobStatus.setText(getResources().getString(R.string.text_tap_here_to_start));

                moveCameraFirstMyLocation();
                moveCameraFirstMyLocation();
              //  ivCall.setVisibility(View.GONE);
                updateUiOrderStatus(true);
                updateUiCancelOrder(false);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
                this.orderStatus = Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION;
                btnJobStatus.setText(getResources().getString(R.string
                        .text_tap_here_to_arrive_destination));
             //ivCall.setVisibility(View.VISIBLE);
                updateUiOrderStatus(true);
                updateUiCancelOrder(false);
                break;
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                this.orderStatus = Const.ProviderStatus.FINAL_ORDER_COMPLETED;
                btnJobStatus.setText(getResources().getString(R.string
                        .text_tap_here_to_end));
                updateUiOrderStatus(true);
                updateUiCancelOrder(false);
                break;
            case Const.ProviderStatus.STORE_CANCELLED_REQUEST:
                Utils.showToast(getResources().getString(R.string.cancel_reuqtest_by_user),ActiveDeliveryActivity.this);
                goToHomeActivity();
                break;
            default:
                // do with default
                break;

        }
        showPickupTimeAndDate();
    }



    private void showPickupTimeAndDate() {
        if (!TextUtils.isEmpty(order.getEstimatedTimeForReadyOrder())
                && (orderStatus == Const.ProviderStatus
                .DELIVERY_MAN_NEW_DELIVERY || orderStatus == Const.ProviderStatus
                .DELIVERY_MAN_ACCEPTED || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_COMING || orderStatus
                == Const.ProviderStatus.DELIVERY_MAN_ARRIVED)) {
            try {
                Date date = parseContent.webFormat.parse(order
                        .getEstimatedTimeForReadyOrder
                                ());
                tvDeliveryDate.setText(getResources().getString(R.string
                        .text_pick_up_order_after) + " " + parseContent.dateTimeFormat.format
                        (date));
                tvDeliveryDate.setVisibility(View.VISIBLE);
            } catch (ParseException e) {
                AppLog.handleException(ActiveDeliveryActivity.class.getName(), e);
            }

        } else {
            tvDeliveryDate.setVisibility(View.GONE);
        }
    }

    /**
     * this method call webservice for reject or cancel active order
     *
     * @param orderStatus in int
     * @param isCanceled  true for cancel order
     */
    private void rejectOrCancelDeliveryOrder(int orderStatus, boolean isCanceled) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
            jsonObject.put(Const.Params.DELIVERY_STATUS, orderStatus);
            if (isCanceled) {
                CancelReasons cancelReasons = new CancelReasons();
                cancelReasons.setCancelledAt(parseContent.webFormat.format(new Date()));
                cancelReasons.setCancelReason(cancelReason);
                UserDetail userDetail = new UserDetail();
                userDetail.setName(preferenceHelper.getFirstName() + " " + preferenceHelper
                        .getLastName());
                userDetail.setPhone(preferenceHelper.getPhoneNumber());
                userDetail.setCountryPhoneCode(preferenceHelper.getPhoneCountyCodeCode());
                userDetail.setId(preferenceHelper.getProviderId());
                userDetail.setEmail(preferenceHelper.getEmail());
                userDetail.setUniqueId(preferenceHelper.getUniqueId());
                cancelReasons.setUserDetail(userDetail);
                jsonObject.put(Const.Params.CANCEL_REASONS, ApiClient.JSONObject(cancelReasons));
            }

            AppLog.Log("REJECT_OR_CANCEL", jsonObject.toString());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.rejectOrCancelDelivery(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    onBackPressed();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(),
                            ActiveDeliveryActivity.this);
                    goToHomeActivity();
                }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });
    }

    /**
     * this method call webservice for set order status by provider
     *
     * @param orderStatus in int
     */
    private void setOrderStatus(int orderStatus) {
        AppLog.Log("ORDER_STATUS", orderStatus + "");
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
            jsonObject.put(Const.Params.DELIVERY_STATUS, orderStatus);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }
        AppLog.Log("ORDER_STATUS", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderStatusResponse> responseCall = apiInterface.setRequestStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    int orderStatus = response.body().getDeliveryStatus();
                    AppLog.Log("ORDER_STATUS", orderStatus + "");
                    checkCurrentOderStatus(orderStatus);
                    loadOrderData(orderStatus);
                } else {

                    Utils.showErrorToast(response.body().getErrorCode(),
                            ActiveDeliveryActivity.this);
                }


            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });

    }

    /**
     * this method is set countDown timer for count a order accepting time
     *
     * @param seconds
     */
    private void startCountDownTimer(int seconds) {
        AppLog.Log("CountDownTimer", "Start");
        if (seconds <= 0) {
            rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED, false);
            return;
        }
        if (!isCountDownTimerStart) {
            isCountDownTimerStart = true;
            final long milliSecond = 1000;
            long millisUntilFinished = seconds * milliSecond;
            countDownTimer = null;
            if (preferenceHelper.getIsNewOrderSoundOn()) {
                soundHelper.playWhenNewOrderSound();
            } else {
                soundHelper.stopWhenNewOrderSound(ActiveDeliveryActivity.this);
            }
            countDownTimer = new CountDownTimer(millisUntilFinished, milliSecond) {

                public void onTick(long millisUntilFinished) {

                    final long seconds = millisUntilFinished / milliSecond;
                    tvOrderTimer.setText(String.valueOf(seconds) + "s");


                }

                public void onFinish() {
                    rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_REJECTED, false);
                    isCountDownTimerStart = false;
                    soundHelper.stopWhenNewOrderSound(ActiveDeliveryActivity.this);
                }

            }.start();
        }
    }

    private void stopCountDownTimer() {
        AppLog.Log("CountDownTimer", "Stop");
        if (isCountDownTimerStart) {
            isCountDownTimerStart = false;
            countDownTimer.cancel();
            tvOrderTimer.setText("");
        }
        soundHelper.stopWhenNewOrderSound(ActiveDeliveryActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        mapbox.onStop();

        stopCountDownTimer();
        unregisterReceiver(orderStatusReceiver);
    }


    private void updateUiOrderStatus(boolean isUpdate)
    {
        if (isUpdate)
        {
            llUpdateStatus.setVisibility(View.VISIBLE);
            flAccept.setVisibility(View.GONE);
        } else {
            llUpdateStatus.setVisibility(View.GONE);
            flAccept.setVisibility(View.VISIBLE);
        }
    }


    /**
     * this method is used to set marker on map
     *
     * @param currentLatLng LatLng
     * @param pickUpLatLng  LatLng
     * @param destLatLng    LatLng
     */
    private void setMarkerOnLocation(LatLng currentLatLng, LatLng pickUpLatLng, LatLng
            destLatLng) {

        Log.i("setMarkerOnLocation","currentLatLng"+currentLatLng.toString());
        Log.i("setMarkerOnLocation","pickUpLatLng"+pickUpLatLng.toString());
        Log.i("setMarkerOnLocation","destLatLng"+destLatLng.toString());
        markerList.clear();
        boolean isBounce = false;
        if (currentLatLng != null)
        {
            // moveCameraFirstMyLocation();
            if (providermarkerh == null)
            {
                if(map!=null) {
                   /* providerMarker = mapboxMap.addMarker(new MarkerOptions().position(currentLatLng)
                            .title(getResources().getString(R.string
                                    .text_my_location)
                            ));*/

                }
                //  providerMarker.setAnchor(0.5f, 0.5f);
                providermarkerh=new MapMarker();
                providermarkerh.setCoordinate(new GeoCoordinate(currentLatLng.getLatitude(),currentLatLng.getLongitude()));
                providermarkerh.setTitle(getResources().getString(R.string.text_my_location));
                map.addMapObject(providermarkerh);
                downloadVehiclePin(this, providermarkerh);
                isBounce = true;
            } else {
                // animateMarkerToGB(providerMarker, currentLatLng, new LatLngInterpolator.Linear());
            }
            markerList.add(currentLatLng);

            if (pickUpLatLng != null)
            {
                if (pickupmarkerh == null)
                {
                    /*IconFactory iconFactory = IconFactory.getInstance(ActiveDeliveryActivity.this);
                    Icon icon = iconFactory.fromResource(R.drawable.ic_pin_pickup);*/

                    Image image = new Image();
                    try {
                        image.setImageResource(R.drawable.ic_pin_pickup);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if(map!=null)
                    {
                /*    pickUpMarker = mapboxMap.addMarker(new MarkerOptions().position(pickUpLatLng)
                            .title(getResources().getString(R.string
                                    .text_pick_up_loc))
                            .icon(icon));*/

                        pickupmarkerh=new MapMarker();
                        pickupmarkerh.setIcon(image);
                        pickupmarkerh.setCoordinate(new GeoCoordinate(pickUpLatLng.getLatitude(),pickUpLatLng.getLongitude()));
                        pickupmarkerh.setTitle(getResources().getString(R.string.text_pick_up_loc));
                        map.addMapObject(pickupmarkerh);


                    }
                }

                else {
                    pickupmarkerh.setCoordinate(new GeoCoordinate(pickUpLatLng.getLatitude(),pickUpLatLng.getLongitude()));
                }
            }

            if (destLatLng != null)
            {
                if (destinationmarkerh == null) {

                   /* IconFactory iconFactory = IconFactory.getInstance(ActiveDeliveryActivity.this);
                    Icon icon = iconFactory.fromResource(R.drawable.ic_pin_delivery);*/

                    Image image = new Image();
                    try {
                        image.setImageResource(R.drawable.ic_pin_delivery);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if(map!=null) {
                        /*destinationMarker = mapboxMap.addMarker(new MarkerOptions().position(destLatLng)
                                .title(getResources().getString(R.string
                                        .text_drop_location))
                                .icon(icon));*/

                        destinationmarkerh=new MapMarker();
                        destinationmarkerh.setCoordinate(new GeoCoordinate(destLatLng.getLatitude(),destLatLng.getLongitude()));
                        destinationmarkerh.setIcon(image);
                        destinationmarkerh.setTitle(getResources().getString(R.string.text_drop_location));
                        map.addMapObject(destinationmarkerh);

                    }
                } else {

                    destinationmarkerh.setCoordinate(new GeoCoordinate(destLatLng.getLatitude(),destLatLng.getLongitude()));
                }
            }

            if (orderStatus == Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY) {
                if (destLatLng != null)
                {
                    markerList.add(destLatLng);
                }
                if (pickUpLatLng != null) {
                    markerList.add(pickUpLatLng);
                }
            } else if (orderStatus <= Const.ProviderStatus.DELIVERY_MAN_COMING) {
                if (pickUpLatLng != null) {
                    markerList.add(pickUpLatLng);
                }
            } else {
                if (destLatLng != null) {
                    markerList.add(destLatLng);
                }

            }

            if (isBounce) {
                try {
                    setLocationBounds(false, markerList);
                } catch (Exception e) {
                    AppLog.handleException(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);

                }
            }

        }

    }

    /**
     * this method used to animate marker on map smoothly
     *
     * @param marker             Marker
     * @param finalPosition      LatLag
     * @param latLngInterpolator LatLngInterpolator
     */
    private void animateMarkerToGB(final Marker marker, final LatLng finalPosition, final
    LatLngInterpolator latLngInterpolator) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(finalPosition.getLatitude(), finalPosition
                    .getLongitude());

            //   final float startRotation = marker.getRotation();
           /* final LatLngInterpolator interpolator = new LatLngInterpolator.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition,
                                endPosition);
                        marker.setPosition(newPosition);
                       // marker.setAnchor(0.5f, 0.5f);
                        if (getDistanceBetweenTwoLatLng(startPosition,
                                finalPosition) > LocationHelper.DISPLACEMENT) {
                       *//*     updateCamera(getBearing(startPosition, new LatLng(finalPosition
                                            .getLatitude(), finalPosition.getLongitude())),
                                    newPosition);*//*
                        }


                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                }
            });
            valueAnimator.start();*/
        }
    }

    /**
     * this method update camera for map
     *

     */
  /*  private void updateCamera(float bearing, LatLng positionLatLng) {
        if (positionLatLng != null && isCameraIdeal) {
            isCameraIdeal = false;
            CameraPosition oldPos = mapboxMap.getCameraPosition();
            CameraPosition pos = new CameraPosition.Builder(oldPos).bearing(bearing).target
                    (positionLatLng).build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos), 3000,
                    new MapboxMap.CancelableCallback()
                    {

                        @Override
                        public void onFinish() {
                            isCameraIdeal = true;
                            AppLog.Log("CAMERA", "FINISH");

                        }

                        @Override
                        public void onCancel() {
                            isCameraIdeal = true;
                            AppLog.Log("CAMERA", "cancelling camera");

                        }
                    });
        }
    }*/

    private void setLocationBounds(boolean isCameraAnim, ArrayList<LatLng> markerList) {
        /*LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        int driverListSize = markerList.size();
        for (int i = 0; i < driverListSize; i++) {
            bounds.include(markerList.get(i));
        }*/

        GeoCoordinate start,end;
        start= new GeoCoordinate(markerList.get(0).getLatitude(),markerList.get(0).getLongitude());
        end=new GeoCoordinate(markerList.get(1).getLatitude(),markerList.get(1).getLongitude());
        Log.i("box",start+","+end);
        GeoBoundingBox boundingBox=new GeoBoundingBox(start,end);
        Log.i("topleft",boundingBox.getTopLeft().toString());
        map.setTransformCenter(new PointF(150,120));

        map.setCenter(start,Map.Animation.LINEAR);


        // map.setPadding(10,10,10,10);
        map.setZoomLevel(12);

       /* //Change the padding as per needed
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds.build(), getResources()
                .getDimensionPixelSize(R.dimen.dimen_map_bounce));

        if (isCameraAnim) {
            if(mapboxMap!=null)
            {
            mapboxMap.animateCamera(cu);
        }}
            else {
            if(mapboxMap!=null) {

                mapboxMap.moveCamera(cu);
            }
        }*/
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.getLatitude() - end.getLatitude());
        double lng = Math.abs(begin.getLongitude() - end.getLongitude());

        if (begin.getLatitude() < end.getLatitude() && begin.getLongitude() < end.getLongitude())
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.getLatitude() >= end.getLatitude() && begin.getLongitude() < end.getLongitude())
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.getLatitude() >= end.getLatitude() && begin.getLongitude() >= end.getLongitude())
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.getLatitude() < end.getLatitude() && begin.getLongitude() >= end.getLongitude())
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private float getDistanceBetweenTwoLatLng(LatLng startLatLng, LatLng endLatLang) {
        Location startLocation = new Location("start");
        Location endlocation = new Location("end");
        endlocation.setLatitude(endLatLang.getLatitude());
        endlocation.setLongitude(endLatLang.getLongitude());
        startLocation.setLatitude(startLatLng.getLatitude());
        startLocation.setLongitude(startLatLng.getLongitude());
        float distance = startLocation.distanceTo(endlocation);
        AppLog.Log("DISTANCE", distance + "in meters");
        return distance;

    }


    public void moveCameraFirstMyLocation() {

        try {
            if (locationHelper != null && locationHelper.getLastLocation() != null)
            {
                currentLocation = locationHelper.getLastLocation();
                if (map != null && currentLocation != null)
                {
                    currentLatLng = new LatLng(currentLocation.getLatitude(),
                            currentLocation.getLongitude());
                    isCameraIdeal = false;
                    zoomCamera(currentLatLng);
/*
                    map.setCenter(new GeoCoordinate(currentLocation.getLatitude(), currentLocation.getLongitude()), Map.Animation.LINEAR);
*/


                    //setMarkerOnLocation(currentLatLng);

                    isCameraIdeal = true;

                }
                locationHelper.setOpenGpsDialog(false);


            }

        } catch (Exception e) {
            //Log.e("moveCameraFirstMyLocation() " + e.getMessage());
        }

    }

    /**
     * this method call webservice for complete a active or running  order
     */
    private void completeOrder() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CompleteOrderResponse> responseCall = apiInterface.completeOrder(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CompleteOrderResponse>() {
            @Override
            public void onResponse(Call<CompleteOrderResponse> call,
                                   Response<CompleteOrderResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    Utils.showMessageToast(response.body().getMessage(),
                            ActiveDeliveryActivity.this);
                    CurrentOrder.getInstance().setCurrency(response.body().getCurrency());
                        /*goToCompleteOrderActivity(response.body().getOrderPayment(), response.body()
                                .getPaymentGatewayName(), userDetail, requestId, true, true);*/
                    goToHistoryActivity();


                } else {



                }



            }

            @Override
            public void onFailure(Call<CompleteOrderResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);
            }
        });

    }

    private void openPickupDeliveryDialog() {
        if (order.isConfirmationCodeRequiredAtPickupDelivery()) {


            if (pickUpDeliveryDialog != null && pickUpDeliveryDialog.isShowing()) {
                return;
            }
            pickUpDeliveryDialog = new CustomDialogVerification
                    (this, getResources().getString(R.string.text_pickup_delivery),
                            getResources().getString(R.string.msg_enter_delivery_pick_up_code),
                            getResources()
                                    .getString(R.string.text_cancel), getResources().getString(R
                            .string.text_submit), null, getResources().getString(R.string
                            .text_confirmation_code), false, InputType.TYPE_CLASS_TEXT, InputType
                            .TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {


                    if (TextUtils.equals(etDialogEditTextTwo.getText().toString(), order
                            .getConfirmationCodeForPickUp())) {
                        setOrderStatus(orderStatus);
                        dismiss();
                    } else {
                        etDialogEditTextTwo.setError(getResources().getString(R.string
                                .error_code_660));
                        etDialogEditTextTwo.requestFocus();
                    }


                }
            };
            pickUpDeliveryDialog.show();
        } else {
            setOrderStatus(orderStatus);
        }

    }

    private void goToHistoryActivity(){
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void openCompleteDeliveryDialog() {
        if (order.isConfirmationCodeRequiredAtCompleteDelivery()) {

            if (completeDeliveryDialog != null && completeDeliveryDialog.isShowing()) {
                return;
            }

            completeDeliveryDialog = new CustomDialogVerification
                    (this, getResources().getString(R.string.text_complete_delivery),
                            getResources().getString(R.string.msg_enter_delivery_code),
                            getResources()

                                    .getString(R.string.text_cancel), getResources().getString(R
                            .string.text_submit), null, getResources().getString(R.string
                            .text_confirmation_code), false, InputType.TYPE_CLASS_TEXT, InputType
                            .TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {

                    if (TextUtils.equals(etDialogEditTextTwo.getText().toString(), order
                            .getConfirmationCodeForCompleteDelivery())) {
                        completeOrder();
                        dismiss();
                    } else {
                        etDialogEditTextTwo.setError(getResources().getString(R.string
                                .error_code_660));
                        etDialogEditTextTwo.requestFocus();
                    }

                }
            };
            completeDeliveryDialog.show();
        } else {
            completeOrder();
        }

    }

    /**
     * this method open Complete order activity witch is contain two fragment
     * Invoice Fragment and Feedback Fragment
     *
     * @param goToInvoiceFirst           set true when you want to open invoice fragment when
     *                                   activity open
     * @param backToActiveDeliveryActivity set true when you want
     *                                   goToActiveDeliveryActivity when press back
     *                                   button
     */
    private void goToCompleteOrderActivity(OrderPayment orderPayment, String payment, UserDetail
            userDetail, String requestId, boolean goToInvoiceFirst, boolean
                                                   backToActiveDeliveryActivity) {
        Intent intent = new Intent(this, CompleteOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Const.GO_TO_INVOICE, goToInvoiceFirst);
        bundle.putParcelable(Const.ORDER_PAYMENT, orderPayment);
        bundle.putString(Const.PAYMENT, payment);
        bundle.putParcelable(Const.USER_DETAIL, userDetail);
        bundle.putString(Const.Params.REQUEST_ID, requestId);
        intent.putExtra(Const.BUNDLE, bundle);
        intent.putExtra(Const.BACK_TO_ACTIVE_DELIVERY, backToActiveDeliveryActivity);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private boolean isOrderPickedUp(int orderStatus) {
        switch (orderStatus) {
            case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
            case Const.ProviderStatus.DELIVERY_MAN_COMING:
            case Const.ProviderStatus.DELIVERY_MAN_NEW_DELIVERY:
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                return false;
            case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
            case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
            case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                return true;
            default:

                // do with default
                break;

        }
        return false;
    }

    /*private void loadOrderData(int orderStatus) {
        String name, image;
        if (isOrderPickedUp(orderStatus)) {
            name = userDetail.getFirstName() + " " + userDetail.getLastName();
            image = BASE_URL + userDetail.getImageUrl();
            tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
            llAddressOne.setVisibility(View.GONE);
            call = *//*order.getPickupAddresses().get(0).getUserDetails().getCountryPhoneCode() +*//* order
                    .getPickupAddresses().get(0).getUserDetails().getPhone();
        } else {
            name = order.getStoreName();
            image = BASE_URL + order.getStoreImage();
            tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
            tvAddressOne.setText(order.getPickupAddresses().get(0).getAddress());
            llAddressOne.setVisibility(View.VISIBLE);
            call = *//*order.getPickupAddresses().get(0).getUserDetails().getCountryPhoneCode() +*//* order
                    .getPickupAddresses().get(0).getUserDetails().getPhone();
        }

        String requestNumber = getResources().getString(R.string.text_request_number)
                + " " + "#" + order.getRequestUniqueId();
        tvOrderNumber.setText(requestNumber);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        tvOrderId.setText(orderNumber);
        tvOrderId.setVisibility(View.VISIBLE);

        tvCustomerName.setText(name);
        Glide.with(ActiveDeliveryActivity.this).load(image).dontAnimate().placeholder
                (ResourcesCompat.getDrawable(
                        getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat.getDrawable(
                        getResources(), R.drawable.placeholder, null)).into
                (ivCustomerImage);
        ivCart.setVisibility(order.getOrderDetails().isEmpty() ? View.GONE : View.VISIBLE);
    }*/

    private void loadOrderData(int orderStatus) {
        String name, image;
        if (isOrderPickedUp(orderStatus)) {
            name = userDetail.getFirstName() + " " + userDetail.getLastName();
            image = BASE_URL + userDetail.getImageUrl();
            tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
            llAddressOne.setVisibility(View.GONE);
            call = /*order.getPickupAddresses().get(0).getUserDetails().getCountryPhoneCode() +*/ order
                    .getDestinationAddresses().get(0).getUserDetails().getPhone();
        } else {
            name = order.getStoreName();
            image = BASE_URL + order.getStoreImage();
            tvAddressTwo.setText(order.getDestinationAddresses().get(0).getAddress());
            tvAddressOne.setText(order.getPickupAddresses().get(0).getAddress());
            llAddressOne.setVisibility(View.VISIBLE);
            call = /*order.getPickupAddresses().get(0).getUserDetails().getCountryPhoneCode() +*/ order
                    .getDestinationAddresses().get(0).getUserDetails().getPhone();
        }

        String requestNumber = getResources().getString(R.string.text_request_number)
                + " " + "#" + order.getRequestUniqueId();
        tvOrderNumber.setText(requestNumber);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        tvOrderId.setText(orderNumber);
        tvOrderId.setVisibility(View.VISIBLE);

        tvCustomerName.setText(name);
        Glide.with(ActiveDeliveryActivity.this).load(image).dontAnimate().placeholder
                (ResourcesCompat.getDrawable(
                        getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat.getDrawable(
                        getResources(), R.drawable.placeholder, null)).into
                (ivCustomerImage);
        ivCart.setVisibility(order.getOrderDetails().isEmpty() ? View.GONE : View.VISIBLE);
    }

    private void updateUiCancelOrder(boolean isEnable) {
        if (isEnable) {
            setToolbarRightIcon(R.drawable.ic_cancel, this);
        } else {
           /* if (TextUtils.isEmpty(order.getDestinationAddresses().get(0).getNote())) {
                ivToolbarRightIcon.setImageDrawable(null);
            } else {
                setToolbarRightIcon(R.drawable.ic_notepad, this);
            }*/
            ivToolbarRightIcon.setImageDrawable(null);

        }

    }

    private void openProviderMessageDailog(){
        if(providerMessageDialog != null && providerMessageDialog.isShowing()){
            return;
        }
        RecyclerView rcvProviderMessage;
        final CustomFontTextView tvOther;
        final CustomFontEditTextView etOthersReason;

        providerMessageDialog = new Dialog(this);
        providerMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        providerMessageDialog.setContentView(R.layout.dialog_proider_message);
        tvOther = providerMessageDialog.findViewById(R.id.tvOther);
        etOthersReason = providerMessageDialog.findViewById(R.id.etOthersReason);
        rcvProviderMessage = providerMessageDialog.findViewById(R.id.rcvProviderMessage);


        providerMessageAdapter = new ProviderMessageAdapter(currentOrder.getProviderMessageList(),this) {
            @Override
            public void onMessageClick(int position) {
                providerMessage = currentOrder.getProviderMessageList().get(position);
                Log.e("aaa","proivderMesssge="+providerMessage);
                etOthersReason.setVisibility(View.VISIBLE);
                etOthersReason.setText(providerMessage);
            }
        };
        rcvProviderMessage.setLayoutManager(new LinearLayoutManager
                (this));
        rcvProviderMessage.setAdapter(providerMessageAdapter);
        rcvProviderMessage.addItemDecoration(new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL));

        tvOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etOthersReason.setVisibility(View.VISIBLE);
                etOthersReason.setText("");
            }
        });
        providerMessageDialog.findViewById(R.id.btnDialogAlertRight).setOnClickListener(new View
                .OnClickListener
                () {
            @Override
            public void onClick(View view) {

                providerMessage = etOthersReason.getText().toString();
                if(!TextUtils.isEmpty(providerMessage.trim())){
                    SendPushToUser();
                }else{
                    Utils.showToast(getResources().getString(R.string.msg_select_push_message), ActiveDeliveryActivity.this);
                }

            }
        });
        providerMessageDialog.findViewById(R.id.btnDialogAlertLeft).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View view) {
                providerMessageDialog.dismiss();
                providerMessage = "";
            }
        });



        WindowManager.LayoutParams params = providerMessageDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        providerMessageDialog.setCancelable(false);
        providerMessageDialog.show();

    }

    private void SendPushToUser() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.REQUEST_ID, requestId);
            jsonObject.put(Const.Params.MESSAGE, providerMessage);

            AppLog.Log("SEND_PUSH_TO_USER", jsonObject.toString());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.sendExtraTimePushToUser(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    providerMessageDialog.dismiss();

                } else {
                    Utils.showErrorToast(response.body().getErrorCode(),
                            ActiveDeliveryActivity.this);
                }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.ACTIVE_DELIVERY_ACTIVITY, t);

            }
        });
    }

    private void openCancelOrderDialog() {
        if (orderCancelDialog != null && orderCancelDialog.isShowing()) {
            return;
        }
        RadioGroup radioGroup;
        final RadioButton rbReasonOne, rbReasonTwo, rbReasonOther;
        final CustomFontEditTextView etOtherReason;
        orderCancelDialog = new Dialog(this);
        orderCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        orderCancelDialog.setContentView(R.layout.dialog_cancel_order);
        rbReasonOne = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonOne);
        rbReasonTwo = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonTwo);
        rbReasonOther = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonOthers);
        radioGroup = (RadioGroup) orderCancelDialog.findViewById(R.id.radioGroup);
        etOtherReason = (CustomFontEditTextView) orderCancelDialog.findViewById(R.id
                .etOthersReason);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rbReasonOne:
                        etOtherReason.setVisibility(View.GONE);
                        cancelReason = rbReasonOne.getText().toString();
                        break;
                    case R.id.rbReasonTwo:
                        etOtherReason.setVisibility(View.GONE);
                        cancelReason = rbReasonTwo.getText().toString();
                        break;
                    case R.id.rbReasonOthers:
                        etOtherReason.setVisibility(View.VISIBLE);
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        });
        orderCancelDialog.findViewById(R.id.btnDialogAlertRight).setOnClickListener(new View
                .OnClickListener
                () {
            @Override
            public void onClick(View view) {
                if (rbReasonOther.isChecked()) {
                    cancelReason = etOtherReason.getText().toString();
                }
                if (!TextUtils.isEmpty(cancelReason)) {
                    rejectOrCancelDeliveryOrder(Const.ProviderStatus.DELIVERY_MAN_CANCELLED, true);
                    orderCancelDialog.dismiss();
                } else {
                    Utils.showToast(getResources().getString(R
                            .string.msg_plz_give_valid_reason), ActiveDeliveryActivity.this);
                }
            }
        });
        orderCancelDialog.findViewById(R.id.btnDialogAlertLeft).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View view) {
                orderCancelDialog.dismiss();
                cancelReason = "";
            }
        });
        WindowManager.LayoutParams params = orderCancelDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        orderCancelDialog.setCancelable(false);
        orderCancelDialog.show();

    }

    private List<Double> getLocationAsPerStatus(int orderStatus) {
        if (orderStatus == Const.ProviderStatus.DELIVERY_MAN_ACCEPTED || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_COMING || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_ARRIVED || orderStatus == Const
                .ProviderStatus.DELIVERY_MAN_NEW_DELIVERY) {
            return order.getPickupAddresses().get(0).getLocation();
        } else {
            return order.getDestinationAddresses().get(0).getLocation();
        }
    }

    /**
     * this method is used to open Google Map app whit given LatLng
     */
    private void goToGoogleMapApp(List<Double> locationList) {


        Log.d("map","======= Google Map ======="+new Gson().toJson(locationList));

        Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr="+locationList.get(0)+","+locationList.get(1));

       // 54.896814 + "," + -1.497883

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_google_app_not_installed), this);
        }
    }

    /**
     * this method is used to open Waze Map app whit given LatLng
     */
    private void goToWazeMapApp(List<Double> locationList) {
        try {
            String url = "waze://?ll=" + locationList.get
                    (0) + "," + locationList.get(1) + "&navigate=yes";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Intent intent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
            Utils.showToast(getResources().getString(R.string.waze_map_msg), this);
        }

       /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.here.com/directions/drive/start:"+locationList.get(0)+","+locationList.get(1)+"/end:"+currentLatLng.getLatitude()+","+currentLatLng.getLongitude()));
        startActivity(browserIntent);*/
    }

    public void gotoheremap(List<Double> locationList)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.here.com/directions/drive/start:"+locationList.get(0)+","+locationList.get(1)+"/end:"+currentLatLng.getLatitude()+","+currentLatLng.getLongitude()));
        startActivity(browserIntent);
    }

    public void openPhotoMapDialog() {
        //Do the stuff that requires permission...
        if (mapDialog != null && mapDialog.isShowing()) {
            return;
        }
        mapDialog = new CustomMapdialog(this, getResources()
                .getString(R.string.text_choose_map), getResources()
                .getString(R.string.text_google_map), getResources()
                .getString(R.string.text_waze_map),getResources()
                .getString(R.string.text_here_map)) {
            @Override
            public void clickedOnCamera() {
                goToGoogleMapApp(getLocationAsPerStatus(orderStatus));
                dismiss();
            }

            @Override
            public void clickedOnGallery() {
                goToWazeMapApp(getLocationAsPerStatus(orderStatus));
                dismiss();
            }

            @Override
            public void clickheremap() {
                gotoheremap(getLocationAsPerStatus(orderStatus));
                dismiss();
            }
        };
        mapDialog.show();

    }

    public void openOrderNoteDialog() {
        if (noteDialog != null && noteDialog.isShowing()) {
            return;
        }

        noteDialog = new CustomDialogAlert(this, getResources().getString(R.string.text_note),
                order.getDestinationAddresses().get(0).getNote(), "",
                getResources().getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        noteDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        locationHelper.setOpenGpsDialog(false);
                        moveCameraFirstMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        locationHelper.setOpenGpsDialog(false);
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    public void makePhoneCall(String phone) {
       /* if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission
                    .CALL_PHONE}, Const
                    .PERMISSION_FOR_CALL);
        } else {*/
       // String phoneno=activeOrderResponse.getProviderPhone();

        Log.i("makePhoneCall",phone);
        String[] prefix={"+440","+44","+91","+910","+","00","0"};
        for (int i=0;i<prefix.length;i++)
        {
            if (phone.startsWith(prefix[i]))
            {
                Log.i("startsWith(prefix[i])", prefix[i]);

                phone= phone.substring(prefix[i].length());
                i=0;

            }
        }
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "0"+phone));
        startActivity(intent);
        //  }


    }

    private void openCallPermissionDialog() {

        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(ActiveDeliveryActivity.this, new
                        String[]{android.Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                dismiss();
            }

        };
        customDialogAlert.show();
    }

    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            makePhoneCall(call);

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ActiveDeliveryActivity.this, android.Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            }
        }
    }


    private void setOrderDetail() {
//        mapbox.setVisibility(View.GONE);
        llOrderDetail.setVisibility(View.VISIBLE);
        tvDialogAlertTitle.setVisibility(View.VISIBLE);
        tvDeliveryNote.setVisibility(View.VISIBLE);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        String deliveryNote = getResources().getString(R.string.text_order_delivery_note)
                + " " + order.getDestinationAddresses().get(0).getNote();
        // tvOrderDetailNumber.setText(orderNumber);
        if (TextUtils.isEmpty(order.getDestinationAddresses().get(0).getNote())) {
            tvDeliveryNote.setVisibility(View.GONE);
        }else{
            tvDeliveryNote.setText(deliveryNote);
        }
        rcvOrderDetailList.setLayoutManager(new LinearLayoutManager(this));
        OrderDetailsAdapter itemAdapter = new OrderDetailsAdapter(this, order.getOrderDetails(),
                order.getCurrency(), false);
        rcvOrderDetailList.setAdapter(itemAdapter);
        rcvOrderDetailList.addItemDecoration(new PinnedHeaderItemDecoration());
    }

    private void openCartDetailDialog() {
        if (cartDetailDialog != null && cartDetailDialog.isShowing()) {
            return;
        }

        cartDetailDialog = new Dialog(this);
        cartDetailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cartDetailDialog.setContentView(R.layout.dialog_cart_detail);
        CustomFontTextView tvOrderNumber = (CustomFontTextView) cartDetailDialog.findViewById(R
                .id.tvOrderNumber);
        CustomFontTextView tvDeliveryNote = (CustomFontTextView) cartDetailDialog.findViewById(R
                .id.tvDeliveryNote);
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getOrderUniqueId();
        String deliveryNote = getResources().getString(R.string.text_order_delivery_note)
                + " " + order.getDestinationAddresses().get(0).getNote();
        tvOrderNumber.setText(orderNumber);
        if (TextUtils.isEmpty(order.getDestinationAddresses().get(0).getNote())) {
            tvDeliveryNote.setVisibility(View.GONE);
        }else{
            tvDeliveryNote.setText(deliveryNote);
        }
        RecyclerView rcvOrderProductItem = (RecyclerView) cartDetailDialog.findViewById(R.id
                .rcvOrderProductItem);
        rcvOrderProductItem.setLayoutManager(new LinearLayoutManager(this));
        OrderDetailsAdapter itemAdapter = new OrderDetailsAdapter(this, order.getOrderDetails(),
                order.getCurrency(), false);
        rcvOrderProductItem.setAdapter(itemAdapter);
        rcvOrderProductItem.addItemDecoration(new PinnedHeaderItemDecoration());
        cartDetailDialog.findViewById(R.id.btnDone).setOnClickListener(new View
                .OnClickListener() {

            @Override
            public void onClick(View v) {
                cartDetailDialog.dismiss();
            }
        });


        WindowManager.LayoutParams params = cartDetailDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        cartDetailDialog.setCancelable(false);
        cartDetailDialog.show();

    }

    protected void openDeliveryOrderItemConfirm() {
        if (orderItemConfirmDialog != null && orderItemConfirmDialog.isShowing()) {
            return;
        }
        orderItemConfirmDialog = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_order_item_confirm), this.getResources()
                .getString(R.string.msg_order_item_confirm), this.getResources()
                .getString(R.string.text_cancel), this.getResources()
                .getString(R.string.text_confirm)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                dismiss();
                openPickupDeliveryDialog();
            }
        };
        orderItemConfirmDialog.show();
    }

    private class OrderStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            goToHomeActivity();
        }
    }

    private void goToAvailableDelivery() {
        Intent intent = new Intent(this, AvailableDeliveryActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }







    // developer s
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        /*if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }
*/    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
      /*  this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mLatLng = mapboxMap.getCameraPosition().target;
                if (mLatLng != null)
                {

                    Geocoder geocoder = new Geocoder(ActiveDeliveryActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            str_Latitude = addresses.get(0).getLatitude();
                            str_Longitude = addresses.get(0).getLongitude();
                            storeLatLng = new LatLng(str_Latitude, str_Longitude);
                            if (addresses.get(0).getSubAdminArea() != null) {
                                str_subAdminarea11 = addresses.get(0).getSubAdminArea();
                            } else {
                                str_subAdminarea11 = "";
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //moveCameraFirstMyLocation();
                }
            }
        });*/
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(ActiveDeliveryActivity.this)) {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(ActiveDeliveryActivity.this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(ActiveDeliveryActivity.this);
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(ActiveDeliveryActivity.this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

    }


    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<ActiveDeliveryActivity> activityWeakReference;

        MainActivityLocationCallback(ActiveDeliveryActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            ActiveDeliveryActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                }

            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            Activity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(LatLng latLng) {
        if (latLng != null) {
           /* if (mapboxMap != null) {
                mapboxMap.animateCamera(com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition(
                        new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                                .target(latLng)
                                //.zoom(12)   old
                                .zoom(14)
                                .build()), 4000);
            }*/
            Log.i("zoomCamera",latLng.toString());
            map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.LINEAR);
            map.setZoomLevel(12);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    }
                    break;

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(ActiveDeliveryActivity.this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            // finish();
                            finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;
                default:
                    break;
            }
        }
    }


    public void downloadVehiclePin(final Context context, final MapMarker marker) {

        try {
            if (!TextUtils.isEmpty(CurrentOrder.getInstance().getVehiclePin())) {
                if (CurrentOrder.getInstance().getBmVehiclePin() == null) {
                    Glide.with(context)
                            .asBitmap()
                            .load(BASE_URL + CurrentOrder.getInstance().getVehiclePin())

                            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable
                            .driver_car)
                            .listener(new RequestListener<Bitmap>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                    AppLog.handleException(getClass().getSimpleName(), e);
                                    AppLog.Log("VEHICLE_PIN", "Download failed");
                                    if (marker != null)
                                    {

                                      /*  IconFactory iconFactory = IconFactory.getInstance(ActiveDeliveryActivity.this);
                                        Icon icon = iconFactory.fromResource(R.drawable.driver_car);
                                        marker.setIcon(icon);*/

                                        Image image = new Image();
                                        try {
                                            image.setImageResource(R.drawable.driver_car);
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }
                                        marker.setIcon(image);
                                    }
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                    if (marker != null)
                                    {
                                       /* IconFactory iconFactory = IconFactory.getInstance(ActiveDeliveryActivity.this);
                                        Icon icon = iconFactory.fromBitmap(resource);*/

                                        Image image = new Image();
                                        image.setBitmap(resource);
                                        marker.setIcon(image);

                                        CurrentOrder.getInstance().setBmVehiclePin(resource);
                                    }
                                    AppLog.Log("VEHICLE_PIN", "Download Successfully");
                                    return true;
                                }


                            }).dontAnimate().override(context.getResources().getDimensionPixelSize(R
                                    .dimen.vehicle_pin_width)
                            , context.getResources().getDimensionPixelSize(R
                                    .dimen.vehicle_pin_height)).preload(context.getResources()
                                    .getDimensionPixelSize(R
                                            .dimen.vehicle_pin_width)
                            , context.getResources().getDimensionPixelSize(R
                                    .dimen.vehicle_pin_height));
                    AppLog.Log("VEHICLE_PIN_SIZE", (context.getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_width) + "*" + context.getResources()
                            .getDimensionPixelSize(R
                                    .dimen.vehicle_pin_height)));
                } else {
                    if (marker != null)
                    {
                      /*  IconFactory iconFactory = IconFactory.getInstance(ActiveDeliveryActivity.this);
                        Icon icon = iconFactory.fromBitmap(CurrentOrder.getInstance().getBmVehiclePin());*/
                        Image image = new Image();
                        image.setBitmap(CurrentOrder.getInstance().getBmVehiclePin());

                        marker.setIcon(image);
                    }
                }
            } else {
                if (marker != null) {
                    Image image = new Image();
                    try {
                        image.setImageResource(R.drawable.driver_car);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    marker.setIcon(image);
                }
            }
        } catch (Exception e) {
            Log.e("downloadVehiclePin() " , e.getMessage());
        }

    }


    //for here map

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(ActiveDeliveryActivity.this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(ActiveDeliveryActivity.this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }


    private void setheremap() {

        mapFragment = getMapFragment();
        Utils.showCustomProgressDialog(ActiveDeliveryActivity.this, false);
        // Set up disk cache path for the map service for this application
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps",
                "com.here.android.tutorial.MapService");
        /*if (!success) {
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
        } else {*/
        // map.setProjectionMode(Map.Projection.MERCATOR);
        Log.i("setheremp","ok");
        LayoutInflater inflater = (LayoutInflater)   getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View myView = inflater.inflate(R.layout.infow_windowlayout, null);
        final TextView title=(TextView)myView.findViewById(R.id.txt_title);


        if (mapOverlay!=null) {
            map.removeMapOverlay(mapOverlay);
        }
        mapOverlay=new MapOverlay(myView,new GeoCoordinate(49.163, -123.137766, 10));
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(
                    final Error error) {
                if (error == Error.NONE) {
                    // retrieve a reference of the map from the map fragment

                    map = mapFragment.getMap();

                    // Set the zoom level to the average between min and max
                    map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);

                    //getProviderLocation();
                    moveCameraFirstMyLocation();
                    Utils.hideCustomProgressDialog();

                    mapFragment.getMapGesture().addOnGestureListener(new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                        @Override
                        public boolean onMapObjectsSelected(List<ViewObject> list) {
                            for (ViewObject viewObject : list) {
                                if (viewObject.getBaseType() == ViewObject.Type.USER_OBJECT) {
                                    MapObject mapObject = (MapObject) viewObject;

                                    if (mapObject.getType() == MapObject.Type.MARKER) {

                                        MapMarker window_marker = ((MapMarker) mapObject);

                                        System.out.println("Title is................."+window_marker.getTitle());
                                        title.setText(window_marker.getTitle());
                                        if (mapOverlay!=null)
                                        {
                                            mapOverlay=new MapOverlay(myView,window_marker.getCoordinate());
                                            mapOverlay.setAnchorPoint(new PointF(150,120));
                                            map.addMapOverlay(mapOverlay);
                                        }


                                        return true;
                                    }
                                }
                            }
                            return false;
                        }

                        @Override
                        public boolean onTapEvent(PointF pointF) {
                            if (mapOverlay!=null)
                            {
                                map.removeMapOverlay(mapOverlay);
                            }

                            Log.e("tagMap", "onTapEvent()");

                            return false;
                        }

                    },0,false);



                } else {
                    Utils.hideCustomProgressDialog();
                    System.out.println("ERROR: Cannot initialize Map Fragment");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getApplicationContext()).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    finishAffinity();
                                                }
                                            }).create().show();
                        }
                    });
                }
            }
        });
        // }


    }




}


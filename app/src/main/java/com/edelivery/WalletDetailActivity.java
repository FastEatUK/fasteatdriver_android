package com.edelivery;

import android.os.Bundle;
import androidx.core.content.res.ResourcesCompat;
import android.view.View;

import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.WalletHistory;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;

public class WalletDetailActivity extends BaseAppCompatActivity {

    private CustomFontTextView tvTransactionDate, tvDescription, tvTransactionTime, tvWithdrawalID,
            tvTagCurrentRate, tvAmountTag;
    private CustomFontTextViewTitle tvAmount, tvCurrentRate, tvTotalWalletAmount;
    private WalletHistory walletHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_detail);
        initToolBar();

        findViewById();
        setViewListener();
        getExtraData();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {

        tvTotalWalletAmount = (CustomFontTextViewTitle) findViewById(R.id.tvTotalWalletAmount);
        tvAmount = (CustomFontTextViewTitle) findViewById(R.id.tvAmount);
        tvWithdrawalID = (CustomFontTextView) findViewById(R.id.tvWithdrawalID);
        tvTransactionDate = (CustomFontTextView) findViewById(R.id.tvTransactionDate);
        tvTransactionTime = (CustomFontTextView) findViewById(R.id.tvTransactionTime);
        tvCurrentRate = (CustomFontTextViewTitle) findViewById(R.id
                .tvCurrentRate);
        tvDescription = (CustomFontTextView) findViewById(R.id.tvDescription);
        tvTagCurrentRate = (CustomFontTextView) findViewById(R.id.tvTagCurrentRate);
        tvAmountTag = (CustomFontTextView) findViewById(R.id.tvAmountTag);
    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            walletHistory = getIntent().getExtras().getParcelable(Const.BUNDLE);
            setTitleOnToolBar(walletComment(walletHistory.getWalletCommentId()));
            walletStatus(walletHistory.getWalletStatus());
            tvWithdrawalID.setText(getResources().getString(R.string
                    .text_id) + " " + walletHistory.getUniqueId());
            try {
                Date date = ParseContent.getInstance().webFormat.parse(walletHistory
                        .getCreatedAt
                                ());
                tvTransactionDate.setText(ParseContent.getInstance().dateFormat3.format(date));
                tvTransactionTime.setText(ParseContent.getInstance().timeFormat_am.format(date));
            } catch (ParseException e) {
                AppLog.handleException(WalletDetailActivity.class.getName(), e);
            }
            tvDescription.setText(walletHistory.getWalletDescription());

            if (walletHistory.getCurrentRate() == 1.0) {
                tvTagCurrentRate.setVisibility(View.GONE);
                tvCurrentRate.setVisibility(View.GONE);
            } else {
                DecimalFormat decimalTwoDigitFormat = new DecimalFormat("0.0000");
                tvTagCurrentRate.setVisibility(View.VISIBLE);
                tvCurrentRate.setVisibility(View.VISIBLE);
                tvCurrentRate.setText("1" + walletHistory.getFromCurrencyCode() + " (" +
                        decimalTwoDigitFormat.format
                                (walletHistory
                                        .getCurrentRate()) + walletHistory.getToCurrencyCode() +
                        ")");
            }
            tvAmount.setText("+" + parseContent.decimalTwoDigitFormat.format(walletHistory
                    .getAddedWallet()) + " " + walletHistory.getToCurrencyCode());
            tvTotalWalletAmount.setText(parseContent.decimalTwoDigitFormat.format(walletHistory
                    .getTotalWalletAmount()) + " " + walletHistory.getToCurrencyCode());
        }

    }

    private void walletStatus(int id) {
        switch (id) {
            case Const.Wallet.ADD_WALLET_AMOUNT:
                tvAmount.setTextColor(ResourcesCompat
                        .getColor(getResources(), R.color
                                .color_app_wallet_added, null));
                tvAmountTag.setText(getResources().getString(R.string.text_added_amount));

                break;
            case Const.Wallet.REMOVE_WALLET_AMOUNT:
                tvAmount.setTextColor(ResourcesCompat
                        .getColor(getResources(), R.color
                                .color_app_wallet_deduct, null));
                tvAmountTag.setText(getResources().getString(R.string.text_deducted_amount));
                break;


        }
    }

    private String walletComment(int id) {
        String comment;
        switch (id) {
            case Const.Wallet.ADDED_BY_ADMIN:
                comment = getResources().getString(R.string
                        .text_wallet_status_added_by_admin);
                break;
            case Const.Wallet.ADDED_BY_CARD:
                comment = getResources().getString(R.string
                        .text_wallet_status_added_by_card);
                break;
            case Const.Wallet.ADDED_BY_REFERRAL:
                comment = getResources().getString(R.string
                        .text_wallet_status_added_by_referral);
                break;
            case Const.Wallet.ORDER_REFUND:
                comment = getResources().getString(R.string
                        .text_wallet_status_order_refund);
                break;
            case Const.Wallet.ORDER_PROFIT:
                comment = getResources().getString(R.string
                        .text_wallet_status_order_profit);
                break;
            case Const.Wallet.ORDER_CANCELLATION_CHARGE:
                comment = getResources().getString(R.string
                        .text_wallet_status_order_cancellation_charge);
                break;
            case Const.Wallet.ORDER_CHARGED:
                comment = getResources().getString(R.string
                        .text_wallet_status_order_charged);
                break;
            case Const.Wallet.WALLET_REQUEST_CHARGE:
                comment = getResources().getString(R.string
                        .text_wallet_status_wallet_request_charge);
                break;


            default:
                // do with default
                comment = "NA";
                break;
        }
        return comment;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.component.BadgeTabLayout;
import com.edelivery.fragments.ActiveFragmentDelivery;
import com.edelivery.fragments.PendingFragmentDelivery;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AvailableDeliveryActivity extends BaseAppCompatActivity {

    private BadgeTabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ScheduledExecutorService updateLocationAndOrderSchedule;
    private boolean isScheduledStart;
    private Handler handler;
    private PendingFragmentDelivery pendingFragmentDelivery;
    private ActiveFragmentDelivery activeFragmentDelivery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_delivery);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        findViewById();
        setViewListener();
        setTitleOnToolBar(getResources().getString(R.string.text_available_deliveries));
        setupViewPager(viewPager);
        closedDeliveryRequestDialog();
        initHandler();

    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        tabLayout = (BadgeTabLayout) findViewById(R.id.deliveryTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.deliveryViewpager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(getResources().getDimensionPixelSize(R.dimen
                    .dimen_app_tab_elevation));
        }
    }

    @Override
    protected void setViewListener() {
        // do somethings

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings

    }


    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PendingFragmentDelivery(), getString(R.string
                .text_pending_deliveries));
        adapter.addFragment(new ActiveFragmentDelivery(), getString(R.string
                .text_active_deliveries));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    pendingFragmentDelivery = (PendingFragmentDelivery) adapter.getItem(0);
                    pendingFragmentDelivery.getOrders();
                } else {
                    activeFragmentDelivery = (ActiveFragmentDelivery) adapter.getItem(1);
                    activeFragmentDelivery.getActiveOrders();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        closedDeliveryRequestDialog();
    }

    public void updateDeliveryCount(int tabPosition, int count) {
        if (tabLayout != null) {
            BadgeTabLayout.Builder builder = tabLayout.with(tabPosition);
            if (count == 0) {
                builder.badge(false);
            } else {
                builder.badge(true);
                builder.badgeCount(count);
            }
            builder.build();
        }
    }

    @Override
    public void onBackPressed() {
        goToNewHomeActivity();
    }


    public void goToActiveDeliveryActivity(String orderId) {
        Intent intent = new Intent(this, ActiveDeliveryActivity.class);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToNewHomeActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void startSchedule() {

        if (!isScheduledStart) {

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            };
            updateLocationAndOrderSchedule = Executors.newSingleThreadScheduledExecutor();
            updateLocationAndOrderSchedule.scheduleWithFixedDelay(runnable, 0,
                    Const.AVAILABLE_DELIVER_SCHEDULED_SECONDS, TimeUnit
                            .SECONDS);
            AppLog.Log(AvailableDeliveryActivity.class.getName(), "Schedule Start");
            isScheduledStart = true;
        }
    }

    public void stopSchedule() {
        if (isScheduledStart) {
            AppLog.Log(AvailableDeliveryActivity.class.getName(), "Schedule Stop");
            updateLocationAndOrderSchedule.shutdown(); // Disable new tasks from being submitted
            // Wait a while for existing tasks to terminate
            try {
                if (!updateLocationAndOrderSchedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    updateLocationAndOrderSchedule.shutdownNow(); // Cancel currently executing
                    // tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!updateLocationAndOrderSchedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(AvailableDeliveryActivity.class.getName(), "Pool did not " +
                                "terminate");

                }
            } catch (InterruptedException e) {
                AppLog.handleException(AvailableDeliveryActivity.class.getName(), e);
                // (Re-)Cancel if current thread also interrupted
                updateLocationAndOrderSchedule.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
            isScheduledStart = false;
        }

    }

    private void initHandler() {
        /**
         * This handler receive a message from  requestStatusScheduledService and update provider
         * location and order status
         */
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {


                if (tabLayout.getSelectedTabPosition() == 0) {
                    pendingFragmentDelivery = (PendingFragmentDelivery) adapter.getItem(0);
                    pendingFragmentDelivery.getOrders();
                } else {
                    activeFragmentDelivery = (ActiveFragmentDelivery) adapter.getItem(1);
                    activeFragmentDelivery.getActiveOrders();

                }

            }

        };


    }
}


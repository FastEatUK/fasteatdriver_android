package com.edelivery.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import com.edelivery.HomeActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edelivery.LoginActivity;
import com.edelivery.R;
import com.edelivery.component.CustomCityDialog;
import com.edelivery.component.CustomCountryDialog;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomPhotoDialog;
import com.edelivery.models.datamodels.Cities;
import com.edelivery.models.datamodels.Countries;
import com.edelivery.models.responsemodels.CityResponse;
import com.edelivery.models.responsemodels.CountriesResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OtpResponse;
import com.edelivery.models.responsemodels.ProviderDataResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.ImageHelper;
import com.edelivery.utils.Utils;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.edelivery.utils.ImageHelper.CHOOSE_PHOTO_FROM_GALLERY;
import static com.edelivery.utils.ImageHelper.TAKE_PHOTO_FROM_CAMERA;

/**
 * Created by elluminati on 30-Jan-17.
 */

public class RegisterFragment extends Fragment implements View.OnClickListener, TextView
        .OnEditorActionListener {

    private ImageView ivRegisterProfileImage;
    private CustomFontEditTextView etRegisterLastName, etRegisterFirstName, etRegisterEmail,
            etRegisterPassword,
            etRegisterAddress, etRegisterMobileNumber, etSelectCountry, etSelectCity,
            etRegisterCountryCode, etRegisterZipCode,
            etRegisterPasswordRetype;
    private FrameLayout ivRegisterProfileImageSelect;
    private CustomFontButton btnRegister;
    private String countryId, cityId;
    private Uri picUri;
    private LinearLayout llReferral;
    private ImageHelper imageHelper;
    private TextInputLayout tilRegisterAddress, tilRegisterZipCode;
    private ArrayList<Countries> countryList = new ArrayList<>();
    private ArrayList<Cities> cityList = new ArrayList<>();
    private int maxPhoneNumberLength, minPhoneNumberLength;
    private LoginActivity loginActivity;
    private CustomDialogAlert closedPermissionDialog;
    private CustomFontTextView tvReferralApply;
    private CustomFontEditTextView etReferralCode;
    private ImageView ivSuccess;
    private String referralCode = "", socialId = "";
    private CustomCityDialog customCityDialog;
    private CustomCountryDialog customCountryDialog;
    private ImageView btnFb, btnGoogle, btnTwitter;
    private TextInputLayout tilPassword, tilRetypePassword;
    private Bitmap bmSocialProfilePic;
    private LinearLayout llSocialLogin;
    private CheckBox cbTcPolicy;
    private CustomFontTextView tvPolicy;
    private String currentPhotoPath;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginActivity = (LoginActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View registerFragView = inflater.inflate(R.layout.fragment_register, container, false);

        ivRegisterProfileImage = (ImageView) registerFragView.findViewById(R.id
                .ivRegisterProfileImage);
        ivRegisterProfileImageSelect = (FrameLayout) registerFragView.findViewById(R.id
                .ivRegisterProfileImageSelect);

        etRegisterLastName = (CustomFontEditTextView) registerFragView.findViewById
                (R.id.etRegisterLastName);
        etRegisterEmail = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etRegisterEmail);
        etRegisterPassword = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etRegisterPassword);
        etRegisterFirstName = (CustomFontEditTextView) registerFragView.findViewById(
                R.id.etRegisterFirstName);
        etRegisterAddress = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etRegisterAddress);
        etRegisterMobileNumber = (CustomFontEditTextView) registerFragView.findViewById
                (R.id.etRegisterMobileNumber);
        etSelectCountry = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etSelectCountry);
        etSelectCity = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etSelectCity);
        btnRegister = (CustomFontButton) registerFragView.findViewById(R.id.btnRegister);
        etRegisterCountryCode = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etRegisterCountryCode);
        etRegisterZipCode = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etRegisterZipCode);
        tilRegisterZipCode = (TextInputLayout) registerFragView.findViewById(R.id
                .tilRegisterZipCode);
        tilRegisterAddress = (TextInputLayout) registerFragView.findViewById(R.id
                .tilRegisterAddress);
        etRegisterPasswordRetype = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etRegisterPasswordRetype);
        etReferralCode = (CustomFontEditTextView) registerFragView.findViewById(R.id
                .etReferralCode);
        tvReferralApply = (CustomFontTextView) registerFragView.findViewById(R.id.tvReferralApply);
        ivSuccess = (ImageView) registerFragView.findViewById(R.id.ivSuccess);
        llReferral = (LinearLayout) registerFragView.findViewById(R.id.llReferral);
        btnFb = (ImageView) registerFragView.findViewById(R.id.btnFb);
        btnGoogle = (ImageView) registerFragView.findViewById(R.id.btnGoogle);
        tilPassword = (TextInputLayout) registerFragView.findViewById(R.id.tilPassword);
        tilRetypePassword = (TextInputLayout) registerFragView.findViewById(R.id.tilRetypePassword);
        llSocialLogin = (LinearLayout) registerFragView.findViewById(R.id.llSocialButton);
        btnTwitter = (ImageView) registerFragView.findViewById(R.id.btnTwitter);
        cbTcPolicy = (CheckBox) registerFragView.findViewById(R.id.cbTcPolicy);
        tvPolicy = (CustomFontTextView) registerFragView.findViewById(R.id.tvPolicy);
        return registerFragView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateUiForOptionalFiled(loginActivity.preferenceHelper.getIsShowOptionalFieldInRegister());
        checkSocialLoginISOn(loginActivity.preferenceHelper.getIsLoginBySocial());
        imageHelper = new ImageHelper(loginActivity);
        tvReferralApply.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        etSelectCountry.setOnClickListener(this);
        etSelectCity.setOnClickListener(this);
        ivRegisterProfileImageSelect.setOnClickListener(this);
        etRegisterZipCode.setOnEditorActionListener(this);
        btnFb.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
        String link = loginActivity.getResources().getString(R.string.text_link_sign_up_privacy)
                + " " +
                "<a href=\"" + loginActivity.preferenceHelper.getTermsANdConditions() + "\"" + ">" +
                getResources().getString(R.string.text_t_and_c) +
                "</a>" + " " + loginActivity.getResources().getString(R.string.text_and) + " " +
                "<a href=\"" + loginActivity.preferenceHelper.getPolicy() + "\"" + ">" +
                getResources().getString(R.string.text_policy) +
                "</a>";
        tvPolicy.setText(Utils.fromHtml(link));
        tvPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        countryList = new ArrayList<>();
        countryList = new ArrayList<>();

        getCountries();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppLog.Log(Const.Tag.REGISTER_FRAGMENT, requestCode + "");
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAKE_PHOTO_FROM_CAMERA:
                    onCaptureImageResult();
                    break;
                case CHOOSE_PHOTO_FROM_GALLERY:
                    onSelectFromGalleryResult(data);
                    break;
                case Crop.REQUEST_CROP:
                    handleCrop(resultCode, data);
                    break;
                default:
                    // result
                    break;

            }
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                checkValidationForRegister();
                break;
            case R.id.ivRegisterProfileImageSelect:
                checkPermission();
                break;
            case R.id.etSelectCountry:
                if (countryList != null) {
                    openCountryCodeDialog();
                }
                break;
            case R.id.etSelectCity:
                if (cityList != null) {
                    openCityNameDialog();
                } else {
                    Utils.showToast(getResources().getString(R.string
                            .msg_select_your_country_first), loginActivity);
                }
                break;
            case R.id.tvReferralApply:
                if (TextUtils.isEmpty(etReferralCode.getText().toString().trim())) {
                    Utils.showToast(loginActivity.getResources().getString(R.string
                            .msg_plz_enter_valid_referral), loginActivity);
                } else {
                    if (TextUtils.isEmpty(countryId)) {
                        Utils.showToast(loginActivity.getResources().getString(R.string
                                .msg_select_your_country_first), loginActivity);
                    } else {
                        checkReferralCode();
                    }
                }
                break;
            case R.id.btnFb:
                loginActivity.loginWithFacebook();
                break;
            case R.id.btnGoogle:
                loginActivity.loginWithGoogle();
                break;
            case R.id.btnTwitter:
                loginActivity.loginWithTwitter();
                break;
            default:
                // do with default
                break;
        }
    }


    public void openPhotoSelectDialog() {

        try {
            //Do the stuff that requires permission...
            CustomPhotoDialog customPhotoDialog = new CustomPhotoDialog(loginActivity,
                    loginActivity.getResources().getString(R.string.text_set_profile_photos)) {
                @Override
                public void clickedOnCamera() {
                    takePhotoFromCamera();
                    dismiss();
                }

                @Override
                public void clickedOnGallery() {
                    choosePhotoFromGallery();
                    dismiss();
                }
            };
            customPhotoDialog.show();
        } catch (Exception e) {
            Log.e("openPhotoSelectDialog() " , e.getMessage());
        }
    }

    private void takePhotoFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = imageHelper.createImageFile();
            currentPhotoPath = file.getAbsolutePath();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           /*     picUri = FileProvider.getUriForFile(loginActivity, loginActivity.getPackageName(),
                        file);*/

                picUri = FileProvider.getUriForFile(loginActivity, getString(R.string.file_provider_authority), file);

                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                picUri = Uri.fromFile(file);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            startActivityForResult(intent, TAKE_PHOTO_FROM_CAMERA);
        } catch (Exception e) {
            Log.e("takePhotoFromCamera() " , e.getMessage());
        }

    }

    private void choosePhotoFromGallery() {

     /*   Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, CHOOSE_PHOTO_FROM_GALLERY);*/
        try {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, CHOOSE_PHOTO_FROM_GALLERY);
        } catch (Exception e) {
            Log.e("choosePhotoFromGallery() " , e.getMessage());
        }

    }

    /**
     * This method is used for crop the placeholder which selected or captured
     */
    public void beginCrop(Uri sourceUri) {
        try {
            Uri outputUri = Uri.fromFile(imageHelper.createImageFile());
            Crop.of(sourceUri, outputUri).asSquare().start(loginActivity, this);
        } catch (Exception e) {
            Log.e("beginCrop() " , e.getMessage());
        }


    }

    /**
     * This method is used for handel result after select placeholder from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        try {
            if (data != null) {
                picUri = data.getData();
                beginCrop(picUri);

            }
        } catch (Exception e) {
            Log.e("onSelectFromGalleryResult() " , e.getMessage());
        }

    }

    /**
     * This method is used for handel result after captured placeholder from camera .
     */
    private void onCaptureImageResult() {

        try {
            beginCrop(picUri);
        } catch (Exception e) {
            Log.e("onCaptureImageResult() " , e.getMessage());
        }

    }

    /**
     * This method is used for  handel crop result after crop the placeholder.
     */
    private void handleCrop(int resultCode, Intent result) {
        try {
            if (resultCode == RESULT_OK) {
                picUri = Crop.getOutput(result);
                AppLog.Log("PIC", picUri.getPath());
                Glide.with(this).load(picUri)
                        .transition(new DrawableTransitionOptions().crossFade()).into
                        (ivRegisterProfileImage);
            } else if (resultCode == Crop.RESULT_ERROR) {
                Utils.showToast(Crop.getError(result).getMessage(), loginActivity);
            }
        } catch (Exception e) {
            Log.e("handleCrop() " , e.getMessage());
        }

    }

    /**
     * this method call a webservice for register a provider
     */

    private void register() {
        try {
            HashMap<String, RequestBody> registerMap = new HashMap<>();
            registerMap.put(Const.Params.FIRST_NAME, ApiClient.makeTextRequestBody
                    (etRegisterFirstName.getText().toString()));
            registerMap.put(Const.Params.LAST_NAME, ApiClient.makeTextRequestBody
                    (etRegisterLastName.getText().toString()));
            registerMap.put(Const.Params.EMAIL, ApiClient.makeTextRequestBody
                    (etRegisterEmail.getText().toString()));
            registerMap.put(Const.Params.ADDRESS, ApiClient.makeTextRequestBody
                    (etRegisterAddress.getText().toString()));
            registerMap.put(Const.Params.PHONE, ApiClient.makeTextRequestBody
                    (etRegisterMobileNumber.getText().toString()));
            registerMap.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody
                    (cityId));
            registerMap.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody
                    (countryId));
            registerMap.put(Const.Params.DEVICE_TOKEN, ApiClient.makeTextRequestBody
                    (loginActivity.preferenceHelper.getDeviceToken()));
            registerMap.put(Const.Params.ZIP_CODE, ApiClient.makeTextRequestBody
                    (etRegisterZipCode.getText().toString()));
            registerMap.put(Const.Params.COUNTRY_PHONE_CODE, ApiClient.makeTextRequestBody
                    (etRegisterCountryCode.getText().toString()));
            registerMap.put(Const.Params.APP_VERSION, ApiClient.makeTextRequestBody
                    (loginActivity.getAppVersion()));
            registerMap.put(Const.Params.IS_PHONE_NUMBER_VERIFIED, ApiClient.makeTextRequestBody
                    (loginActivity.preferenceHelper.getIsPhoneNumberVerified()));
            registerMap.put(Const.Params.IS_EMAIL_VERIFIED, ApiClient.makeTextRequestBody
                    (loginActivity.preferenceHelper.getIsEmailVerified()));
            registerMap.put(Const.Params.DEVICE_TYPE, ApiClient.makeTextRequestBody
                    (Const.ANDROID));
            registerMap.put(Const.Params.REFERRAL_CODE, ApiClient.makeTextRequestBody
                    (referralCode));
            registerMap.put(Const.Params.SOCIAL_ID, ApiClient.makeTextRequestBody(socialId));
            if (TextUtils.isEmpty(socialId)) {
                registerMap.put(Const.Params.PASS_WORD, ApiClient.makeTextRequestBody
                        (etRegisterPassword.getText().toString()));
                registerMap.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody(Const.MANUAL));
            } else {
                registerMap.put(Const.Params.PASS_WORD, ApiClient.makeTextRequestBody
                        (""));
                registerMap.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody(Const.SOCIAL));
            }

            Utils.showCustomProgressDialog(loginActivity, false);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ProviderDataResponse> register;
            if (picUri == null) {
                register = apiInterface.register(null, registerMap);
            } else {
                if (TextUtils.isEmpty(socialId)) {
                    register = apiInterface.register(ApiClient.makeMultipartRequestBody
                            (loginActivity, picUri, currentPhotoPath, Const.Params
                                    .IMAGE_URL), registerMap);
                } else {
                    register = apiInterface.register(ApiClient.makeMultipartRequestBodySocial(
                            loginActivity, getImageFile(bmSocialProfilePic), Const.Params
                                    .IMAGE_URL), registerMap);
                }
            }
            register.enqueue(new Callback<ProviderDataResponse>() {
                @Override
                public void onResponse(Call<ProviderDataResponse> call, Response<ProviderDataResponse>
                        response) {

                    if (response != null && response.body() != null && loginActivity.parseContent.isSuccessful(response) && loginActivity.parseContent.parseUserStorageData(response)) {
                        Utils.showMessageToast(response.body().getMessage(), loginActivity);
                        loginActivity.goToHomeActivity();

                    } else {
                        loginActivity.preferenceHelper.clearVerification();
                    }
                }

                @Override
                public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                }
            });

        } catch (Exception e) {
            Log.e("register() " , e.getMessage());
        }


    }

    protected boolean isValidate() {
        String msg = null;

        if (TextUtils.isEmpty(etRegisterFirstName.getText().toString().trim())) {

            msg = loginActivity.getString(R.string.msg_please_enter_valid_name);
            etRegisterFirstName.setError(msg);
            etRegisterFirstName.requestFocus();

        } else if (TextUtils.isEmpty(etRegisterLastName.getText().toString().trim())) {

            msg = loginActivity.getString(R.string.msg_please_enter_valid_name);
            etRegisterLastName.setError(msg);
            etRegisterLastName.requestFocus();

        } else if (!Patterns.EMAIL_ADDRESS.matcher(etRegisterEmail.getText().toString().trim()).matches()) {

            msg = loginActivity.getString(R.string.msg_please_enter_valid_email);
            etRegisterEmail.setError(msg);
            etRegisterEmail.requestFocus();

        } else if (etRegisterPassword.getVisibility() == View.VISIBLE && TextUtils.isEmpty
                (etRegisterPassword.getText().toString().trim())) {

            msg = loginActivity.getString(R.string.msg_please_enter_password);
            etRegisterPassword.setError(msg);
            etRegisterPassword.requestFocus();

        } else if (etRegisterPassword.getVisibility() == View.VISIBLE && etRegisterPassword
                .getText().toString().trim().length() < 6) {
            msg = loginActivity.getString(R.string.msg_please_enter_valid_password);
            etRegisterPassword.setError(msg);
            etRegisterPassword.requestFocus();
        } else if (etRegisterPassword.getVisibility() == View.VISIBLE && !TextUtils.equals
                (etRegisterPasswordRetype.getText().toString().trim(),
                        etRegisterPassword.getText().toString().trim())) {
            msg = loginActivity.getString(R.string.msg_enter_password_not_match);
            etRegisterPasswordRetype.setError(msg);
            etRegisterPasswordRetype.requestFocus();
        } else if (TextUtils.isEmpty(etSelectCountry.getText().toString().trim())) {
            msg = loginActivity.getString(R.string.msg_please_select_country);
            etSelectCountry.setError(msg);
            etSelectCountry.requestFocus();
        } else if (TextUtils.isEmpty(etSelectCity.getText().toString().trim())) {
            msg = loginActivity.getString(R.string.msg_please_select_city);
            etSelectCity.setError(msg);
            etSelectCity.requestFocus();
        } else if (TextUtils.isEmpty(etRegisterMobileNumber.getText().toString().trim())) {
            msg = loginActivity.getString(R.string.msg_please_enter_valid_mobile_number);
            etRegisterMobileNumber.setError(msg);
            etRegisterMobileNumber.requestFocus();
        } else if (etRegisterMobileNumber.getText().toString().trim().length()
                > maxPhoneNumberLength || etRegisterMobileNumber.getText().toString().trim().length
                () < minPhoneNumberLength) {
            msg = loginActivity.getString(R.string.msg_please_enter_valid_mobile_number) + " " +
                    "" + minPhoneNumberLength + loginActivity.getString(R.string.text_or)
                    + maxPhoneNumberLength + " " + loginActivity.getString(R.string
                    .text_digits);
            etRegisterMobileNumber.setError(msg);
            etRegisterMobileNumber.requestFocus();
        } else if (loginActivity.preferenceHelper.getIsProfilePictureRequired() && picUri == null) {
            msg = getResources().getString(R.string
                    .msg_plz_upload_profile_pic);
            Utils.showToast(msg, loginActivity);
        } else if (!cbTcPolicy.isChecked()) {
            msg = getResources().getString(R.string
                    .msg_plz_accept_tc);
            Utils.showToast(msg, loginActivity);
        }

        return TextUtils.isEmpty(msg);
    }

    private void openCountryCodeDialog() {
        try {
            if (customCountryDialog != null && customCountryDialog.isShowing()) {
                return;
            }

            customCountryDialog = new CustomCountryDialog(loginActivity,
                    countryList) {
                @Override
                public void onSelect(int position) {
                    setCountry(position);
                    Utils.hideSoftKeyboard(loginActivity);
                    dismiss();
                }
            };
            customCountryDialog.show();

        } catch (Exception e) {
            Log.e(" " , e.getMessage());
        }
    }


    private void openCityNameDialog() {

        try {
            if (customCityDialog != null && customCityDialog.isShowing()) {
                return;
            }

            customCityDialog = new CustomCityDialog(loginActivity, cityList) {
                @Override
                public void onSelect(int position) {

                    cityId = cityList.get(position).getId();
                    etSelectCity.setText(cityList.get(position).getCityNickName());
                    Utils.hideSoftKeyboard(loginActivity);
                    etSelectCity.setError(null);
                    etRegisterMobileNumber.requestFocus();
                    dismiss();
                }
            };
            customCityDialog.show();

        } catch (Exception e) {
            Log.e("openCityNameDialog() " , e.getMessage());
        }
    }

    /**
     * this method call a webservice for get country list
     */
    private void getCountries() {
        try {
            Utils.showCustomProgressDialog(loginActivity, false);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CountriesResponse> countriesItemCall = apiInterface.getCountries();
            countriesItemCall.enqueue(new Callback<CountriesResponse>() {
                @Override
                public void onResponse(Call<CountriesResponse> call, Response<CountriesResponse>
                        response) {
                    countryList = loginActivity.parseContent.parseCountries(response);
                    if (loginActivity.locationHelper.getLastLocation() != null) {
                        Location location = loginActivity.locationHelper.getLastLocation();
                        //  new GetCityAsyncTask(location.getLatitude(), location.getLongitude()).execute();
                        if (location != null)
                        {
                            getCityGeocodeMapbox(location.getLatitude(), location.getLongitude());
                        }
                    }else {
                        setCountry(0);
                    }
                }

                @Override
                public void onFailure(Call<CountriesResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);

                }
            });
        } catch (Exception e) {
            Log.e("getCountries() " , e.getMessage());
        }

    }

    private void getCityGeocodeMapbox(double latitude, double longitude)
    {
        MapboxGeocoding reverseGeocode = MapboxGeocoding.builder()
                .accessToken(getResources().getString(R.string
                        .MAP_BOX_ACCESS_TOKEN))
                .query(Point.fromLngLat(longitude, latitude))
                .build();
        reverseGeocode.enqueueCall(new Callback<GeocodingResponse>() {
            @Override
            public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response)
            {
                if (response != null)
                {
                    String countryName, cityName;
                    List<CarmenFeature> results = response.body().features();
                    if (results.size() > 0) {
                        Log.d("TAG", "onResponse: " + response.body().toJson());
                        LatLng latLng = null;
                        if (response.body().query().size() > 0)
                        {
                            latLng = new LatLng(Double.parseDouble(response.body().query().get(1).toString()), Double.parseDouble(response.body().query().get(0).toString()));
                        }

                        if (response.body().features().size() > 0)
                        {
                            if (response.body().features().get(0).placeName() != null && !response.body().features().get(0).placeName().equals(""))
                            {
                              //  currentAddress = response.body().features().get(0).placeName();
                            }
                        }

                        if (response.body().features().size() > 0)
                        {
                            if (response.body().features().get(0).context().size() > 0)
                            {
                                for(int i=0;i<response.body().features().get(0).context().size();i++)
                                {
                                    if(response.body().features().get(0).context().get(i).id().contains("locality"))
                                    {
                                    }
                                    else if(response.body().features().get(0).context().get(i).id().contains("place"))
                                    {

                                    }
                                    else if(response.body().features().get(0).context().get(i).id().contains("district"))
                                    {
                                        if(response.body().features().get(0).context().get(i).text()!=null)
                                        {
                                            cityName = response.body().features().get(0).context().get(i).text();
                                        }
                                        else {
                                            cityName  ="";
                                        }
                                    }
                                    else if(response.body().features().get(0).context().get(i).id().contains("region"))
                                    {
                                    }
                                    else if(response.body().features().get(0).context().get(i).id().contains("country"))
                                    {

                                        if(response.body().features().get(0).context().get(i).text()!=null)
                                        {
                                            countryName=response.body().features().get(0).context().get(i).text();
                                            checkCountryCode(countryName);
                                        }
                                        else {
                                            countryName="";
                                        }
                                    }
                                    else
                                    {
                                    }
                                }
                                }
                            }
                        }
                        else {
                        }
                    }
                }
            @Override
            public void onFailure(Call<GeocodingResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "" + R.string.error_code_413, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * this method call a webservice for get city list
     */
    private void getCities(String countryId) {
        try {
            Utils.showCustomProgressDialog(loginActivity, false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.COUNTRY_ID, countryId);
            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CityResponse> cityResponseCall = apiInterface.getCities(ApiClient
                    .makeJSONRequestBody(jsonObject));
            cityResponseCall.enqueue(new Callback<CityResponse>() {
                @Override
                public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {

                    if (response != null && response.body() != null && loginActivity.parseContent.isSuccessful(response) && loginActivity.parseContent.parseCities(response) != null) {
                        cityList = loginActivity.parseContent.parseCities(response);
                    }
                }

                @Override
                public void onFailure(Call<CityResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                }
            });

        } catch (Exception e) {
            Log.e("getCities() " , e.getMessage());
        }

    }

    private void updateUiForOptionalFiled(boolean isUpdate) {
        try {
            if (isUpdate) {
                tilRegisterAddress.setVisibility(View.VISIBLE);
            } else {
                tilRegisterAddress.setVisibility(View.GONE);
                etRegisterMobileNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
                etRegisterMobileNumber.setOnEditorActionListener(this);
            }

        } catch (Exception e) {
            Log.e("updateUiForOptionalFiled() " , e.getMessage());
        }

    }

    private void checkValidationForRegister() {
        try {
            if (isValidate()) {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put(Const.Params.TYPE, String.valueOf(Const.TYPE_PROVIDER));

                    switch (loginActivity.checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            jsonObject.put(Const.Params.EMAIL, etRegisterEmail.getText().toString
                                    ());
                            jsonObject.put(Const.Params.PHONE, etRegisterMobileNumber
                                    .getText()
                                    .toString());
                            jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, etRegisterCountryCode
                                    .getText()
                                    .toString());
                            getOtpVerify(jsonObject);
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            jsonObject.put(Const.Params.PHONE, etRegisterMobileNumber
                                    .getText()
                                    .toString());
                            jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, etRegisterCountryCode
                                    .getText()
                                    .toString());
                            getOtpVerify(jsonObject);
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            jsonObject.put(Const.Params.EMAIL, etRegisterEmail.getText().toString
                                    ());
                            getOtpVerify(jsonObject);
                            break;
                        default:
                            register();
                            break;
                    }
                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
                }

            }
        } catch (Exception e) {
            Log.e("checkValidationForRegister() " , e.getMessage());
        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etRegisterZipCode:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkValidationForRegister();
                    return true;
                }
                break;
            case R.id.etRegisterMobileNumber:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkValidationForRegister();
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }

        return false;
    }

    /**
     * this method call a webservice for get OTP of email or mobile
     *
     * @param jsonObject
     */
    private void getOtpVerify(JSONObject jsonObject) {
        try {
            Utils.showCustomProgressDialog(loginActivity, false);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<OtpResponse> otpResponseCall = apiInterface.getOtpVerify(ApiClient
                    .makeJSONRequestBody(jsonObject));
            otpResponseCall.enqueue(new Callback<OtpResponse>() {
                @Override
                public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && loginActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        AppLog.Log("SMS_OTP", response.body().getOtpForSms());
                        AppLog.Log("EMAIL_OTP", response.body().getOtpForEmail());
                        switch (loginActivity.checkWitchOtpValidationON()) {
                            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                                openOTPVerifyDialog(response.body().getOtpForEmail(), response
                                        .body().getOtpForSms(), getResources().getString(R
                                        .string.text_email_otp), getResources().getString(R.string
                                        .text_phone_otp), true);
                                break;
                            case Const.SMS_VERIFICATION_ON:
                                openOTPVerifyDialog(response.body().getOtpForEmail(), response
                                        .body().getOtpForSms(), "", getResources().getString(R
                                        .string
                                        .text_phone_otp), false);
                                break;
                            case Const.EMAIL_VERIFICATION_ON:
                                openOTPVerifyDialog(response.body().getOtpForEmail(), response
                                        .body().getOtpForSms(), "", getResources().getString(R
                                        .string
                                        .text_email_otp), false);
                                break;

                            default:
                                // do with default
                                break;
                        }


                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), loginActivity);
                    }
                }


                @Override
                public void onFailure(Call<OtpResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                }
            });

        } catch (Exception e) {
            Log.e("getOtpVerify() " , e.getMessage());
        }


    }

    private void openOTPVerifyDialog(final String otpEmailVerification, final String
            otpSmsVerification, String editTextOneHint, String ediTextTwoHint, boolean
                                             isEditTextOneVisible) {

        try {
            CustomDialogVerification customDialogVerification = new CustomDialogVerification
                    (loginActivity,
                            getResources().getString(R.string.text_verify_detail), getResources()
                            .getString(R
                                    .string.msg_verify_detail), getResources().getString(R.string
                            .text_cancel)
                            , getResources().getString(R.string.text_ok), editTextOneHint,
                            ediTextTwoHint,
                            isEditTextOneVisible,
                            InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {


                    switch (loginActivity.checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextOne.getText().toString(),
                                    otpEmailVerification)) {
                                if (TextUtils.equals(etDialogEditTextTwo
                                                .getText().toString(),
                                        otpSmsVerification)) {
                                    loginActivity.preferenceHelper.putIsEmailVerified(true);
                                    loginActivity.preferenceHelper.putIsPhoneNumberVerified(true);
                                    register();
                                } else {
                                    etDialogEditTextTwo.setError(getResources().getString(R.string
                                            .msg_sms_otp_wrong));
                                }

                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_email_otp_wrong));
                            }
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextTwo
                                            .getText().toString(),
                                    otpSmsVerification)) {
                                loginActivity.preferenceHelper.putIsPhoneNumberVerified(true);
                                register();
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_sms_otp_wrong));
                            }
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextTwo.getText().toString(),
                                    otpEmailVerification)) {
                                loginActivity.preferenceHelper.putIsEmailVerified(true);
                                register();
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_email_otp_wrong));
                            }
                            break;
                        default:
                            // do with default
                            break;
                    }

                }
            };
            customDialogVerification.show();

        } catch (Exception e) {
            Log.e("openOTPVerifyDialog() " , e.getMessage());
        }

    }

    private void checkCountryCode(String country) {


        try {
            int countryListSize = countryList.size();
            for (int i = 0; i < countryListSize; i++) {
                if (countryList.get(i).getCountryName().toUpperCase().startsWith(country.toUpperCase
                        ())) {
                    setCountry(i);
                    Log.d("call","===== Call Geocode ========="+i);

                    return;
                }
            }

            setCountry(0);

        } catch (Exception e) {
            Log.e("checkCountryCode() " , e.getMessage());
        }

    }

    private void setCountry(int position) {

        try {
            if (!countryList.isEmpty() && !TextUtils.equals(countryId, countryList.get(position)
                    .getId())) {
                countryId = countryList.get(position).getId();
                etSelectCity.getText().clear();
                getCities(countryId);
                etRegisterCountryCode.setText(countryList.get(position).getCountryPhoneCode());
                etSelectCountry.setText(countryList.get(position).getCountryName());
                maxPhoneNumberLength = countryList.get(position).getMaxPhoneNumberLength();
                minPhoneNumberLength = countryList.get(position).getMinPhoneNumberLength();
                setContactNoLength(maxPhoneNumberLength);
                etSelectCountry.setError(null);
                etReferralCode.setEnabled(true);
                etReferralCode.getText().clear();
                ivSuccess.setVisibility(View.GONE);
                if (loginActivity.preferenceHelper.getIsReferralOn() && countryList.get(position)
                        .isReferralProvider()) {
                    llReferral.setVisibility(View.VISIBLE);
                } else {
                    llReferral.setVisibility(View.GONE);
                }

            }

        } catch (Exception e) {
            Log.e("setCountry() " , e.getMessage());
        }


    }

    private void setContactNoLength(int length) {

        try {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(length);
            etRegisterMobileNumber.setFilters(FilterArray);
        } catch (Exception e) {
            Log.e("setContactNoLength() " , e.getMessage());
        }

    }

    public void clearError() {

        try{
            if (etRegisterFirstName != null) {
                etRegisterLastName.setError(null);
                etRegisterFirstName.setError(null);
                etRegisterEmail.setError(null);
                etRegisterPassword.setError(null);
                etRegisterAddress.setError(null);
                etRegisterMobileNumber.setError(null);
                etSelectCountry.setError(null);
                etSelectCity.setError(null);
                etRegisterCountryCode.setError(null);
                etRegisterZipCode.setError(null);

            }

        }catch (Exception e){
            Log.e("clearError() " , e.getMessage());
        }


    }

    /**
     * this method will make decision according to permission result
     *
     * @param grantResults set result from system or OS
     */
    private void goWithCameraAndStoragePermission(int[] grantResults) {

        try{
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(loginActivity, android.Manifest
                        .permission.CAMERA)) {
                    openCameraPermissionDialog();
                } else {
                    closedPermissionDialog();
                }
            } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(loginActivity, android.Manifest
                        .permission.READ_EXTERNAL_STORAGE)) {
                    openCameraPermissionDialog();
                } else {
                    closedPermissionDialog();
                }
            } else {
                openPhotoSelectDialog();
            }
        }catch (Exception e){
            Log.e("goWithCameraAndStoragePermission() " , e.getMessage());
        }


    }

    private void openCameraPermissionDialog() {

        try{
            if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
                return;
            }
            closedPermissionDialog = new CustomDialogAlert(loginActivity, getResources().getString(R
                    .string
                    .text_attention), getResources().getString(R.string
                    .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                    (R.string.text_re_try)) {
                @Override
                public void onClickLeftButton() {
                    closedPermissionDialog();
                }

                @Override
                public void onClickRightButton() {
                    requestPermissions(new String[]{android.Manifest
                            .permission
                            .CAMERA, android.Manifest
                            .permission
                            .READ_EXTERNAL_STORAGE}, Const
                            .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                    closedPermissionDialog();
                }

            };
            closedPermissionDialog.show();
        }catch (Exception e){
            Log.e("openCameraPermissionDialog() " , e.getMessage());
        }


    }

    private void closedPermissionDialog() {
        try{
            if (closedPermissionDialog != null && closedPermissionDialog.isShowing()) {
                closedPermissionDialog.dismiss();
                closedPermissionDialog = null;

            }
        }catch (Exception e){
            Log.e("closedPermissionDialog() " , e.getMessage());
        }

    }

    public void checkPermission() {

        try{
            if (ContextCompat.checkSelfPermission(loginActivity, android.Manifest.permission
                    .CAMERA) !=
                    PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission
                    (loginActivity, android.Manifest.permission
                            .READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest
                        .permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
            } else {
                openPhotoSelectDialog();
            }
        }catch (Exception e){
            Log.e("checkPermission() " , e.getMessage());
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    //Do som thing
                    break;
            }
        }
    }

    /**
     * this method call a webservice for check referral code enter by user
     */
    private void checkReferralCode() {

        try{
            Utils.showCustomProgressDialog(loginActivity, false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.REFERRAL_CODE, etReferralCode.getText().toString().trim());
                jsonObject.put(Const.Params.COUNTRY_ID, countryId);
                jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> responseCall = apiInterface.getCheckReferral(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && loginActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        tvReferralApply.setVisibility(View.GONE);
                        ivSuccess.setVisibility(View.VISIBLE);
                        etReferralCode.setEnabled(false);
                        referralCode = etReferralCode.getText().toString();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), loginActivity);
                        etReferralCode.getText().clear();
                    }


                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                }
            });
        }catch (Exception e){
            Log.e("checkReferralCode() " , e.getMessage());
        }

    }


    public void updateUiForSocialLogin(String email, String socialId, String firstName, String
            lastName, Uri profileUri) {

        try{
            if (!TextUtils.isEmpty(email)) {
                etRegisterEmail.setText(email);
                etRegisterEmail.setEnabled(false);
                etRegisterEmail.setFocusableInTouchMode(false);
                loginActivity.preferenceHelper.putIsEmailVerified(true);
            }
            this.socialId = socialId;
            etRegisterFirstName.setText(firstName);
            etRegisterLastName.setText(lastName);
            picUri = profileUri;
            etRegisterPassword.setVisibility(View.GONE);
            etRegisterPasswordRetype.setVisibility(View.GONE);
            tilPassword.setVisibility(View.GONE);
            tilRetypePassword.setVisibility(View.GONE);

            if (picUri != null) {
                Utils.showCustomProgressDialog(loginActivity, false);
                Glide.with(loginActivity).asBitmap()
                        .load(picUri.toString())
                        .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.man_user)
                        .listener(new RequestListener<Bitmap>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                AppLog.handleException(getClass().getSimpleName(), e);
                                Utils.showToast(e.getMessage(), loginActivity);
                                Utils.hideCustomProgressDialog();
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                bmSocialProfilePic = resource;
                                ivRegisterProfileImage.setImageBitmap(bmSocialProfilePic);
                                Utils.hideCustomProgressDialog();
                                return true;
                            }

                        }).into(ivRegisterProfileImage);
            }
        }catch (Exception e){
            Log.e("updateUiForSocialLogin() " , e.getMessage());
        }

    }

    private File getImageFile(Bitmap bitmap) {

        File imageFile = new File(loginActivity.getFilesDir(), "name.jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            AppLog.handleException(getClass().getSimpleName(), e);
        }
        return imageFile;
    }

    private void checkSocialLoginISOn(boolean isSocialLogin) {

        try{
            if (isSocialLogin) {
                llSocialLogin.setVisibility(View.VISIBLE);
            } else {
                llSocialLogin.setVisibility(View.GONE);
            }
        }catch (Exception e){
            Log.e("checkSocialLoginISOn() " , e.getMessage());
        }

    }

    /**
     * this class will help to get current city or county according current location
     */
   /* private class GetCityAsyncTask extends AsyncTask<String, Void, Address>
    {


        double lat, lng;

        public GetCityAsyncTask(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        protected Address doInBackground(String... params) {

            Geocoder geocoder = new Geocoder(loginActivity, new Locale("en_US"));
            try {
                List<Address> addressList = geocoder.getFromLocation(lat,
                        lng, 1);
                if (addressList != null && !addressList.isEmpty()) {

                    Address address = addressList.get(0);
                    return address;
                }

            } catch (IOException e) {
                AppLog.handleException(RegisterFragment.class.getName(), e);
                publishProgress();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            setCountry(0);
        }

        @Override
        protected void onPostExecute(Address address) {
            String countryName, cityName;
            if (address != null)
            {
                countryName = address.getCountryName();
                checkCountryCode(countryName);
                if (address.getLocality() != null) {
                    cityName = address.getLocality();
                } else if (address.getSubAdminArea() != null) {
                    cityName = address.getSubAdminArea();
                } else {
                    cityName = address.getAdminArea();
                }
                AppLog.Log(RegisterFragment.class.getName(), "countryName= " + countryName);
                    AppLog.Log(RegisterFragment.class.getName(), "cityName= " + cityName);
            }
        }
    }*/
}

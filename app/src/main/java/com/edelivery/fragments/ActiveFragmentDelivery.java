package com.edelivery.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.edelivery.AvailableDeliveryActivity;
import com.edelivery.CompleteOrderActivity;
import com.edelivery.HistoryActivity;
import com.edelivery.R;
import com.edelivery.adapter.ActiveDeliveryAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.AvailableOrder;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.responsemodels.AvailableOrdersResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 09-Mar-17.
 */

public class ActiveFragmentDelivery extends Fragment implements View.OnClickListener {


    public SwipeRefreshLayout srlActiveDelivery;
    public ArrayList<AvailableOrder> activeOrderList = new ArrayList<>();
    private RecyclerView rcvActiveDelivery;
    private ActiveDeliveryAdapter activeDeliveryAdapter;
    private LinearLayout ivEmpty;
    private OrderStatusReceiver orderStatusReceiver;
    private AvailableDeliveryActivity availableDeliveryActivity;

    public LinearLayout skeletonLayout;
    public ShimmerLayout shimmer;
    public LayoutInflater inflater;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        availableDeliveryActivity = (AvailableDeliveryActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View activeFrag = inflater.inflate(R.layout.fragment_active_delivery, container, false);
        rcvActiveDelivery = (RecyclerView) activeFrag.findViewById(R.id.rcvActiveDelivery);
        srlActiveDelivery = (SwipeRefreshLayout) activeFrag.findViewById(R.id.srlActiveDelivery);
        skeletonLayout = (LinearLayout) activeFrag.findViewById(R.id.skeletonLayout);
        shimmer = (ShimmerLayout) activeFrag.findViewById(R.id.shimmerSkeleton);
        ivEmpty = (LinearLayout) activeFrag.findViewById(R.id.ivEmpty);
        return activeFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        orderStatusReceiver = new OrderStatusReceiver();
        activeOrderList = new ArrayList<>();
        srlActiveDelivery.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getActiveOrders();
            }
        });

        setShimmerEffect();
        initRcvActiveDelivery();
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.Action.ACTION_ORDER_STATUS);
        intentFilter.addAction(Const.Action.ACTION_STORE_CANCELED_REQUEST);
        availableDeliveryActivity.registerReceiver(orderStatusReceiver, intentFilter);

    }
    public void setShimmerEffect() {

        this.inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        showSkeleton(true);

    }
    @Override
    public void onStop() {
        super.onStop();
        availableDeliveryActivity.unregisterReceiver(orderStatusReceiver);
    }
    public void showSkeleton(boolean show) {

        if (show) {

            skeletonLayout.removeAllViews();

            int skeletonRows = getSkeletonRowCount(getActivity());
            for (int i = 0; i <= skeletonRows; i++) {
                ViewGroup rowLayout = (ViewGroup) inflater
                        .inflate(R.layout.skeleton_row_layout, null);
                skeletonLayout.addView(rowLayout);
            }
            shimmer.setVisibility(View.VISIBLE);
            shimmer.startShimmerAnimation();
            skeletonLayout.setVisibility(View.VISIBLE);
            skeletonLayout.bringToFront();

            animateReplaceSkeleton(shimmer);
        } else {
            shimmer.stopShimmerAnimation();
            shimmer.setVisibility(View.GONE);
        }
    }

    public void animateReplaceSkeleton(View listView) {

        listView.setVisibility(View.VISIBLE);
        listView.setAlpha(0f);
        listView.animate().alpha(1f).setDuration(8000).start();

    }
    public void getActiveOrders() {
        JSONObject jsonObject = new JSONObject();
        try {

                jsonObject.put(Const.Params.PROVIDER_ID, PreferenceHelper.getInstance(getActivity()).getProviderId());
                jsonObject.put(Const.Params.SERVER_TOKEN,PreferenceHelper.getInstance(getActivity()).getSessionToken());


        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
        }
        AppLog.Log("GET_ACTIVE_ORDERS", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableOrdersResponse> orderResponseCall = apiInterface.getActiveOrders(ApiClient
                .makeJSONRequestBody(jsonObject));
        orderResponseCall.enqueue(new Callback<AvailableOrdersResponse>() {
            @Override
            public void onResponse(Call<AvailableOrdersResponse> call,
                                   Response<AvailableOrdersResponse> response) {
                activeOrderList.clear();
                if (response != null && response.body() != null && ParseContent.getInstance().isSuccessful(response)) {
                    activeOrderList.addAll(availableDeliveryActivity
                            .parseContent.parseOrders(response));
                }
                updateUiList();
                initRcvActiveDelivery();
                availableDeliveryActivity.updateDeliveryCount(1, activeOrderList.size());
                srlActiveDelivery.setRefreshing(false);
                showSkeleton(false);
            }

            @Override
            public void onFailure(Call<AvailableOrdersResponse> call, Throwable t) {
                showSkeleton(false);
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);
            }
        });
    }

    @Override
    public void onClick(View view) {
        // do somethings
    }

    public void initRcvActiveDelivery() {
        if (activeDeliveryAdapter != null) {
            activeDeliveryAdapter.notifyDataSetChanged();
        } else {
            activeDeliveryAdapter = new ActiveDeliveryAdapter(availableDeliveryActivity,
                    activeOrderList);
            rcvActiveDelivery.setLayoutManager(new LinearLayoutManager
                    (availableDeliveryActivity));
            rcvActiveDelivery.setAdapter(activeDeliveryAdapter);
            rcvActiveDelivery.addItemDecoration(new DividerItemDecoration(availableDeliveryActivity,
                    LinearLayoutManager.VERTICAL));
            rcvActiveDelivery.addOnItemTouchListener(new RecyclerTouchListener
                    (availableDeliveryActivity, rcvActiveDelivery, new ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            AvailableOrder availableOrder = activeOrderList.get
                                    (position);
                            Order order = availableOrder
                                    .getOrderList()
                                    .get(0);
                            switch (order.getDeliveryStatus()) {
                                case Const.ProviderStatus.FINAL_ORDER_COMPLETED:
                                    /*goToCompleteOrderActivity(order,
                                            availableOrder.getId(), true, true);*/
                                    goToHistoryActivity();
                                    break;
                                case Const.ProviderStatus.DELIVERY_MAN_ACCEPTED:
                                case Const.ProviderStatus.DELIVERY_MAN_ARRIVED:
                                case Const.ProviderStatus.DELIVERY_MAN_COMING:
                                case Const.ProviderStatus.DELIVERY_MAN_PICKED_ORDER:
                                case Const.ProviderStatus.DELIVERY_MAN_STARTED_DELIVERY:
                                case Const.ProviderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                                    availableDeliveryActivity.goToActiveDeliveryActivity
                                            (availableOrder.getId());
                                    break;
                                default:
                                    Utils.showToast(availableDeliveryActivity.getResources()
                                                    .getString(R.string.text_delivery_status) + "" +
                                                    " " +
                                                    order.getDeliveryStatus(),
                                            availableDeliveryActivity);
                                    break;

                            }

                        }

                        @Override
                        public void onLongClick(View view, int position) {
                            // do somethings

                        }
                    }));
        }


    }

    private void goToHistoryActivity() {
        Intent intent = new Intent(availableDeliveryActivity, HistoryActivity.class);
        startActivity(intent);
        availableDeliveryActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void updateUiList() {
        if (activeOrderList.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvActiveDelivery.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvActiveDelivery.setVisibility(View.VISIBLE);
        }

    }

    public int getSkeletonRowCount(Context context) {
        int pxHeight = getDeviceHeight(context);
        int skeletonRowHeight = (int) getResources()
                .getDimension(R.dimen.row_layout_height); //converts to pixel
        return (int) Math.ceil(pxHeight / skeletonRowHeight);
    }

    public int getDeviceHeight(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return metrics.heightPixels;
    }
    /**
     * this method open Complete order activity witch is contain two fragment
     * Invoice Fragment and Feedback Fragment
     *
     * @param goToInvoiceFirst           set true when you want to open invoice fragment when
     *                                   activity open
     * @param backActiveDeliveryActivity set true when you want
     *                                   goToActiveDeliveryActivity when press back
     *                                   button
     */
    private void goToCompleteOrderActivity(Order order, String
            requestId, boolean goToInvoiceFirst, boolean backActiveDeliveryActivity) {
        Intent intent = new Intent(availableDeliveryActivity, CompleteOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Const.GO_TO_INVOICE, goToInvoiceFirst);
        bundle.putParcelable(Const.ORDER_PAYMENT, null);
        bundle.putString(Const.PAYMENT, null);
        bundle.putParcelable(Const.USER_DETAIL, order);
        bundle.putString(Const.Params.REQUEST_ID, requestId);
        intent.putExtra(Const.BUNDLE, bundle);
        intent.putExtra(Const.BACK_TO_ACTIVE_DELIVERY, backActiveDeliveryActivity);
        startActivity(intent);
        availableDeliveryActivity.overridePendingTransition(R.anim.slide_in_right, R.anim
                .slide_out_left);
    }

    private class OrderStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Utils.showCustomProgressDialog(availableDeliveryActivity, false);
            getActiveOrders();
        }
    }
}

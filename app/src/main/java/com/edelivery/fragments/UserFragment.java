package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.BankDetailActivity;
import com.edelivery.HelpActivity;
import com.edelivery.PaymentActivity;
import com.edelivery.ProfileActivity;
import com.edelivery.R;
import com.edelivery.ReferralShareActivity;
import com.edelivery.SettingActivity;
import com.edelivery.adapter.UserMenuAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.utils.AppLog;

/**
 * Created by elluminati on 14-Feb-17.
 */

public class UserFragment extends BaseFragments {

    private RecyclerView rcvUserMenu;
    private CustomFontTextView tvAppVersion;
    private CustomDialogAlert logoutDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity.setTitleOnToolBar(homeActivity.getResources().getString(R.string.text_user));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View userFragView = inflater.inflate(R.layout.fragment_user, container, false);
        rcvUserMenu = (RecyclerView) userFragView.findViewById(R.id.rcvUserMenu);
        tvAppVersion = (CustomFontTextView) userFragView.findViewById(R.id.tvAppVersion);

        return userFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRcvUserMenu();
        setAppVersion();

    }

    @Override
    public void onClick(View view) {

    }

    private void initRcvUserMenu() {
        UserMenuAdapter userMenuAdapter = new UserMenuAdapter(homeActivity);
        rcvUserMenu.setLayoutManager(new LinearLayoutManager(homeActivity));
        rcvUserMenu.setAdapter(userMenuAdapter);
        rcvUserMenu.addOnItemTouchListener(new RecyclerTouchListener(homeActivity, rcvUserMenu, new
                ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        AppLog.Log("POSITION", position + "");
                        switch (position) {
                            case 0:
                                goToProfileActivity();
                                break;
                            /*case 1:
                                goToBankDetailActivity();
                                break;
                            case 2:
                                homeActivity.goToDocumentActivity(false);
                                break;*/
                            case 1:
                                goToReferralShareActivity();
                                break;
                            case 2:
                                goToSettingActivity();
                                break;
                           /* case 5:
                                goToPaymentsActivity();
                                break;
                            case 6:
                                homeActivity.goToVehicleDetailActivity(false);
                                break;
                            case 7:
                                goToHelpActivity();
                                break;*/
                            case 3:
                                openLogoutDialog();
                                break;
                            default:
                                // do with default
                                break;
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void openLogoutDialog() {
        if (logoutDialog != null && logoutDialog.isShowing()) {
            return;
        }

        logoutDialog = new CustomDialogAlert(homeActivity, homeActivity
                .getResources()
                .getString(R.string.text_log_out), homeActivity.getResources()
                .getString(R.string.msg_are_you_sure), homeActivity.getResources()
                .getString(R.string.text_cancel), homeActivity.getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                homeActivity.logOut();
                dismiss();
            }
        };
        logoutDialog.show();
    }


    private void setAppVersion() {
        tvAppVersion.setText(homeActivity.getResources().getString(R.string.text_app_version)
                + " " + homeActivity.getAppVersion());
    }

    private void goToProfileActivity() {
        Intent intent = new Intent(homeActivity, ProfileActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToBankDetailActivity() {
        Intent intent = new Intent(homeActivity, BankDetailActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToSettingActivity() {
        Intent intent = new Intent(homeActivity, SettingActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToReferralShareActivity() {
        Intent intent = new Intent(homeActivity, ReferralShareActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToPaymentsActivity() {
        Intent intent = new Intent(homeActivity, PaymentActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToHelpActivity() {
        Intent intent = new Intent(homeActivity, HelpActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}

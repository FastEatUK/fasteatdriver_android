package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.WalletTransactionActivity;
import com.edelivery.WalletTransactionDetailActivity;
import com.edelivery.adapter.WalletTransactionAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.models.datamodels.WalletRequestDetail;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.WalletTransactionResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 01-Nov-17.
 */

public class WalletTransactionFragment extends Fragment {
    public static final String TAG = WalletTransactionFragment.class.getName();
    private RecyclerView rcvWalletData;
    private WalletTransactionActivity activity;
    private CustomDialogAlert customDialogAlert;

    private ArrayList<WalletRequestDetail> walletRequestDetails = new ArrayList<>();
    private WalletTransactionAdapter transactionAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (WalletTransactionActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet_transection, container, false);
        rcvWalletData = (RecyclerView) view.findViewById(R.id.rcvWalletData);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRcvWalletTransaction();
        getWalletTransaction();

    }

    private void initRcvWalletTransaction() {
        walletRequestDetails = new ArrayList<>();
        rcvWalletData.setLayoutManager(new LinearLayoutManager(activity));
        transactionAdapter = new WalletTransactionAdapter(this, walletRequestDetails);
        rcvWalletData.setAdapter(transactionAdapter);
    }

    private void getWalletTransaction() {

        Utils.showCustomProgressDialog(activity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.ID, activity.preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
        } catch (JSONException e) {
            AppLog.handleException(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletTransactionResponse> call = apiInterface.getWalletTransaction(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<WalletTransactionResponse>() {
            @Override
            public void onResponse(Call<WalletTransactionResponse> call,
                                   Response<WalletTransactionResponse> response) {
                Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        walletRequestDetails.clear();
                        walletRequestDetails.addAll(response.body().getWalletRequestDetail());
                        Collections.sort(walletRequestDetails, new
                                Comparator<WalletRequestDetail>() {
                                    @Override
                                    public int compare(WalletRequestDetail lhs,
                                                       WalletRequestDetail rhs) {
                                        return compareTwoDate(lhs.getCreatedAt(), rhs
                                                .getCreatedAt());
                                    }
                                });
                        transactionAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), activity);
                    }


                }



            @Override
            public void onFailure(Call<WalletTransactionResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);

            }
        });
    }

    private void canceledWithdrawalRequest(String walletId) {
        Utils.showCustomProgressDialog(activity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.WALLET_STATUS, Const.Wallet.WALLET_STATUS_CANCELLED);
            jsonObject.put(Const.Params.ID, walletId);
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
        } catch (JSONException e) {
            AppLog.handleException(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> call = apiInterface.cancelWithdrawalRequest(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call,
                                   Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        getWalletTransaction();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), activity);
                    }

                }



            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);

            }
        });

    }

    public void goToWalletTransactionActivity(WalletRequestDetail walletRequestDetail) {
        Intent intent = new Intent(activity, WalletTransactionDetailActivity.class);
        intent.putExtra(Const.BUNDLE, walletRequestDetail);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public void openCancelWithdrawalRequestDialog(final String walletId) {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            return;
        }

        customDialogAlert = new CustomDialogAlert(activity, activity
                .getResources()
                .getString(R.string.text_cancel_wallet_request), activity.getResources()
                .getString(R.string.msg_are_you_sure), activity.getResources()
                .getString(R.string.text_cancel), activity.getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                canceledWithdrawalRequest(walletId);
                dismiss();
            }
        };
        customDialogAlert.show();

    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = activity.parseContent.webFormat;
            SimpleDateFormat dateFormat = activity.parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(WalletTransactionFragment.class.getName(), e);
        }
        return 0;
    }
}

package com.edelivery.fragments;

import android.content.res.TypedArray;
import android.os.Bundle;
import androidx.annotation.Nullable;


import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edelivery.LoginActivity;
import com.edelivery.R;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomLanguageDialog;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.ProviderDataResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 30-Jan-17.
 */

public class LoginFragment extends Fragment implements TextView.OnEditorActionListener, View
        .OnClickListener {


    private CustomFontEditTextView etLoginEmail, etLoginPassword;
    private CustomFontTextView tvForgotPassword, tvLanguage;
    private CustomFontButton btnLogin;
    private TextInputLayout tilEmail;
    private LoginActivity loginActivity;
    private ImageView btnFb, btnGoogle, btnTwitter;
    private CustomDialogVerification forgotPassDialog;
    private CustomLanguageDialog customLanguageDialog;
    private LinearLayout llSocialLogin;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginActivity = (LoginActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {

        View loginFragView = inflater.inflate(R.layout.fragment_login, container, false);
        etLoginEmail = (CustomFontEditTextView) loginFragView.findViewById(R.id.etLoginEmail);
        etLoginPassword = (CustomFontEditTextView) loginFragView.findViewById(R.id.etLoginPassword);
        tvForgotPassword = (CustomFontTextView) loginFragView.findViewById(R.id.tvForgotPassword);
        btnLogin = (CustomFontButton) loginFragView.findViewById(R.id.btnLogin);
        tilEmail = (TextInputLayout) loginFragView.findViewById(R.id.tilEmail);
        tvLanguage = (CustomFontTextView) loginFragView.findViewById(R.id.tvLanguage);
        btnFb = (ImageView) loginFragView.findViewById(R.id.btnFb);
        btnGoogle = (ImageView) loginFragView.findViewById(R.id.btnGoogle);
        llSocialLogin = (LinearLayout) loginFragView.findViewById(R.id.llSocialButton);
        btnTwitter = (ImageView) loginFragView.findViewById(R.id.btnTwitter);
        return loginFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setLanguageName();
        enabledLoginBy();
        checkSocialLoginISOn(loginActivity.preferenceHelper.getIsLoginBySocial());
        tvForgotPassword.setOnClickListener(this);
        etLoginPassword.setOnEditorActionListener(this);
        btnLogin.setOnClickListener(this);
        tvLanguage.setOnClickListener(this);
        btnFb.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (isValidate()) {
                    login("","");
                }
                break;
            case R.id.tvForgotPassword:
                openForgotPasswordDialog();
                break;
            case R.id.tvLanguage:
                /*openLanguageDialog();*/
                break;
            case R.id.btnFb:
                loginActivity.loginWithFacebook();
                break;
            case R.id.btnGoogle:
                loginActivity.loginWithGoogle();
                break;
            case R.id.btnTwitter:
                loginActivity.loginWithTwitter();
                break;
            default:
                // do with default
                break;
        }
    }

    protected boolean isValidate() {
        String msg = null;
        if (TextUtils.equals(tilEmail.getHint().toString(), getResources().getString(R.string
                .text_email_or_phone))) {
            if (Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = validatePassword();
            } else if (Patterns.PHONE.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = validatePassword();
            } else {
                msg = getString(R.string.msg_please_enter_valid_email_or_phone);
                etLoginEmail.setError(msg);
                etLoginEmail.requestFocus();
            }

        } else if (TextUtils.equals(tilEmail.getHint().toString().trim(), getResources().getString(R
                .string.text_phone))) {
            if (!Patterns.PHONE.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = getString(R.string.msg_please_enter_valid_phone);
                etLoginEmail.setError(msg);
                etLoginEmail.requestFocus();
            } else {
                msg = validatePassword();
            }

        } else if (TextUtils.equals(tilEmail.getHint().toString().trim(), getResources().getString(R
                .string.text_email))) {
            if (!Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = getString(R.string.msg_please_enter_valid_email);
                etLoginEmail.setError(msg);
                etLoginEmail.requestFocus();
            } else {
                msg = validatePassword();
            }

        }
        return TextUtils.isEmpty(msg);
    }

    private String validatePassword() {
        String msg = null;
        if (TextUtils.isEmpty(etLoginPassword.getText().toString().trim())) {
            msg = getString(R.string.msg_enter_password);
            etLoginPassword.setError(msg);
            etLoginPassword.requestFocus();
        } else if (etLoginPassword.getText().toString().trim().length() < 6) {
            msg = getString(R.string.msg_please_enter_valid_password);
            etLoginPassword.setError(msg);
            etLoginPassword.requestFocus();
        }
        return msg;
    }

    public void login(String socialId, String email) {

        try{
            JSONObject jsonObject = new JSONObject();

            try {
                if (TextUtils.isEmpty(socialId)) {
                    jsonObject.put(Const.Params.EMAIL, etLoginEmail.getText().toString());
                    jsonObject.put(Const.Params.PASS_WORD, etLoginPassword.getText().toString());
                    jsonObject.put(Const.Params.SOCIAL_ID, socialId);
                    jsonObject.put(Const.Params.LOGIN_BY, Const.MANUAL);
                } else {
                    jsonObject.put(Const.Params.EMAIL, email);
                    jsonObject.put(Const.Params.PASS_WORD, "");
                    jsonObject.put(Const.Params.SOCIAL_ID, socialId);
                    jsonObject.put(Const.Params.LOGIN_BY, Const.SOCIAL);
                }
                jsonObject.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
                jsonObject.put(Const.Params.DEVICE_TOKEN, loginActivity.preferenceHelper
                        .getDeviceToken());
                jsonObject.put(Const.Params.APP_VERSION, loginActivity.getAppVersion());
            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, e);
            }

            Utils.showCustomProgressDialog(loginActivity, false);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ProviderDataResponse> providerDataResponseCall = apiInterface.login(ApiClient
                    .makeJSONRequestBody(jsonObject));
            providerDataResponseCall.enqueue(new Callback<ProviderDataResponse>() {
                @Override
                public void onResponse(Call<ProviderDataResponse> call, Response<ProviderDataResponse
                        > response) {
                    if (response != null && response.body() != null &&loginActivity != null && loginActivity.parseContent != null && loginActivity.parseContent.isSuccessful(response) && loginActivity.parseContent.parseUserStorageData(response)) {
//                        Utils.showMessageToast(response.body().getMessage(), loginActivity);

                        loginActivity.goToHomeActivity();
                    }


                }

                @Override
                public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
                }
            });

        }catch (Exception e){
            Log.e("login() " , e.getMessage());
        }

    }

    private void openForgotPasswordDialog() {

        try{
            if (forgotPassDialog != null && forgotPassDialog.isShowing()) {
                return;
            }

            forgotPassDialog = new CustomDialogVerification
                    (loginActivity, loginActivity.getString(R.string.text_forgot_password),
                            loginActivity.getString(R.string.msg_forgot_password), loginActivity
                            .getString(R.string.text_cancel), loginActivity.getString(R
                            .string.text_ok), null, loginActivity.getString(R.string
                            .text_email), false, InputType.TYPE_CLASS_TEXT, InputType
                            .TYPE_TEXT_VARIATION_EMAIL_ADDRESS) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {

                    String email = etDialogEditTextTwo.getText().toString();
                    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches
                            ()) {
                        etDialogEditTextTwo.setError(loginActivity.getString(R.string
                                .msg_please_enter_valid_email));
                    } else {
                        forgotPassword(email);
                        dismiss();
                    }


                }
            };
            forgotPassDialog.show();
        }catch (Exception e){
            Log.e("openForgotPasswordDialog() " , e.getMessage());
        }


    }

    /**
     * this method call webservice for forgot password
     *
     * @param email user email id
     */
    private void forgotPassword(String email) {

        try{
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.TYPE, Const
                        .TYPE_PROVIDER);
                jsonObject.put(Const.Params.EMAIL, email);
            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, e);
            }
            Utils.showCustomProgressDialog(loginActivity, false);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> responseCall = apiInterface.forgotPassword(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && loginActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        Utils.showMessageToast(response.body().getMessage(), loginActivity);

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), loginActivity);
                    }

                }



                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
                }
            });

        }catch (Exception e){
            Log.e("forgotPassword() " , e.getMessage());
        }


    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etLoginPassword:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isValidate()) {
                        login("", "");
                    }
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }

        return false;
    }

    private void enabledLoginBy() {
        try{
            if (loginActivity.preferenceHelper.getIsLoginByEmail() && loginActivity.preferenceHelper
                    .getIsLoginByPhone()) {
                tilEmail.setHint(getResources().getString(R.string.text_email_or_phone));
            } else if (loginActivity.preferenceHelper.getIsLoginByPhone()) {
                tilEmail.setHint(getResources().getString(R.string.text_phone));
            } else {
                tilEmail.setHint(getResources().getString(R.string.text_email));
            }
        }catch (Exception e){
            Log.e("enabledLoginBy() " , e.getMessage());
        }

    }

    public void clearError() {
        try{
            if (etLoginEmail != null) {
                etLoginEmail.setError(null);
                etLoginPassword.setError(null);
            }

        }catch (Exception e){
            Log.e("clearError() " , e.getMessage());
        }


    }

    private void openLanguageDialog() {

        try{
            if (customLanguageDialog != null && customLanguageDialog.isShowing()) {
                return;
            }
            customLanguageDialog = new CustomLanguageDialog(loginActivity) {
                @Override
                public void onSelect(String languageName, String languageCode) {
                    tvLanguage.setText(languageName);
                    if (!TextUtils.equals(loginActivity.preferenceHelper.getLanguageCode(),
                            languageCode)) {
                        loginActivity.preferenceHelper.putLanguageCode(languageCode);
                        loginActivity.finishAffinity();
                        loginActivity.restartApp();
                    }
                    dismiss();
                }
            };
            customLanguageDialog.show();
        }catch (Exception e){
            Log.e("openLanguageDialog() " , e.getMessage());
        }

    }

    private void setLanguageName() {

        try{
            TypedArray array = loginActivity.getResources().obtainTypedArray(R.array.language_code);
            TypedArray array2 = loginActivity.getResources().obtainTypedArray(R.array.language_name);
            int size = array.length();
            for (int i = 0; i < size; i++) {
                if (TextUtils.equals(loginActivity.preferenceHelper.getLanguageCode(), array.getString
                        (i))) {
                    tvLanguage.setText(array2.getString(i));
                    break;
                }
            }
        }catch (Exception e){
            Log.e("setLanguageName() " , e.getMessage());
        }


    }


    private void checkSocialLoginISOn(boolean isSocialLogin) {
        try{
            if (isSocialLogin) {
                llSocialLogin.setVisibility(View.GONE);
            } else {
                llSocialLogin.setVisibility(View.GONE);
            }
        }catch (Exception e){
            Log.e("checkSocialLoginISOn() " , e.getMessage());
        }

    }

}

package com.edelivery.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.edelivery.HistoryDetailActivity;
import com.edelivery.R;
import com.edelivery.adapter.HistoryAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.OrderHistory;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.TreeSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.R.id.ivToolbarRightIcon;


/**
 * Created by elluminati on 01-Apr-17.
 */

public class HistoryFragment extends BaseFragments {


    public LinearLayout llDateFilter;
    private CustomFontTextView btnApplyFilter, btnResetFilter;
    private CustomFontTextView tvFromDate, tvToDate;
    private RecyclerView rcvOrderHistory;
    private HistoryAdapter ordersHistoryAdapter;
    private ArrayList<OrderHistory> orderHistoryShortList = new ArrayList<>();
    private ArrayList<OrderHistory>  orderHistoryOriznalList = new ArrayList<>();
    private TreeSet<Integer> separatorSet = new TreeSet<>();
    private ArrayList<Date> dateList = new ArrayList<>();
    private DatePickerDialog.OnDateSetListener fromDateSet, toDateSet;
    private Calendar calendar;
    private int day;
    private int month;
    private int year;
    private boolean isFromDateSet, isToDateSet;
    private long fromDateSetTime, toDateSetTime;
    private LinearLayout ivEmpty;
    private SwipeRefreshLayout srlOrdersHistory;
    private PinnedHeaderItemDecoration pinnedHeaderItemDecoration;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity.setTitleOnToolBar(homeActivity.getResources().getString(R.string
                .text_history));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View historyFragView = inflater.inflate(R.layout.fragment_order_history, container, false);
        llDateFilter = (LinearLayout) historyFragView.findViewById(R.id.llDateFilter);
        btnApplyFilter = (CustomFontTextView) historyFragView.findViewById(R.id.tvApply);
        btnResetFilter = (CustomFontTextView) historyFragView.findViewById(R.id.tvReset);
        tvFromDate = (CustomFontTextView) historyFragView.findViewById(R.id.tvFromDate);
        tvToDate = (CustomFontTextView) historyFragView.findViewById(R.id.tvToDate);
        rcvOrderHistory = (RecyclerView) historyFragView.findViewById(R.id.rcvOrderHistory);
        ivEmpty = (LinearLayout) historyFragView.findViewById(R.id.ivEmpty);
        srlOrdersHistory = (SwipeRefreshLayout) historyFragView.findViewById(R.id.srlOrdersHistory);
        return historyFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvToDate.setOnClickListener(this);
        tvFromDate.setOnClickListener(this);
        btnResetFilter.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);
        dateList = new ArrayList<>();
        separatorSet = new TreeSet<>();
        orderHistoryShortList = new ArrayList<>();
        orderHistoryOriznalList = new ArrayList<>();
        getOrderHistory("", "");

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);


        fromDateSet = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                fromDateSetTime = calendar.getTimeInMillis();
                isFromDateSet = true;
                tvFromDate.setText(homeActivity.parseContent.dateFormat.format(calendar.getTime
                        ()));
            }
        };
        toDateSet = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                toDateSetTime = calendar.getTimeInMillis();
                isToDateSet = true;
                tvToDate.setText(homeActivity.parseContent.dateFormat.format(calendar.getTime
                        ()));
            }
        };

        srlOrdersHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrderHistory("", "");
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        homeActivity.setToolbarRightIcon(R.drawable.ic_icon_filter, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        homeActivity.ivToolbarRightIcon.setImageDrawable(null);
    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case ivToolbarRightIcon:
                updateUiFilter();
                break;
            case R.id.tvApply:
                getOrderHistory(homeActivity.parseContent.dateFormat2.format(new Date
                        (fromDateSetTime)), homeActivity.parseContent.dateFormat2.format(new
                        Date(toDateSetTime)));
                break;
            case R.id.tvReset:
                clearData();
                getOrderHistory("", "");
                break;
            case R.id.tvFromDate:
                openFromDatePicker();
                break;
            case R.id.tvToDate:
                openToDatePicker();
                break;
            default:
                // do with default
                break;
        }
    }

    private void updateUiFilter() {
        if (llDateFilter.getVisibility() == View.VISIBLE) {
            llDateFilter.setVisibility(View.GONE);
            homeActivity.ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable
                    (homeActivity,
                            R.drawable.ic_icon_filter));
        } else {
            llDateFilter.setVisibility(View.VISIBLE);
            homeActivity.ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable
                    (homeActivity,
                            R.drawable.ic_cancel));
        }
    }

    /**
     * this method call webservice for get order history
     *
     * @param fromDate date in format (mm/dd/yyyy)
     * @param toDate   date in format (mm/dd/yyyy)
     */
    private void getOrderHistory(String fromDate, String toDate) {
        srlOrdersHistory.setRefreshing(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.START_DATE, fromDate);
            jsonObject.put(Const.Params.END_DATE, toDate);
            jsonObject.put(Const.Params.PROVIDER_ID, homeActivity.preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, homeActivity.preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderHistoryResponse> responseCall = apiInterface.getOrdersHistory(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderHistoryResponse>() {
            @Override
            public void onResponse(Call<OrderHistoryResponse> call, Response<OrderHistoryResponse
                    > response) {
                if (response != null && response.body() != null && homeActivity.parseContent.isSuccessful(response)) {
                    srlOrdersHistory.setRefreshing(false);
                    if (response.body().isSuccess()) {
                        orderHistoryOriznalList.clear();
                        orderHistoryOriznalList.addAll(response.body().getOrderList());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), homeActivity);
                    }
                    updateUiHistoryList();
                    initRcvHistoryList();
                    llDateFilter.setVisibility(View.GONE);
                    homeActivity.ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable
                            (homeActivity,
                                    R.drawable.ic_icon_filter));
                }

            }

            @Override
            public void onFailure(Call<OrderHistoryResponse> call, Throwable t) {
                AppLog.handleThrowable(HistoryFragment.class.getName(), t);
            }
        });
    }


    /**
     * This method is give arrayList which have unique date arrayList and is also add Date list
     * in treeSet
     */
    private void makeShortHistoryList() {
        orderHistoryShortList.clear();
        dateList.clear();
        separatorSet.clear();
        try {
            SimpleDateFormat sdf = homeActivity.parseContent.dateFormat;
            final Calendar cal = Calendar.getInstance();

            Collections.sort(orderHistoryOriznalList, new Comparator<OrderHistory>() {
                @Override
                public int compare(OrderHistory lhs, OrderHistory rhs) {
                    return compareTwoDate(lhs.getCompletedAt(), rhs.getCompletedAt());
                }
            });

            HashSet<Date> listToSet = new HashSet<Date>();
            for (int i = 0; i < orderHistoryOriznalList.size(); i++) {
                AppLog.Log(HistoryFragment.class.getName(), orderHistoryOriznalList.get(i)
                        .getCompletedAt() + "");

                if (listToSet.add(sdf.parse(orderHistoryOriznalList.get(i).getCompletedAt()))) {
                    dateList.add(sdf.parse(orderHistoryOriznalList.get(i).getCompletedAt()));
                }

            }

            for (int i = 0; i < dateList.size(); i++) {

                cal.setTime(dateList.get(i));
                OrderHistory item = new OrderHistory();
                item.setCompletedAt(sdf.format(dateList.get(i)));
                orderHistoryShortList.add(item);

                separatorSet.add(orderHistoryShortList.size() - 1);
                for (int j = 0; j < orderHistoryOriznalList.size(); j++) {
                    Calendar messageTime = Calendar.getInstance();
                    messageTime.setTime(sdf.parse(orderHistoryOriznalList.get(j)
                            .getCompletedAt()));
                    if (cal.getTime().compareTo(messageTime.getTime()) == 0) {
                        orderHistoryShortList.add(orderHistoryOriznalList.get(j));
                    }
                }
            }
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = homeActivity.parseContent.webFormat;
            SimpleDateFormat dateFormat = homeActivity.parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        return 0;
    }

    private void initRcvHistoryList() {
        makeShortHistoryList();
        if (ordersHistoryAdapter != null) {
            pinnedHeaderItemDecoration.disableCache();
            ordersHistoryAdapter.notifyDataSetChanged();
        } else {
            rcvOrderHistory.setLayoutManager(new LinearLayoutManager(homeActivity));
            ordersHistoryAdapter = new HistoryAdapter(separatorSet, orderHistoryShortList) {
                @Override
                public void onOrderClick(int position) {
                    goToHistoryOderDetailActivity(orderHistoryShortList.get(position).getId());
                }
            };
            rcvOrderHistory.setAdapter(ordersHistoryAdapter);
            pinnedHeaderItemDecoration = new PinnedHeaderItemDecoration();
            rcvOrderHistory.addItemDecoration(pinnedHeaderItemDecoration);
           /* rcvOrderHistory.addOnItemTouchListener(new RecyclerTouchListener(homeActivity,
                    rcvOrderHistory, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                        goToHistoryOderDetailActivity(orderHistoryShortList.get(position).getId());

                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));*/
        }
    }

    private void goToHistoryOderDetailActivity(String orderId) {
        Intent intent = new Intent(homeActivity, HistoryDetailActivity.class);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        homeActivity.startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private void openFromDatePicker() {
        DatePickerDialog fromPiker = new DatePickerDialog(homeActivity, fromDateSet, year,
                month, day);
        fromPiker.setTitle(getResources().getString(R.string.text_select_from_date));
        if (isToDateSet) {
            fromPiker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        } else {
            fromPiker.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        }
        fromPiker.show();
    }

    private void openToDatePicker() {
        DatePickerDialog toPiker = new DatePickerDialog(homeActivity, toDateSet, year, month,
                day);
        toPiker.setTitle(getResources().getString(R.string.text_select_to_date));
        if (isFromDateSet) {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
            toPiker.getDatePicker().setMinDate(fromDateSetTime);
        } else {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        toPiker.show();
    }

    private void clearData() {
        isFromDateSet = false;
        isToDateSet = false;
        tvToDate.setText(getResources().getString(R.string.text_to));
        tvFromDate.setText(getResources().getString(R.string.text_from));
    }

    private void updateUiHistoryList() {
        if (orderHistoryOriznalList.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvOrderHistory.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvOrderHistory.setVisibility(View.VISIBLE);
        }

    }
}

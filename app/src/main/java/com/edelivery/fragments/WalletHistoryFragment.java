package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.WalletDetailActivity;
import com.edelivery.WalletTransactionActivity;
import com.edelivery.adapter.WalletHistoryAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.WalletHistory;
import com.edelivery.models.responsemodels.WalletHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 01-Nov-17.
 */

public class WalletHistoryFragment extends Fragment {

    public static final String TAG = WalletHistoryFragment.class.getName();
    private RecyclerView rcvWalletData;
    private ArrayList<WalletHistory> walletHistory = new ArrayList<>();
    private WalletTransactionActivity activity;
    private WalletHistoryAdapter walletHistoryAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (WalletTransactionActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet_history, container, false);
        rcvWalletData = (RecyclerView) view.findViewById(R.id.rcvWalletData);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRcvWalletHistory();
        getWalletHistory();
    }

    private void getWalletHistory() {

        Utils.showCustomProgressDialog(activity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.ID, activity.preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
        } catch (JSONException e) {
            AppLog.handleException(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletHistoryResponse> call = apiInterface.getWalletHistory(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<WalletHistoryResponse>() {
            @Override
            public void onResponse(Call<WalletHistoryResponse> call,
                                   Response<WalletHistoryResponse> response) {
                AppLog.Log("WALLET_HISTORY", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        walletHistory.clear();
                        walletHistory.addAll(response.body().getWalletHistory());
                        Collections.sort(walletHistory, new Comparator<WalletHistory>() {
                            @Override
                            public int compare(WalletHistory lhs, WalletHistory rhs) {
                                return compareTwoDate(lhs.getCreatedAt(), rhs.getCreatedAt());
                            }
                        });
                        walletHistoryAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), activity);
                    }


                }



            @Override
            public void onFailure(Call<WalletHistoryResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);

            }
        });
    }

    private void initRcvWalletHistory() {
        walletHistory = new ArrayList<>();
        rcvWalletData.setLayoutManager(new LinearLayoutManager(activity));
        walletHistoryAdapter = new WalletHistoryAdapter(this, walletHistory);
        rcvWalletData.setAdapter(walletHistoryAdapter);
        rcvWalletData.addOnItemTouchListener(new RecyclerTouchListener(activity, rcvWalletData, new
                ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        goToWalletDetailActivity(walletHistory.get(position));
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void goToWalletDetailActivity(WalletHistory walletHistory) {
        Intent intent = new Intent(activity, WalletDetailActivity.class);
        intent.putExtra(Const.BUNDLE, walletHistory);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = activity.parseContent.webFormat;
            SimpleDateFormat dateFormat = activity.parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(WalletHistoryResponse.class.getName(), e);
        }
        return 0;
    }
}

package com.edelivery.fragments;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.edelivery.AvailableDeliveryActivity;
import com.edelivery.BaseAppCompatActivity;
import com.edelivery.HomeActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.MapBoxMapReady;
import com.edelivery.interfaces.LatLngInterpolator;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.LocationDataSourceHERE;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapState;
import com.here.android.positioning.StatusListener;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Looper.getMainLooper;
import static com.edelivery.BuildConfig.BASE_URL;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

/**
 * Created by elluminati on 09-Feb-17.
 */

public class HomeFragment extends BaseFragments implements
        BaseAppCompatActivity.OrderListener, PermissionsListener, OnMapReadyCallback, MapBoxMapReady, LocationHelper.OnLocationReceived, PositioningManager.OnPositionChangedListener, Map.OnTransformListener {


    public CustomFontTextView tvAvailableDelivers, tvOfflineView;
    public LocationHelper locationHelper;
    private Location currentLocation, lastLocation;
    private LinearLayout llAvailableDelivery, llNotApproved;
    private Marker myLocationMarker;
    private LatLng currentLatLng;
    private ImageView ivTargetLocation;
    private SwitchCompat switchAcceptJob, switchGoOffline;
    private CustomFontTextView tvOnlineStatus;
    private boolean isCameraIdeal = true;


    private MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    public MainActivityLocationCallback callback = new MainActivityLocationCallback(getActivity());
    private MapView mapbox;
    public com.mapbox.mapboxsdk.geometry.LatLng storeLatLng;
    private boolean isCurrentLocationGet = false;
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    String str_subAdminarea11 = null;
    private LocationDataSourceHERE mHereLocation;
    private PositioningManager mPositioningManager;
    //for here map
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private AndroidXMapFragment mapFragment = null;
    private Map map = null;
    RelativeLayout relativeLayout;
    private final int FIVE_SECONDS = 2000;

    MapMarker mapMarker;
    Boolean isZoom = false;
    ProgressDialog dialog;

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getChildFragmentManager().findFragmentById(R.id.mapview);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
       /* Mapbox.getInstance(getActivity(), getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));*/


        View homeFragView = inflater.inflate(R.layout.fragment_home, container, false);
       /* mapbox = homeFragView.findViewById(R.id.mapBoxView);
        mapbox.onCreate(savedInstanceState);
        new CustomEventMapBoxView(getActivity(), mapbox, this);*/
        relativeLayout = (RelativeLayout) homeFragView.findViewById(R.id.rl_map_view);
        ivTargetLocation = (ImageView) homeFragView.findViewById(R.id.ivTargetLocation);
        tvAvailableDelivers = (CustomFontTextView) homeFragView.findViewById(R.id
                .tvAvailableDelivers);
        switchAcceptJob = (SwitchCompat) homeFragView.findViewById(R.id.switchAcceptJob);
        switchGoOffline = (SwitchCompat) homeFragView.findViewById(R.id.switchGoOffline);
        tvOnlineStatus = (CustomFontTextView) homeFragView.findViewById(R.id.tvOnlineStatus);
        llAvailableDelivery = (LinearLayout) homeFragView.findViewById(R.id.llAvailableDelivery);
        tvOfflineView = (CustomFontTextView) homeFragView.findViewById(R.id.tvOfflineView);
        llNotApproved = (LinearLayout) homeFragView.findViewById(R.id.llNotApproved);
        // checkPermissions();
        return homeFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            setheremap();
            initLocationHelper();
            homeActivity.setTitleOnToolBar(getString(R.string.app_name));
            switchGoOffline.setOnClickListener(this);
            switchAcceptJob.setOnClickListener(this);
            ivTargetLocation.setOnClickListener(this);
            llAvailableDelivery.setOnClickListener(this);
            updateUiWhenApprovedByAdmin();
            initProgress();
            callCurrentLocation();  // for call current location
        } catch (Exception e) {

        }


    }

    public void initProgress() {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Gathering your current location.Please wait....");
        dialog.show();
    }

    public void callCurrentLocation() {
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                if (!locationHelper.isOpenGpsDialog()) {
                    locationHelper.setOpenGpsDialog(true);
                    locationHelper.setLocationSettingRequest(homeActivity, REQUEST_CHECK_SETTINGS,
                            new OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    // moveCameraFirstMyLocation();
                                    moveCameraFirstMyLocation();
                                    moveCameraFirstMyLocation();
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                                @Override
                                public void noFound() {
                                    Log.i("HOME_FRAGMENT", "noFound");
                                    moveCameraFirstMyLocation();
                                    moveCameraFirstMyLocation();
                                }
                            });
                } else {
                    moveCameraFirstMyLocation();
                }
                handler.postDelayed(this, FIVE_SECONDS);
            }
        }, FIVE_SECONDS);
    }

    @Override
    public void onResume() {
        super.onResume();
        //mapbox.onResume();
        homeActivity.setOrderListener(this);
        // moveCameraFirstMyLocation();
        startServiceIfProviderOnline();
        getRequestCount();
    }

    @Override
    public void onPause() {
        //mapbox.onPause();
        homeActivity.setOrderListener(null);
        super.onPause();

    }


    @Override
    public void onStart() {
        super.onStart();
        checkPermission();
        //mapbox.onStart();
        // moveCameraFirstMyLocation();
    }

    @Override
    public void onStop() {
        super.onStop();
        locationHelper.onStop();
        //mapbox.onStop();
    }

    @Override
    public void onDestroy() {
        //mapbox.onDestroy();
        super.onDestroy();

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //mapbox.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        //mapbox.onLowMemory();
    }


    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.ivTargetLocation:
                if (!locationHelper.isOpenGpsDialog()) {
                    isZoom = false;
                    locationHelper.setOpenGpsDialog(true);
                    locationHelper.setLocationSettingRequest(homeActivity, REQUEST_CHECK_SETTINGS,
                            new OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    // moveCameraFirstMyLocation();
                                    moveCameraFirstMyLocation();
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                                @Override
                                public void noFound() {
                                    Log.i("HOME_FRAGMENT", "noFound");
                                    moveCameraFirstMyLocation();
                                }
                            });
                } else {
                    moveCameraFirstMyLocation();
                }

                break;
            case R.id.switchGoOffline:
                setProviderOnlineStatus();
                break;
            case R.id.llAvailableDelivery:
                goToAvailableDelivery();
                break;
            case R.id.switchAcceptJob:
                setProviderAcceptJobStatus();
                break;
            default:
                // do with default
                break;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.e(Const.Tag.HOME_FRAGMENT, "GoogleClientLocationChanged" + location);
        currentLocation = location;
        Utils.hideCustomProgressDialog();
        moveCameraFirstMyLocation();
        currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        setMarkerOnLocation(currentLatLng);
        lastLocation = currentLocation;
    }

    @Override
    public void onConnected(Bundle bundle) {
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
        Utils.showCustomProgressDialog(homeActivity, true);
        lastLocation = locationHelper.getLastLocation();
        locationHelper.startLocationUpdate();
        if (!locationHelper.isOpenGpsDialog()) {
            locationHelper.setOpenGpsDialog(true);
            locationHelper.setLocationSettingRequest(homeActivity, REQUEST_CHECK_SETTINGS,
                    new OnSuccessListener() {
                        @Override
                        public void onSuccess(Object o) {
                            // moveCameraFirstMyLocation();
                            moveCameraFirstMyLocation();
                        }
                    }, new LocationHelper.NoGPSDeviceFoundListener() {
                        @Override
                        public void noFound() {
                            Log.i("HOME_FRAGMENT", "noFound");
                            moveCameraFirstMyLocation();
                        }
                    });
        }
        // moveCameraFirstMyLocation();
        startServiceIfProviderOnline();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private void initLocationHelper() {
        try {

            locationHelper = new LocationHelper(homeActivity);
            locationHelper.setLocationReceivedLister(this);
        } catch (Exception e) {
            Log.e("initLocationHelper() " , e.getMessage());
        }
    }

    private void checkPermission() {
        try {
            if (ContextCompat.checkSelfPermission(homeActivity, Manifest.permission
                    .ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(homeActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(homeActivity, new String[]{Manifest.permission
                        .ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const
                        .PERMISSION_FOR_LOCATION);
            } else {
                Log.e("permi_grant", "permi_grant");
                //Do the stuff that requires permission...
                locationHelper.onStart();
                callCurrentLocation();  // for call current location

            }
        } catch (Exception e) {
            Log.e("checkPermission() " , e.getMessage());
        }

    }

    public void moveCameraFirstMyLocation() {
        try {
            if (locationHelper != null && locationHelper.getLastLocation() != null) {
                currentLocation = locationHelper.getLastLocation();
                if (map != null && currentLocation != null) {
                    currentLatLng = new LatLng(currentLocation.getLatitude(),
                            currentLocation.getLongitude());
                    isCameraIdeal = false;
                    Log.i("zoomCamera", "moveCameraFirstMyLocation" + currentLatLng.toString());

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    zoomCamera(currentLatLng);

                    setMarkerOnLocation(currentLatLng);
                    isCameraIdeal = true;
                }
                Utils.hideCustomProgressDialog();
                locationHelper.setOpenGpsDialog(false);

            }

        } catch (Exception e) {
            Utils.hideCustomProgressDialog();
            Log.e("moveCameraFirstMyLocation() " , e.getMessage());
        }

    }

    private void updateCamera(LatLng positionLatLng) {

        try {
            if (positionLatLng != null && isCameraIdeal) {
                isCameraIdeal = false;
                zoomCamera(new LatLng(positionLatLng.getLatitude(), positionLatLng.getLongitude()));
                Log.i("zoomCamera", "updateCamera" + positionLatLng.toString());
            }
        } catch (Exception e) {
            Log.e("updateCamera() " , e.getMessage());
        }
    }

    private void setMarkerOnLocation(LatLng latLng) {

        try {
            if (latLng != null && mapMarker == null && map != null && homeActivity != null && homeActivity.parseContent != null) {
              /*  myLocationMarker = mapboxMap.addMarker(new MarkerOptions().position(latLng)
                        .title(homeActivity.getResources().getString(R.string.text_my_location)));
*/
                mapMarker = new MapMarker();
                GeoCoordinate mycordinates = new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude());
                mapMarker.setCoordinate(mycordinates);
                map.addMapObject(mapMarker);

                zoomCamera(latLng);
                Log.i("zoomcamera", "setMarkerOnLocation" + latLng.toString());
                downloadVehiclePin(homeActivity, mapMarker, latLng);

                zoomCamera(latLng);
               /* animateMarkerToGB(mapMarker, latLng, new
                        LatLngInterpolator.Linear());*/
            } else {
                Log.i("zoomcamera", "animateMarkerToGB" + latLng.toString());
                animateMarkerToGB(mapMarker, latLng, new
                        LatLngInterpolator.Linear());

                // zoomCamera(latLng);
                // zoomCamera(latLng);

            }
        } catch (Exception e) {
            Log.i("setMarkerOnLocation", e.toString());
            Log.e("setMarkerOnLocation() " , e.getMessage());
        }


    }

    private void animateMarkerToGB(final MapMarker marker, final LatLng finalPosition, final
    LatLngInterpolator latLngInterpolator) {

        try {
            if (marker != null && finalPosition != null && latLngInterpolator != null && marker.getCoordinate() != null) {

                final GeoCoordinate startPosition = marker.getCoordinate();
                final GeoCoordinate endPosition = new GeoCoordinate(finalPosition.getLatitude(), finalPosition
                        .getLongitude());

                final LatLngInterpolator interpolator = new LatLngInterpolator.LinearFixed();

                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(3000); // duration 3 second
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        try {
                            float v = animation.getAnimatedFraction();
                            LatLng newPosition = latLngInterpolator.interpolate(v, new LatLng(startPosition.getLatitude(), startPosition.getLongitude()),
                                    new LatLng(endPosition.getLatitude(), endPosition.getLongitude()));
                            GeoCoordinate geoCoordinate = new GeoCoordinate(newPosition.getLatitude(), newPosition.getLongitude());
                            marker.setCoordinate(geoCoordinate);
                            // marker.setAnchor(0.5f, 0.5f);
                          /*  Float bearing = getBearing(startPosition, new LatLng(finalPosition
                                    .getLatitude(), finalPosition.getLongitude()));
                            bearing = Float.isNaN(bearing) ? 0 : bearing;*/

                            if (startPosition != null && finalPosition != null) {

                                if (getDistanceBetweenTwoLatLng(new LatLng(startPosition.getLatitude(), startPosition.getLongitude()),
                                        new LatLng(endPosition.getLatitude(), endPosition.getLongitude())) > LocationHelper.DISPLACEMENT) {
                                    updateCamera(newPosition);
                                }
                            }

                        } catch (Exception ex) {
                            //I don't care atm..
                        }
                    }
                });
                valueAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);


                    }
                });
                valueAnimator.start();
            }
        } catch (Exception e) {
            Log.e("animateMarkerToGB() " , e.getMessage());
        }

    }

    private float getBearing(LatLng begin, LatLng end) {

        try {

            double lat = Math.abs(begin.getLatitude() - end.getLatitude());
            double lng = Math.abs(begin.getLongitude() - end.getLongitude());

            if (begin.getLatitude() < end.getLatitude() && begin.getLongitude() < end.getLongitude())
                return (float) (Math.toDegrees(Math.atan(lng / lat)));
            else if (begin.getLatitude() >= end.getLatitude() && begin.getLongitude() < end.getLongitude())
                return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
            else if (begin.getLatitude() >= end.getLatitude() && begin.getLongitude() >= end.getLongitude())
                return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
            else if (begin.getLatitude() < end.getLatitude() && begin.getLongitude() >= end.getLongitude())
                return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);

        } catch (Exception e) {
            Log.e("getBearing() " , e.getMessage());
        }
        return -1;
    }

    private double getDistanceBetweenTwoLatLng(LatLng startLatLng, LatLng endLatLang) {


        /*Location startLocation = new Location("start");
        Location endlocation = new Location("end");
        endlocation.setLatitude(endLatLang.getLatitude());
        endlocation.setLongitude(endLatLang.getLongitude());
        startLocation.setLatitude(startLatLng.getLatitude());
        startLocation.setLongitude(startLatLng.getLongitude());*/

        GeoCoordinate startLocation, endlocation;

        startLocation = new GeoCoordinate(startLatLng.getLatitude(), startLatLng.getLongitude());
        endlocation = new GeoCoordinate(endLatLang.getLatitude(), endLatLang.getLongitude());

        double distance = startLocation.distanceTo(endlocation);
        return distance;


    }


    private void changeProviderOnlineStatus(boolean isOnline, boolean isActiveJob) {

        try {
            if (homeActivity != null && homeActivity.preferenceHelper != null && homeActivity.preferenceHelper.getProviderId() != null && homeActivity
                    .preferenceHelper.getSessionToken() != null) {
                Utils.showCustomProgressDialog(homeActivity, false);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.PROVIDER_ID, homeActivity
                            .preferenceHelper.getProviderId());
                    jsonObject.put(Const.Params.SERVER_TOKEN, homeActivity
                            .preferenceHelper.getSessionToken());
                    jsonObject.put(Const.Params.IS_ONLINE, isOnline);
                    jsonObject.put(Const.Params.IS_ACTIVE_FOR_JOB, isActiveJob);
                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
                }
                Log.i("changeProviderOnlineStatus", String.valueOf(isActiveJob));

                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<IsSuccessResponse> responseCall = apiInterface.changeProviderOnlineStatus(ApiClient
                        .makeJSONRequestBody(jsonObject));
                responseCall.enqueue(new Callback<IsSuccessResponse>() {
                    @Override
                    public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                            response) {
                        Utils.hideCustomProgressDialog();
                        if (response != null && homeActivity.parseContent.isSuccessful(response) && response.body() != null && homeActivity.parseContent != null && homeActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                            homeActivity.preferenceHelper.putIsProviderOnline(response.body()
                                    .isOnline());
                            homeActivity.preferenceHelper.putIsProviderActiveForJob(response.body()
                                    .isActiveForJob());
                            updateUiOnlineOrActiveJobStatus(homeActivity.preferenceHelper
                                    .getIsProviderOnline());
                            startServiceIfProviderOnline();
                        } else {
                            updateUiOnlineOrActiveJobStatus(homeActivity.preferenceHelper
                                    .getIsProviderOnline());
                            Utils.showErrorToast(response.body().getErrorCode(), homeActivity);
                        }
                    }


                    @Override
                    public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                        Utils.hideCustomProgressDialog();
                        AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);
                        updateUiOnlineOrActiveJobStatus(homeActivity.preferenceHelper.getIsProviderOnline
                                ());
                    }
                });
            }
        } catch (Exception e) {
            Log.e("changeProviderOnlineStatus() " , e.getMessage());
        }


    }

    private void setProviderOnlineStatus() {
        try {
            if (homeActivity.preferenceHelper.getIsProviderOnline()) {
                changeProviderOnlineStatus(!homeActivity.preferenceHelper.getIsProviderOnline(), false);
            } else {
                changeProviderOnlineStatus(!homeActivity.preferenceHelper.getIsProviderOnline(), true);
            }
        } catch (Exception e) {
            Log.e("setProviderOnlineStatus() " , e.getMessage());
        }


    }

    private void setProviderAcceptJobStatus() {
        try {
            if (homeActivity.preferenceHelper.getIsProviderActiveForJob()) {
                changeProviderOnlineStatus(homeActivity.preferenceHelper.getIsProviderOnline(),
                        !homeActivity.preferenceHelper.getIsProviderActiveForJob());
            } else {
                changeProviderOnlineStatus(homeActivity.preferenceHelper.getIsProviderOnline(),
                        !homeActivity.preferenceHelper.getIsProviderActiveForJob());
            }
        } catch (Exception e) {
            Log.e("setProviderAcceptJobStatus() " , e.getMessage());
        }


    }

    private void goToAvailableDelivery() {
        try {

            Intent intent = new Intent(homeActivity, AvailableDeliveryActivity.class);
            startActivity(intent);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } catch (Exception e) {
            Log.e("goToAvailableDelivery() " , e.getMessage());
        }
    }

    private void startServiceIfProviderOnline() {
        try {
            if (homeActivity.preferenceHelper.getIsProviderOnline()) {
                homeActivity.startUpdateLocationAndOrderService();
            } else {
                homeActivity.stopUpdateLocationAndOrderService();
            }
        } catch (Exception e) {
            Log.e("startServiceIfProviderOnline() " , e.getMessage());
        }

    }


    private void updateUiOnlineOrActiveJobStatus(boolean isOnline) {

        try {
            if (switchGoOffline != null && tvOnlineStatus != null && switchAcceptJob != null) {
                if (isOnline) {
                    switchGoOffline.setEnabled(true);
                    switchGoOffline.setChecked(true);
                    tvOnlineStatus.setText(homeActivity.getResources().getString(R.string.text_go_offline));
                    switchAcceptJob.setEnabled(true);
                    tvOfflineView.setVisibility(View.GONE);

                } else {
                    switchGoOffline.setEnabled(true);
                    switchGoOffline.setChecked(false);
                    tvOnlineStatus.setText(homeActivity.getResources().getString(R.string.text_go_online));
                    switchAcceptJob.setEnabled(false);
                    tvOfflineView.setVisibility(View.VISIBLE);
                }
                switchAcceptJob.setChecked(homeActivity.preferenceHelper.getIsProviderActiveForJob());
            }

        } catch (Exception e) {
            Log.e("updateUiOnlineOrActiveJobStatus() " , e.getMessage());
        }


    }


    public void updateUiWhenApprovedByAdmin() {
        try {
            if (homeActivity != null && llNotApproved != null && llAvailableDelivery != null && homeActivity.preferenceHelper != null) {

                if (homeActivity.preferenceHelper.getIsApproved()) {
                    llNotApproved.setVisibility(View.GONE);
                    updateUiOnlineOrActiveJobStatus(homeActivity.preferenceHelper.getIsProviderOnline());
                    llAvailableDelivery.setClickable(true);
                } else {
                    llNotApproved.setVisibility(View.VISIBLE);
                    tvOfflineView.setVisibility(View.GONE);
                    switchGoOffline.setEnabled(false);
                    switchAcceptJob.setEnabled(false);
                    llAvailableDelivery.setClickable(false);
                    homeActivity.preferenceHelper.putIsProviderOnline(false);
                }

            }
        } catch (Exception e) {
            Log.e("updateUiWhenApprovedByAdmin() " , e.getMessage());
        }


    }

    @Override
    public void onOrderReceive() {
        if (homeActivity != null) {
            homeActivity.getProviderDetail();
            getRequestCount();
        }
    }

    private void getRequestCount() {

        try {
            if (homeActivity != null && homeActivity.preferenceHelper != null && homeActivity.preferenceHelper.getProviderId() != null) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.PROVIDER_ID,
                            homeActivity.preferenceHelper.getProviderId());
                    jsonObject.put(Const.Params.SERVER_TOKEN,
                            homeActivity.preferenceHelper.getSessionToken());

                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
                }
                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<IsSuccessResponse> responseCall = apiInterface.getRequestCount(ApiClient
                        .makeJSONRequestBody(jsonObject));
                responseCall.enqueue(new Callback<IsSuccessResponse>() {
                    @Override
                    public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                            response) {
                        Utils.hideCustomProgressDialog();
                        if (response != null && response.body() != null && homeActivity.parseContent.isSuccessful(response) && response.body().isSuccess() && homeActivity.currentOrder != null) {
                            homeActivity.currentOrder.setAvailableOrders(response.body()
                                    .getRequestCount());
                            tvAvailableDelivers.setText(String.valueOf(homeActivity.currentOrder
                                    .getAvailableOrders()));
                        } else {
                            if (homeActivity != null && response != null && response.body() != null)
                                Utils.showErrorToast(response.body().getErrorCode(), homeActivity);
                        }


                    }

                    @Override
                    public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                        AppLog.handleThrowable(HomeActivity.class.getName(), t);
                    }
                });
            }
        } catch (Exception e) {
            Log.e("getRequestCount() " , e.getMessage());
        }


    }


    // developer s
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        /*if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }*/
    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
       /* this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);
        moveCameraFirstMyLocation();

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mLatLng = mapboxMap.getCameraPosition().target;
                if (mLatLng != null)
                {

                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            str_Latitude = addresses.get(0).getLatitude();
                            str_Longitude = addresses.get(0).getLongitude();
                            storeLatLng = new LatLng(str_Latitude, str_Longitude);
                            if (addresses.get(0).getSubAdminArea() != null) {
                                str_subAdminarea11 = addresses.get(0).getSubAdminArea();
                            } else {
                                str_subAdminarea11 = "";
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });*/
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(getActivity(), loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(getActivity());

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

    }

    @Override
    public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {
        Log.i("PositioningManager", geoPosition.getCoordinate().toString());
    }

    @Override
    public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {
        Log.i("PositioningManager", locationMethod.name().toString());
    }

    @Override
    public void onMapTransformStart() {

    }

    @Override
    public void onMapTransformEnd(MapState mapState) {

    }


    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<Activity> activityWeakReference;

        MainActivityLocationCallback(Activity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            Activity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                    Log.i("zoomcamera", "LocationEngineResult" + location.toString());
                }

            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            Activity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(LatLng latLng) {
        if (latLng != null) {
           /* mapboxMap.animateCamera(com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition(
                    new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                            .target(latLng)
                            .zoom(12)
                            .build()), 4000);*/


            Utils.hideCustomProgressDialog();
            try {
                // Log.i("zoomCamera",latLng.toString());

                if (!isZoom) {
                    map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.LINEAR);
                    map.setZoomLevel(14);
                    map.setZoomLevel(14);
                    isZoom = true;

                }
                Log.e("zoom", "zoom12345");


            } catch (Exception e) {
                //   Log.i("zoomCamera",e.toString());
                e.printStackTrace();
            }

        }
    }


  /*  @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i("setheremap()","ok");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //  permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(getContext(), "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            homeActivity.finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    Log.i("setheremap()","ok");
                  //  setheremap();
                    break;


                default:
                    break;
            }
        }
    }
*/

    public void downloadVehiclePin(final Context context, final MapMarker marker, final LatLng latLng) {

        try {
            if (!TextUtils.isEmpty(CurrentOrder.getInstance().getVehiclePin())) {
                if (CurrentOrder.getInstance().getBmVehiclePin() == null) {
                    Glide.with(context)
                            .asBitmap()
                            .load(BASE_URL + CurrentOrder.getInstance().getVehiclePin())

                            .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable
                            .driver_car)
                            .listener(new RequestListener<Bitmap>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                    AppLog.handleException(getClass().getSimpleName(), e);
                                    AppLog.Log("VEHICLE_PIN", "Download failed");
                                    if (marker != null) {
                                        /*IconFactory iconFactory = IconFactory.getInstance(getActivity());
                                        Icon icon = iconFactory.fromResource(R.drawable.driver_car);*/
                                        Image icon = new Image();
                                        try {
                                            icon.setImageResource(R.drawable.driver_car);
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }

                                        marker.setIcon(icon);
                                        zoomCamera(latLng);

                                    }
                                    return true;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                    if (marker != null) {

                                        Image icon = new Image();
                                        try {
                                            icon.setImageResource(R.drawable.driver_car);
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }

                                        marker.setIcon(icon);

                                        CurrentOrder.getInstance().setBmVehiclePin(resource);
                                        zoomCamera(latLng);
                                    }
                                    AppLog.Log("VEHICLE_PIN", "Download Successfully");
                                    return true;
                                }


                            }).dontAnimate().override(context.getResources().getDimensionPixelSize(R
                                    .dimen.vehicle_pin_width)
                            , context.getResources().getDimensionPixelSize(R
                                    .dimen.vehicle_pin_height)).preload(context.getResources()
                                    .getDimensionPixelSize(R
                                            .dimen.vehicle_pin_width)
                            , context.getResources().getDimensionPixelSize(R
                                    .dimen.vehicle_pin_height));
                    AppLog.Log("VEHICLE_PIN_SIZE", (context.getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_width) + "*" + context.getResources()
                            .getDimensionPixelSize(R
                                    .dimen.vehicle_pin_height)));
                } else {
                    if (marker != null) {


                        Image icon = new Image();
                        try {
                            icon.setImageResource(R.drawable.driver_car);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        marker.setIcon(icon);
                        zoomCamera(latLng);
                    }
                }
            } else {
                if (marker != null) {

                    Image icon = new Image();
                    try {
                        icon.setImageResource(R.drawable.driver_car);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    marker.setIcon(icon);
                    zoomCamera(latLng);
                }
            }
        } catch (Exception e) {
            Log.e("downloadVehiclePin() " , e.getMessage());
        }
    }


    //for here maps

  /*  protected void checkPermissions() {

        Log.i("setheremap()","ok");
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(getContext(), permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
                Log.i("setheremap()","ok");
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_CODE_ASK_PERMISSIONS);
            Log.i("setheremap()","ok");


        } else {
            Log.i("setheremap()","ok");
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }*/


    private void setheremap() {
        Log.i("setheremap()", "ok");

        mapFragment = getMapFragment();
        Utils.showCustomProgressDialog(getContext(), false);
        // Set up disk cache path for the map service for this application
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getContext().getExternalFilesDir(null) + File.separator + ".here-maps",
                "com.here.android.tutorial.MapService");
        /*if (!success) {
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
        } else {*/
        // map.setProjectionMode(Map.Projection.MERCATOR);

        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(
                    final Error error) {
                if (error == Error.NONE) {
                    // retrieve a reference of the map from the map fragment
                    Log.i("setheremap()", "ok");

                    map = mapFragment.getMap();
                    Utils.hideCustomProgressDialog();
                    // Set the zoom level to the average between min and max
                    map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);
                    Log.e("zoom", "zoom7888");
                    mPositioningManager = PositioningManager.getInstance();
                    mHereLocation = LocationDataSourceHERE.getInstance(
                            new StatusListener() {
                                @Override
                                public void onOfflineModeChanged(boolean b) {

                                }

                                @Override
                                public void onAirplaneModeEnabled() {

                                }

                                @Override
                                public void onWifiScansDisabled() {

                                }

                                @Override
                                public void onBluetoothDisabled() {

                                }

                                @Override
                                public void onCellDisabled() {

                                }

                                @Override
                                public void onGnssLocationDisabled() {

                                }

                                @Override
                                public void onNetworkLocationDisabled() {

                                }

                                @Override
                                public void onServiceError(ServiceError serviceError) {

                                }

                                @Override
                                public void onPositioningError(PositioningError positioningError) {

                                }

                                @Override
                                public void onWifiIndoorPositioningNotAvailable() {

                                }

                                @Override
                                public void onWifiIndoorPositioningDegraded() {

                                }
                            });


                    if (mHereLocation == null) {
                        Toast.makeText(homeActivity, "LocationDataSourceHERE.getInstance(): failed, exiting", Toast.LENGTH_LONG).show();
                        homeActivity.finish();
                    }
                    mPositioningManager.setDataSource(mHereLocation);
                    mPositioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(
                            homeActivity));
                    // start position updates, accepting GPS, network or indoor positions
                    if (mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK)) {
                        try {
                            mapFragment.getPositionIndicator().setVisible(false);
                        } catch (Exception e) {
                            Log.e("errorMain", "PositionIndicator.setVisible(boolean)' on a null object");
                        }

                    } else {
                        Log.e("Error_heremap", "PositioningManager.start: failed, exiting");
                       /* Toast.makeText(homeActivity, "PositioningManager.start: failed, exiting", Toast.LENGTH_LONG).show();
                        homeActivity.finish();*/
                    }
                    //getProviderLocation();
                    moveCameraFirstMyLocation();


                } else {
                    Utils.hideCustomProgressDialog();
                    System.out.println("ERROR: Cannot initialize Map Fragment");

                    homeActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(getContext()).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    homeActivity.finishAffinity();
                                                }
                                            }).create().show();
                        }
                    });
                }
            }
        });
        // }


    }


    // Define positioning listener

    // Register positioning listener


}

package com.edelivery.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edelivery.CompleteOrderActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.BuildConfig.BASE_URL;

/**
 * Created by elluminati on 26-Apr-17.
 */

public class FeedbackFragment extends Fragment implements TextView
        .OnEditorActionListener, View.OnClickListener {


    private CustomFontTextView tvProviderNameFeedback;
    private RatingBar ratingBarFeedback;
    private CustomFontEditTextView etFeedbackReview;
    private CustomFontButton btnSubmitFeedback;
    private ImageView ivProviderImageFeedback;
    private CompleteOrderActivity completeOrderActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        completeOrderActivity = (CompleteOrderActivity) getActivity();
        completeOrderActivity.setTitleOnToolBar(getResources().getString(R.string.text_feedback));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View feedFrag = inflater.inflate(R.layout.fragment_feedback, container, false);
        ratingBarFeedback = (RatingBar) feedFrag.findViewById(R.id.ratingBarFeedback);
        ivProviderImageFeedback = (ImageView) feedFrag.findViewById(R.id.ivProviderImageFeedback);
        tvProviderNameFeedback = (CustomFontTextView) feedFrag.findViewById(R.id
                .tvProviderNameFeedback);
        etFeedbackReview = (CustomFontEditTextView) feedFrag.findViewById(R.id.etFeedbackReview);
        btnSubmitFeedback = (CustomFontButton) feedFrag.findViewById(R.id.btnSubmitFeedback);
        return feedFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
        btnSubmitFeedback.setOnClickListener(this);
        etFeedbackReview.setOnEditorActionListener(this);
        ratingBarFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingBarFeedback.setRating(v);
            }
        });

    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnSubmitFeedback:
                submitRatting();
                break;

            default:
                // do with default
                break;
        }
    }

    /**
     * this method call a webservice for give feedback to user
     */
    private void giveFeedback() {
        Utils.showCustomProgressDialog(completeOrderActivity, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall;
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, completeOrderActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.PROVIDER_ID, completeOrderActivity.preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.REQUEST_ID, completeOrderActivity.requestId);

            if (completeOrderActivity.storeDetail == null) {
                jsonObject.put(Const.Params.PROVIDER_RATING_TO_USER, ratingBarFeedback
                        .getRating());
                jsonObject.put(Const.Params.PROVIDER_REVIEW_TO_USER, etFeedbackReview
                        .getText().toString());
                responseCall = apiInterface.setFeedbackUser(ApiClient
                        .makeJSONRequestBody(jsonObject));
            } else {
                jsonObject.put(Const.Params.PROVIDER_RATING_TO_STORE, ratingBarFeedback
                        .getRating());
                jsonObject.put(Const.Params.PROVIDER_REVIEW_TO_STORE, etFeedbackReview
                        .getText().toString());
                responseCall = apiInterface.setFeedbackStore(ApiClient
                        .makeJSONRequestBody(jsonObject));
            }
            AppLog.Log("FEED_BACK_PARAM", jsonObject.toString());
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                        Utils.hideCustomProgressDialog();
                        if (response != null && response.body() != null && completeOrderActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                            Utils.showMessageToast(response.body().getMessage(),
                                    completeOrderActivity);
                            completeOrderActivity.onBackPressed();
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    completeOrderActivity);
                        }

                    }



                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(FeedbackFragment.class.getName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(FeedbackFragment.class.getName(), e);
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etFeedbackReview:
                if (i == EditorInfo.IME_ACTION_DONE) {
                    submitRatting();
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }
        return false;
    }

    private void submitRatting() {
        if (ratingBarFeedback.getRating() == 0) {
            Utils.showToast(getResources().getString(R.string.msg_plz_give_rating),
                    completeOrderActivity);
        } else {
            giveFeedback();
        }
    }

    private void loadData() {
        if (completeOrderActivity.availableOrderList != null && completeOrderActivity.userDetail
                == null
                && completeOrderActivity.storeDetail == null) {
            Glide.with(this).load(BASE_URL + completeOrderActivity.availableOrderList
                    .getUserImage())
                    .dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(this
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (this.getResources(), R.drawable.placeholder, null)).into
                    (ivProviderImageFeedback);
            tvProviderNameFeedback.setText(completeOrderActivity.availableOrderList
                    .getUserFirstName() + " " +
                    "" + completeOrderActivity.availableOrderList.getUserLastName());

        } else if (completeOrderActivity.userDetail != null && completeOrderActivity.storeDetail
                == null && completeOrderActivity.availableOrderList == null) {
            Glide.with(this).load(BASE_URL + completeOrderActivity.userDetail.getImageUrl()
            ).dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(this
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (this.getResources(), R.drawable.placeholder, null)).into
                    (ivProviderImageFeedback);
            tvProviderNameFeedback.setText(completeOrderActivity.userDetail.getFirstName() + " " +
                    "" + completeOrderActivity.userDetail.getLastName());

        } else {
            Glide.with(this).load(BASE_URL + completeOrderActivity.storeDetail.getImageUrl()
            ).dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(this
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (this.getResources(), R.drawable.placeholder, null)).into
                    (ivProviderImageFeedback);
            tvProviderNameFeedback.setText(completeOrderActivity.storeDetail.getName());
        }


    }

}

package com.edelivery.fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.R;
import com.edelivery.adapter.CardAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Card;
import com.edelivery.models.responsemodels.CardsResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by elluminati on 01-Apr-17.
 */

public class StripeFragment extends BasePaymentFragments {


    public static final String TAG = StripeFragment.class.getName();
    private static final Pattern CODE_PATTERN = Pattern
            .compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    public ArrayList<Card> cardList = new ArrayList<>();
    private RecyclerView rcvStripCards;
    private CardAdapter cardAdapter;
    private Dialog addCardDialog;
    private String cardType;
    private CustomFontEditTextView etCreditCardNum, etCvc, etMonth, etYear,etCardholderName;
    private CustomFontTextView tvAddCard;
    private Card selectCard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View stripeFragView = inflater.inflate(R.layout.fragment_stripe, container, false);
        rcvStripCards = (RecyclerView) stripeFragView.findViewById(R.id.rcvStripCards);
        tvAddCard = (CustomFontTextView) stripeFragView.findViewById(R.id.tvAddCard);

        return stripeFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cardList = new ArrayList<>();
        tvAddCard.setOnClickListener(this);
        getAllCards();

    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.tvAddCard:
                openAddCardDialog();
                break;
            default:
                // do with default
                break;
        }

    }

    /**
     * this method called webservice for get all save credit card in stripe
     */
    private void getAllCards() {
        Utils.showCustomProgressDialog(paymentActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, paymentActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, paymentActivity
                    .preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardsResponse> responseCall = apiInterface.getAllCreditCards(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CardsResponse>() {
            @Override
            public void onResponse(Call<CardsResponse> call, Response<CardsResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && paymentActivity.parseContent.isSuccessful(response) &&response.body().isSuccess()) {
                        cardList.clear();
                        cardList.addAll(response.body().getCards());
                        setSelectCreditCart();
                        initRcvCards();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), paymentActivity);
                    }
                }


            @Override
            public void onFailure(Call<CardsResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });

    }

    private void initRcvCards() {
        if (cardAdapter != null) {
            cardAdapter.notifyDataSetChanged();
        } else {
            rcvStripCards.setLayoutManager(new LinearLayoutManager(paymentActivity));
            cardAdapter = new CardAdapter(this, cardList);
            rcvStripCards.setAdapter(cardAdapter);
            rcvStripCards.addItemDecoration(new DividerItemDecoration(paymentActivity,
                    LinearLayoutManager.VERTICAL));
        }
    }

    private void openAddCardDialog() {

        if (addCardDialog != null && addCardDialog.isShowing()) {
            return;
        }

        addCardDialog = new Dialog(paymentActivity);
        addCardDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addCardDialog.setContentView(R.layout.dialog_add_credit_card);
        addCardDialog.findViewById(
                R.id.btnDialogAlertRight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()) {
                    getTokenFromCreditCard();
                }
            }
        });

        addCardDialog.findViewById(
                R.id.btnDialogAlertLeft).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCardDialog.dismiss();
            }
        });
        etCreditCardNum = (CustomFontEditTextView) addCardDialog.findViewById(R.id
                .etCreditCardNumber);
        etCvc = (CustomFontEditTextView) addCardDialog.findViewById(R.id.etCvv);
        etYear = (CustomFontEditTextView) addCardDialog.findViewById(R.id.etYear);
        etMonth = (CustomFontEditTextView) addCardDialog.findViewById(R.id.etMonth);
        etCardholderName = (CustomFontEditTextView) addCardDialog.findViewById(R.id.etCreditCardHolder);
        setCardTextWatcher();
        WindowManager.LayoutParams params = addCardDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        addCardDialog.getWindow().setAttributes(params);
        addCardDialog.setCancelable(false);
        addCardDialog.show();
    }

    private void closedAddCardDialog() {
        if (addCardDialog != null && addCardDialog.isShowing()) {
            addCardDialog.dismiss();
        }
    }

    private String getCreditCardType(String number) {
        if (!Utils.isBlank(number)) {
            if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_AMERICAN_EXPRESS)) {
                return com.stripe.android.model.Card.AMERICAN_EXPRESS;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_DISCOVER)) {
                return com.stripe.android.model.Card.DISCOVER;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card.PREFIXES_JCB)) {
                return com.stripe.android.model.Card.JCB;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_DINERS_CLUB)) {
                return com.stripe.android.model.Card.DINERS_CLUB;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card.PREFIXES_VISA)) {
                return com.stripe.android.model.Card.VISA;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_MASTERCARD)) {
                return com.stripe.android.model.Card.MASTERCARD;
            } else {
                return com.stripe.android.model.Card.UNKNOWN;
            }
        }
        return com.stripe.android.model.Card.UNKNOWN;

    }

    /***
     * this method used set textWatcher on card editText
     */
    private void setCardTextWatcher() {


        etCreditCardNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                cardType = getCreditCardType(s.toString());
                if (etCreditCardNum.getText().toString().length() == 19) {
                    etMonth.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // do with text
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
                    String input = s.toString();
                    String numbersOnly = keepNumbersOnly(input);
                    String code = formatNumbersAsCode(numbersOnly);
                    etCreditCardNum.removeTextChangedListener(this);
                    etCreditCardNum.setText(code);
                    etCreditCardNum.setSelection(code.length());
                    etCreditCardNum.addTextChangedListener(this);
                }
            }

            private String keepNumbersOnly(CharSequence s) {
                return s.toString().replaceAll("[^0-9]", ""); // Should of
                // course be
                // more robust
            }

            private String formatNumbersAsCode(CharSequence s) {
                int groupDigits = 0;
                String tmp = "";
                int sSize = s.length();
                for (int i = 0; i < sSize; ++i) {
                    tmp += s.charAt(i);
                    ++groupDigits;
                    if (groupDigits == 4) {
                        tmp += "-";
                        groupDigits = 0;
                    }
                }
                return tmp;
            }
        });
        etYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (etYear.getText().toString().length() == 2) {
                    etCvc.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // do with text
            }

            @Override
            public void afterTextChanged(Editable s) {
                // do with text
            }
        });

        etMonth.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (etMonth.getText().toString().length() == 2) {
                    etYear.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // do with text
            }

            @Override
            public void afterTextChanged(Editable s) {
                // do with text
            }
        });

    }

    private boolean isValidate() {
        String msg = null;
        String month = etMonth.getText().toString();
        if(TextUtils.isEmpty(etCardholderName.getText().toString().trim())){
            msg = getString(R.string.msg_enter_holder_name);
            etCardholderName.requestFocus();
        }else if (etCreditCardNum.getText().toString().isEmpty()) {
            msg = getString(R.string.msg_enter_card_number);
            etCreditCardNum.requestFocus();
        } else if (etYear.getText().toString().isEmpty()) {
            msg = getString(R.string.msg_enter_year);
            etYear.requestFocus();
        } else if (month.isEmpty()) {
            msg = getString(R.string.msg_enter_month);
            etMonth.requestFocus();

        } else if (etCvc.getText().toString().isEmpty()) {
            msg = getString(R.string.msg_enter_cvv);
            etCvc.requestFocus();

        } else if (Integer.valueOf(month) > 12) {
            msg = getString(R.string.msg_enter_valid_month);
            etMonth.requestFocus();
        }

        if (msg != null) {
            Utils.showToast(msg, paymentActivity);
            return false;

        }

        return true;

    }

    /**
     * this method is generate stripe token from credit card information
     */
    private void getTokenFromCreditCard() {
        Utils.showCustomProgressDialog(paymentActivity, false);

        com.stripe.android.model.Card card = new com.stripe.android.model.Card(etCreditCardNum
                .getText().toString(),
                Integer.parseInt(etMonth.getText().toString()),
                Integer.parseInt(etYear.getText().toString()), etCvc.getText()
                .toString());
        if (card.validateCard()) {

            String stripeKey = paymentActivity.gatewayItem.getPaymentKeyId();
            AppLog.Log("STRIPE_KEY", stripeKey);

            if(getActivity() != null){
                new Stripe(getActivity()).createToken(card, stripeKey,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                String expDate = token.getCard().getExpMonth()+","+token.getCard().getExpYear();
                                addCreditCard(token.getId(), token.getCard().getLast4(), cardType,
                                        paymentActivity.gatewayItem.getId(),expDate);
                            }

                            public void onError(Exception error) {
                                Utils.showToast(error.getMessage(),
                                        paymentActivity);
                                Utils.hideCustomProgressDialog();
                            }
                        });
            }

        } else if (!card.validateNumber()) {
            // handleError("The card number that you entered is invalid");
            Utils.showToast(getString(R.string.msg_number_invalid), paymentActivity);
            Utils.hideCustomProgressDialog();
        } else if (!card.validateExpiryDate()) {
            // handleError("");
            Utils.showToast(getString(R.string.msg_invalid_month_or_year), paymentActivity);
            Utils.hideCustomProgressDialog();
        } else if (!card.validateCVC()) {
            // handleError("");
            Utils.showToast(getString(R.string.msg_cvc_invalid), paymentActivity);
            Utils.hideCustomProgressDialog();

        } else {
            // handleError("");
            Utils.showToast(getString(R.string.msg_card_invalid), paymentActivity);
            Utils.hideCustomProgressDialog();
        }
    }

    /**
     * this method call a webservice for save credit card
     */
    private void addCreditCard(String paymentToken, String lastFourDigits, String cardType, String
            paymentId, String expDate) {
        Utils.showCustomProgressDialog(paymentActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, paymentActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, paymentActivity
                    .preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.PAYMENT_TOKEN, paymentToken);
            jsonObject.put(Const.Params.LAST_FOUR, lastFourDigits);
            jsonObject.put(Const.Params.CARD_TYPE, cardType);
            jsonObject.put(Const.Params.CARD_EXPIRY_DATE,expDate);
            jsonObject.put(Const.Params.CARD_HOLDER_NAME,etCardholderName.getText().toString());
            jsonObject.put(Const.Params.PAYMENT_ID, paymentId);
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardsResponse> responseCall = apiInterface.getAddCreditCard(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CardsResponse>() {
            @Override
            public void onResponse(Call<CardsResponse> call, Response<CardsResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && paymentActivity.parseContent.isSuccessful(response) &&response.body().isSuccess()) {
                        cardList.add(response.body().getCard());
                        initRcvCards();
                        setSelectCreditCart();
                        closedAddCardDialog();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), paymentActivity);
                    }
                }


            @Override
            public void onFailure(Call<CardsResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });

    }

    public void selectCreditCard(final int selectedCardPosition) {
        Utils.showCustomProgressDialog(paymentActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, paymentActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, paymentActivity
                    .preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
            jsonObject.put(Const.Params.CARD_ID, cardList.get
                    (selectedCardPosition).getId());
        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardsResponse> responseCall = apiInterface.selectCreditCard(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CardsResponse>() {
            @Override
            public void onResponse(Call<CardsResponse> call, Response<CardsResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && paymentActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        for (Card card : cardList) {
                            card.setIsDefault(false);
                        }
                        cardList.get(selectedCardPosition).setIsDefault(response.body()
                                .getCard()
                                .isIsDefault());
                        setSelectCreditCart();
                        cardAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), paymentActivity);
                    }

            }

            @Override
            public void onFailure(Call<CardsResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });
    }

    private void deleteCreditCard(final int deleteCardPosition) {
        Utils.showCustomProgressDialog(paymentActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, paymentActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, paymentActivity
                    .preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.CARD_ID, cardList.get
                    (deleteCardPosition).getId());
            jsonObject.put(Const.Params.TYPE, Const.TYPE_PROVIDER);
        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.deleteCreditCard(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && paymentActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        if (cardList.get(deleteCardPosition).isIsDefault()) {
                            cardList.remove(deleteCardPosition);
                            if (!cardList.isEmpty()) {
                                selectCreditCard(0);
                            } else {
                                setSelectCreditCart();
                            }
                        } else {
                            cardList.remove(deleteCardPosition);
                            setSelectCreditCart();
                        }
                        cardAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), paymentActivity);
                    }
                }


            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });

    }

    public void openDeleteCard(final int position) {
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(paymentActivity, paymentActivity
                .getResources()
                .getString(R.string.text_delete_card), paymentActivity.getResources()
                .getString(R.string.msg_are_you_sure), paymentActivity.getResources()
                .getString(R.string.text_cancel), paymentActivity.getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                dismiss();
                deleteCreditCard(position);

            }
        };
        customDialogAlert.show();

    }

    private void setSelectCreditCart() {
        selectCard = null;
        for (Card card : cardList) {
            if (card.isIsDefault()) {
                selectCard = card;
                return;
            }
        }

    }
    public void addWalletAmount() {
        if (selectCard != null) {
            paymentActivity.addWalletAmount(selectCard.getId(), paymentActivity
                    .gatewayItem.getId());
        } else {
            Utils.showToast(paymentActivity.getResources().getString(R.string
                    .msg_plz_add_card_first), paymentActivity);
        }
    }

}

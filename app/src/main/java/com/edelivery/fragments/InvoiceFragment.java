package com.edelivery.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.edelivery.CompleteOrderActivity;
import com.edelivery.R;
import com.edelivery.adapter.InvoiceAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.InvoicePayment;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.responsemodels.InvoiceResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.singleton.CurrentOrder;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.Const.Params.REQUEST_ID;

/**
 * Created by elluminati on 26-Apr-17.
 */

public class InvoiceFragment extends Fragment implements View.OnClickListener {

    private LinearLayout llInvoiceDistance, llInvoicePayment;
    private OrderPayment orderPayment;
    private CustomFontTextViewTitle tvOderTotal;
    private CustomFontButton btnInvoiceSubmit;
    private RecyclerView rcvInvoice;
    private CompleteOrderActivity completeOrderActivity;
    private CustomFontTextView tvInvoiceMsg;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        completeOrderActivity = (CompleteOrderActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View invoiceFrag = inflater.inflate(R.layout.fragment_order_invoice, container, false);
        llInvoiceDistance = (LinearLayout) invoiceFrag.findViewById(R.id.llInvoiceDistance);
        llInvoicePayment = (LinearLayout) invoiceFrag.findViewById(R.id.llInvoicePayment);
        btnInvoiceSubmit = (CustomFontButton) invoiceFrag.findViewById(R.id.btnInvoiceSubmit);
        tvOderTotal = (CustomFontTextViewTitle) invoiceFrag.findViewById(R.id.tvOderTotal);
        rcvInvoice = (RecyclerView) invoiceFrag.findViewById(R.id.rcvInvoice);
        tvInvoiceMsg = (CustomFontTextView) invoiceFrag.findViewById(R.id.tvInvoiceMsg);
        return invoiceFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnInvoiceSubmit.setOnClickListener(this);
        orderPayment = completeOrderActivity.orderPayment;
        if (orderPayment != null) {
            setInvoiceData();
            setInvoiceDistanceAndTime();
            setInvoicePayments();
            setInvoiceMessage(!completeOrderActivity.backToActiveDelivery);
        } else {
            getOrderInvoice(completeOrderActivity.requestId);
        }
        if (completeOrderActivity.availableOrderList == null && completeOrderActivity
                .userDetail == null) {
            btnInvoiceSubmit.setVisibility(View.GONE);
        }


    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnInvoiceSubmit:
                setShowInvoice(completeOrderActivity.requestId);
                break;

            default:
                // do with default
                break;
        }
    }

    private void setInvoiceData() {

        rcvInvoice.setLayoutManager(new LinearLayoutManager(completeOrderActivity));
        rcvInvoice.setAdapter(new InvoiceAdapter(completeOrderActivity.parseContent.parseInvoice
                (orderPayment)));


    }

    /**
     * this method will help to load a image in invoice screen
     *
     * @param title    set tile text
     * @param subTitle set sub title text
     * @param id       sett image id in image resource
     * @return invoicePayment object
     */
    private InvoicePayment loadInvoiceImage(String title, String subTitle, int id) {

        InvoicePayment invoicePayment = new InvoicePayment();
        invoicePayment.setTitle(title);
        invoicePayment.setValue(subTitle);
        invoicePayment.setImageId(id);
        return invoicePayment;

    }

    /**
     * this method used to set invoice distance and time view
     */
    private void setInvoiceDistanceAndTime() {

        /*String unit = orderPayment.isDistanceUnitMile() ? getResources().getString(R.string
                .unit_mile) : getResources().getString(R.string.unit_km);
*/
        ArrayList<InvoicePayment> invoicePayments = new ArrayList<>();
       /* invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_time_h_m), ), R.drawable
                        .ic_wall_clock));*/
        invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_distance),
                appendString(orderPayment.getTotalDistance(), "km"), R.drawable.ic_route));

        invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_pay),
                completeOrderActivity.payment, orderPayment.isPaymentModeCash() ? R.drawable
                        .ic_cash : R.drawable.ic_credit_card));

        int size = invoicePayments.size();
        for (int i = 0; i < size; i++) {
            LinearLayout currentLayout = (LinearLayout) llInvoiceDistance.getChildAt(i);
            ImageView imageView = (ImageView) currentLayout.getChildAt(0);
            imageView.setImageDrawable
                    (AppCompatResources.getDrawable
                            (completeOrderActivity, invoicePayments.get(i).getImageId()));
            LinearLayout currentSubSubLayout = (LinearLayout) currentLayout.getChildAt(1);
            ((CustomFontTextView) currentSubSubLayout.getChildAt(0)).setText(invoicePayments.get
                    (i).getTitle());
            ((CustomFontTextView) currentSubSubLayout.getChildAt(1)).setText(invoicePayments.get
                    (i).getValue());
        }


    }

    /**
     * this method used to set invoice payment view
     */
    private void setInvoicePayments() {
        String currency = CurrentOrder.getInstance().getCurrency();


        ArrayList<InvoicePayment> invoicePayments = new ArrayList<>();
        /*invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_wallet),
                appendString(currency, orderPayment.getWalletPayment()), R.drawable.ic_wallet));*/


        if (orderPayment.isPaymentModeCash()) {
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_cash),
                    appendString(currency, orderPayment.getCashPayment()), R.drawable.ic_cash));
        } else {
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_card),
                    appendString(currency, orderPayment.getCardPayment()), R.drawable
                            .ic_credit_card));
        }
        if (orderPayment.getPromoPayment() > 0) {
            llInvoicePayment.addView(LayoutInflater.from(completeOrderActivity).inflate(R.layout
                            .layout_invoice_item,
                    null));
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_promo),
                    appendString(currency, orderPayment.getPromoPayment()), R.drawable
                            .ic_promo_code));
        }
        int size = invoicePayments.size();
        for (int i = 0; i < size; i++) {
            LinearLayout currentLayout = (LinearLayout) llInvoicePayment.getChildAt(i);
            ImageView imageView = (ImageView) currentLayout.getChildAt(0);
            imageView.setImageDrawable
                    (AppCompatResources.getDrawable
                            (completeOrderActivity, invoicePayments.get(i).getImageId()));
            LinearLayout currentSubSubLayout = (LinearLayout) currentLayout.getChildAt(1);
            ((CustomFontTextView) currentSubSubLayout.getChildAt(0)).setText(invoicePayments.get
                    (i).getTitle());
            ((CustomFontTextView) currentSubSubLayout.getChildAt(1)).setText(invoicePayments.get
                    (i).getValue());
        }

        tvOderTotal.setText(currency + completeOrderActivity.parseContent.decimalTwoDigitFormat
                .format(orderPayment.getTotal
                        ()));
    }


    private String appendString(String string, Double value) {
        return string + completeOrderActivity.parseContent.decimalTwoDigitFormat.format(value);
    }

    private String appendString(Double value, String unit) {
        return completeOrderActivity.parseContent.decimalTwoDigitFormat.format(value) +
                unit;
    }

    private void getOrderInvoice(String orderId) {
        Utils.showCustomProgressDialog(completeOrderActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, completeOrderActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.PROVIDER_ID, completeOrderActivity.preferenceHelper
                    .getProviderId());
            jsonObject.put(REQUEST_ID, orderId);
        } catch (JSONException e) {
            AppLog.handleThrowable(InvoiceFragment.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> invoiceResponseCall = apiInterface.getInvoice(ApiClient
                .makeJSONRequestBody(jsonObject));
        invoiceResponseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && completeOrderActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    completeOrderActivity.payment = response.body().getPayment();
                    orderPayment = response.body().getOrderPayment();
                    CurrentOrder.getInstance().setCurrency(response.body().getCurrency());
                    setInvoiceData();
                    setInvoiceDistanceAndTime();
                    setInvoicePayments();
                    setInvoiceMessage(false);
                } else {

                    Utils.showErrorToast(response.body().getErrorCode(), completeOrderActivity);
                }


            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
            }
        });
    }

    private void setShowInvoice(String orderId) {
        Utils.showCustomProgressDialog(completeOrderActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, completeOrderActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.PROVIDER_ID, completeOrderActivity.preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.REQUEST_ID, orderId);
            jsonObject.put(Const.Params.IS_PROVIDER_SHOW_INVOICE, true);
        } catch (JSONException e) {
            AppLog.handleThrowable(InvoiceFragment.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.setShowInvoice(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response.body().isSuccess()) {
                    //completeOrderActivity.goToFeedbackFragment();
                    completeOrderActivity.onBackPressed();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), completeOrderActivity);
                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
            }
        });
    }

    private void setInvoiceMessage(boolean isDefaultText) {
        if (orderPayment.isPaymentModeCash() && !isDefaultText) {
            tvInvoiceMsg.setText(getResources().getString(R.string
                    .msg_pay_cash));
        } else {
            tvInvoiceMsg.setText(getResources().getString(R.string
                    .msg_pay_other));
        }


    }
}

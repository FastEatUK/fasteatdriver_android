package com.edelivery.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.edelivery.AvailableDeliveryActivity;
import com.edelivery.BaseAppCompatActivity;
import com.edelivery.R;
import com.edelivery.adapter.PendingDeliveryAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.AvailableOrder;
import com.edelivery.models.responsemodels.AvailableOrdersResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 09-Mar-17.
 */

public class PendingFragmentDelivery extends Fragment implements BaseAppCompatActivity
        .OrderListener, View.OnClickListener {

    public SwipeRefreshLayout srlPendingDelivery;
    public ArrayList<AvailableOrder> pendingOrderList = new ArrayList<>();
    private RecyclerView rcvPendingDelivery;
    private PendingDeliveryAdapter pendingDeliveryAdapter;
    private LinearLayout ivEmpty;
    private AvailableDeliveryActivity availableDeliveryActivity;

    public LinearLayout skeletonLayout;
    public ShimmerLayout shimmer;
    public LayoutInflater inflater;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        availableDeliveryActivity = (AvailableDeliveryActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View pendingFrag = inflater.inflate(R.layout.fragment_pending_delivery, container, false);
        rcvPendingDelivery = (RecyclerView) pendingFrag.findViewById(R.id.rcvPendingDelivery);
        srlPendingDelivery = (SwipeRefreshLayout) pendingFrag.findViewById(R.id.srlPendingDelivery);
        skeletonLayout = (LinearLayout) pendingFrag.findViewById(R.id.skeletonLayout);
        shimmer = (ShimmerLayout) pendingFrag.findViewById(R.id.shimmerSkeleton);
        ivEmpty = (LinearLayout) pendingFrag.findViewById(R.id.ivEmpty);
        return pendingFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pendingOrderList = new ArrayList<>();
        srlPendingDelivery.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrders();
            }
        });
        setShimmerEffect();
        initRcvPendingDelivery();
    }

    public void setShimmerEffect() {

        this.inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        showSkeleton(true);

    }
    @Override
    public void onStart() {
        super.onStart();
        availableDeliveryActivity.setOrderListener(this);
        availableDeliveryActivity.startSchedule();
    }

    @Override
    public void onStop() {
        super.onStop();
        availableDeliveryActivity.setOrderListener(null);
        availableDeliveryActivity.stopSchedule();
    }

    @Override
    public void onClick(View view) {
        // do somethings
    }

    public void getOrders() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, availableDeliveryActivity.preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, availableDeliveryActivity.preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
        }

        AppLog.Log("GET_PENDING_ORDERS", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableOrdersResponse> orderResponseCall = apiInterface.getNewOrder(ApiClient
                .makeJSONRequestBody(jsonObject));
        orderResponseCall.enqueue(new Callback<AvailableOrdersResponse>() {
            @Override
            public void onResponse(Call<AvailableOrdersResponse> call,
                                   Response<AvailableOrdersResponse> response) {
                pendingOrderList.clear();
                if (response != null && response.body() != null ) {
                    pendingOrderList.addAll(availableDeliveryActivity
                            .parseContent.parseOrders
                                    (response));
                }
                updateUiList();
                initRcvPendingDelivery();
                availableDeliveryActivity.updateDeliveryCount(0, pendingOrderList.size());
                srlPendingDelivery.setRefreshing(false);
                Utils.hideCustomProgressDialog();
                showSkeleton(false);
            }

            @Override
            public void onFailure(Call<AvailableOrdersResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);
                showSkeleton(false);
            }
        });
    }

    public void initRcvPendingDelivery() {
        if (pendingDeliveryAdapter != null) {
            pendingDeliveryAdapter.notifyDataSetChanged();
        } else {
            pendingDeliveryAdapter = new PendingDeliveryAdapter(availableDeliveryActivity,
                    pendingOrderList);
            rcvPendingDelivery.setLayoutManager(new LinearLayoutManager(availableDeliveryActivity));
            rcvPendingDelivery.setAdapter(pendingDeliveryAdapter);
            rcvPendingDelivery.addItemDecoration(new DividerItemDecoration
                    (availableDeliveryActivity,
                            LinearLayoutManager.VERTICAL));
            rcvPendingDelivery.addOnItemTouchListener(new RecyclerTouchListener
                    (availableDeliveryActivity, rcvPendingDelivery, new ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            availableDeliveryActivity.goToActiveDeliveryActivity
                                    (pendingOrderList.get(position)
                                            .getId());
                        }

                        @Override
                        public void onLongClick(View view, int position) {
                            // do somethings

                        }
                    }));
        }
    }

    public void showSkeleton(boolean show) {

        if (show) {

            skeletonLayout.removeAllViews();

            int skeletonRows = getSkeletonRowCount(getActivity());
            for (int i = 0; i <= skeletonRows; i++) {
                ViewGroup rowLayout = (ViewGroup) inflater
                        .inflate(R.layout.skeleton_row_layout, null);
                skeletonLayout.addView(rowLayout);
            }
            shimmer.setVisibility(View.VISIBLE);
            shimmer.startShimmerAnimation();
            skeletonLayout.setVisibility(View.VISIBLE);
            skeletonLayout.bringToFront();

            animateReplaceSkeleton(shimmer);
        } else {
            shimmer.stopShimmerAnimation();
            shimmer.setVisibility(View.GONE);
        }
    }

    public void animateReplaceSkeleton(View listView) {

        listView.setVisibility(View.VISIBLE);
        listView.setAlpha(0f);
        listView.animate().alpha(1f).setDuration(8000).start();

    }
    private void updateUiList() {
        if (pendingOrderList.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvPendingDelivery.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvPendingDelivery.setVisibility(View.VISIBLE);
            NotificationManager notificationManager = (NotificationManager) availableDeliveryActivity.getSystemService(Context.NOTIFICATION_SERVICE);
            if(notificationManager !=null) {
                notificationManager.cancelAll();
            }
        }

    }

    @Override
    public void onOrderReceive() {
        getOrders();
        srlPendingDelivery.setRefreshing(true);
    }
    public int getSkeletonRowCount(Context context) {
        int pxHeight = getDeviceHeight(context);
        int skeletonRowHeight = (int) getResources()
                .getDimension(R.dimen.row_layout_height); //converts to pixel
        return (int) Math.ceil(pxHeight / skeletonRowHeight);
    }

    public int getDeviceHeight(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return metrics.heightPixels;
    }
}

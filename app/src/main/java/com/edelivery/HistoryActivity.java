package com.edelivery;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.edelivery.adapter.HistoryAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.HistoryFragment;
import com.edelivery.models.datamodels.OrderHistory;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.TreeSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends BaseAppCompatActivity {

    public LinearLayout llDateFilter;
    private CustomFontTextView btnApplyFilter, btnResetFilter;
    private CustomFontTextView tvFromDate, tvToDate;
    private RecyclerView rcvOrderHistory;
    private HistoryAdapter ordersHistoryAdapter;
    private ArrayList<OrderHistory> orderHistoryShortList = new ArrayList<>();
    private ArrayList<OrderHistory>  orderHistoryOriznalList = new ArrayList<>();
    private TreeSet<Integer> separatorSet = new TreeSet<>();
    private ArrayList<Date> dateList = new ArrayList<>();
    private DatePickerDialog.OnDateSetListener fromDateSet, toDateSet;
    private Calendar calendar;
    private int day;
    private int month;
    private int year;
    private boolean isFromDateSet, isToDateSet;
    private long fromDateSetTime, toDateSetTime;
    private LinearLayout ivEmpty;
    private SwipeRefreshLayout srlOrdersHistory;
    private PinnedHeaderItemDecoration pinnedHeaderItemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_history));
        findViewById();
        setViewListener();
        closedDeliveryRequestDialog();
        dateList = new ArrayList<>();
        separatorSet = new TreeSet<>();
        orderHistoryShortList = new ArrayList<>();
        orderHistoryOriznalList = new ArrayList<>();

    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        llDateFilter = (LinearLayout) findViewById(R.id.llDateFilter);
        btnApplyFilter = (CustomFontTextView) findViewById(R.id.tvApply);
        btnResetFilter = (CustomFontTextView) findViewById(R.id.tvReset);
        tvFromDate = (CustomFontTextView) findViewById(R.id.tvFromDate);
        tvToDate = (CustomFontTextView) findViewById(R.id.tvToDate);
        rcvOrderHistory = (RecyclerView) findViewById(R.id.rcvOrderHistory);
        ivEmpty = (LinearLayout) findViewById(R.id.ivEmpty);
        srlOrdersHistory = (SwipeRefreshLayout) findViewById(R.id.srlOrdersHistory);
        getOrderHistory("", "");

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);


        fromDateSet = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                fromDateSetTime = calendar.getTimeInMillis();
                isFromDateSet = true;
                tvFromDate.setText(parseContent.dateFormat.format(calendar.getTime
                        ()));
            }
        };
        toDateSet = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                toDateSetTime = calendar.getTimeInMillis();
                isToDateSet = true;
                tvToDate.setText(parseContent.dateFormat.format(calendar.getTime
                        ()));
            }
        };

        srlOrdersHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrderHistory("", "");
            }
        });
    }

    @Override
    protected void setViewListener() {
        tvToDate.setOnClickListener(this);
        tvFromDate.setOnClickListener(this);
        btnResetFilter.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();

    }

    public void goToAvailableDelivery() {
        Intent intent = new Intent(this, AvailableDeliveryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
// do somethings
        switch (v.getId()) {
            case R.id.ivToolbarRightIcon:
                updateUiFilter();
                break;
            case R.id.tvApply:
                getOrderHistory(parseContent.dateFormat2.format(new Date
                        (fromDateSetTime)), parseContent.dateFormat2.format(new
                        Date(toDateSetTime)));
                break;
            case R.id.tvReset:
                clearData();
                getOrderHistory("", "");
                break;
            case R.id.tvFromDate:
                openFromDatePicker();
                break;
            case R.id.tvToDate:
                openToDatePicker();
                break;
            default:
                // do with default
                break;
        }
    }

    private void updateUiFilter() {
        if (llDateFilter.getVisibility() == View.VISIBLE) {
            llDateFilter.setVisibility(View.GONE);
            ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable
                    (this,R.drawable.ic_icon_filter));
        } else {
            llDateFilter.setVisibility(View.VISIBLE);
            ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable
                    (this,R.drawable.ic_cancel));
        }
    }

    /**
     * this method call webservice for get order history
     *
     * @param fromDate date in format (mm/dd/yyyy)
     * @param toDate   date in format (mm/dd/yyyy)
     */
    private void getOrderHistory(String fromDate, String toDate) {
        srlOrdersHistory.setRefreshing(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.START_DATE, fromDate);
            jsonObject.put(Const.Params.END_DATE, toDate);
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderHistoryResponse> responseCall = apiInterface.getOrdersHistory(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderHistoryResponse>() {
            @Override
            public void onResponse(Call<OrderHistoryResponse> call, Response<OrderHistoryResponse
                    > response) {
                if (response != null && response.body() != null && parseContent.isSuccessful(response)) {
                    srlOrdersHistory.setRefreshing(false);
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        orderHistoryOriznalList.clear();
                        orderHistoryOriznalList.addAll(response.body().getOrderList());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), HistoryActivity.this);
                    }
                    updateUiHistoryList();
                    initRcvHistoryList();
                    llDateFilter.setVisibility(View.GONE);
                    ivToolbarRightIcon.setImageDrawable(AppCompatResources.getDrawable
                            (HistoryActivity.this,R.drawable.ic_icon_filter));
                }

            }

            @Override
            public void onFailure(Call<OrderHistoryResponse> call, Throwable t) {
                AppLog.handleThrowable(HistoryFragment.class.getName(), t);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        closedDeliveryRequestDialog();
        setToolbarRightIcon(R.drawable.ic_icon_filter, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ivToolbarRightIcon.setImageDrawable(null);
    }

    /**
     * This method is give arrayList which have unique date arrayList and is also add Date list
     * in treeSet
     */
    private void makeShortHistoryList() {
        orderHistoryShortList.clear();
        dateList.clear();
        separatorSet.clear();
        try {
            SimpleDateFormat sdf = parseContent.dateFormat;
            final Calendar cal = Calendar.getInstance();

            Collections.sort(orderHistoryOriznalList, new Comparator<OrderHistory>() {
                @Override
                public int compare(OrderHistory lhs, OrderHistory rhs) {
                    return compareTwoDate(lhs.getCompletedAt(), rhs.getCompletedAt());
                }
            });

            HashSet<Date> listToSet = new HashSet<Date>();
            for (int i = 0; i < orderHistoryOriznalList.size(); i++) {
                AppLog.Log(HistoryFragment.class.getName(), orderHistoryOriznalList.get(i)
                        .getCompletedAt() + "");

                if (listToSet.add(sdf.parse(orderHistoryOriznalList.get(i).getCompletedAt()))) {
                    dateList.add(sdf.parse(orderHistoryOriznalList.get(i).getCompletedAt()));
                }

            }

            for (int i = 0; i < dateList.size(); i++) {

                cal.setTime(dateList.get(i));
                OrderHistory item = new OrderHistory();
                item.setCompletedAt(sdf.format(dateList.get(i)));
                orderHistoryShortList.add(item);

                separatorSet.add(orderHistoryShortList.size() - 1);
                for (int j = 0; j < orderHistoryOriznalList.size(); j++) {
                    Calendar messageTime = Calendar.getInstance();
                    messageTime.setTime(sdf.parse(orderHistoryOriznalList.get(j)
                            .getCompletedAt()));
                    if (cal.getTime().compareTo(messageTime.getTime()) == 0) {
                        orderHistoryShortList.add(orderHistoryOriznalList.get(j));
                    }
                }
            }
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = parseContent.webFormat;
            SimpleDateFormat dateFormat = parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        return 0;
    }

    private void initRcvHistoryList() {
        makeShortHistoryList();
        if (ordersHistoryAdapter != null) {
            pinnedHeaderItemDecoration.disableCache();
            ordersHistoryAdapter.notifyDataSetChanged();
        } else {
            rcvOrderHistory.setLayoutManager(new LinearLayoutManager(HistoryActivity.this));
            ordersHistoryAdapter = new HistoryAdapter(separatorSet, orderHistoryShortList) {
                @Override
                public void onOrderClick(int position) {
                    goToHistoryOderDetailActivity(orderHistoryShortList.get(position).getId());

                }
            };
            rcvOrderHistory.setAdapter(ordersHistoryAdapter);
            pinnedHeaderItemDecoration = new PinnedHeaderItemDecoration();
            rcvOrderHistory.addItemDecoration(pinnedHeaderItemDecoration);
            /*rcvOrderHistory.addOnItemTouchListener(new RecyclerTouchListener(this,
                    rcvOrderHistory, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                        goToHistoryOderDetailActivity(orderHistoryShortList.get(position).getId());

                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));*/
        }
    }

    private void goToHistoryOderDetailActivity(String orderId) {
        Intent intent = new Intent(this, HistoryDetailActivity.class);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    @Override
    public void onBackPressed() {
        goToAvailableDelivery();
    }

    private void openFromDatePicker() {
        DatePickerDialog fromPiker = new DatePickerDialog(this, fromDateSet, year,
                month, day);
        fromPiker.setTitle(getResources().getString(R.string.text_select_from_date));
        if (isToDateSet) {
            fromPiker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        } else {
            fromPiker.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        }
        fromPiker.show();
    }

    private void openToDatePicker() {
        DatePickerDialog toPiker = new DatePickerDialog(this, toDateSet, year, month,
                day);
        toPiker.setTitle(getResources().getString(R.string.text_select_to_date));
        if (isFromDateSet) {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
            toPiker.getDatePicker().setMinDate(fromDateSetTime);
        } else {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        toPiker.show();
    }

    private void clearData() {
        isFromDateSet = false;
        isToDateSet = false;
        tvToDate.setText(getResources().getString(R.string.text_to));
        tvFromDate.setText(getResources().getString(R.string.text_from));
    }

    private void updateUiHistoryList() {
        if (orderHistoryOriznalList.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvOrderHistory.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvOrderHistory.setVisibility(View.VISIBLE);
        }

    }

}

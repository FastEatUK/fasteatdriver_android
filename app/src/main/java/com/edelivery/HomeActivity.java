package com.edelivery;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.HistoryFragment;
import com.edelivery.fragments.HomeFragment;
import com.edelivery.fragments.UserFragment;
import com.edelivery.models.datamodels.Provider;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OtpResponse;
import com.edelivery.models.responsemodels.ProviderDataResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.service.EdeliveryUpdateLocationAndOrderService;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.FontsOverride;
import com.edelivery.utils.SoundHelper;
import com.edelivery.utils.Utils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.LocationDataSourceHERE;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

public class HomeActivity extends BaseAppCompatActivity implements PositioningManager.OnPositionChangedListener {

    private CustomDialogVerification customDialogVerification;
    private Dialog dialogEmailOrPhoneVerification;
    private CustomFontEditTextView etDialogEditTextOne, etDialogEditTextTwo;
    private OtpResponse otpResponse;
    private String phone, email;
    private CustomDialogAlert exitDialog;
    private Menu bottomNavigationMenu;


    public static ProgressDialog dialog;
    //for here map
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/ClanPro-News.otf");
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

       try{
           initToolBar();
           findViewById();
           setViewListener();
           IsToolbarNavigationVisible(false);
           initBottomBar();
           goToHomeFragmentDifferentTransition();
           SoundHelper.getInstance(this).stopWhenNewOrderSound(this);
           getProviderDetail();
           checkPermissions();
       }catch (Exception e)
       {

       }



       /* positionIndicator = mMap.getPositionIndicator();
        positionIndicator.setVisible(true);
        positionIndicator.setAccuracyIndicatorVisible(true);*/


        // Log.i("onCreategetProviderId",preferenceHelper.getProviderId());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (bottomNavigationMenu != null && bottomNavigationMenu.findItem(R.id.action_erning).isChecked()) {
            bottomNavigationMenu.findItem(R.id.action_home).setChecked(true);
            goToHomeFragment();
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        //do something

    }

    @Override
    protected void setViewListener() {
        //do something
    }

    @Override
    protected void onBackNavigation() {
        //do something
    }


    private void initBottomBar() {

        try {

            BottomNavigationView bottomNavigationView = (BottomNavigationView)
                    findViewById(R.id.bottom_navigation);

            bottomNavigationMenu = bottomNavigationView.getMenu();
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_home:
                                    goToHomeFragment();
                                    break;
                                case R.id.action_erning:
                                    goToEarningActivity();
                                    break;
                                case R.id.action_history:
                                    goToHistoryFragment();
                                    break;
                                case R.id.action_user:
                                    goToUserFragment();
                                    break;
                                default:
                                    //do something
                                    break;
                            }
                            return true;
                        }
                    });

        } catch (Exception e) {
            Log.e("initBottomBar()" , e.getMessage());
        }

//        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

    }


    public void addFragment(Fragment fragment, boolean addToBackStack,
                            boolean isAnimate, String tag) {

        try {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            if (isAnimate) {
                ft.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_left, R.anim.slide_in_left,
                        R.anim.slide_out_right);
            }
            if (addToBackStack) {
                ft.addToBackStack(tag);
            }
            ft.replace(R.id.contain_frame, fragment, tag);
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            Log.e("addFragment() " , e.getMessage());
        }

    }

    public void replaceFragmentFromStack(Fragment fragment) {
        try {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.contain_frame, fragment);
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            Log.e("replaceFragmentFromStack() " , e.getMessage());
        }

    }

    private void goToHomeFragment() {
        try {
            if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.HOME_FRAGMENT) == null) {
                HomeFragment homeFragment = new HomeFragment();
                addFragment(homeFragment, false, true, Const.Tag.HOME_FRAGMENT);
            }
        } catch (Exception e) {
            Log.e("goToHomeFragment() " , e.getMessage());
        }


    }

    private void goToHomeFragmentDifferentTransition() {
        try {
            if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.HOME_FRAGMENT) == null) {
                HomeFragment homeFragment = new HomeFragment();
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();

                ft.setCustomAnimations(R.anim.fade_in_out,
                        R.anim.slide_out_left, R.anim.fade_in_out,
                        R.anim.slide_out_right);
                ft.replace(R.id.contain_frame, homeFragment, Const.Tag.HOME_FRAGMENT);
                ft.commitAllowingStateLoss();
            }
        } catch (Exception e) {
            Log.e("goToHomeFragmentDifferentTransition() " , e.getMessage());
        }

    }

    private void goToHistoryFragment() {
        try {
            if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.HISTORY_FRAGMENT) == null) {
                HistoryFragment orderHistoryFragment = new HistoryFragment();
                addFragment(orderHistoryFragment, false, true, Const.Tag.HISTORY_FRAGMENT);
            }
        } catch (Exception e) {
            Log.e("goToHistoryFragment() " , e.getMessage());
        }

    }

    private void goToUserFragment() {

        try {
            if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.USER_FRAGMENT) == null) {
                UserFragment userFragment = new UserFragment();
                addFragment(userFragment, false, true, Const.Tag.USER_FRAGMENT);
            }
        } catch (Exception e) {
            Log.e("goToUserFragment() " , e.getMessage());
        }

    }

    public void startUpdateLocationAndOrderService() {

        try {
            if (!isMyServiceRunning(EdeliveryUpdateLocationAndOrderService.class)) {
                Intent intent = new Intent(this, EdeliveryUpdateLocationAndOrderService.class);
                intent.setAction(Const.Action.START_FOREGROUND_ACTION);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        } catch (Exception e) {
            Log.e("startUpdateLocationAndOrderService() " , e.getMessage());
        }

    }

    public void stopUpdateLocationAndOrderService() {
        try {
            if (isMyServiceRunning(EdeliveryUpdateLocationAndOrderService.class)) {
                Intent intent = new Intent(this, EdeliveryUpdateLocationAndOrderService.class);
                intent.setAction(Const.Action.STOP_FOREGROUND_ACTION);
                stopService(intent);
            }
        } catch (Exception e) {
            Log.e("stopUpdateLocationAndOrderService() " , e.getMessage());
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {

        try {

            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (serviceClass != null) {
                for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer
                        .MAX_VALUE)) {
                    if (serviceClass.getName().equals(service.service.getClassName())) {
                        return true;
                    }
                }
            }


        } catch (Exception e) {
            Log.e("isMyServiceRunning() " , e.getMessage());
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        //do something
    }

    @Override
    public void onBackPressed() {
        try {
            openExitDialog();
        } catch (Exception e) {
            Log.e("onBackPressed() " , e.getMessage());
        }
    }


    private void checkDocumentUploadAndApproved(Provider provider) {

        try {
            if (preferenceHelper != null && preferenceHelper.getIsApproved()) {
                if (preferenceHelper.getIsAdminDocumentMandatory() && !preferenceHelper
                        .getIsProviderAllDocumentsUpload()) {
                    goToDocumentActivity(true);
                } else if (provider != null && provider.getVehicleIds() != null && provider.getVehicleIds().isEmpty() && !preferenceHelper
                        .getIsProviderAllVehicleDocumentsUpload()) {
                    goToVehicleDetailActivity(true);
                } else {
                    if (currentOrder != null && currentOrder.getAvailableOrders() == 0) {
                        openEmailOrPhoneConfirmationDialog(getResources().getString(R
                                .string.text_confirm_detail), getResources().getString(R
                                .string.msg_plz_confirm_your_detail), getResources()
                                .getString(R.string
                                        .text_log_out), getResources().getString(R.string
                                .text_ok));
                    }
                }

            } else {
                if (preferenceHelper.getIsAdminDocumentMandatory() && !preferenceHelper
                        .getIsProviderAllDocumentsUpload()) {
                    goToDocumentActivity(true);
                } else if (provider.getVehicleIds().isEmpty() && !preferenceHelper
                        .getIsProviderAllVehicleDocumentsUpload()) {
                    goToVehicleDetailActivity(true);
                }
            }
        } catch (Exception e) {
            Log.e("checkDocumentUploadAndApproved() " , e.getMessage());
        }


    }

    /**
     * this method called webservice for get OTP for mobile or email
     *
     * @param jsonObject
     */
    private void getOtpVerify(JSONObject jsonObject) {

        try {
            Utils.showCustomProgressDialog(this, false);
            if (customDialogVerification != null && customDialogVerification.isShowing()) {
                return;
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<OtpResponse> otpResponseCall = apiInterface.getOtpVerify(ApiClient
                    .makeJSONRequestBody(jsonObject));
            otpResponseCall.enqueue(new Callback<OtpResponse>() {
                @Override
                public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        AppLog.Log("SMS_OTP", response.body().getOtpForSms());
                        AppLog.Log("EMAIL_OTP", response.body().getOtpForEmail());
                        otpResponse = response.body();
                        switch (checkWitchOtpValidationON()) {
                            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                                openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                        otpResponse
                                                .getOtpForSms(), getResources().getString(R
                                                .string.text_email_otp), getResources().getString
                                                (R.string
                                                        .text_phone_otp), true);
                                break;
                            case Const.SMS_VERIFICATION_ON:
                                openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                        otpResponse
                                                .getOtpForSms(), "", getResources().getString(R
                                                .string
                                                .text_phone_otp), false);
                                break;
                            case Const.EMAIL_VERIFICATION_ON:
                                openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                        otpResponse
                                                .getOtpForSms(),
                                        "", getResources().getString(R
                                                .string
                                                .text_email_otp), false);
                                break;
                            default:
                                // do with default
                                break;
                        }

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), HomeActivity.this);
                    }


                }

                @Override
                public void onFailure(Call<OtpResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                }
            });
        } catch (Exception e) {
            Log.e("getOtpVerify() " , e.getMessage());
        }


    }

    /**
     * this method open dialog which is help to verify OTP witch is send to email or mobile
     *
     * @param otpEmailVerification set email otp number
     * @param otpSmsVerification   set mobile otp number
     * @param editTextOneHint      set hint text in edittext one
     * @param ediTextTwoHint       set hint text in edittext two
     * @param isEditTextOneVisible set true edittext one visible
     */
    private void openEmailOrPhoneOTPVerifyDialog(final String otpEmailVerification, final String
            otpSmsVerification, String editTextOneHint, String ediTextTwoHint, boolean
                                                         isEditTextOneVisible) {

        try {
            if (customDialogVerification != null && customDialogVerification.isShowing()) {
                return;
            }
            customDialogVerification = new CustomDialogVerification
                    (this,
                            getResources().getString(R.string.text_verify_detail), getResources()
                            .getString(R
                                    .string.msg_verify_detail), getResources().getString(R.string
                            .text_log_out)
                            , getResources().getString(R.string.text_ok), editTextOneHint,
                            ediTextTwoHint,
                            isEditTextOneVisible,
                            InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                    finish();
                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                   /* jsonObject.put(Const.Params.PROVIDER_ID,preferenceHelper.getProviderId());
                    jsonObject.put(Const.Params.SERVER_TOKEN,preferenceHelper.getSessionToken());*/
                        switch (checkWitchOtpValidationON()) {
                            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                                if (TextUtils.equals(etDialogEditTextOne.getText().toString(),
                                        otpEmailVerification)) {
                                    if (TextUtils.equals(etDialogEditTextTwo
                                                    .getText().toString(),
                                            otpSmsVerification)) {
                                        jsonObject.put(Const.Params.IS_PHONE_NUMBER_VERIFIED,
                                                true);
                                        jsonObject.put(Const.Params.IS_EMAIL_VERIFIED,
                                                true);
                                        jsonObject.put(Const.Params.EMAIL, email);
                                        jsonObject.put(Const.Params.PHONE, phone);
                                        customDialogVerification.dismiss();
                                        setOTPVerification(jsonObject);
                                    } else {
                                        etDialogEditTextTwo.setError(getResources().getString(R.string
                                                .msg_sms_otp_wrong));
                                    }

                                } else {
                                    etDialogEditTextOne.setError(getResources().getString(R.string
                                            .msg_email_otp_wrong));
                                }
                                break;
                            case Const.SMS_VERIFICATION_ON:
                                if (TextUtils.equals(etDialogEditTextTwo
                                                .getText().toString(),
                                        otpSmsVerification)) {
                                    jsonObject.put(Const.Params.IS_PHONE_NUMBER_VERIFIED,
                                            true);
                                    jsonObject.put(Const.Params.PHONE, phone);
                                    customDialogVerification.dismiss();
                                    setOTPVerification(jsonObject);
                                } else {
                                    etDialogEditTextTwo.setError(getResources().getString(R.string
                                            .msg_sms_otp_wrong));
                                }
                                break;
                            case Const.EMAIL_VERIFICATION_ON:
                                if (TextUtils.equals(etDialogEditTextTwo.getText().toString(),
                                        otpEmailVerification)) {
                                    jsonObject.put(Const.Params.IS_EMAIL_VERIFIED,
                                            true);
                                    jsonObject.put(Const.Params.EMAIL, email);
                                    customDialogVerification.dismiss();
                                    setOTPVerification(jsonObject);
                                } else {
                                    etDialogEditTextTwo.setError(getResources().getString(R.string
                                            .msg_email_otp_wrong));
                                }
                                break;
                            default:
                                // do with default
                                break;
                        }
                    } catch (JSONException e) {
                        AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
                    }


                }
            };
            customDialogVerification.show();

        } catch (Exception e) {
            Log.e("openEmailOrPhoneOTPVerifyDialog() " , e.getMessage());
        }

    }

    /**
     * this method open dialog which confirm user email or mobile detail
     *
     * @param titleDialog      set dialog title
     * @param messageDialog    set dialog message
     * @param titleLeftButton  set dialog left button text
     * @param titleRightButton set dialog right button text
     */

    private void openEmailOrPhoneConfirmationDialog(String titleDialog, String messageDialog,
                                                    String titleLeftButton, String
                                                            titleRightButton) {

        try {
            CustomFontTextView tvDialogEdiTextMessage, tvDialogEditTextTitle, btnDialogEditTextLeft,
                    btnDialogEditTextRight;
            TextInputLayout dialogItlOne;
            LinearLayout llConfirmationPhone;
            CustomFontEditTextView etRegisterCountryCode;
            if (customDialogVerification != null && customDialogVerification.isShowing()
                    || dialogEmailOrPhoneVerification != null && dialogEmailOrPhoneVerification
                    .isShowing()) {
                return;
            }
            dialogEmailOrPhoneVerification = new Dialog(this);
            dialogEmailOrPhoneVerification.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogEmailOrPhoneVerification.setContentView(R.layout.dialog_confrimation_email_or_phone);

            tvDialogEdiTextMessage = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                    (R.id
                            .tvDialogAlertMessage);
            tvDialogEditTextTitle = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                    (R.id.tvDialogAlertTitle);
            btnDialogEditTextLeft = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                    (R.id.btnDialogAlertLeft);
            btnDialogEditTextRight = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                    (R.id.btnDialogAlertRight);
            etDialogEditTextOne = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                    .findViewById(R.id.etDialogEditTextOne);
            etDialogEditTextTwo = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                    .findViewById(R.id.etDialogEditTextTwo);
            etRegisterCountryCode = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                    .findViewById(R.id.etRegisterCountryCode);
            etDialogEditTextOne.setText(preferenceHelper.getEmail());
            etDialogEditTextTwo.setText(preferenceHelper.getPhoneNumber());
            etRegisterCountryCode.setText(preferenceHelper.getPhoneCountyCodeCode());

            llConfirmationPhone = (LinearLayout) dialogEmailOrPhoneVerification.findViewById(R.id
                    .llConfirmationPhone);
            dialogItlOne = (TextInputLayout) dialogEmailOrPhoneVerification.findViewById(R.id
                    .dialogItlOne);

            btnDialogEditTextLeft.setOnClickListener(this);
            btnDialogEditTextRight.setOnClickListener(this);

            tvDialogEditTextTitle.setText(titleDialog);
            tvDialogEdiTextMessage.setText(messageDialog);
            btnDialogEditTextLeft.setText(titleLeftButton);
            btnDialogEditTextRight.setText(titleRightButton);


            btnDialogEditTextRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Const.Params.ID, preferenceHelper.getProviderId());
                        jsonObject.put(Const.Params.TYPE, String.valueOf(Const.TYPE_PROVIDER));
                        switch (checkWitchOtpValidationON()) {
                            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                                if (Patterns.EMAIL_ADDRESS.matcher(etDialogEditTextOne.getText
                                        ().toString()).matches()) {
                                    if (etDialogEditTextTwo.getText().toString().trim().length()
                                            > preferenceHelper.getMaxPhoneNumberLength() ||
                                            etDialogEditTextTwo.getText()
                                                    .toString().trim()
                                                    .length
                                                            () < preferenceHelper
                                                    .getMinPhoneNumberLength()) {

                                        etDialogEditTextTwo.setError(getResources().getString(R
                                                .string.msg_please_enter_valid_mobile_number) + " " +
                                                "" + preferenceHelper.getMinPhoneNumberLength() +
                                                getResources().getString(R
                                                        .string
                                                        .text_or)
                                                + preferenceHelper.getMaxPhoneNumberLength() + " " +
                                                getResources().getString
                                                        (R.string
                                                                .text_digits));
                                    } else {
                                        jsonObject.put(Const.Params.EMAIL, etDialogEditTextOne
                                                .getText().toString());
                                        jsonObject.put(Const.Params.PHONE, etDialogEditTextTwo.getText
                                                ().toString());
                                        jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper
                                                .getPhoneCountyCodeCode());
                                        dialogEmailOrPhoneVerification.dismiss();
                                        email = etDialogEditTextOne.getText().toString();
                                        phone = etDialogEditTextTwo.getText().toString();
                                        getOtpVerify(jsonObject);
                                    }

                                } else {
                                    etDialogEditTextOne.setError(getResources().getString(R.string
                                            .msg_please_enter_valid_email));
                                }
                                break;
                            case Const.SMS_VERIFICATION_ON:
                                if (etDialogEditTextTwo.getText().toString().trim().length()
                                        > preferenceHelper.getMaxPhoneNumberLength() ||
                                        etDialogEditTextTwo.getText()
                                                .toString().trim()
                                                .length
                                                        () < preferenceHelper
                                                .getMinPhoneNumberLength()) {

                                    etDialogEditTextTwo.setError(getResources().getString(R
                                            .string.msg_please_enter_valid_mobile_number) + " " +
                                            "" + preferenceHelper.getMinPhoneNumberLength() +
                                            getResources().getString(R
                                                    .string
                                                    .text_or)
                                            + preferenceHelper.getMaxPhoneNumberLength() + " " +
                                            getResources().getString
                                                    (R.string
                                                            .text_digits));
                                } else {
                                    jsonObject.put(Const.Params.PHONE, etDialogEditTextTwo.getText
                                            ().toString());
                                    jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper
                                            .getPhoneCountyCodeCode());
                                    dialogEmailOrPhoneVerification.dismiss();
                                    phone = etDialogEditTextTwo.getText().toString();
                                    getOtpVerify(jsonObject);
                                }
                                break;
                            case Const.EMAIL_VERIFICATION_ON:
                                if (Patterns.EMAIL_ADDRESS.matcher(etDialogEditTextOne.getText
                                        ().toString()).matches()) {
                                    jsonObject.put(Const.Params.EMAIL, etDialogEditTextOne
                                            .getText().toString());
                                    dialogEmailOrPhoneVerification.dismiss();
                                    email = etDialogEditTextOne.getText().toString();
                                    getOtpVerify(jsonObject);
                                } else {
                                    etDialogEditTextOne.setError(getResources().getString(R.string
                                            .msg_please_enter_valid_email));
                                }
                                break;
                            default:
                                // do with default
                                break;
                        }


                    } catch (JSONException e) {
                        AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
                    }
                }
            });
            btnDialogEditTextLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logOut();
                    dialogEmailOrPhoneVerification.dismiss();
                }
            });
            WindowManager.LayoutParams params = dialogEmailOrPhoneVerification.getWindow()
                    .getAttributes();
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            dialogEmailOrPhoneVerification.setCancelable(false);

            switch (checkWitchOtpValidationON()) {
                case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                    etDialogEditTextOne.setVisibility(View.VISIBLE);
                    dialogItlOne.setVisibility(View.VISIBLE);
                    llConfirmationPhone.setVisibility(View.VISIBLE);
                    dialogEmailOrPhoneVerification.show();
                    break;
                case Const.EMAIL_VERIFICATION_ON:
                    etDialogEditTextOne.setVisibility(View.VISIBLE);
                    dialogItlOne.setVisibility(View.VISIBLE);
                    llConfirmationPhone.setVisibility(View.GONE);
                    dialogEmailOrPhoneVerification.show();
                    break;
                case Const.SMS_VERIFICATION_ON:
                    etDialogEditTextOne.setVisibility(View.GONE);
                    dialogItlOne.setVisibility(View.GONE);
                    llConfirmationPhone.setVisibility(View.VISIBLE);
                    dialogEmailOrPhoneVerification.show();
                    break;
                default:
                    etDialogEditTextOne.setVisibility(View.GONE);
                    dialogItlOne.setVisibility(View.GONE);
                    llConfirmationPhone.setVisibility(View.GONE);
                    break;
            }
        } catch (Exception e) {
            Log.e("openEmailOrPhoneConfirmationDialog() " , e.getMessage());
        }


    }

    protected void openExitDialog() {

        try {
            if (exitDialog != null && exitDialog.isShowing()) {
                return;
            }
            exitDialog = new CustomDialogAlert(this, this
                    .getResources()
                    .getString(R.string.text_exit), this.getResources()
                    .getString(R.string.msg_are_you_sure), this.getResources()
                    .getString(R.string.text_cancel), this.getResources()
                    .getString(R.string.text_ok)) {
                @Override
                public void onClickLeftButton() {
                    dismiss();

                }

                @Override
                public void onClickRightButton() {
                    dismiss();
                    finish();
                }
            };
            exitDialog.show();
        } catch (Exception e) {
            Log.e("openExitDialog() " , e.getMessage());
        }

    }

    /**
     * this method called a webservice for get provider detail
     */
    public void getProviderDetail() {
        try {
            if (preferenceHelper != null && preferenceHelper.getProviderId() != null && preferenceHelper.getSessionToken() != null) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.PROVIDER_ID,
                            preferenceHelper.getProviderId());
                    jsonObject.put(Const.Params.SERVER_TOKEN,
                            preferenceHelper.getSessionToken());
                    jsonObject.put(Const.Params.APP_VERSION, getAppVersion());

                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
                 }
                Log.i("getProviderDetail", jsonObject.toString());
                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<ProviderDataResponse> responseCall = apiInterface.getProviderDetail(ApiClient
                        .makeJSONRequestBody(jsonObject));
                responseCall.enqueue(new Callback<ProviderDataResponse>() {
                    @Override
                    public void onResponse(Call<ProviderDataResponse> call, Response<ProviderDataResponse
                            > response) {
                        if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.parseUserStorageData(response) && response.body().getProvider() != null) {
                            checkDocumentUploadAndApproved(response.body().getProvider());
                            HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager()
                                    .findFragmentByTag(Const
                                            .Tag.HOME_FRAGMENT);
                            if (homeFragment != null) {
                                homeFragment.updateUiWhenApprovedByAdmin();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                        AppLog.handleThrowable(HomeActivity.class.getName(), t);
                    }
                });
            }
        } catch (Exception e) {
            Log.e("getProviderDetail() " , e.getMessage());
        }


    }

    /**
     * this method called a webservice for set otp verification result in web
     */
    private void setOTPVerification(JSONObject jsonObject) {

        try {
            Utils.showCustomProgressDialog(this, false);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> responseCall = apiInterface.setOtpVerification(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        preferenceHelper.putIsEmailVerified(response.body().isSuccess());
                        preferenceHelper.putIsPhoneNumberVerified(response.body().isSuccess());
                        preferenceHelper.putEmail(email);
                        preferenceHelper.putPhoneNumber(phone);
                        Utils.showMessageToast(response.body().getMessage(), HomeActivity.this);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), HomeActivity.this);
                    }


                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(HomeActivity.class.getName(), t);
                }
            });
        } catch (Exception e) {
            Log.e("setOTPVerification() " , e.getMessage());
        }

    }

    private void goToEarningActivity() {
        try {

            Intent intent = new Intent(this, EarningActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } catch (Exception e) {
            Log.e("goToEarningActivity() " , e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager()
                        .findFragmentByTag(Const.Tag
                                .HOME_FRAGMENT);
                switch (resultCode) {
                    case Activity.RESULT_OK:
                       /* dialog=new ProgressDialog(HomeActivity.this);
                        dialog.setMessage("Gathering your current location.Please wait....");
                        dialog.show();*/
                        //Utils.showCustomProgressDialog(HomeActivity.this,true);
                        // All required changes were successfully made
                       /* if (homeFragment != null) {

                            homeFragment.moveCameraFirstMyLocation();
                        }*/
                        Log.e("click_get", "2");
                        break;
                    case Activity.RESULT_CANCELED:

                        Log.e("click_get", "1");
                        // The user was asked to change settings, but chose not to
                       /* if (homeFragment != null) {
                            homeFragment.locationHelper.setOpenGpsDialog(false);
                        }*/

                        //finishAffinity();
                        break;
                    default:
                        Log.e("click_get", "3");
                        break;
                }
                break;
        }
    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(HomeActivity.this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(HomeActivity.this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            switch (requestCode) {
                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(HomeActivity.this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            // finish();
                            finish();
                            return;
                        } else {

                            Log.e("permisision_granted", "permission_granted");
                            Log.e("click_get", "4");
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    //  setheremap();
                    break;

            }
        }
    }

    @Override
    public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {

    }

    @Override
    public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {

    }

    // Define positioning listener


}

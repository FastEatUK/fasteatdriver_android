package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.View;

import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.WalletRequestDetail;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;

import java.text.ParseException;
import java.util.Date;

public class WalletTransactionDetailActivity extends BaseAppCompatActivity {


    private CustomFontTextView tvTransactionDate, tvWithdrawalID, tvTransactionTime;
    private CustomFontTextViewTitle tvWalletRequestAmount, tvApproveByAdmin,
            tvTotalWalletAmount, tvMode;
    private WalletRequestDetail walletRequestDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_trans_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        findViewById();
        setViewListener();
        getExtraData();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        tvTotalWalletAmount = (CustomFontTextViewTitle) findViewById(R.id.tvTotalWalletAmount);
        tvWalletRequestAmount = (CustomFontTextViewTitle) findViewById(R.id.tvWalletRequestAmount);
        tvApproveByAdmin = (CustomFontTextViewTitle) findViewById(R.id.tvApproveByAdmin);
        tvWithdrawalID = (CustomFontTextView) findViewById(R.id.tvWithdrawalID);
        tvTransactionDate = (CustomFontTextView) findViewById(R.id.tvTransactionDate);
        tvTransactionTime = (CustomFontTextView) findViewById(R.id.tvTransactionTime);
        tvMode = (CustomFontTextViewTitle) findViewById(R.id.tvMode);

    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            walletRequestDetail = getIntent().getExtras().getParcelable(Const.BUNDLE);
            setTitleOnToolBar(walletSate(walletRequestDetail.getWalletStatus()));
            tvApproveByAdmin.setText(parseContent.decimalTwoDigitFormat.format
                    (walletRequestDetail
                            .getApprovedRequestedWalletAmount()) + " " + walletRequestDetail
                    .getAdminCurrencyCode());
            tvTotalWalletAmount.setText(parseContent.decimalTwoDigitFormat.format
                    (Const.Wallet.WALLET_STATUS_COMPLETED == walletRequestDetail.getWalletStatus
                            () ? walletRequestDetail
                            .getAfterTotalWalletAmount() : walletRequestDetail
                            .getTotalWalletAmount()) + " " +
                    walletRequestDetail
                            .getAdminCurrencyCode
                                    ());

            String amount = parseContent
                    .decimalTwoDigitFormat
                    .format(walletRequestDetail.getWalletStatus() == Const
                            .Wallet
                            .WALLET_STATUS_COMPLETED || walletRequestDetail.getWalletStatus() ==
                            Const.Wallet.WALLET_STATUS_TRANSFERRED ? walletRequestDetail
                            .getApprovedRequestedWalletAmount() : walletRequestDetail
                            .getRequestedWalletAmount()) + " " +
                    walletRequestDetail
                            .getAdminCurrencyCode();
            tvWalletRequestAmount.setText(amount);
            tvWithdrawalID.setText(getResources().getString(R.string
                    .text_id) + " " + walletRequestDetail.getUniqueId());
            try {
                Date date = ParseContent.getInstance().webFormat.parse(walletRequestDetail
                        .getCreatedAt
                                ());
                tvTransactionDate.setText(ParseContent.getInstance().dateFormat3
                        .format(date));
                tvTransactionTime.setText(ParseContent.getInstance().timeFormat_am
                        .format(date));
            } catch (ParseException e) {
                AppLog.handleException(WalletDetailActivity.class.getName(), e);
            }
            if (walletRequestDetail.isPaymentModeCash()) {
                tvMode.setText(getResources().getString(R.string
                        .text_cash_payment));
                tvMode.setCompoundDrawablesRelativeWithIntrinsicBounds(AppCompatResources
                        .getDrawable
                                (this, R.drawable.ic_cash_2), null, null, null);
            } else {
                tvMode.setText(getResources().getString(R.string
                        .text_bank_payment));
                tvMode.setCompoundDrawablesRelativeWithIntrinsicBounds(AppCompatResources
                        .getDrawable
                                (this, R.drawable.ic_bank_building_24dp), null, null, null);
            }
        }

    }

    private String walletSate(int id) {
        String comment;
        switch (id) {
            case Const.Wallet.WALLET_STATUS_CREATED:
                comment = getResources().getString(R.string
                        .text_wallet_status_created);
                break;
            case Const.Wallet.WALLET_STATUS_ACCEPTED:
                comment = getResources().getString(R.string
                        .text_wallet_status_accepted);
                break;
            case Const.Wallet.WALLET_STATUS_TRANSFERRED:
                comment = getResources().getString(R.string
                        .text_wallet_status_transferred);
                break;
            case Const.Wallet.WALLET_STATUS_COMPLETED:
                comment = getResources().getString(R.string
                        .text_wallet_status_completed);
                break;
            case Const.Wallet.WALLET_STATUS_CANCELLED:
                comment = getResources().getString(R.string
                        .text_wallet_status_cancelled);
                break;

            default:
                // do with default
                comment = "NA";
                break;
        }
        return comment;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

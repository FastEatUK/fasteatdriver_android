package com.edelivery.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.edelivery.HomeActivity;
import com.edelivery.R;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EdeliveryUpdateLocationAndOrderService extends Service implements LocationHelper
        .OnLocationReceived {

    public static final String TAG = "LocationAndOrderService";
    private static final String CHANNEL_ID = "channel_01";
    private ScheduledExecutorService updateLocationAndOrderSchedule;
    private boolean isScheduledStart;
    private Handler handler;
    private Location currentLocation, lastLocation;
    private PreferenceHelper preferenceHelper;
    private LocationHelper locationHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        preferenceHelper = PreferenceHelper.getInstance(this);
        initLocationHelper();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent !=null) {
            if (!TextUtils.isEmpty(intent.getAction())) {
                if (intent.getAction().equals(Const.Action.START_FOREGROUND_ACTION)) {
                    AppLog.Log(TAG, "Received Start Foreground Intent");
                    startForeground(Const.FOREGROUND_NOTIFICATION_ID,
                            getNotification(getResources().getString(R.string.app_name)));
                } else if (intent.getAction().equals(
                        Const.Action.STOP_FOREGROUND_ACTION)) {
                    AppLog.Log(TAG, "Received Stop Foreground Intent");
                    stopForeground(true);
                    stopSelf();
                }
            }
        }
        initUpdateLocationAndOrderHandler();
        checkPermission();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        locationHelper.onStop();
        stopUpdateLocationAndOrderSchedule();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    public void startUpdateLocationAndOrderSchedule() {

        if (!isScheduledStart) {

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            };
            updateLocationAndOrderSchedule = Executors.newSingleThreadScheduledExecutor();
            updateLocationAndOrderSchedule.scheduleWithFixedDelay(runnable, 0,
                    Const.LOCATION_SCHEDULED_SECONDS, TimeUnit
                            .SECONDS);
            AppLog.Log(TAG, "Schedule Start");
            isScheduledStart = true;
        }
    }

    public void stopUpdateLocationAndOrderSchedule() {
        if (isScheduledStart) {
            AppLog.Log(TAG, "Schedule Stop");
            updateLocationAndOrderSchedule.shutdown(); // Disable new tasks from being submitted
            // Wait a while for existing tasks to terminate
            try {
                if (!updateLocationAndOrderSchedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    updateLocationAndOrderSchedule.shutdownNow(); // Cancel currently executing
                    // tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!updateLocationAndOrderSchedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(TAG, "Pool did not terminate");

                }
            } catch (InterruptedException e) {
                AppLog.handleException(TAG, e);
                // (Re-)Cancel if current thread also interrupted
                updateLocationAndOrderSchedule.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
            isScheduledStart = false;
        }

    }

    private void initUpdateLocationAndOrderHandler() {
        /**
         * This handler receive a message from  requestStatusScheduledService and update provider
         * location and order status
         */
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!TextUtils.isEmpty(preferenceHelper.getSessionToken()) &&
                        preferenceHelper.getIsProviderOnline()) {
                    if (currentLocation != null && Utils.isInternetConnected
                            (EdeliveryUpdateLocationAndOrderService.this))
                    {
                        updateProviderLocation();
                    }
                } else {
                    stopForeground(true);
                    stopSelf();
                }


            }

        };

    }

    private void initLocationHelper() {
        locationHelper = new LocationHelper(getApplicationContext());
        locationHelper.setLocationReceivedLister(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;

    }

    @Override
    public void onConnected(Bundle bundle) {
        AppLog.Log(TAG, "GoogleClientConnected");
        currentLocation = locationHelper.getLastLocation();
        locationHelper.startLocationUpdate();
        startUpdateLocationAndOrderSchedule();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(TAG, "GoogleClientConnectionFailed");
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLog.Log(TAG, "GoogleClientConnectionSuspended");
    }

    private void updateProviderLocation() {
        if (lastLocation == null) {
            lastLocation = currentLocation;
        }
        AppLog.Log("UPDATE_LOCATION", currentLocation
                .getLatitude() + " " + currentLocation
                .getLongitude() + " " + currentLocation.getBearing());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper
                    .getProviderId());
            jsonObject.put(Const.Params.LONGITUDE, currentLocation
                    .getLongitude());
            jsonObject.put(Const.Params.LATITUDE, currentLocation
                    .getLatitude());
            jsonObject.put(Const.Params.BEARING, getBearing(lastLocation,
                    currentLocation));
        } catch (JSONException e) {
            AppLog.handleThrowable(TAG, e);
        }


        Log.d("response","====== updated location ========"+jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.updateProviderLocation(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                lastLocation = currentLocation;
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(TAG, t);
            }
        });
    }

    public float getBearing(Location begin, Location end) {
        float bearing = begin.bearingTo(end);
        AppLog.Log("BEARING", String.valueOf(bearing));
        return bearing;
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

        } else {
            locationHelper.onStart();
        }
    }


  /*  private void initAlarmManager() {
        if (!TextUtils.isEmpty(preferenceHelper.getSessionToken()) &&
                preferenceHelper.getIsProviderOnline()) {
            int repeatingTimeInMillis = 120000;/// 2 minute
            Intent intent = new Intent(this, AlarmReceiver.class);
            intent.setAction(Const.Action.ACTION_START_UPDATE_LOCATION_SERVICE);
            PendingIntent pintent = PendingIntent.getBroadcast(this, 213, intent, 0);
            AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, repeatingTimeInMillis,
                        pintent);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarm.setExact(AlarmManager.RTC_WAKEUP, repeatingTimeInMillis, pintent);
            } else {
                alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime
                        () + 60000, repeatingTimeInMillis, pintent);
            }
        }


    }*/

    /**
     * this method get Notification object which help to notify user as foreground service
     *
     * @param notificationDetails
     * @return
     */
    private Notification getNotification(String notificationDetails) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_DEFAULT);

            mNotificationManager.createNotificationChannel(mChannel);
        }

        Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent exitIntent = new Intent(this, HomeActivity.class);
        exitIntent.setAction(Const.Action.STOP_FOREGROUND_ACTION);

        PendingIntent pexitIntent = PendingIntent.getService(this, 0,
                exitIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setSmallIcon(getNotificationIcon())
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        getNotificationIcon()))
                .setContentTitle(notificationDetails)
                .setContentText(getResources().getString(R.string.msg_service))
                .setContentIntent(notificationPendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        builder.setAutoCancel(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.addAction(R.drawable.ic_cross,
                    getResources().getString(R.string.text_exit).toUpperCase(), pexitIntent);
        }
        return builder.build();
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build
                .VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_stat_provider : R.mipmap.ic_launcher;
    }

}

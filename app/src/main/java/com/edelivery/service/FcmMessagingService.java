package com.edelivery.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import android.text.TextUtils;

import com.edelivery.LoginActivity;
import com.edelivery.R;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 08-Feb-2017.
 * <p/>
 * This Class is handle a Notification which send by Google FCM server.
 */
public class FcmMessagingService extends FirebaseMessagingService {


    public static final String MESSAGE = "message";
    public static final String LOGIN_IN_OTHER_DEVICE = "2092";
    public static final String NEW_ORDER = "2031";
    public static final String ADMIN_APPROVED = "2032";
    public static final String ADMIN_DECLINE = "2033";
    public static final String STORE_REJECTED = "2034";
    public static final String STORE_CANCELED = "2035";
    public static final String STORE_CANCELED_REQUEST = "2036";
    private static final String CHANNEL_ID = "channel_01";
    private static final String CHANNEL_ID_ORDER = "channel_02";
    private Map<String, String> data;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if (remoteMessage != null) {

            AppLog.Log("FcmMessagingService", "From:" + remoteMessage.getFrom());
            AppLog.Log("FcmMessagingService", "Data:" + remoteMessage.getData());

            data = remoteMessage.getData();
            String message = data.get(MESSAGE);
            if (!TextUtils.isEmpty(message)) {
                oderStatus(message);
            }
        }

    }

    private void sendNotification(String message, int activityId) {
        int notificationId = 2017;
        Intent intent = null;
        Uri uri = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.order);

        switch (activityId) {
            case Const.LOGIN_ACTIVITY:
                intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case Const.HOME_ACTIVITY:
                intent = getPackageManager().getLaunchIntentForPackage
                        (getPackageName());
                intent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                break;
            default:
                // do with default
                break;
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel mChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            if (message.equals(this.getResources().getString(R.string.push_message_2031))) {
                mChannel = new NotificationChannel(CHANNEL_ID_ORDER, name, NotificationManager.IMPORTANCE_DEFAULT);
                AudioAttributes attributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                mChannel.setSound(uri, attributes);
                mChannel.enableLights(true);


            } else {
                mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            }
            notificationManager.createNotificationChannel(mChannel);

        }


        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(LoginActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder
                (this)
                .setPriority(Notification.PRIORITY_MAX)
                // .setDefaults(Notification.DEFAULT_SOUND)
                .setStyle
                        (new NotificationCompat.BigTextStyle().bigText(message).setBigContentTitle
                                (this.getResources().getString(R.string.app_name)))
                .setContentTitle(this.getResources()
                        .getString(R.string.app_name)).setContentText(message).setAutoCancel
                        (true).setSmallIcon(getNotificationIcon()).setContentIntent
                        (notificationPendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (message.equals(this.getResources().getString(R.string.push_message_2031))) {
                notificationBuilder.setChannelId(CHANNEL_ID_ORDER); // Channel ID for new order
            } else {
                notificationBuilder.setChannelId(CHANNEL_ID); // Channel ID
            }
        }
        if (PreferenceHelper.getInstance(this).getIsPushNotificationSoundOn()) {
            if (message.equals(this.getResources().getString(R.string.push_message_2031))) {
                notificationBuilder.setSound(uri);
            } else {
                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND
                        | Notification.DEFAULT_LIGHTS);
            }
        }
        notificationManager.notify(notificationId, notificationBuilder
                .build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build
                .VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap
                .ic_stat_provider : R.mipmap.ic_launcher;
    }

    private void oderStatus(String status) {
        switch (status) {
            case LOGIN_IN_OTHER_DEVICE:
                sendNotification(getMessage(status), Const.LOGIN_ACTIVITY);
                break;
            case NEW_ORDER:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcastWithData(Const.Action.ACTION_NEW_ORDER);
                break;
            case ADMIN_APPROVED:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_ADMIN_APPROVED);
                break;
            case ADMIN_DECLINE:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_ADMIN_DECLINE);
                break;
            case STORE_CANCELED_REQUEST:
            case STORE_CANCELED:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_STORE_CANCELED_REQUEST);
                break;

            default:
                sendNotification(status, Const.HOME_ACTIVITY);
                break;
        }

    }

    private String getMessage(String code) {
        String msg = "";
        String messageCode = Const.PUSH_MESSAGE_PREFIX + code;
        try {
            msg = this.getResources().getString(
                    this.getResources().getIdentifier(messageCode, Const.STRING,
                            this.getPackageName()));
        } catch (Resources.NotFoundException e) {
            msg = messageCode;
            AppLog.Log(FcmMessagingService.class.getName(), msg);
        }
        return msg;
    }

    private void sendBroadcast(String action) {
        sendBroadcast(new Intent(action));
    }

    private void sendBroadcastWithData(String action) {
        Intent intent = new Intent(action);
        Bundle bundle = new Bundle();
        bundle.putString(Const.Params.PUSH_DATA1, data.get(Const.Params.PUSH_DATA1));
        bundle.putString(Const.Params.PUSH_DATA2, data.get(Const.Params.PUSH_DATA2));
        intent.putExtra(Const.Params.NEW_ORDER, bundle);
        sendBroadcast(intent);
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        AppLog.Log(FcmMessagingService.class.getSimpleName(), "FCM Token Refresh = " + token);
        PreferenceHelper.getInstance(this).putDeviceToken(token);
        if (!TextUtils.equals(token, PreferenceHelper.getInstance(this).getDeviceToken()) &&
                !TextUtils
                        .isEmpty(PreferenceHelper.getInstance(this).getSessionToken())) {
            upDeviceToken(token);
        }
    }

    private void upDeviceToken(String deviceToken) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, PreferenceHelper.getInstance(this)
                    .getSessionToken());
            jsonObject.put(Const.Params.DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.Params.PROVIDER_ID, PreferenceHelper.getInstance(this)
                    .getProviderId());
        } catch (JSONException e) {
            AppLog.handleThrowable(FcmMessagingService.class.getSimpleName(), e);
        }


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.updateDeviceToken(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                if (ParseContent.getInstance().isSuccessful(response)) {
                    if (!response.body().isSuccess()) {
                        AppLog.Log(FcmMessagingService.class.getSimpleName(), String.valueOf
                                (response
                                        .body().getErrorCode()));
                    }

                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(FcmMessagingService.class.getSimpleName(), t);
            }
        });

    }
}
